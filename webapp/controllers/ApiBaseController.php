<?php
namespace webapp\controllers;

use Yii;
use yii\rest\Controller;
use yii\rest\ActiveController;
use webapp\controllers\BaseController;

use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\Request;
use common\helpers\Helper;
class ApiBaseController extends ActiveController
{
    
    //接口开始时间
    public  $startTime;
    
    public function beforeAction($action)
    {
        $request=Yii::$app->request;
        header('Access-Control-Allow-origin:*');
        header('Access-Control-Allow-Methods:POST');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With, postman-token, authorization');
        header("Content-type:application/json");
        if($request->isOptions){
            echo json_encode([
                'success' => true,
                'code'    => 200
            ]);
            exit;
        }

        return parent::beforeAction($action);
    }
    
    /**
     * 记录接口耗时
     * @see \yii\rest\Controller::afterAction()
     * @date 2017-05-18
     */
    public function afterAction($action, $result)
    {
        $file = Yii::getAlias('@runtime').'/apilogs/'.date('Ymd').'.log';
        if(!file_exists(dirname($file))){
            @mkdir(dirname($file),0777,true);
        }
        $params = [
            'method'         => Yii::$app->request->method,
            'time'           => date('Y-m-d H:i:s'),
            'time_consuming' => sprintf('%.4f',(Helper::microtimeFloat()-$this->startTime)),
            'link'           => Yii::$app->request->absoluteUrl,
            'params'         => Yii::$app->request->post()
        ];
    
        @file_put_contents($file, json_encode($params)."\n",FILE_APPEND);
        return parent::afterAction($action, $result);
    }
    
    public function actions()
    {
         $actions = parent::actions();
         // 禁用所有内置 操作
         unset($actions['index'], $actions['view'],$actions['create'], $actions['update'],$actions['delete']);

         return $actions;
     }

}
