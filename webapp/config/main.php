<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'order_api',
    'timeZone' => 'Asia/Shanghai',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'webapp\controllers',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'log',
    ],
    'defaultRoute'=>'main',
    'modules' => [        
        'v1' => [
            'basePath' => '@webapp/modules/v1',
            'class'    => 'webapp\modules\v1\Module',
        ],
        'v2' => [
            'basePath' => '@webapp/modules/v2',
            'class'    => 'webapp\modules\v2\Module',
        ],
        'v3' => [
            'basePath' => '@webapp/modules/v3',
            'class'    => 'webapp\modules\v3\Module',
        ],

    ],
    'components' => [
        
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '127.0.0.1',
            'port' => 6379,
            'database' => 1,
            //'password' => 'zhAo609Wei'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],       
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning','trace', 'info'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules'=>[
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'h3k4j5h3b5v6f2d3c4c5vh6j7k7k7',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'text/json' => 'yii\web\JsonParser',
            ],
        ],
        // 自定义响应格式
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                $data = $response->data;
                $success = $response->isSuccessful;
                if($success && isset($response->data['success'])){
                    $success = $response->data['success'];
                    unset($response->data['success']);
                }

                $response->data = [
                    'success' => $success,
                    'data' => $data,
                ];
                if(!$success){
                    if(isset($data['message'])){
                        $response->data['message']=$data['message'];
                    }else{
                        $response->data['message']="服务器错误，但没有指定错误信息";
                    }
                    if(isset($data['code'])){
                        $response->data['code']=$data['code'];
                    }
                    if(isset($response->data['data'])){
                        unset($response->data['data']);
                    }
                }
                $response->statusCode = 200;

                //跨域
                $response->headers->set('Access-Control-Allow-origin','*');
                $response->headers->set('Access-Control-Allow-Methods','POST,PUT,DELETE,HEAD,PATCH,OPTIONS,GET');
                $response->headers->set('Access-Control-Allow-Headers','Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With, postman-token, authorization');
            },
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
    ],
    'params' => $params,
];
