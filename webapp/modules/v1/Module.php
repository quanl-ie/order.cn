<?php
//入口
namespace webapp\modules\v1;
use Yii;
use yii\web\Response;
use yii\web\Request;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'webapp\modules\v1\controllers';
    public $requestedRoute = '';

    public function init()
    {
        parent::init();  
    }
}
