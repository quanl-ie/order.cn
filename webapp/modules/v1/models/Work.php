<?php
namespace webapp\modules\v1\models;

use common\helpers\Helper;
use common\helpers\QueuePush;
use common\models\Common;
use common\models\Department;
use common\models\PushMessage;
use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Work extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work';
    }

    /**
     * 状态中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getStatusDesc($index)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '已完成',
            5 => '待收款',
            6 => '组长待指派',
            7 => '已改派'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 下次服务中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getNextServiceDesc($index)
    {
        $data = [
            1 => '由公司指派',
            2 => '本次服务技师',
            3 => '由组长指派'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 下次服务时间中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getNextServiceTimeDesc($index)
    {
        $data = [
            1 => '由公司指定',
            2 => '负责人指定',
            3 => '由组长指定'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 自动创建
     * @param $orderNo
     * @param $processUser
     * @param $workStage
     * @param $planTime
     * @return bool
     */
    public static function autoCreate($departmentId,$orderNo,$workStage,$planTime,$planTimeType)
    {
        $model = new self();
        $model->department_id   = $departmentId;
        $model->order_no        = $orderNo;
        $model->work_no         = Work::getWorkNo($orderNo);
        $model->process_user    = $departmentId;
        $model->process_time    = time();
        $model->work_stage      = $workStage;
        $model->plan_time       = $planTime;
        $model->plan_time_type  = $planTimeType;
        $model->name            = '';
        $model->type            = 1;
        $model->level           = 1;
        $model->status          = 1;
        $model->notice_type     = 0;
        $model->reason          = '';
        $model->reason_detail   = '';
        $model->create_type     = 1;
        $model->visit_status    = 0;
        $model->create_time     = time();
        $model->update_time     = time();
        $model->is_first        = 1;
        $model->assign_technician_type = 0;
        $model->assign_plantime_type   = 0;
        $model->cancel_status   = 1;
        if($model->save()) {
            return $model->work_no;
        }

        throw new \Exception('生成失败',30002);
    }

    /**
     * 生成工单号
     * @param $orderNo
     * @return string
     * @author xi
     * @date 2018-1-31
     */
    public static function getWorkNo($orderNo)
    {
         $count = Work::find()->where(['order_no'=>$orderNo])->count();
         return $orderNo . sprintf('%02d',$count+1);
    }

    /**
     * 技师工作台
     * @param $technicianId
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getWorkbenchList($technicianId,$service_time,$work_no)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkRelTechnician::tableName(). ' as b'],' a.work_no = b.work_no');
        $where = [
            'a.status' => [2,3,5],
            'b.technician_id' => $technicianId,
            'b.type' => 1,
            'a.cancel_status' => 1
        ];
        $db->where($where)
            ->andWhere("b.is_self <> 2 and a.plan_time > 0 and plan_time >= ".$service_time['start_time']." and plan_time <= ".$service_time['end_time']."a.work_no <> $work_no");

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            $db->select('a.order_no,a.work_no,a.plan_time,a.status,a.work_stage,b.technician_id');

            $db->orderBy('a.`status` <> 3 , a.`status` <> 5 , a.`status` <> 2,a.plan_time asc');
            $db->asArray();
            $list = $db->all();

            $orderNos = array_column($list,'order_no');
            $orderArr = WorkOrder::findAllByOrderNos($orderNos);

            $workNos = array_column($list,'work_no');
            //开始服务时间
            $beginServicesArr = WorkProcess::getTypeTimeData($workNos,4);
            //服务阶段
            $workStageIds = array_column($list,'work_stage');
            $serviceFlowRes =  ServiceFlow::findAllByAttributes(['id'=>$workStageIds],'id,service_flow_name');
            $serviceFlowArr = [];
            if($serviceFlowRes){
                $serviceFlowArr = array_column($serviceFlowRes,'service_flow_name','id');
            }

            foreach ($list as $key=>$val)
            {
                $accountId = $addressId = $saleOrderId = $workType = 0;
                if(isset($orderArr[$val['order_no']])){
                    $accountId = $orderArr[$val['order_no']]['account_id'];
                    $addressId = $orderArr[$val['order_no']]['address_id'];
                    $saleOrderId = $orderArr[$val['order_no']]['sale_order_id'];
                    $workType    = $orderArr[$val['order_no']]['work_type'];
                }

                $list[$key]['status_desc']   = self::getStatusDesc($val['status']);
                $list[$key]['account_id']    = $accountId;
                $list[$key]['address_id']    = $addressId;
                $list[$key]['sale_order_id'] = $saleOrderId;
                $list[$key]['work_type']     = $workType;
                $list[$key]['work_stage_desc'] = isset($serviceFlowArr[$val['work_stage']])?$serviceFlowArr[$val['work_stage']]:'';
                $list[$key]['begin_service_time'] = isset($beginServicesArr[$val['work_no']])?$beginServicesArr[$val['work_no']]:'';
            }

            return [
                'totalCount' => $totalNum,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'totalCount' => $totalNum,
                'list'       => []
            ];
        }
    }

    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($where,$page=1,$pageSize=10,$type,$technicianId,$sort)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkRelTechnician::tableName(). ' as b'],' a.work_no = b.work_no');
        $db->where($where);
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }

            $db->select('a.order_no,a.work_no,a.plan_time,a.plan_time_type,a.status,a.cancel_status,a.work_stage,b.technician_id,b.type,b.is_self,a.assign_type');

            $db->orderBy("a.plan_time $sort");
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $orderNos = array_column($list,'order_no');
            $orderArr = WorkOrder::findAllByOrderNos($orderNos);

            $workNos = array_column($list,'work_no');
            //开始服务时间
            $beginServicesArr = WorkProcess::getTypeTimeData($workNos,4);

            $workRelTechRes = WorkRelTechnician::find()
                ->where("type != 2 and is_self != 2")
                ->andWhere(['technician_id' => $technicianId,'work_no' => $workNos])
                ->asArray()->all();
            $workRelTechArr = array_column($workRelTechRes,'work_no','work_no');



            foreach ($list as $key=>$val)
            {
                $accountId = $addressId = $saleOrderId = $workType = 0;
                $subjectName = '';
                if(isset($orderArr[$val['order_no']]))
                {
                    $accountId   = $orderArr[$val['order_no']]['account_id'];
                    $addressId   = $orderArr[$val['order_no']]['address_id'];
                    $saleOrderId = $orderArr[$val['order_no']]['sale_order_id'];
                    $workType    = $orderArr[$val['order_no']]['work_type'];
                    $subjectName = $orderArr[$val['order_no']]['subject_name'];
                }

                if($type == 2 && isset($workRelTechArr[$val['work_no']]))
                {
                    unset($list[$key]);
                    continue;
                }

                $list[$key]['status']        = $val['type'] == 2?7:$val['status'];
                $list[$key]['status_desc']   = self::getStatusDesc($list[$key]['status']);
                if($val['assign_type'] == 2 && $val['status'] == 6)
                {
                    $list[$key]['status']        = $val['type'] == 2?7:1;
                    $list[$key]['status_desc']   = self::getStatusDesc(1);
                }

                $list[$key]['cancel_status_desc'] = $val['cancel_status']==1?'未关闭':'已关闭';
                $list[$key]['account_id']         = $accountId;
                $list[$key]['address_id']         = $addressId;
                $list[$key]['sale_order_id']      = $saleOrderId;
                $list[$key]['work_type']          = $workType;
                $list[$key]['begin_service_time'] = isset($beginServicesArr[$val['work_no']])?$beginServicesArr[$val['work_no']]:'';
                $list[$key]['subject_name']       = $subjectName?$subjectName:'';
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }



    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getWorkList($where)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkRelTechnician::tableName(). ' as b'],' a.work_no = b.work_no');
        $db->where($where);

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            $db->select('a.order_no,a.work_no,a.plan_time,a.status,a.cancel_status,a.work_stage,b.technician_id,b.type');

            $db->orderBy('a.plan_time asc, a.create_time asc');
            $db->asArray();
            $list = $db->all();

            $workNos = array_column($list,'work_no');
            //使用配件的工单
            $work = WorkCost::find()->select('work_no','cost_id')
                ->where(['work_no' => $workNos, 'cost_type' => 2])->asArray()->all();
            return ['list' => $work];
        }
        else
        {
            return ['list' => []];
        }
    }

    /**
     * 订单详情
     * @param $workNo
     * @return array
     */
    public static function detail($workNo, $technicianId)
    {
        $query = self::findOneByAttributes(['work_no'=>$workNo]);
        if($query)
        {
            $orderNo = $query['order_no'];
            $workOrderArr = \webapp\modules\v1\models\WorkOrder::findOneByOrderNo($orderNo);
            if(!$workOrderArr){
                return ModelBase::errorMsg('未找到工单信息',20003);
            }

            $finshArr = $beginServicesArr = $cancelArr = [];
            //开始服务时间
            if($query['status'] >= 3){
                $beginServicesArr = WorkProcess::getTypeTimeData($workNo,4);
            }
            //完成时间
            if($query['status'] >= 4){
                $beginServicesArr = WorkProcess::getTypeTimeData($workNo,4);
                $finshArr = WorkProcess::getTypeTimeData($workNo,5);
            }

            //关闭信息
            if($query['cancel_status'] == 2){
                $cancelArr = WorkProcess::findOneByAttributes(['work_no'=>$workNo,'type'=>10],'comment,create_time');
            }
            //收费项目
            $workCostData = WorkCost::getList($workNo);
            $workCostList = $workCostData['list'];
            //判断是否是改派
            $workRelTechnician = WorkRelTechnician::findOne(['work_no' => $workNo, 'technician_id' => $technicianId]);

            if($query['status'] == 6) {
                $query['status'] = 1;
            }

            if($workRelTechnician && $workRelTechnician->type == 2) {
                $query['status'] = 6;
            }
            $result = [
                'work_department_id' => $query['department_id'],
                'signature'          => $query['signature'],
                'work_no'            => $query['work_no'],
                'order_no'           => $query['order_no'],
                'subject_name'       => $workOrderArr['subject_name']?$workOrderArr['subject_name']:'',
                'account_id'         => $workOrderArr['account_id'],
                'department_id'      => $workOrderArr['department_id'],
                'address_id'         => $workOrderArr['address_id'],
                'sale_order_id'      => $workOrderArr['sale_order_id'],
                'work_type'          => $workOrderArr['work_type'],
                'is_scope'           => $workOrderArr['is_scope'],
                'scope_img'          => $workOrderArr['scope_img'] != '' ? explode(',',$workOrderArr['scope_img']) : [],
                'plan_time'          => $query['plan_time'],
                'plan_time_type'     => $query['plan_time_type'],
                'work_stage'         => $query['work_stage'],
                'fault_img'          => $workOrderArr['fault_img']!=''?explode(',',$workOrderArr['fault_img']):[],
                'remark'             => $workOrderArr['description'],
                'status'             => $query['status'],
                'status_desc'        => Work::getStatusDesc($query['status']),
                'cancel_status'      => $query['cancel_status'],
                'cancel_status_desc' => $query['cancel_status']==1?'未关闭':'已关闭',
                'begin_service_time' => isset($beginServicesArr[$query['work_no']])?$beginServicesArr[$query['work_no']]:0,
                'finsh_time'         => isset($finshArr[$query['work_no']])?$finshArr[$query['work_no']]:0,
                'cancel_time'        => isset($cancelArr['create_time'])?$cancelArr['create_time']:0,
                'cancel_season'      => isset($cancelArr['comment'])?$cancelArr['comment']:'',
                'work_cost_list'     => $workCostList,
                'assign_type'        => $query['assign_type']
            ];

            return $result;
        }
        return ModelBase::errorMsg('未找到工单信息',20002);
    }

    /**
     * 开始服务
     * @param $workNo
     * @return array
     */
    public static function beginService($workNo,$technicianId,$department_id,$operator_user_type,$type)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'work_no' => $workNo,
                'status'  => 2,
                'cancel_status' => 1
            ];
            $model = Work::findOne($where);
            if(!$model->status) {
                return ModelBase::errorMsg('工单已取消，无法继续操作！',20005);
            }
            if($model)
            {
                $model->status = 3;
                if($model->save(false))
                {
                    //改成服务中
                    $res = WorkOrderStatus::updateAll(['status'=>3,'update_time'=>time()],['order_no'=>$model->order_no]);
                    $work_process = WorkProcess::add($workNo, $department_id, $technicianId, $operator_user_type, $type, '技师开始服务');

                    if($res && $work_process)
                    {

                        //发消息
//                        $technicianName = self::getTechnicianName($technicianId);
//                        Message::send(23,$model->order_no,$technicianName,$technicianId,$workNo);
                        //订单流转过程
                        $orderDetail = WorkOrderDetail::find()
                            ->where(['order_no'=>$model->order_no])
                            ->asArray()->one();
                        if($orderDetail) {
                            WorkOrderProcess::add($model->order_no, 3, '');
                        }

                        $transaction->commit();

                        //技师开始服务动态填加
                        $params = ['jsId' => $technicianId];
                        WorkOrderDynamic::add(13,$model->order_no,$params,$workNo);

                        //技师排班
                        ScheduleTechnicianQueue::push($workNo,3,date('Y-m-d'), date('Y-m-d',$model->plan_time));

                        return [
                            'work_no' => $workNo
                        ];
                    }
                }
            }
            $transaction->rollBack();
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
        }
        return ModelBase::errorMsg('操作失败',20005);
    }

    /**
     * 完成服务
     * @param $workNo
     * @return array
     */
    public static function finshService($workNo,$technicianId)
    {
        $model = self::findOne(['work_no'=>$workNo,'status'=>5,'cancel_status'=>1]);
        if($model)
        {
            $model->status          = 4;
            $model->confirm_status  = 1;
            $model->visit_status    = 2;
            $model->update_time = time();
            if($model->save())
            {
                //添加技师备件 使用/回收 记录
                WorkCost::getCostStorage($workNo);
                //技师完成动态填加
                $params = [
                    'jsId' => $technicianId,
                ];
                WorkOrderDynamic::add(15,$model->order_no,$params,$workNo);

                //查一下是否预约了下一个工单
                $workArr = Work::findOneByAttributes(['order_no'=>$model->order_no,'status'=>[1,2,6]]);
                if(!$workArr) {
                    WorkOrderStatus::changeStatus($model->department_id, $model->order_no, 4, 0);
                    WorkOrderProcess::add($model->order_no, 4, '待验收');

                    $departmentId = 0;
                    $departArr = Common::getDepartmentIdByTechnicianId($technicianId);
                    if(isset($departArr['department_id'])){
                        $departmentId = $departArr['department_id'];
                    }

                    WorkProcess::add($workNo,$departmentId,$technicianId,2,5,'已完成');
                }

                //发消息
//                $technicianName = self::getTechnicianName($technicianId);
                //Message::send(23,$model->order_no,$technicianName,$technicianId,$workNo);

                //技师排班
                ScheduleTechnicianQueue::push($workNo,4,date('Y-m-d'), date('Y-m-d', $model->plan_time));

                //完成服务给未评价的单子加上数据
                WorkAppraisal::add($model->order_no,$model->work_no,$model->update_time,$model->department_id);

                return [
                    'work_no' => $workNo
                ];
            }
        }
        return ModelBase::errorMsg('操作失败',20005);
    }

    /**
     * 历史工单列表
     * @param $orderNo
     * @return array
     */
    public static function historyServiceList($orderNo)
    {
        $query = self::findAllByAttributes(['order_no'=>$orderNo,'status'=>4,'cancel_status'=>1]);
        if($query)
        {
            //订单信息
            $orderArr = WorkOrder::findOneByAttributes(['order_no'=>$orderNo]);
            if(!$orderArr){
                return ModelBase::errorMsg('未找到数据',20004);
            }

            $workNos = array_column($query,'work_no');
            //查出技师信息
            $technicianArr = WorkRelTechnician::findTechnicianIdsByWorkNos($workNos);
            //查出工单产品记录
            $productArr = WorkProduct::findAllByWorkNos($workNos);
            //查出工单完成时间
            $workFinshTimeArr = WorkProcess::getTypeTimeData($workNos,5);

            $result = [];
            foreach ($query as $val)
            {
                $result [] = [
                    'work_no'     => $val['work_no'],
                    'update_time' => $val['update_time'],
                    'work_stage'  => $val['work_stage'],
                    'reason'      => $val['reason'],
                    'reason_detail'=> $val['reason_detail'],
                    'plan_time'   => $val['plan_time'],
                    'work_img'    => trim($val['work_img']) == ''? '' : explode(',',$val['work_img']),
                    'scene_img'   => trim($val['scene_img']) ==''? '' : explode(',',$val['scene_img']),
                    'is_scope'    => $orderArr['is_scope'],
                    'scope_img'   => $orderArr['scope_img'] == ''? '' : explode(',',$orderArr['scope_img']),
                    'technician_id' => !empty($technicianArr)?$technicianArr:[],
                    'work_product'  => isset($productArr[$val['work_no']])?$productArr[$val['work_no']]:[],
                    'service_record'=> $val['service_record'],
                    'is_first'      => $val['is_first'],
                    'assign_technician_type' => $val['assign_technician_type'],
                    'assign_plantime_type'   => $val['assign_plantime_type'],
                    'assign_technician_type_desc' => self::getNextServiceDesc($val['assign_technician_type']) ? self::getNextServiceDesc($val['assign_technician_type']) : '无',
                    'assign_plantime_type_desc'   => self::getNextServiceTimeDesc($val['assign_plantime_type']) ? self::getNextServiceTimeDesc($val['assign_plantime_type']) : '无',
                    'finsh_time'  => isset($workFinshTimeArr[$val['work_no']])?$workFinshTimeArr[$val['work_no']]:''
                ];
            }

            return $result;
        }

        return ModelBase::errorMsg('未找到数据',20003);
    }


    /**
     * 修改服务时间
     * @param $workNo
     * @param $srcType
     * @param $srcId
     * @param $processUserId
     * @param $plantime
     * @return array
     */
    public static function changePlantime($workNo,$departmentId,$processUserId,$plantime,$plantimeType)
    {
        $where = [
            'work_no' => $workNo,
            'department_id'   => $departmentId,
            'cancel_status' => 1
        ];
        $model = Work::findOne($where);
        if($model)
        {
            $oldPlantime = $model->plan_time;
            $model->process_user = $processUserId;
            $model->plan_time    = $plantime;
            $model->plan_time_type = $plantimeType;
            $model->update_time  = time();
            if($model->save(false))
            {
                //更新订单的时间
                $attr = [
                    'plan_time'      => $plantime,
                    'plan_time_type' => $plantimeType,
                    'update_time'    => time()
                ];
                WorkOrder::updateAll($attr,['order_no'=>$model->order_no]);

                //发消息
                //Message::send(31,$model->order_no,'',0,$workNo);

                //技师排班
                ScheduleTechnicianQueue::push($workNo,6, date('Y-m-d',$plantime), date('Y-m-d',$oldPlantime));

                return [
                    'work_no' => $workNo
                ];
            }
        }
        return ModelBase::errorMsg('修改失败',20007);
    }

    /**
     * 查询工单信息
     */
    public static function findOneByWorkNo ($workNo) {
        $result = [];
        $where = [
            'work_no'   => $workNo,
        ];
        $query = self::find()
            ->where($where)
            ->asArray()->one();
        if($query)
        {
            return $query;
        }
        return $result;
    }

    /**
     * 工单关闭/取消服务
     * @param $workNo
     * @param $departmentId
     * @param $processUserId
     * @param $reason
     * @return array
     * @throws \Exception
     * @author xi
     * @date 2018-6-21
     */
    public static function close($workNo,$departmentId,$processUserId,$reason)
    {
        $where = [
            'work_no' => $workNo,
        ];
        $model = self::find()
            ->where($where)
            ->andWhere('cancel_status <> 2')
            ->one();
        if($model)
        {
            if($model->is_first == 1){
                return ModelBase::errorMsg('第一个工单不能取消',20012);
            }

            $model->cancel_status = 2;
            $model->update_time = time();
            if($model->save(false))
            {
                //工单过程
                WorkProcess::add($workNo,$departmentId,$processUserId,1,10,$reason);
                //订单变成待验收
                WorkOrderStatus::changeStatus($departmentId,$model->order_no,4,$departmentId);

                //调队列处理技师排版
                $query = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo]);
                if($query)
                {
                    $tid = $query[$workNo];
                    QueuePush::sendPush('reduce','tech-schedule','reduce',[$tid,Department::getDepartmentTopId($departmentId),$departmentId]);
                }


                return [
                    'work_no' => $workNo
                ];
            }
        }
        return ModelBase::errorMsg(' 取消失败',20007);
    }

    /**
     * 技师指派
     * @param $workNo
     * @param $srcType
     * @param $srcId
     * @param $processUserId
     * @param $plantime
     * @param $technicianId
     * @param $technicianType
     * @param $technicianLeader
     * @return array
     * @author xi
     * @date 2018-2-4
     */
    public static function assign($workNo,$departmentTopId,$departmentId,$opUserType,$processUserId,$plantime,$plantimeType,$technicianIds,$technicianType,$technicianLeader)
    {
        $transaction = self::getDb()->beginTransaction();

        try
        {
            $where = [
                'work_no' => $workNo,
                //'department_id'   => $departmentId, 最终接手这工单的机构id
                'status'   => [1,2,6],
                'cancel_status' => 1
            ];
            $model = self::find()
                ->where($where)
                ->one();
            if($model)
            {
                if($plantime>0){
                    $model->plan_time      = $plantime;
                    $model->plan_time_type = $plantimeType;
                }
                if($technicianType == 2){
                    $model->status = 6;
                    $model->assign_type=2;
                }
                else {
                    $model->status = 2;
                    $model->assign_type=1;
                }

                $model->update_time = time();

                if($model->save(false))
                {
                    //更新订单时间
                    if($plantime>0) {
                        WorkOrder::updateAll(['plan_time' => $plantime, 'plan_time_type' => $plantimeType], ['order_no' => $model->order_no]);
                    }


                    //如果是指派多技师，查出他们的组长 (组长指派不执行)
                    if($technicianType == 1 ) //多技师指派
                    {
                        $leaderId = Common::getGroupingLeader($departmentTopId,$departmentId,$technicianIds[0]);
                        if($leaderId>0){
                            $workTechnician = WorkRelTechnician::findOne(['work_no'=>$workNo,'is_self'=>2,'technician_id'=>$leaderId]);
                            if(empty($workTechnician)){
                                WorkRelTechnician::add($workNo,$leaderId,2,$opUserType,$processUserId);
                            }

                        }
                    }

                    //填加技师关系
                    if($technicianIds)
                    {
                        foreach ( $technicianIds as $val)
                        {
                            $isSelf = 3;
                            if($technicianType == 2){
                                $isSelf = 2;
                            }
                            else if( $technicianType ==1 && $val == $technicianLeader){
                                $isSelf = 1;
                            }
                            $workTechnician = WorkRelTechnician::findOne(['work_no'=>$workNo,'is_self'=>$isSelf,'type'=>1,'technician_id'=>$val]);
                            if(empty($workTechnician)){
                                WorkRelTechnician::add($workNo,$val,$isSelf,$opUserType,$processUserId);
                            }
                        }

                        //如果以前有改派过的，给了 type_status 改 0，以免一个单子会在 待服务，已关闭出现
                        $where = [
                            'work_no'       => $workNo,
                            'type'          => 2,
                            'type_status'   => 1,
                            'technician_id' => $technicianIds
                        ];
                        WorkRelTechnician::updateAll(['type_status'=>0],$where);
                        unset($relTechArr,$where);
                    }

                    if($opUserType == 11)
                    {
                        //技师支配技师动态填加
                        $params = [
                            'zhipaiJs' => $processUserId,
                            'jsId'      => $technicianIds,
                            'jsLeader' => $technicianLeader
                        ];
                        WorkOrderDynamic::add(6,$model->order_no,$params,$workNo);
                    }
                    else
                    {
                        //机构指派技师动态填加
                        $params = [
                            'accountId'    => $processUserId,
                            'departmentId' => $departmentId,
                            'jsId'         => $technicianIds,
                            'jsLeader'     => $technicianLeader
                        ];
                        WorkOrderDynamic::add(5,$model->order_no,$params,$workNo);
                    }

                    $orderModel = WorkOrder::findOne(['order_no'=>$model->order_no]);

                    $orderModel->is_assign_technician = 1;
                    $orderModel->save(false);

                    //指派的为技师时订单改成待服务
                    WorkOrderStatus::changeStatus($orderModel->department_id,$model->order_no,2,$departmentId);
                    WorkOrderStatus::changeStatus($departmentId,$model->order_no,2,$departmentId,'');

                    //更改指派机构状态为 指派指师
                    WorkOrderAssign::changeAssignType($model->order_no,$departmentId,$opUserType,$processUserId);
                    $transaction->commit();
                      //APP推送消息
                    @PushMessage::push($model->order_no,"zp");
                    //QueuePush::sendPush('increase','tech-schedule','increase',[$technicianIds,2]);
                    //QueuePush::sendPush('increase','tech-schedule','increase',[$technicianIds,4]);

                    //技师排班
                    if($model->plan_time >0 && $technicianType == 1){
                        ScheduleTechnicianQueue::push($workNo,1,date('Y-m-d',$model->plan_time), date('Y-m-d',$model->plan_time));
                    }

                    return [
                        'work_no' => $workNo
                    ];

                }
            }
            $transaction->rollBack();
            return ModelBase::errorMsg(' 指派失败',20009);
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg(' 指派失败',20011);
        }
    }



    /**
     * 技师改派
     * @param $workNo
     * @param $srcType
     * @param $srcId
     * @param $processUserId
     * @param $plantime
     * @param $technicianId
     * @param $reason
     * @return array
     * @author xi
     * @date 2018-2-4
     */
    public static function reAssign($workNo,$departmentTopId,$departmentId,$opUserType,$processUserId,$plantime,$plantimeType,$technicianIds,$reason,$technicianType,$technicianLeader)
    {
        //$technicianType 1 多技师待服务 2组长待指派

        $transaction = self::getDb()->beginTransaction();
        try
        {
            $where = [
                'work_no'  => $workNo,
                //'department_id'   => $departmentId, 最终接手该工单的dpartmentId
                'status'   => [1,2,6],
                'cancel_status' => 1
            ];
            $model = self::find()
                ->where($where)
                ->one();
            if($model)
            {
                $oldPlanTime   = $model->plan_time;
                $oldAssignType = $model->assign_type;

                //记录旧的指派技师id
                $oldTechnicianIds = [];
                $oldTechnicianRes = WorkRelTechnician::findAllByAttributes(['work_no'=>$workNo,'type'=>1],'technician_id');
                foreach ($oldTechnicianRes as $val){
                    $oldTechnicianIds[] = $val['technician_id'];
                }
                if($technicianType == 2){
                    $model->status = 6; //组长待指派
                    $model->assign_type=2; //组长
                }
                else {
                    $model->status = 2; //待服务
                    $model->assign_type=1;//多技师
                }

                if($plantime >0){
                    $model->plan_time      = $plantime;
                    $model->plan_time_type = $plantimeType;
                }
                $model->update_time = time();

                $model->department_id = $departmentId;
                if($model->save(false))
                {
                    //更新订单时间
                    if($plantime >0) {
                        WorkOrder::updateAll(['plan_time' => $plantime, 'plan_time_type' => $plantimeType], ['order_no' => $model->order_no]);
                    }
                }

                $delJsIds = $newAddJsIds = [];

                    //如果旧的是指派多技师
                    if($oldAssignType == 1)
                    {
                        //如果新改派的是组长( 查一下旧的指派组长是否与新的一样，一样就不处理，不一样删除加新的)
                        if($technicianType == 2)
                        {
                            $groupLeaderArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'is_self'=>2,'type'=>1]);
                            //如果一样就把组员删除了,保留组长
                            if($groupLeaderArr && $groupLeaderArr['technician_id'] == $technicianIds[0])
                            {
                                $query = WorkRelTechnician::findAllByAttributes(" type=1 and work_no = '$workNo' and technician_id<>".$groupLeaderArr['technician_id'],'technician_id');
                                if($query){
                                    $delJsIds = array_column($query,'technician_id');
                                }


                                $attr = [
                                    'type'         => 2,
                                    'update_time'  => time(),
                                    'op_user_type' => $opUserType,
                                    'op_user_id'   => $processUserId,
                                    'reason'       => $reason
                                ];
                                WorkRelTechnician::updateAll($attr," type=1 and work_no = '$workNo' and technician_id<>".$groupLeaderArr['technician_id']);
                            }
                            //如果都不一样就把旧的全部人员删除了，加上新组长
                            else
                            {
                                $query = WorkRelTechnician::findAllByAttributes(" type=1 and work_no = '$workNo' ",'technician_id');
                                if($query){
                                    $delJsIds = array_column($query,'technician_id');
                                }

                                $attr = [
                                    'type'         => 2,
                                    'update_time'  => time(),
                                    'op_user_type' => $opUserType,
                                    'op_user_id'   => $processUserId,
                                    'reason'       => $reason
                                ];
                                WorkRelTechnician::updateAll($attr," work_no = '$workNo' and type=1 ");
                                WorkRelTechnician::add($workNo,$technicianIds[0],2,$opUserType,$processUserId);
                            }
                        }
                        else  //如果新指派的是多技师 ( 查出是否同一组，如果同组算出差积， 不同组旧的删除，新的加入 )
                        {
                            //查出新改派的组id
                            $newAssignGroupingId = Common::getGroupingIdByJsId($departmentId,$technicianIds[0]);

                            //查出旧指派的技师所属组id
                            $oldAssignGroupingId =0;
                            $oldJsArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'type'=>1]);
                            if($oldJsArr){
                                $oldAssignGroupingId = Common::getGroupingIdByJsId($departmentId,$oldJsArr['technician_id']);
                            }

                            //如果新改派与旧指派的不是一组，全部改成已改派，加上新的指派技师

                            if($newAssignGroupingId != $oldAssignGroupingId)
                            {

                                $attr = [
                                    'type'         => 2,
                                    'update_time'  => time(),
                                    'op_user_type' => $opUserType,
                                    'op_user_id'   => $processUserId,
                                    'reason'       => $reason
                                ];
                                WorkRelTechnician::updateAll($attr," work_no = '$workNo' and type = 1 ");

                                //加上组长
                                $groupLeaderId = Common::getGroupingLeader($departmentTopId,$departmentId,$technicianIds[0]);
                                if($groupLeaderId>0){
                                    WorkRelTechnician::add($workNo,$groupLeaderId,2,$opUserType,$processUserId);
                                }
                                //加上新指师
                                foreach ($technicianIds as $val)
                                {
                                    $isSelf = 3;
                                    if($technicianLeader == $val){
                                        $isSelf = 1;
                                    }
                                    WorkRelTechnician::add($workNo,$val,$isSelf,$opUserType,$processUserId);
                                }
                            }
                            else //同一个组，算出差积
                            {
                                $oldJsIds = [];
                                $jsArr = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo],'is_self<>2');
                                if($jsArr){
                                    $oldJsIds = $jsArr[$workNo];
                                }

                                $oldLeaderJsId = 0;
                                $leaderJsArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'is_self'=>1,'type'=>1],'technician_id');
                                if($leaderJsArr){
                                    $oldLeaderJsId = $leaderJsArr['technician_id'];
                                }


                                //算出新加的
                                $newAddJsIds = array_diff($technicianIds,$oldJsIds);
                                //算出要删除的
                                $delJsIds = array_diff($oldJsIds,$technicianIds);


                              //上次指派技师和本次指派技师相同，且负责人相同
                                if(empty($delJsIds) && empty($newAddJsIds) && $oldLeaderJsId == $technicianLeader){
                                    return self::errorMsg('不可重复指派与上次相同的技师');
                                }

                                //删除旧的技师
                                if($delJsIds)
                                {
                                    $attr = [
                                        'type'         => 2,
                                        'update_time'  => time(),
                                        'op_user_type' => $opUserType,
                                        'op_user_id'   => $processUserId,
                                        'reason'       => $reason
                                    ];
                                    WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$delJsIds,'type'=>1,'is_self'=>[1,3]]);
                                }

                                if($newAddJsIds){
                                    foreach ($newAddJsIds as $val){
                                        WorkRelTechnician::add($workNo,$val,3,$opUserType,$processUserId);
                                    }
                                }

                                //如果修改时间，同时又修改了负责人
                                if(!$delJsIds && !$newAddJsIds && $oldPlanTime != $model->plan_time && $oldLeaderJsId != $technicianLeader)
                                {
                                    $transaction->rollBack();


                                    self::changeLeader($workNo,$technicianLeader);

                                    return self::curlChagePlantime($departmentId,$workNo,$processUserId,$plantime);
                                }
                                //如果只修改了服务时间，特殊处理
                                else if(!$delJsIds && !$newAddJsIds && $oldPlanTime != $model->plan_time)
                                {
                                    //如果只修改时间，先让回滚调接口处理修改时间
                                    $transaction->rollBack();
                                    return self::curlChagePlantime($departmentId,$workNo,$processUserId,$plantime);
                                }
                                //只修改了负责人
                                else if(!$delJsIds && !$newAddJsIds && $oldLeaderJsId != $technicianLeader)
                                {
                                    $transaction->rollBack();
                                    self::changeLeader($workNo,$technicianLeader);

                                    return [
                                        'work_no' => $workNo
                                    ];
                                }
                                else {
                                    self::changeLeader($workNo,$technicianLeader);
                                }
                            }
                        }
                    }
                    else  //如果旧的指派是组长
                    {
                        //如果新改派的是组长
                        if($technicianType == 2)
                        {
                            $groupLeaderArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'is_self'=>2,'type'=>1]);
                            if($groupLeaderArr)
                            {
                                if($groupLeaderArr['technician_id'] !=$technicianIds[0])
                                {
                                    $attr = [
                                        'type'         => 2,
                                        'update_time'  => time(),
                                        'op_user_type' => $opUserType,
                                        'op_user_id'   => $processUserId,
                                        'reason'       => $reason
                                    ];
                                    WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$groupLeaderArr['technician_id'],'type'=>1]);
                                    WorkRelTechnician::add($workNo,$technicianIds[0],2,$opUserType,$processUserId);
                                }
                                else
                                {
                                    $attr = [
                                        'type'         => 2,
                                        'update_time'  => time(),
                                        'op_user_type' => $opUserType,
                                        'op_user_id'   => $processUserId,
                                        'reason'       => $reason
                                    ];
                                    WorkRelTechnician::updateAll($attr,"work_no='$workNo' and type = 1 and is_self<>2 and technician_id<>".$groupLeaderArr['technician_id'] );

                                    //如果只修改时间，先让回滚调接口处理修改时间
                                    //$transaction->rollBack();

                                    //如果只修改了服务时间，特殊处理
                                    if($oldPlanTime != $model->plan_time){
                                        return self::curlChagePlantime($departmentId,$workNo,$processUserId,$plantime);
                                    }
                                }
                            }
                            else
                            {
                                $attr = [
                                    'type'         => 2,
                                    'update_time'  => time(),
                                    'op_user_type' => $opUserType,
                                    'op_user_id'   => $processUserId,
                                    'reason'       => $reason
                                ];
                                WorkRelTechnician::updateAll($attr,"work_no='$workNo' and type = 1");
                                WorkRelTechnician::add($workNo,$technicianIds[0],2,$opUserType,$processUserId);
                            }

                        }
                        else  //新改派是多技师
                        {
                            //查一下旧的组长是否与新技师组长一个人
                            $groupLeaderArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'is_self'=>2,'type'=>1]);
                            $newGroupLeader = Common::getGroupingLeader($departmentTopId,$departmentId,$technicianIds[0]);
                            if($groupLeaderArr)
                            {
                                if($groupLeaderArr['technician_id'] != $newGroupLeader && $newGroupLeader>0)
                                {
                                    $attr = [
                                        'type'         => 2,
                                        'update_time'  => time(),
                                        'op_user_type' => $opUserType,
                                        'op_user_id'   => $processUserId,
                                        'reason'       => $reason
                                    ];
                                    WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$groupLeaderArr['technician_id'],'type'=>1]);
                                    WorkRelTechnician::add($workNo,$newGroupLeader,2,$opUserType,$processUserId);
                                }
                                else if($newGroupLeader == 0)
                                {
                                    $attr = [
                                        'type'         => 2,
                                        'update_time'  => time(),
                                        'op_user_type' => $opUserType,
                                        'op_user_id'   => $processUserId,
                                        'reason'       => $reason
                                    ];
                                    WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$groupLeaderArr['technician_id'],'type'=>1]);
                                }
                            }
                            else if($newGroupLeader>0)
                            {
                                WorkRelTechnician::add($workNo,$newGroupLeader,2,$opUserType,$processUserId);
                            }


                            $oldJsIds = [];
                            $jsArr = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo],'is_self<>2');
                            if($jsArr){
                                $oldJsIds = $jsArr[$workNo];
                            }

                            //算出新加的
                            $newAddJsIds = array_diff($technicianIds,$oldJsIds);
                            //算出要删除的
                            $delJsIds = array_diff($oldJsIds,$technicianIds);

                            if($delJsIds)
                            {
                                $attr = [
                                    'type'         => 2,
                                    'update_time'  => time(),
                                    'op_user_type' => $opUserType,
                                    'op_user_id'   => $processUserId,
                                    'reason'       => $reason
                                ];
                                WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$delJsIds,'type'=>1,'is_self'=>[1,3]]);
                            }

                            if($newAddJsIds){
                                foreach ($newAddJsIds as $val){
                                    WorkRelTechnician::add($workNo,$val,3,$opUserType,$processUserId);
                                }
                            }
                            //把负责人给加上( 因为担心会出现多个负责人，所以先全部改成协助人，之后再改成负责人)
                            WorkRelTechnician::updateAll(['is_self' => 3], "work_no = '$workNo' and is_self<>2 and type =1");
                            WorkRelTechnician::updateAll(['is_self' => 1], "work_no = '$workNo' and technician_id = $technicianLeader and is_self<>2 and type =1");
                        }
                    }


                    Work::updateAll(['is_reassign'=>2],['work_no'=>$workNo]);


                //查看订单
                $orderModel = WorkOrder::findOne(['order_no'=>$model->order_no]);


                //如果指派的机构为原下单机构,改派技师时之前指派的机构订单为取消状态
                if($orderModel->department_id==$departmentId){
                    $orderStatus = WorkOrderStatus::find()->where(['order_no'=>$model->order_no])->orderBy(['id'=> SORT_DESC])->one();
                    if($orderStatus){
                        WorkOrderStatus::changeStatus($orderStatus->department_id,$model->order_no,6,$departmentId);
                        //上次指派的机构变成已改派，订单状态为已取消
                        $workOrderAssign = WorkOrderAssign::find()->where(['order_no'=>$model->order_no])->orderBy(['id'=> SORT_DESC])->one();;
                        if($workOrderAssign){
                            if($workOrderAssign->type==1){
                                $workOrderAssign->type=2;
                                $workOrderAssign->save(false);
                            }
                        }
                        //订单回归到原下单机构,本机构里不用还原
                        if($workOrderAssign->service_department_id != $departmentId){
                            WorkOrderAssign::add($model->order_no,1,$departmentId,$departmentId,'',2);
                        }
                    }
                    WorkOrderStatus::changeStatus($departmentId,$model->order_no,2,$departmentId,'');


                }else{
                    //改派的为技师时订单改成待服务
                    WorkOrderStatus::changeStatus($orderModel->department_id,$model->order_no,2,$departmentId);
                    WorkOrderStatus::changeStatus($departmentId,$model->order_no,2,$departmentId,'');
                }

                   /* //更改指派机构状态为 指派指师
                    WorkOrderAssign::changeAssignType($model->order_no,$departmentId);
                    WorkOrderAssign::reAssignChangeType($model->order_no,$departmentId);*/

                if($opUserType == 11)
                {
                    //技师支配技师动态填加
                    $params = [
                        'zhipaiJs' => $processUserId,
                        'jsId'     => $technicianIds,
                        'jsLeader' => $technicianLeader
                    ];
                    WorkOrderDynamic::add(8,$model->order_no,$params,$workNo);
                }
                else
                {
                    //机构改派动态填加
                    $params = [
                        'accountId'    => $processUserId,
                        'departmentId' => $departmentId,
                        'jsId'         => $technicianIds,
                        'reason'       => $reason,
                    ];
                    WorkOrderDynamic::add(8,$model->order_no,$params,$workNo);
                }

                    /*
                    //推送及消息
                    Message::pushReAssignWork($model->order_no,$workNo,$plantime,$oldPlanTime,$technicianIds,$oldTechnicianIds);*/

                    //若之前
                  $transaction->commit();
                  @PushMessage::push($model->order_no,"gp",$workNo,$oldTechnicianIds);
                  @PushMessage::push($model->order_no,"zp",$workNo,$technicianIds);

                   //改派技师时排班日历数据修改
                    QueuePush::sendPush('increase','tech-schedule','increase',[$technicianIds,2]);
                    QueuePush::sendPush('increase','tech-schedule','increase',[$technicianIds,4]);

                    //技师排班
                    if($model->plan_time >0){
                        $params = [
                            'newAddJsIds' => isset($newAddJsIds)?$newAddJsIds:[],
                            'delJsIds'    => $delJsIds
                        ];
                        ScheduleTechnicianQueue::push($workNo,2,date('Y-m-d',$model->plan_time), date('Y-m-d',$oldPlanTime), json_encode($params));
                    }

                    return [
                        'work_no' => $workNo,
                        'oldTids' => isset($delJsIds) ? $delJsIds : ''
                    ];

            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            throw $e;
            return ModelBase::errorMsg(' 改派失败'.$e->getMessage() . $e->getFile() . $e->getLine(),20013);
        }
        $transaction->rollBack();
        return ModelBase::errorMsg(' 改派失败',20012);
    }

    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getPcList($where,$page=1,$pageSize=10,$sort=0)
    {
        $db = static::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkOrder::tableName(). ' as b'],' a.order_no = b.order_no');
        $db->innerJoin([WorkOrderDetail::tableName() . ' as c'],'b.order_no = c.order_no');

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        $db->groupBy('a.order_no');

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('b.order_no,a.work_no,a.plan_time,a.plan_time_type,a.status,a.work_stage,b.account_id,b.address_id,b.work_type,c.sale_order_id,is_reassign,a.is_first');
            if($sort){
                $db->orderBy('a.plan_time desc');
            }else{
                $db->orderBy('a.plan_time asc');
            }
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $workNos = array_column($list,'work_no');
            //开始服务时间
            $beginServicesArr = WorkProcess::getTypeTimeData($workNos,4);
            //完成时间
            $finshServicesArr = WorkProcess::getTypeTimeData($workNos,5);
            //技师id
            $technicianArr = WorkRelTechnician::findTechnicianLeader($workNos);

            //服务流程
            $workStageIds = array_column($list,'work_stage');
            $workStageRes = ServiceFlow::findAllByAttributes(['id'=>$workStageIds],'id,title');
            $workStageArr = array_column($workStageRes,'title','id');

            //关闭原因
            $cancelReasonArr = WorkProcess::getReason($workNos,10);

            foreach ($list as $key=>$val)
            {
                $tmptechnicianArr = [];
                $tmpTechnicianId  = 0;
                if(isset($technicianArr[$val['work_no']])){
                    $tmpJsArr = $technicianArr[$val['work_no']];
                    if(isset($tmpJsArr[1]))
                    {
                        $tmpTechnicianId = $tmpJsArr[1]['technician_id'];
                        $tmptechnicianArr = [
                            'technician_id' => $tmpJsArr[1]['technician_id'],
                            'is_self'       => 1,
                            'job_title'     => '负责人'
                        ];
                    }
                    else if(isset($tmpJsArr[2]))
                    {
                        $tmpTechnicianId = $tmpJsArr[2]['technician_id'];
                        $tmptechnicianArr = [
                            'technician_id' => $tmpJsArr[2]['technician_id'],
                            'is_self'       => 2,
                            'job_title'     => '组长'
                        ];
                    }
                    else if(isset($tmpJsArr[3]))
                    {
                        $tmpTechnicianId = $tmpJsArr[3]['technician_id'];
                        $tmptechnicianArr = [
                            'technician_id' => $tmpJsArr[3]['technician_id'],
                            'is_self'       => 2,
                            'job_title'     => ''
                        ];
                    }
                }

                $list[$key]['status_desc']   = self::getStatusDesc($list[$key]['status']);
                $list[$key]['begin_service_time'] = isset($beginServicesArr[$val['work_no']])?$beginServicesArr[$val['work_no']]:'';
                $list[$key]['finsh_service_time'] = isset($finshServicesArr[$val['work_no']])?$finshServicesArr[$val['work_no']]:'';
                $list[$key]['technician_arr']     = $tmptechnicianArr;
                $list[$key]['technician_id']      = $tmpTechnicianId;
                $list[$key]['work_flow_desc']     = isset($workStageArr[$val['work_stage']])?$workStageArr[$val['work_stage']]:'';
                $list[$key]['cancel_reason']      = isset($cancelReasonArr[$val['work_no']])?$cancelReasonArr[$val['work_no']]:'';
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 订单详情
     * @param $workNo
     * @param $srcType
     * @param $srcId
     * @return array
     */
    public static function getDetail($workNo,$departmentId)
    {
        $result = [];
        $query = self::findOneByAttributes(['work_no'=>$workNo,'department_id'=>$departmentId]);
        if($query)
        {
            //服务完成时间
            $finshServiceTime = WorkProcess::getTypeTimeData($workNo,5);
            //产品记录
            $productLogs = WorkProduct::getList($workNo);
            //收费项目
            $workCost = WorkCost::getList($workNo);
            //订单信息
            $orderInfo = WorkOrder::findOneByOrderNo($query['order_no']);
            //收费标准
            $orderStandard = WorkOrderStandard::findOneByAttributes(['order_no'=>$query['order_no']],'type,type_id,standard_id');
            //技师信息
            $technicianArr = WorkRelTechnician::findTechnicianIdsByWorkNo($workNo);
            //技师分成
            $costDivideArr = WorkCostDivide::getListByWorkNo($workNo);

            $result = [
                'order_no'           => $query['order_no'],
                'work_no'            => $query['work_no'],
                'settlement_status'  => $query['settlement_status'],
                'confirm_status'     => $query['confirm_status'],
                'status'             => $query['status']==6?2:$query['status'],
                'status_desc'        => self::getStatusDesc($query['status']==6?2:$query['status']),
                'cancel_status'      => $query['cancel_status'],
                'plan_time'          => $query['plan_time'],
                'plan_time_type'     => $query['plan_time_type'],
                'assign_type'        => $query['assign_type'],
                'finsh_service_time' => isset($finshServiceTime[$workNo])?$finshServiceTime[$workNo]:'',
                'work_img'           => explode(',',$query['work_img']),
                'scene_img'          => array_filter(explode(',',$query['scene_img'])),
                'service_record'     => $query['service_record'],
                'product_logs'       => $productLogs,
                'work_cost'          => $workCost['list'],
                'order_info'         => $orderInfo,
                'order_standard'     => $orderStandard,
                'technicianArr'      => isset($technicianArr[$workNo])?$technicianArr[$workNo]:[],
                'visit_status'       => $query['visit_status'],
                'is_first'           => $query['is_first'],
                'cost_divide_arr'    => $costDivideArr,
                'work_stage'         => $query['work_stage'],
            ];
        }
        return $result;
    }


    /**
     * 根据工单信息查技师id
     * @param array $workNos
     * @return array
     */
    public static function findTechnicianIdsByOrderNos(array $orderNos)
    {
        $query = self::find()
            ->where(['order_no'=>$orderNos,'cancel_status'=>1])
            ->select('technician_id,order_no,min(id) as min_id')
            ->groupBy('order_no')
            ->asArray()->all();
        if($query)
        {
            return array_column($query,'technician_id','order_no');
        }
        return [];
    }
    
    /**
     * 未结算工单
     * @param  int  $id   技师ID
     * @param  int  $page 页数
     * @author li
     * @date   2018-02-05
     * @return array
     */
    public static function getWaitCostOrder($id, $page, $pageSize = 10) {
        $data = self::find()
                ->where(['technician_id' => $id, 'settlement_status' => 1, "status" => 4])
                ->orderBy('process_time asc')
                ->offset(($page-1)*$pageSize)
                ->limit($pageSize)
                ->asArray()->all();
        //根据工单号查询订单的服务内容
        $servicesContent = $orderNos = [];
        foreach ($data as $v) {
            $orderNos[] = $v['order_no'];
        }
        //去重
        $orderNos = array_unique($orderNos);
        //根据订单号查询服务内容
        $saleOrderIds = WorkOrderDetail::getSaleOrder($orderNos);
        foreach ($data as $k => $v) {
            $servicesContent[$k]['work_no'] = $v['work_no']; 
            $servicesContent[$k]['process_time'] = date("Y-m-d H:i:s",$v['process_time']);
            $servicesContent[$k]['order_no'] = $v['order_no'];
            if(isset($saleOrderIds[$v['order_no']])) {
                $servicesContent[$k]['service_content'] = $saleOrderIds[$v['order_no']]['service_content']; 
            }
        }
        return $servicesContent;
    }


    /**
     * 查询当前订单在当天是否有上一个订单
     * @date 2017-11-20
     * @param $time
     * @param int $id 技师ID
     */
    public static function getFrontOrder($time, $id) {
        //得到当前时间的年月日
        $day = date("Y-m-d",$time);
        //起始时间
        $start_time = strtotime($day."00:00:00");

        $where = "plan_time>=$start_time and plan_time<=$time and status=1";
        $order = self::find()->where($where)->andWhere(['technician_id' => $id])->orderBy('plan_time desc')->asArray()->one();
        if(!empty($order)) {
            $frontOrder = WorkOrder::getOrder($order['order_no']);
            //获取服务地址
            $address = AccountAddress::getOrderAddress($frontOrder['address_id']);
            foreach ($address as $v) {
                $frontOrder['address'] = $v['address'];
                $frontOrder['lon']     = $v['lon'];
                $frontOrder['lat']     = $v['lat'];
            }
            return $frontOrder;
        }
        return [];
    }


    /**
     * 查询当前订单在当天是否有上一个订单
     * @date 2017-11-20
     * @param $time
     * @param int $id 技师IDiddi
     */
    public static function getAfterOrder($time, $id) {
        //得到当前时间的年月日
        $day = date("Y-m-d",$time);
        //起始时间
        //结束时间
        $end_time   = strtotime($day."23:59:59");
        $where = "plan_time>=$time and plan_time<=$end_time and status=1";
        $order = self::find()->where($where)->andWhere(['technician_id' => $id])->orderBy('plan_time asc')->asArray()->one();
        if(!empty($order)) {
            $afterOrder= WorkOrder::getOrder($order['order_no']);
            //获取服务地址
            $address = AccountAddress::getOrderAddress($afterOrder['address_id']);
            foreach ($address as $v) {
                $afterOrder['address'] = $v['address'];
                $afterOrder['lon']     = $v['lon'];
                $afterOrder['lat']     = $v['lat'];
            }
            return $afterOrder;
        }
        return [];
    }

    /**
     * 获取工单列表
     * @param $orderNo
     * @return array
     * @author xi
     * @date 2018-2-11
     */
    public static function getListByOrderNo($orderNo)
    {
        $result = [];
        $query = self::findAllByAttributes(['order_no' => $orderNo]);
        if($query)
        {
            $workNos = array_column($query,'work_no');
            //服务完成时间
            $finshServiceTime = WorkProcess::getTypeTimeData($workNos,5);
            //产品记录
            $productLogs = WorkProduct::getList($workNos);
            //收费项目
            $workCostArr = WorkCost::getListByWorkNos($workNos);
            //技师信息
            $technicianArr = WorkRelTechnician::findTechnicianIdsByWorkNos($workNos);

            foreach ($query as $val)
            {
                $result[] = [
                    'order_no'           => $val['order_no'],
                    'work_no'            => $val['work_no'],
                    'status'             => $val['status'],
                    'status_desc'        => self::getStatusDesc($val['status']),
                    'reason'             => $val['reason'],
                    'reason_detail'      => $val['reason_detail'],
                    'plan_time'          => $val['plan_time'],
                    'finsh_service_time' => isset($finshServiceTime[$val['work_no']])?$finshServiceTime[$val['work_no']]:'',
                    'work_img'           => trim($val['work_img'])!=''? explode(',',$val['work_img']) : [],
                    'scene_img'          => trim($val['scene_img'])!=''?array_filter(explode(',',$val['scene_img'])):[],
                    'service_record'     => $val['service_record'],
                    'product_logs'       => isset($productLogs[$val['work_no']])?$productLogs[$val['work_no']]:[],
                    'technician_id'      => isset($technicianArr[$val['work_no']])?$technicianArr[$val['work_no']]:0,
                    'work_cost'          => isset($workCostArr[$val['work_no']])?$workCostArr[$val['work_no']]:[],
                ];
            }
        }
        return $result;
    }

    /**
     * 根据订单查出工单信息
     * @param $orderNo
     * @return array
     */
    public static function getDetailByOrderNo($orderNo)
    {
        $result = [];
        $query = self::findAllByAttributes(['order_no'=>$orderNo]);
        if($query)
        {
            $workNos = array_column($query,'work_no');
            //服务完成时间
            $finshServiceTime = WorkProcess::getTypeTimeData($workNos,5);
            //产品记录
            $productLogs = WorkProduct::getList($workNos);
            $productLogs = ArrayHelper::index($productLogs,null,'work_no');
            //收费项目
            $workCost = WorkCost::getList($workNos);
            $workCost = ArrayHelper::index($workCost['list'],null,'work_no');

            //技师信息
            $technicianArr = WorkRelTechnician::findTechnicianIdsByWorkNo($workNos);

            //阶段 work_stage
            $workStage = array_column($query,'work_stage');
            $workStageRes = ServiceFlow::findAllByAttributes(['id'=>$workStage],'title as service_flow_name,title,id');
            $workStageArr = [];
            if($workStageRes){
                $workStageArr = array_column($workStageRes,'service_flow_name','id');
            }

            //技师分成
            $costDivideArr = [];
            $costDivideRes = WorkCostDivide::getListByWorkNo($workNos);

            if($costDivideRes){
                $costDivideArr = ArrayHelper::index($costDivideRes,null,'work_no');
            }
            //工单支付方式
            $workPayTypeArr = WorkOrderTrade::findAllByAttributes(['work_no'=>$workNos,'trade_status'=>1],'trade_pay_code,work_no','work_no');
            if($workPayTypeArr){
                $workPayTypeArr = array_column($workPayTypeArr,'trade_pay_code','work_no');
            }
            //获取工单的评价状态
            $workAppraisalStatus = WorkAppraisal::getAppraisalStatus($workNos);
            //获取订单中的工单评论信息
            $workAppraisalData = WorkAppraisal::getAppraisalData($workNos);
            foreach ($query as $val)
            {
                $result[] = [
                    'work_no'            => $val['work_no'],
                    'status'             => $val['status']==6?2:$val['status'],
                    'status_desc'        => self::getStatusDesc($val['status']==6?2:$val['status']),
                    'cancel_status'      => $val['cancel_status'],
                    'plan_time'          => $val['plan_time'],
                    'plan_time_type'     => $val['plan_time_type'],
                    'finsh_service_time' => isset($finshServiceTime[$val['work_no']])?$finshServiceTime[$val['work_no']]:'',
                    'work_img'           => array_filter(explode(',',$val['work_img'])),
                    'scene_img'          => array_filter(explode(',',$val['scene_img'])),
                    'signature'          => array_filter(explode(',',$val['signature'])),
                    'service_record'     => $val['service_record'],
                    'product_logs'       => isset($productLogs[$val['work_no']])?$productLogs[$val['work_no']]:[],
                    'work_cost'          => isset($workCost[$val['work_no']])?$workCost[$val['work_no']]:[],
                    'technicianArr'      => isset($technicianArr[$val['work_no']])?$technicianArr[$val['work_no']]:[],
                    'visit_status'       => $val['visit_status'],
                    'is_first'           => $val['is_first'],
                    'work_stage'         => $val['work_stage'],
                    'work_stage_desc'    => isset($workStageArr[$val['work_stage']])?$workStageArr[$val['work_stage']]:'',
                    'cost_divide_arr'    => isset($costDivideArr[$val['work_no']])?$costDivideArr[$val['work_no']]:[],
                    'create_time'        => $val['create_time'],
                    'pay_type'           => isset($workPayTypeArr[$val['work_no']])?$workPayTypeArr[$val['work_no']]:'',
                    'appraisal_status'   => isset($workAppraisalStatus[$val['work_no']])?$workAppraisalStatus[$val['work_no']]:'',
                    'appraisal_arr'      => isset($workAppraisalData[$val['work_no']])?$workAppraisalData[$val['work_no']]:[],
                ];
            }
        }
        return $result;
    }


    /*
     * 获取指定时间段内所有没有结算的工单
     * */
    public static function getWorkForSettlement($start_time,$end_time,$src_type,$src_id)
    {

        $where = "c.update_time>=$start_time and c.update_time<=$end_time";

        $db = self::find();

        $db->from(self::tableName() . ' as b' );
        $db->innerJoin([WorkOrder::tableName().' as a'],'b.order_no = a.order_no');
        $db->innerJoin([WorkProcess::tableName().' as c'],'b.work_no = c.work_no');
        $db->innerJoin([WorkCostDivide::tableName().' as d'],'d.work_no = b.work_no');
        $db->innerJoin([WorkOrderDetail::tableName().' as e'],'b.order_no = e.order_no');
        $db->where($where);
        $db->andWhere(['b.settlement_status' => 1, 'b.status' => 4]);
        $db->andWhere(['b.src_type'=>$src_type,'b.src_id'=>$src_id]);
        $db->andWhere(['c.type'=>5]);
        $db->select(' c.update_time as update_time,a.order_no as order_no,e.sale_order_id,b.work_no as work_no,b.id as work_id,b.technician_id as tech_id,d.cost_id as cost_id,d.cost_name as cost_name,d.divide_amount as divide_amount');
        $db->orderBy('b.technician_id asc');

       // $sql = $db->createCommand()->getRawSql();
       // print_r($sql);exit;
        $query = $db->asArray()->all();
        $result = [];
        $tech_arr = [];
        $work_list  = [];
        $result_work_list =[];

        if($query)
        {
            $orderNos = array_unique(array_column($query,'order_no'));
            $techIds = array_unique(array_column($query,'tech_id'));
            $techInfos = Technician::getTechnicianNames($techIds);
            $saleOrders = WorkOrderDetail::getSaleOrder($orderNos);

                foreach ($query as $data)
                {
                    $result [$data['tech_id']]['cost_items'][] = ['cost_id'=>$data['cost_id'],'cost_name'=>$data['cost_name'],'amount'=>$data['divide_amount']/10000];
                    $result[$data['tech_id']]['work_info'][] = $data['work_no'];
                    $work_list [$data['work_no']]['work_info'][]=  ['tech_id'=>$data['tech_id'],'work_id'=>$data['work_id'],'finish_time'=>$data['update_time'],'work_no'=>$data['work_no'],'order_no'=>$data['order_no']];
                    $work_list [$data['work_no']]['cost_items'][] = $data['divide_amount']/10000;
                }
                foreach ($result  as $tech_id=>$value)
                {
                    $cost_items = [];
                    if($value)
                    {
                        foreach ($value['cost_items'] as $cost)
                        {
                            if(isset($cost_items[$cost['cost_id']]))
                            {
                                $tmp = $cost_items[$cost['cost_id']];
                                $tmp['amount']+= $cost['amount'];
                            }else{
                                $tmp =[];
                                $tmp = $cost;
                            }
                            $cost_items[$cost['cost_id']] = $tmp;
                        }
                    }
                    $tech_arr[] = ['tech_id'=>$tech_id,'tech_name'=>$techInfos[$tech_id],'work_number'=>count(array_unique($value['work_info'])),'work_amount'=>array_sum(array_column($value['cost_items'],'amount')),'cost_items'=>$cost_items];
                }

                foreach ($work_list as $work_no =>$work_info)
                {
                    $info = $work_info['work_info'][0];
                    $result_work_list[] = ['tech_id'=>$info['tech_id'],'work_id'=>$info['work_id'],'work_no'=>$work_no,'finish_time'=>date('Y-m-d H:i:s',$info['finish_time']),'amount'=>array_sum($work_info['cost_items']),'service_content'=>$saleOrders[$info['order_no']]['service_content']];
                }
        }
        return [
            'technicianList' => $tech_arr,
            'workList' => $result_work_list,
        ];
    }

    /**
     * 根据订单查出技师信息
     * @param array $orderNos
     * @return array
     */
    public static function findTechnicianIdsByOrderNosNew(array $orderNos)
    {
        $query = self::find()
            ->where(['order_no'=>$orderNos,'cancel_status'=>1])
            ->select('technician_id,order_no,work_no,min(id) as min_id')
            ->groupBy('order_no')
            ->asArray()->all();
        if($query)
        {
            $workNos = array_column($query,'work_no');
            $workRelTechnicianArr = WorkRelTechnician::findTechnicianLeader($workNos);

            $result = [];
            foreach ($query as $val)
            {
                $technicianId = 0;
                $title = '';
                if(isset($workRelTechnicianArr[$val['work_no']]))
                {
                    $tmp = $workRelTechnicianArr[$val['work_no']];
                    if(isset($tmp[1]) && $tmp[1]){
                        $technicianId = $tmp[1]['technician_id'];
                        $title = '负责人';
                    }
                    else if(isset($tmp[2]) && $tmp[2]){
                        $technicianId = $tmp[2]['technician_id'];
                        $title = '组长';
                    }
                    else if(isset($tmp[3]) && $tmp[3]){
                        $technicianId = $tmp[3]['technician_id'];
                        $title = '协助';
                    }
                }

                if($technicianId>0 && $title)
                {
                    $result [$val['order_no']] = [
                        'technician_id' => $technicianId,
                        'title'         => $title,
                        'work_no'       => $val['work_no']
                    ];
                }
            }
            return $result;

        }
        return [];
    }
    /**
     * 根据订单查出所有工单
     * @param array $orderNos
     * @return array
     */
    public static function findWorkIdsByOrderNos($orderNos=[])
    {
        $query = self::find()
            ->where(['order_no'=>$orderNos,'cancel_status'=>1])
            ->select('order_no,work_no,id')
            ->asArray()->all();
        if($query)
        {
            foreach ($query as $key=>$val) {
                $result[$val['order_no']][] = $val['work_no'];
            }
            return $result;
        }
        return [];
    }

    public static function getGroupWork($techician_ids) {
        $db = self::find();
        $db->from(self::tableName() . ' as a' );
        $db->innerJoin([WorkRelTechnician::tableName().' as b'],'b.work_no = a.work_no');
        $db->where(['b.type' => 1, 'b.technician_id' => $techician_ids,'a.cancel_status' => 1, 'a.status' => [1,2,3,5,6]]);
        return $db->asArray()->all();
    }

    /**
     * 请求接口来修改时间
     * @param $srcType
     * @param $srcId
     * @param $workNo
     * @param $processUserId
     * @param $plantime
     * @return array
     */
    public static function curlChagePlantime($departmentId,$workNo,$processUserId,$plantime)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/change-plantime";
        $postData = [
            'department_id'          => $departmentId,
            'work_no'         => $workNo,
            'process_user_id' => $processUserId,
            'plantime'        => $plantime,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return $jsonArr['data'];
        }
        return ModelBase::errorMsg($jsonArr['message'],$jsonArr['code']);
    }

    /**
     * 更改负责人
     * @param $workNo
     * @param $technicianLeader
     */
    public static function changeLeader($workNo,$technicianLeader)
    {
        WorkRelTechnician::updateAll(['is_self' => 3], "work_no = '$workNo' and is_self<>2 and type =1");
        WorkRelTechnician::updateAll(['is_self' => 1], "work_no = '$workNo' and technician_id = $technicianLeader and is_self<>2 and type =1");
    }

    /**
     * 开始服务
     * @param $workNo
     * @param $departmentId
     * @param $operatorUserId
     * @param $workRelTechId
     * @return array
     */
    public static function pcBeginService($workNo,$departmentId,$operatorUserId,$workRelTechId)
    {
        $transaction = self::getDb()->beginTransaction();
        try
        {
            $model = self::findOne(['work_no'=> $workNo, 'status' => 2]);
            if($model)
            {
                $model->status = 3;
                $model->update_time = time();
                if($model->save(false))
                {
                    //工单过程表加上
                    WorkProcess::add($workNo, $departmentId,$operatorUserId,1,4,'机构点击开始服务');
                    //订单过程
                    WorkOrderProcess::add($model->order_no,3,'机构点击开始服务');
                    //订单状态改成服务中
                    WorkOrderStatus::batchChangeStatus($model->order_no,3,'机构点击开始服务');

                    $transaction->commit();

                    //下级机构代替技师开始服务动态填加
                    $params = [
                        'accountId' => $operatorUserId,
                    ];
                    WorkOrderDynamic::add(12,$model->order_no,$params,$workNo);

                    //技师排班
                    ScheduleTechnicianQueue::push($workNo,3,date('Y-m-d'), date('Y-m-d',$model->plan_time));

                    return [
                        'work_no' => $workNo
                    ];
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('操作失败'.$e->getMessage(), 20021);
        }

        return ModelBase::errorMsg('操作失败', 20022);
    }

    /**
     * 批量取消工单
     * @param $orderNo
     * @return bool
     * @throws \Exception
     * @author xi
     */
    public static function batchCancelWork($orderNo)
    {
        if(self::updateAll(['cancel_status'=> 2 ,'update_time' => time()],['order_no' => $orderNo])){
            return true;
        }
        return false;
    }


    /**
     * 修改
     */
    public static function addSignature($workNo, $process_user, $signature)
    {
        $work = self::findOne(['work_no' => $workNo, 'cancel_status' => 1]);
        if(!$work) {
            return ModelBase::errorMsg('工单不存在', 200001);
        }
        $work->process_user = $process_user;
        $work->update_time  = time();
        $work->signature    = $signature;
        if($work->save()) {
            return [
                'work_no' => $workNo
            ];
        }
        return ModelBase::errorMsg('操作失败', 20002);
    }

    /**
     * 统计订单有多少工单
     * @param $orderNos
     * @return array
     */
    public static function totalWorkOrderCount($orderNos,$andWhere = [])
    {
        $db = self::find();
        $db->where(['order_no'=>$orderNos,'cancel_status'=>1]);
        if($andWhere){
            $db->andWhere($andWhere);
        }
        $db->select("count(id) as `count` , order_no");
        $db->groupBy('order_no');
        $db->asArray();
        $query = $db->all();

        if($query)
        {
            return array_column($query,'count','order_no');
        }

        return [];
    }
    /**
     * 统计订单中工单的服务流程
     * sxz 2018-7-23
     *
     */
    public static function workStageArr($orderNos,$andWhere = [])
    {
        $db = self::find();
        $db->where(['order_no'=>$orderNos,'cancel_status'=>1]);
        if($andWhere){
            $db->andWhere($andWhere);
        }
        $db->select("order_no,status,work_no,work_stage,create_time,plan_time,plan_time_type,service_record");
        //$db->groupBy('order_no');
        $db->asArray();
        $query = $db->all();
        if($query)
        {
            //工单的服务流程
            $workStage = array_column($query,'work_stage');
            $workStageRes = ServiceFlow::findAllByAttributes(['id'=>$workStage],'title as service_flow_name,title,id');
            $workStageArr = [];
            if($workStageRes){
                $workStageArr = array_column($workStageRes,'service_flow_name','id');
            }
            $workNos = array_column($query,'work_no');
            //工单的完成时间
            $finshServicesArr = [];
            $finshServicesArr = WorkProcess::getTypeTimeData($workNos,5);
            $newQuery = [];
            foreach($query as $key => $value){
                $value['work_stage_desc']    = isset($workStageArr[$value['work_stage']])?$workStageArr[$value['work_stage']]:'';
                $value['finsh_service_time'] = isset($finshServicesArr[$value['work_no']])?$finshServicesArr[$value['work_no']]:'';
                $newQuery[$value['order_no']][]= $value;
                /*if($value['status'] != 4){
                    unset($value);
                }else{
                    $value['work_stage_desc']    = isset($workStageArr[$value['work_stage']])?$workStageArr[$value['work_stage']]:'';
                    $value['finsh_service_time'] = isset($finshServicesArr[$value['work_no']])?$finshServicesArr[$value['work_no']]:'';
                    $newQuery[$value['order_no']][]= $value;
                }*/
            }
            if($newQuery){
                foreach ($newQuery as $kk=>$vv){
                    if(isset($vv) && is_array($vv)){
                        $sort = array_column($vv, 'finsh_service_time');
                        array_multisort($sort, SORT_DESC, $vv);
                        $newQuery[$kk] = $vv;
                    }
                }
            }
            return $newQuery;
        }
        return [];
    }

    /**
     * 收费确认列表
     */
    public static function feeList($where,$page,$pageSize)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->leftJoin([WorkOrder::tableName().' as b'],'a.order_no = b.order_no');
        $db->innerJoin([WorkCost::tableName().' as c'],'a.work_no = c.work_no');
        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        $db->groupBy("a.work_no");
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum > 0) {
            if ($pageSize <= 0) {
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum / $pageSize);
            if ($page < 1) {
                $page = 1;
            }
            $db->select('a.work_no,a.fee_remark,a.order_no,b.work_type');
            $db->orderBy("a.update_time desc");
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $workNos = array_column($list,"work_no");
            //工单负责的技师
            $relTec = WorkRelTechnician::findAllByAttributes(['work_no'=>$workNos,'is_self'=>1,'type'=>1],"technician_id,work_no","work_no");
            //工单收费总额
            $cost_amount = WorkCost::findAllByAttributes(['work_no'=>$workNos],'sum(cost_amount) as cost_amount,work_no','work_no','1000','work_no');
            //实收金额
            $cost_real_amount = WorkCost::findAllByAttributes(['work_no'=>$workNos,'payer_id'=>3],'sum(cost_real_amount) as cost_real_amount,work_no','work_no','1000','work_no');
            //收费时间
            $feeTime = WorkProcess::findAllByAttributes(['work_no'=>$workNos,'type'=>5],'create_time,work_no','work_no');
            foreach ($list as $key=>$val){
                if(isset($relTec[$val['work_no']])){
                    $list[$key]['technician_id'] = $relTec[$val['work_no']]['technician_id'];
                }else{
                    $list[$key]['technician_id'] = '';
                }
                if(isset($cost_amount[$val['work_no']])){
                    $list[$key]['cost_amount'] = $cost_amount[$val['work_no']]['cost_amount'];
                }else{
                    $list[$key]['cost_amount'] = 0;
                }
                if(isset($cost_real_amount[$val['work_no']])){
                    $list[$key]['cost_real_amount'] = $cost_real_amount[$val['work_no']]['cost_real_amount'];
                }else{
                    $list[$key]['cost_real_amount'] = 0;
                }
                if(isset($feeTime[$val['work_no']])){
                    $list[$key]['create_time'] = $feeTime[$val['work_no']]['create_time'];
                }else{
                    $list[$key]['create_time'] = '';
                }
            }
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        return [
            'page'       => $page,
            'totalCount' => $totalNum,
            'totalPage'  => 0,
            'list'       => []
        ];
    }
}
