<?php
namespace webapp\modules\v1\models;

use common\helpers\Helper;
use common\models\Common;
use common\models\Push;
use common\lib\Push as JPush;
use webapp\models\ModelBase;
use Yii;

class Message
{

    public static function send($type,$orderNo,$workTechnicianName='',$workTechnicianId = 0,$workNo='')
    {
        $orderData = WorkOrder::findOneByOrderNo($orderNo);
        if($orderData)
        {
            $srcType = $orderData['src_type'];
            $srcId   = $orderData['src_id'];
            $serviceProvideId = $orderData['service_provide_id'];

            $userName       = WorkOrder::getUserName($orderData['create_user_id']);
            $accountName    = WorkOrder::getAccountName($orderData['account_id']);
            $className      = WorkOrder::getClassNameByStoreId($orderData['sale_order_id']);
            $workName       = WorkOrder::getWorktypeName($orderData['work_type']);
            $storeName      = WorkOrder::getStoreName($orderData['service_provide_id']);

            $srcTypeName    = WorkOrder::getBName($srcId);
            $planTime       = date("Y-m-d H:i:s",$orderData['plan_time']);

            $content = [
                'plan_time'  => date("Y-m-d H:i:s",$orderData['plan_time']),
                'order_no'   => $orderNo,
                'class_name' => $className,
                'work_name'  => $workName,
                'status'     => $orderData['status'],
                'work_no'    => $workNo
            ];
            $workArr = Work::find()->where(['work_no' => $workNo])->asArray()->one();
            if($workArr) {
                $content['status'] = $workArr['status'];
            }
            $content = json_encode($content);


            //商家/门店新建订单（不指定服务商）
            if(in_array($type,[1,15]))
            {
                //推送
                $tmpData = WorkOrder::getDetail($orderNo,$srcType,$srcId);
                $tempArr = Push::orderData($tmpData);
                if($tempArr){
                    foreach ($tempArr as $val){
                        \common\lib\Push::pushMsg($val['title'],$val['content'],$val['technician_id'],1,$val['order_code'],$val['order_status']);
                    }
                }

                $title = "【新建订单】".$userName."为客户".$accountName."新建".$className."-".$workName."订单";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds){
                    foreach ($srcUserIds as $val){
                        self::callSendApi($srcType,$srcId,$title,$content,$srcId,$val);
                    }
                }

                //给所有服务商发送
                $title = "【抢单通知】您有新的抢单机会，快去看看吧";
                $i = 1;
                while (true)
                {
                    $srcUserIds = WorkOrder::getServiceProviderUserIds($srcType,$srcId,$i,1000);
                    if($srcUserIds) {
                        foreach ($srcUserIds as $val) {
                            self::callSendApi(ModelBase::SRC_FWS, $val, $title, $content, $srcId, $val);
                        }
                    }
                    else {
                        break;
                    }
                    $i++;
                }
            }
            //商家/门店新建订单（指定服务商）
            else if(in_array($type,[5,17]))
            {
                $title = "【新建订单】".$userName."为客户".$accountName."新建".$className."-".$workName."订单，并指派给".$storeName;
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }

                $title = "【接单提醒】".$srcTypeName."为您指派订单了，快去接单并安排技师服务吧";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $srcId, $val);
                    }
                }
            }
            //商家/门店取消订单
            else if(in_array($type,[2,16]) || ($type == 14 && $orderData['src_type'] == ModelBase::SRC_SJ))
            {
                //查出工单下的技师
                $workArr = Work::findAllByAttributes(['order_no'=>$orderNo],'technician_id');
                if($workArr){
                    $technicianids = array_filter(array_column($workArr,'technician_id'));
                    foreach ($technicianids as $technicianId)
                    {
                        //发送推送
                        \common\lib\Push::pushMsg("订单被取消了","[".$planTime."][".$className."][".$workName."]订单被取消了，请查看！",$technicianId,1,$orderNo,6);

                        //发送消息
                        $title = "订单已取消！";
                        self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,$srcId,$technicianId);
                    }
                }

                //服务商发布
                $title = "【订单被取消】您有订单已被".$srcTypeName."取消";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $srcId, $val);
                    }
                }

                //商家发送
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }


            }
            //运营后台取消订单
            else if($type == 3)
            {
                $title = "【订单被取消】您有订单已被优服务平台取消";

                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, -1, $val);
                    }
                }
                //给服务商发送
                if($serviceProvideId<0){
                    $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                    if($srcUserIds) {
                        foreach ($srcUserIds as $val) {
                            self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, -1, $val);
                        }
                    }
                }
                //给技师发送
                $workArr = Work::findAllByAttributes(['order_no'=>$orderNo],'technician_id');
                if($workArr) {
                    $technicianids = array_filter(array_column($workArr, 'technician_id'));
                    foreach ($technicianids as $technicianId) {
                        $title = "订单已取消！";
                        self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,-1,$technicianId);
                    }
                }
            }
            //响应超时（不指定服务商，无人抢单）
            else if($type == 4)
            {
                $title = "【订单响应超时】您有订单因没有服务商接单，已自动取消";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, -1, $val);
                    }
                }
            }
            else if($type == 6)
            {
                $title = "【服务商已接单】".$storeName."已接单";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $serviceProvideId, $val);
                    }
                }

                $title = "【接单成功】您已确认接单，请尽快指派技师";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $serviceProvideId, $val);
                    }
                }
            }
            //服务商拒绝接单（指定服务商）
            else if($type == 7)
            {
                $title = "【订单被拒接】您有订单已被".$storeName."拒接";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $serviceProvideId, $val);
                    }
                }
            }
            //工单指派
            else if($type == 8)
            {
                $title = '您有新的工单！请查看！';

                //如果指派了技师，给技师发推送
                if($workNo != ''){
                    $work_status = $orderData['status'];
                    foreach ($workTechnicianId as $jsId)
                    {
                        $workArr = Work::findOneByAttributes(['work_no'=>$workNo],'plan_time');
                        $pushContent = date('m月d日 H:i',$workArr['plan_time'])."|" . $className.$workName;
                        $workObj = Work::findOne(['work_no' => $workNo]);
                        if($workObj && $workObj->status == 6) {
                            $work_status = 9;
                            $title = '您有新的待指派工单！请查看！';
                        }
                        \common\lib\Push::pushMsg($title,$pushContent,$jsId,1,['work_no'=>$workNo,'order_no'=>$orderNo],$work_status);
                        //发送消息
                        self::callSendApi(ModelBase::SRC_JS,$jsId,$title,$content,$srcId,$jsId);
                    }
                }
            }
            //工单改派
            else if($type == 9)
            {

                $title = '工单已改派';

                //如果指派了技师，给技师发推送
                if($workNo != ''){
                    $jsArr = WorkRelTechnician::find()->where(['work_no'=>$workNo,'type'=>2,'is_self'=>1])->orderBy('id desc')->asArray()->one();
                    if($jsArr)
                    {
                        $workArr = Work::findOneByAttributes(['work_no'=>$workNo],'plan_time');
                        $pushContent = date('m月d日 H:i',$workArr['plan_time'])."|" . $className.$workName;
                        \common\lib\Push::pushMsg($title,$pushContent,$jsArr['technician_id'],1,2,$orderData['status']);
                        //发送消息
                        self::callSendApi(ModelBase::SRC_JS,$jsArr['technician_id'],$title,$content,$srcId,$jsArr['technician_id']);
                    }
                }

                $title = '您有新的工单！请查看！';

                //如果指派了技师，给技师发推送
                if($workNo != ''){
                    $workArr = Work::findOneByAttributes(['work_no'=>$workNo],'plan_time');
                    $pushContent = date('m月d日 H:i',$workArr['plan_time'])."|" . $className.$workName;
                    \common\lib\Push::pushMsg($title,$pushContent,$workTechnicianId,1,['work_no'=>$orderNo,'order_no'=>$orderNo],$orderData['status']);
                    //发送消息
                    self::callSendApi(ModelBase::SRC_JS,$workTechnicianId,$title,$content,$srcId,$workTechnicianId);
                }
            }
            //服务商新建订单
            else if($type == 13)
            {
                $title = "【新建订单】".$userName."为客户".$accountName."新建".$className."-".$workName."订单";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }
            }
            //服务商取消
            else if($type == 14)
            {
                //查出工单下的技师
                $workArr = Work::findAllByAttributes(['order_no'=>$orderNo],'technician_id');
                if($workArr){
                    $technicianids = array_filter(array_column($workArr,'technician_id'));
                    foreach ($technicianids as $technicianId)
                    {
                        //发送推送
                        \common\lib\Push::pushMsg("订单被取消了","[".$planTime."][".$className."][".$workName."]订单被取消了，请查看！",$technicianId,1,$orderNo,6);

                        //发送消息
                        $title = "订单已取消！";
                        self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,$srcId,$technicianId);
                    }
                }

                //服务商发布
                $title = "【订单被取消】您有订单已被".$srcTypeName."取消";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $srcId, $val);
                    }
                }
            }
            //技师开始服务
            else if($type == 23)
            {
                $title = "【技师开始服务】技师".$workTechnicianName."已开始服务";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $workTechnicianId, $val);
                    }
                }

                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $workTechnicianId, $val);
                    }
                }
            }
            //技师完成服务
            else if($type == 24)
            {
                $title = "【技师完成服务】技师".$workTechnicianName."已完成服务";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $workTechnicianId, $val);
                    }
                }

                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $workTechnicianId, $val);
                    }
                }
            }
            //技师抢单（不指定服务商）
            else if($type == 25)
            {
                //查出工单下的技师
                $workArr = Work::findOneByAttributes(['order_no'=>$orderNo,'status'=>2],'technician_id');
                if($workArr) {
                    $technicianId = $workArr['technician_id'];
                    $technicianName = WorkOrder::getTechnicianName($technicianId);
                }

                if(isset($technicianId))
                {
                    $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                    if($srcUserIds){
                        $title = "【技师已接单】".$technicianName."技师已接单";
                        foreach ($srcUserIds as $val) {
                            self::callSendApi($srcType, $srcId, $title, $content, $technicianId, $val);
                        }
                    }

                    $title = "【技师已抢单】".$technicianName."技师抢到了新订单";
                    $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                    if($srcUserIds)
                    {
                        foreach ($srcUserIds as $val) {
                            self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $technicianId, $val);
                        }
                    }
                }
            }
            //指派超时
            else if($type == 26)
            {
                $title = "【订单指派超时】您有订单因服务商长时间未指派技师，快去提醒下吧";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, -1, $val);
                    }
                }
                $title = "【订单指派提醒】您有订单长时间未指派技师，快去指派吧";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, -1, $val);
                    }
                }
            }
            //上门超时
            else if($type == 27)
            {
                $title = "【订单上门超时】您有订单已到达服务时间，技师还未开始服务";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, -1, $val);
                    }
                }
                //给服务商发
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, -1, $val);
                    }
                }

                //查出工单下的技师
                $workArr = Work::findAllByAttributes(['order_no'=>$orderNo,'status'=>2],'technician_id');
                if($workArr) {
                    $technicianids = array_filter(array_column($workArr, 'technician_id'));
                    foreach ($technicianids as $technicianId) {
                        $title = "您有订单已上门超时";
                        self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,-1,$technicianId);
                    }
                }
            }
            //服务商抢单（不指定服务商）
            else if($type == 28)
            {
                $title = "【服务商已接单】".$storeName."已抢单";
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $serviceProvideId, $val);
                    }
                }

                $title = "【抢单成功】您已成功抢单，请尽快指派技师";
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, $serviceProvideId, $val);
                    }
                }
            }
            //催单  @todo  未完成
            else if($type == 29)
            {
                $title = "【客户催单】订单未开始服务,请查看";

                $work = Work::findOne(['order_no' => $orderNo]);

                $content = json_decode($content, true);
                $content['status'] = 8;
                $content['work_no'] = $work->work_no;
                $content = json_encode($content);
                $srcUserIds = WorkOrder::getUserIds($srcType,$srcId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, -1, $val);
                    }
                }
                //给服务商发
                $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS,$serviceProvideId);
                if($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi(ModelBase::SRC_FWS, $serviceProvideId, $title, $content, -1, $val);
                    }
                }
                //查询订单未开始的工单
                $workOrder = WorkOrder::findOne(['order_no' => $orderNo, 'status' => 2]);
                if(!$workOrder) {
                    return ModelBase::errorMsg('未找到订单号：'.$orderNo,20004);
                }

                $workDetail = WorkRelTechnician::findOne(['work_no' => $work->work_no]);
                $technicianId = $workDetail->technician_id;
                $title = "【客户催单】有订单未未开始服务";
                self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,-1,$technicianId);

                //推送
                \common\lib\Push::pushMsg($title,$title,$technicianId,1,2,$orderData['status']);

            }
            //用户下单
            else if($type == 21)
            {
                $title = "【待指派订单】来自微信客户的订单，快去处理吧！";
                $srcUserIds = WorkOrder::getUserIds($srcType, $srcId);
                if ($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }
            }
            //用户取消
            else if($type == 22)
            {
                $title = "【客户取消订单】微信客户已取消订单，请知晓。";
                $srcUserIds = WorkOrder::getUserIds($srcType, $srcId);
                if ($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }
            }
            //厂商修改时间
            else if($type == 30 || ($type == 31 && $orderData['src_type'] == ModelBase::SRC_SJ) || $type == 32)
            {
                $title = "预约服务时间已修改，请查看";
                if($type == 32){
                    $title = "【客户修改时间】微信客户已修改服务时间，请知晓。";
                }

                $srcUserIds = WorkOrder::getUserIds($srcType, $srcId);
                if ($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }

                //如果指派了服务商就给服商发消息
                $fwsArr = WorkOrderAssign::findOneByAttributes(['order_no'=>$orderNo,'type'=>1],'service_provider_id');
                if($fwsArr){
                    $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS, $fwsArr['service_provider_id']);
                    if ($srcUserIds) {
                        foreach ($srcUserIds as $val) {
                            self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                        }
                    }
                }

                //如果指派了技师，给技师发推送
                if($workNo != ''){
                    $jsIds = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo]);
                    if($jsIds){
                        foreach ($jsIds as $val){
                            \common\lib\Push::pushMsg($title,$title,$val,1,2,$orderData['status']);

                            //发送消息
                            self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$srcId,$val);
                        }
                    }
                }

            }
            //服务商修改时间
            else if($type == 31)
            {
                $title = "预约服务时间已修改，请查看";
                $srcUserIds = WorkOrder::getUserIds($srcType, $srcId);
                if ($srcUserIds) {
                    foreach ($srcUserIds as $val) {
                        self::callSendApi($srcType, $srcId, $title, $content, $srcId, $val);
                    }
                }

                //如果指派了技师，给技师发推送
                if($workNo != '')
                {
                    $className = Common::getCityName($orderData['sale_order_id']);
                    $workName  = Common::getWorktypeName($orderData['work_type']);
                    $workData = Work::findOneByWorkNo($workNo);
                    $pushContent = date('m月d日 H:i',strtotime($workData['plan_time']))."|" . $className.$workName;

                    $jsIds = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo]);
                    if($jsIds){
                        foreach ($jsIds[$workNo] as $val){
                            JPush::pushMsg($title,$pushContent,$val,1,['work_no'=>$orderNo,'order_no'=>$orderNo],$orderData['status']);

                            //发送消息
                            self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$srcId,$val);
                        }
                    }
                }
            }
            //关闭工单
            else if($type == 33)
            {
                $title = "工单已关闭";

                $fwsArr = WorkOrderAssign::findOneByAttributes(['order_no'=>$orderNo,'type'=>1],'service_provider_id');
                if($fwsArr){
                    $srcUserIds = WorkOrder::getUserIds(ModelBase::SRC_FWS, $fwsArr['service_provider_id']);
                    if ($srcUserIds) {
                        foreach ($srcUserIds as $val) {
                            self::callSendApi(ModelBase::SRC_FWS, $fwsArr['service_provider_id'], $title, $content, $fwsArr['service_provider_id'], $val);
                        }
                    }
                }

                //如果指派了技师，给技师发推送
                if($workNo != ''){
                    $jsIds = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo]);
                    if($jsIds){

                        $workArr = Work::findOneByAttributes(['work_no'=>$workNo],'plan_time');
                        $pushContent = date('m月d日 H:i',$workArr['plan_time'])."|" . $className.$workName;
                        foreach ($jsIds as $val){
                            \common\lib\Push::pushMsg($title,$pushContent,$val,1,2,$orderData['status']);

                            //发送消息
                            self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$srcId,$val);
                        }
                    }
                }
            }


            //技师开始服务  开始服务前30分钟触发
            else if($type == 200)
            {
                //查出工单下的技师
                $workArr = Work::findAllByAttributes(['order_no'=>$orderNo,'status'=>2],'technician_id');
                if($workArr) {
                    $technicianids = array_filter(array_column($workArr, 'technician_id'));
                    foreach ($technicianids as $technicianId) {
                        $title = "服务即将开始！您准备好了吗？";
                        self::callSendApi(ModelBase::SRC_JS,$technicianId,$title,$content,-1,$technicianId);
                    }
                }
            }
        }
        else {
            return ModelBase::errorMsg('未找到订单号：'.$orderNo,20004);
        }
        return [
            'order_no' =>$orderNo
        ];
    }

    /**
     * 工单改派 推送及发消息
     * @param $orderNo
     * @param $workNo
     * @param $technicianIds
     * @param $planTime
     */
    public static function pushReAssignWork($orderNo,$workNo,$planTime,$oldPlantime,$newTechnicianIds,$oldTechnicianIds)
    {
        if($orderNo && $workNo && $newTechnicianIds)
        {
            $orderData = WorkOrder::findOneByOrderNo($orderNo);
            if ($orderData)
            {
                $className = Common::getCityName($orderData['sale_order_id']);
                $workName  = Common::getWorktypeName($orderData['work_type']);

                $pushContent = date('m月d日 H:i',$planTime)."|" . $className.$workName;

                $content = [
                    'plan_time'  => date("Y-m-d H:i:s",$planTime),
                    'order_no'   => $orderNo,
                    'class_name' => $className,
                    'work_name'  => $workName,
                    'status'     => $orderData['status'],
                    'work_no'    => $workNo
                ];
                $content = json_encode($content);

                $groupLeader = Common::getGroupingLeader($orderData['src_type'],$orderData['src_id'],$newTechnicianIds[0]);
                if($groupLeader>0){
                    array_push($newTechnicianIds,$groupLeader);
                }

                //算出新加的
                $newAddJsIds = array_diff($newTechnicianIds,$oldTechnicianIds);
                //算出要删除的
                $delJsIds = array_diff($oldTechnicianIds,$newTechnicianIds);
                //算出未改变的
                $noChangeJsIds = array_intersect($newTechnicianIds,$oldTechnicianIds);

                if($newAddJsIds)
                {
                    $newAddJsIds = array_unique($newAddJsIds);
                    $work_status =  $orderData['status'];
                    $title = '您有新的工单！请查看！';
                    $workObj = Work::findOne(['work_no' => $workNo]);
                    if($workObj && $workObj->status == 6) {
                        $work_status = 9;
                        $title = '您有新的待指派工单！请查看！';
                    }
                    foreach ($newAddJsIds as $val)
                    {
                        JPush::pushMsg($title,$pushContent,$val,1,['work_no'=>$workNo,'order_no'=>$orderNo],$work_status);
                        //发送消息
                        self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$orderData['src_id'],$val);
                    }
                }
                if($delJsIds)
                {
                    $title = '工单已改派';
                    foreach ($delJsIds as $val)
                    {
                        JPush::pushMsg($title,$pushContent,$val,1,2,$orderData['status']);
                        //发送消息
                        self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$orderData['src_id'],$val);
                    }
                }
                if($noChangeJsIds)
                {
                    $title = '同行技师有变化!';
                    if($planTime!=$oldPlantime){
                        $title = '服务时间已修改!';
                    }

                    foreach ($noChangeJsIds as $val)
                    {
                        JPush::pushMsg($title,$pushContent,$val,1,['work_no'=>$orderNo,'order_no'=>$orderNo],$orderData['status']);
                        //发送消息
                        self::callSendApi(ModelBase::SRC_JS,$val,$title,$content,$orderData['src_id'],$val);
                    }
                }
            }
        }
    }


    /**
     * 调用 api 进行发送
     * @param $srcType
     * @param $srcId
     * @param $title
     * @param $content
     * @param $publishId
     * @param $subscribeId
     * @return array
     * @author  xi
     * @date 2018-1-9
     */
    public static function callSendApi($srcType,$srcId,$title,$content,$publishId,$subscribeId)
    {
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/send-msg";
        $postData = [
            'src_type'     => $srcType,
            'src_id'       => $srcId,
            'title'        => $title,
            'content'      => $content,
            'publish_id'   => $publishId,
            'subscribe_id' => $subscribeId,
            'msg_class'    => 1,
            'msg_type'     => 2,
            'publish_type' => 1,
            'link_info'    => '',
            'notice_type'  => 1,
            'notice_url'   => ''
        ];

        $jsonStr = Helper::curlPostJson($url,$postData);
        $result = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success']==true)
        {
            return [
                'success' => true,
                'message' => '发送成功'
            ];
        }
        else if(isset($result['success']))
        {
            return [
                'success' => false,
                'message' => $result['message']
            ];
        }
        return [
            'success' => false,
            'message' => $jsonStr
        ];
    }
}