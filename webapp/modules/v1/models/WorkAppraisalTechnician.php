<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use yii\db\Query;

class WorkAppraisalTechnician extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_appraisal_technician';
    }

    /**
     * 获取技师平均星级
     * 平均星级=所有已评价工单的评分/已评价工单数（保留一位小数，四舍五入）
     * @param $technicianId
     * @return mixed|string
     * @author xi
     */
    public static function getAvgStartByTechnicianId($technicianId)
    {
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkAppraisal::tableName().'` as b'], 'a.work_no = b.work_no')
            ->where(['a.technician_id' => $technicianId,'b.status'=>1])
            ->select("(sum(b.star_num)/count(1)) as avgStar")
            ->groupBy("a.technician_id")
            ->asArray()
            ->one();

        if($query){
            return sprintf('%.1f',round($query['avgStar'],1));
        }
        return "0";
    }

    /**
     * 获取技师勋章列表
     * @param $technicianId
     * @return array
     * @author xi
     */
    public static function getTechnicianIdMedalList($technicianId)
    {
        $where = [
            'a.technician_id' => $technicianId,
            'b.star_num'      => 5,
            'b.status'        => 1
        ];

        $query = self::find()
            ->from(self::tableName() . " as a")
            ->innerJoin(['`'.WorkAppraisal::tableName().'` as b'] , 'a.work_no = b.work_no')
            ->innerJoin(['`'.WorkAppraisalTag::tableName().'` as c'] , 'b.work_no = c.work_no')
            ->innerJoin(['`'.WorkAppraisalDict::tableName().'` as d'] , 'c.tag_id = d.id')
            ->where($where)
            ->groupBy("c.tag_id")
            ->orderBy("d.num asc")
            ->select("d.id,d.title,d.icon,d.gray_icon ,count(1) as count")
            ->asArray()->all();

        if($query)
        {
            $result = [];
            foreach ($query as $key=>$val)
            {
                if($val['icon'] !=''){
                    $val['icon'] = Yii::$app->params['js.uservices.cn'] . $val['icon'];
                }
                if($val['gray_icon'] !=''){
                    $val['gray_icon'] = Yii::$app->params['js.uservices.cn'] . $val['gray_icon'];
                }
                $result [$val['id']] = $val;
            }
            return $result;
        }
        return [];
    }

    /**
     * 获取技师在本公司排行
     * 总排名为该技师所在公司的平均星级排名；
     * @param $technicianId
     * @param $directCompanyId
     * @return string
     * @throws \yii\db\Exception
     */
    public static function getTechnicianRanking($technicianId,$directCompanyId)
    {
        self::getDb()->createCommand("SET @num = 0")->execute();
        $subQuery = (new Query())
            ->select('(@num :=@num + 1) AS id,sum(a.star_num) / count(1) AS avgStar,b.technician_id')
            ->from(WorkAppraisal::tableName() . " as a")
            ->innerJoin(['`'.WorkAppraisalTechnician::tableName().'` as b'] , 'a.work_no = b.work_no')
            ->where(['a.direct_company_id'=>$directCompanyId])
            ->groupBy('b.technician_id')
            ->orderBy('avgStar DESC, b.technician_id');

        $query = (new Query())
            ->from(['t'=>$subQuery])
            ->where(['technician_id'=>$technicianId])
            ->one(self::getDb());

        if($query)
        {
            return $query['id'];
        }
        return "0";
    }

    /**
     * 客户评价列表
     * @param $technicianId
     * @param $page
     * @param $pageSize
     * @return array
     * @author xi
     */
    public static function getCustomerAppraisalList($technicianId,$showDay,$page,$pageSize)
    {
        $where = [
            'b.technician_id' => $technicianId,
            'a.`status`' => 1,
            'a.appraisal_status' => 2
        ];

        $db = self::find();
        $db->from(WorkAppraisal::tableName() . " as a");
        $db->innerJoin(['`'.WorkAppraisalTechnician::tableName().'` as b'], 'a.work_no = b.work_no');
        $db->where($where);
        //$db->andWhere(['>','a.create_time', strtotime("+ $showDay day", strtotime('today'))]);
        if($showDay > 0) {
            $db->andWhere("(UNIX_TIMESTAMP(FROM_UNIXTIME(a.create_time,'%Y-%m-%d')) + (86400 * $showDay)) - ".strtotime('today') ." <= 0");
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1){
                $page = 1;
            }

            $db->select('a.star_num,a.star,a.`desc`');
            $db->orderBy('a.create_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $list = $db->asArray()->all();

            foreach ($list as $key=>$val){
                $list[$key]['star'] = WorkAppraisal::getStarDesc($val['star']);
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }

    }

}