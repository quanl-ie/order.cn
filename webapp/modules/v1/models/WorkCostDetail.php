<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/21
 * Time: 18:36
 */
namespace webapp\modules\v1\models;

use common\models\CostItem;
use common\models\CostLevelItem;
use common\models\CostPayType;
use webapp\models\ModelBase;
use Yii;

class WorkCostDetail extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost_detail';
    }
}