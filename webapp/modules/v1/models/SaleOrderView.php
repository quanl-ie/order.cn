<?php
namespace webapp\modules\v1\models;

use common\helpers\Helper;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use Yii;
use yii\data\Pagination;
use webapp\models\ServiceGoods;

class  SaleOrderView extends SaleOrder
{

    public static function tableName()
    {
        return 'sale_order_view';
    }
      //查询类目，服务类型
      public static function getProdInfo($sale_order_id){
            $where = [
                  "id" =>$sale_order_id
            ];
            $query = self::find()
                  ->where($where)
                  ->select('type_name,class_name,prod_name')
                  ->asArray()
                  ->one();
            return $query;
      }
}