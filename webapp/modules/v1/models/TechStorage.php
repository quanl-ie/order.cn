<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/23
 * Time: 15:49
 */
namespace webapp\modules\v1\models;

use common\models\CostPayType;
use common\models\CostItem;
use common\models\Department;
use common\models\PushMessage;
use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use webapp\modules\v1\models\CostRule;
use webapp\modules\v1\models\SaleOrder;
use webapp\modules\v1\models\AccountAddress;
use Yii;
use yii\helpers\ArrayHelper;

class TechStorage extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'tech_storage';
    }
}