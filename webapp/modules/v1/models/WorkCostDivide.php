<?php
namespace webapp\modules\v1\models;

use common\models\CostItem;
use common\models\CostPayType;
use webapp\models\ModelBase;
use Yii;

class WorkCostDivide extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost_divide';
    }

    /**
     * 获取列表
     * @param $workNo
     * @return array
     */
    public static function getListByWorkNo($workNo)
    {
        $query = self::find()
            ->where(['work_no' => $workNo])
            ->select("id,cost_name,expect_amount,divide_amount,comment,work_no,divide_rules")
            ->orderBy('id asc')
            ->asArray()->all();

        return $query;
    }
}