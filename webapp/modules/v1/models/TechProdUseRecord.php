<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/23
 * Time: 16:40
 */
namespace webapp\modules\v1\models;
use webapp\models\ModelBase;

class TechProdUseRecord extends ModelBase
{

    public static function tableName()
    {
        return 'tech_prod_use_record';
    }
}