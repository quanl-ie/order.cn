<?php
namespace webapp\modules\v1\models;

use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use Yii;

class WorkOrderTrade extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_trade';
    }

    /**
     * 添加工单支付
     */
    public static function savePayInfo($pay_type, $work_no, $totalAmount,$technicianId,$payTime = '')
    {
        $payModel = self::findOne(['work_no' => $work_no]);
        if($payModel)
        {
            //如果是已支付
            if($payModel->trade_status == 1){
                return ModelBase::errorMsg('已支付订单不可以更改支付方式', 20030);
            }

            $payModel->pay_amount     = $totalAmount;
            $payModel->trade_pay_code = $pay_type;
            if($payTime != ''){
                $payModel->pay_time = $payTime;
            }
            $payModel->technician_id = $technicianId;
            if($payModel->save()) {
                return [
                    'id' => $payModel->id
                ];
            }
        }
        else
        {
            $model = new self();
            $model->work_no        = $work_no;
            $model->pay_amount     = $totalAmount;
            $model->trade_status   = 0;
            $model->trade_pay_code = $pay_type;
            $model->technician_id = $technicianId;
            if($payTime != ''){
                $model->pay_time = $payTime;
            }
            if($model->save()) {
                return [
                    'id' => $model->id
                ];
            }
        }
        return ModelBase::errorMsg('操作失败', 20001);
    }

    /**
     * 修改交易数据
     * @author lwy
     */
    public static function editPayInfo($workNo, $trade_no, $trade_status, $pay_time)
    {
        $payModel = self::findOne(['work_no' => $workNo]);
        if($payModel)
        {
            $payModel->trade_no     = $trade_no;
            $payModel->trade_status = $trade_status;
            $payModel->pay_time     = $pay_time;
            if($payModel->save())
            {
                $workModel = Work::findOne(['work_no'=>$workNo]);
                if($workModel)
                {
                    $workModel->status = 4;
                    $workModel->update_time = time();
                    if($workModel->save(false))
                    {
                        //添加技师备件 使用/回收 记录
                        WorkCost::getCostStorage($workNo);
                        //技师完成动态填加
                        $params = [
                            'jsId' => $payModel->technician_id,
                        ];
                        WorkOrderDynamic::add(15,$workModel->order_no,$params,$workNo);

                        //技师排班
                        ScheduleTechnicianQueue::push($workNo,4,date('Y-m-d'), date('Y-m-d',$workModel->plan_time));

                        //完成服务给未评价的单子加上数据
                        WorkAppraisal::add($workModel->order_no,$workModel->work_no,$workModel->update_time,$workModel->department_id);

                        //查一下是否预约了下一个工单
                        $workArr = Work::findOneByAttributes(['order_no'=>$workModel->order_no,'status'=>[1,2,6]]);
                        if(!$workArr){
                            WorkOrderStatus::changeStatus($workModel->department_id,$workModel->order_no,4,$workModel->department_id,'待验收');
                            WorkProcess::add($workNo,$workModel->department_id,0,0,5,'异步回到支付完成');
                        }
                    }
                }

                return [
                    'id' => $payModel->id
                ];
            }
        }
        return ModelBase::errorMsg('操作失败', 20001);
    }

    /**
     * 获取支付方式
     * @param $workNo
     * @return string
     */
    public static function getPaymentMethod($workNo)
    {
        $paymentMethod = '';
        $workOrderTradeArr = WorkOrderTrade::findOneByAttributes(['work_no'=>$workNo,'trade_status'=>1],'trade_pay_code');
        if($workOrderTradeArr)
        {
            if($workOrderTradeArr['trade_pay_code'] == 0){
                $paymentMethod = '微信';
            }
            else if($workOrderTradeArr['trade_pay_code'] == 1){
                $paymentMethod = '支付宝';
            }
            else if($workOrderTradeArr['trade_pay_code'] == 3){
                $paymentMethod = '现金';
            }
        }
        return $paymentMethod;
    }

}