<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/6/25
 * Time: 14:20
 */
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
class WorkOrderTechnician extends ModelBase
{
      public static function getDb()
      {
            return Yii::$app->order_db;
      }

      public static function tableName()
      {
            return 'work_rel_technician';
      }
      //通过工单查询技师
      public static function  getTechnicianByWorkId($id){
            $where = [
                  'work_no'=> $id,
                  'type'   => 1
            ];
            $query = self::find()
                  ->where($where)
                  ->select('work_no,technician_id,is_self,type')
                  ->asArray()
                  ->all();
            return $query;
      }
      //更改推送状态
      public static function changePushStatus($id){
            $attributes = [
                  'push_status'   => 2
            ];
            self::updateAll($attributes,['id'=>$id]);
      }
      //判断是否已指派技师
      public static function getType($work_no){
            $where = [
                  'push_status'=>1,
                  'work_no'=>$work_no,
                  'type'   => 1,
                  'is_self' =>3
            ];
            $query = self::find()
                  ->where($where)
                  ->select('technician_id')
                  ->asArray()
                  ->one();
            return $query;
      }
}