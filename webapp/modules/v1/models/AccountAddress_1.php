<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class AccountAddress extends ModelBase
{

    public static function tableName()
    {
        return 'account_address';
    }
    
    /**
     * 获取订单地址
     * @aauthor li
     * @param  array $addressIds 用户地址ID
     * @date   2018-01-04
     * @return array
     */
    public static function getOrderAddress($addressIds) {
        $address = self::find()->where(['id' => $addressIds])->select(['id','lon','lat','address'])->asArray()->all();
        $addressArr = [];
        foreach ($address as $k => $v) {
            $addressArr[$v['id']] = $v;
        }
        return $addressArr;
    }
}