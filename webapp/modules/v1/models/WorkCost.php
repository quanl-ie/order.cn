<?php
namespace webapp\modules\v1\models;

use common\models\CostItem;
use common\models\CostLevelItem;
use common\models\CostPayType;
use common\models\Department;
use common\models\DictEnum;
use common\models\Product;
use webapp\models\ModelBase;
use Yii;

class WorkCost extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost';
    }

    /**
     * 获取列表
     * @param $workNo
     * @return array
     */
    public static function getList($workNo)
    {
        $query = self::find()
            ->where(['work_no' => $workNo,'status'=>1])
            ->select('id,work_no,payer_id,cost_id,cost_price,cost_number,cost_amount,cost_real_amount,comment,voucher_img,cost_type')
            ->orderBy('id asc')
            ->asArray()->all();
        if($query)
        {
            $totalAmount     = array_sum(array_column($query,'cost_amount'));
            $totalRealAmount = 0;//array_sum(array_column($query,'cost_real_amount'));;
            //获取详细
            $costId = array_column($query,'id');
            $detail = WorkCostDetail::findAllByAttributes(['work_cost_id'=>$costId],'*','work_cost_id');
            foreach ($detail as $k=>$v)
            {
                if($v['prod_unit']) {
                    $detail[$k]['prod_unit_name'] = DictEnum::getDesc('enum_unit_id',$v['prod_unit']);
                }else{
                    $detail[$k]['prod_unit_name'] = '个';
                }
            }
            foreach ($query as $key=>$val)
            {
                if($val['payer_id'] == 3) {
                    $totalRealAmount+=$val['cost_real_amount'];
                }

                if($val['voucher_img'] == ''){
                    $query[$key]['voucher_img'] = '';
                }
                else {
                    $query[$key]['voucher_img'] = explode(',',$val['voucher_img']);
                }
                $query[$key]['cost_price'] = sprintf('%0.2f',round($val['cost_price'],2));
                $query[$key]['cost_amount'] = sprintf('%0.2f',round($val['cost_price']*$val['cost_number'],2));
                $query[$key]['cost_real_amount'] = sprintf('%0.2f',round($val['cost_real_amount'],2));
                if(isset($detail[$val['id']]))
                {
                    $query[$key]['product'][] = $detail[$val['id']];
                }else{
                    $query[$key]['product'] = [];
                }
            }

            return [
                'list' => $query,
                'totalAmount' => $totalAmount,
                'totalRealAmount' => $totalRealAmount
            ];
        }

        return [
            'list' => [],
            'totalAmount' => 0,
            'totalRealAmount' => 0
        ];
    }

    /**
     * 获取列表
     * @param $orderNo
     * @return array
     */
    public static function getListByOrderNo($orderNo,$where)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a' );
        $db->innerJoin([Work::tableName() . ' as b'],'a.work_no = b.work_no');
        $db->where(['a.order_no' => $orderNo,'a.status'=>1]);
        $db->select('a.id,a.order_no,a.work_no,a.payer_id,a.cost_id,a.cost_price,a.cost_number,a.cost_amount,a.cost_real_amount,a.comment,a.voucher_img,b.work_stage');
        $db->orderBy('a.id asc');

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        $query = $db->asArray()->all();

        if($query)
        {
            //总价
            $totalAmount = array_sum(array_column($query,'cost_real_amount'));
            //服务流程
            $workStageIds = array_column($query,'work_stage');
            $workStageArr = ServiceFlow::findAllByAttributes(['id'=>$workStageIds],'id,service_flow_name','id');

            //收费项目
            $costIds = array_unique(array_column($query,'cost_id'));
            $costArr = CostItem::getCostNameByIds($costIds);

            //付费方
            $payTypeArr = CostPayType::getNameRel();

            foreach ($query as $key=>$val)
            {
                if($val['voucher_img'] == ''){
                    $query[$key]['voucher_img'] = [];
                }
                else {
                    $query[$key]['voucher_img'] = explode(',',$val['voucher_img']);
                }

                //服务流程
                $query[$key]['work_stage_desc'] = isset($workStageArr[$val['work_stage']])?$workStageArr[$val['work_stage']]['service_flow_name']:'';
                //收费项目
                $query[$key]['cost_desc'] = isset($costArr[$val['cost_id']])?$costArr[$val['cost_id']]:'';
                //付费方
                $query[$key]['payer_desc'] = isset($payTypeArr[$val['payer_id']])?$payTypeArr[$val['payer_id']]:'';
            }

            return [
                'list' => $query,
                'totalAmount' => $totalAmount,
            ];
        }

        return [
            'list' => [],
            'totalAmount' => 0,
        ];
    }

    /**
     * 获取列表
     * @param $workNos
     * @return array
     */
    public static function getListByWorkNos(array $workNos,$andWhere=[])
    {
        $result = [];

         $db = self::find();
         $db->where(['work_no' => $workNos,'status'=>1]);
         if($andWhere){
             $db->andWhere($andWhere);
         }
         $db->select('id,work_no,payer_id,cost_id,cost_price,cost_number,cost_amount,cost_real_amount,comment,voucher_img');
         $db->orderBy('id asc');
        $query = $db->asArray()->all();

        if($query)
        {
            //收费项目
            $costIds = array_unique(array_column($query,'cost_id'));
            $costArr = CostLevelItem::getCostAttrByIds($costIds);

            //付费方
            $payTypeArr = CostPayType::getNameRel();
            
            foreach ($query as $key=>$val)
            {
                if($val['voucher_img'] == ''){
                    $val['voucher_img'] = [];
                }
                else {
                    $val['voucher_img'] = explode(',',$val['voucher_img']);
                }

                //收费项目
                $val['cost_desc'] = isset($costArr[$val['cost_id']])?$costArr[$val['cost_id']]['cost_item_name']:'';
                $val['cost_unit'] = isset($costArr[$val['cost_id']])?$costArr[$val['cost_id']]['unit']:'';
                //付费方
                $val['payer_desc'] = isset($payTypeArr[$val['payer_id']])?$payTypeArr[$val['payer_id']]:'';
                $val['cost_price'] = sprintf('%.2f', round($val['cost_price'],2));
                $val['cost_amount'] = sprintf('%.2f', round($val['cost_amount'],2));
                $val['cost_real_amount'] = sprintf('%.2f', round($val['cost_real_amount'],2));

                $result[$val['work_no']][] = $val;
            }


        }
        return $result;
    }

    /**
     * 算出用户付费总费用
     * @param $workNos
     * @return array
     */
    public static function getUserPayByWorkNos($workNos)
    {
        $where = [
            'status'   => 1,
            'work_no'  => $workNos,
            'payer_id' => 3
        ];
        $query = self::find()
            ->where($where)
            ->select("work_no,sum(cost_real_amount) as amount")
            ->groupBy('work_no')
            ->asArray()
            ->all();

        if($query)
        {
            return array_column($query,'amount','work_no');
        }

        return [];
    }

    /**
     * 统计付款方总金额
     * @param $workNos
     * @param int $payerId
     * @return array
     */
    public static function totalAmountByWorkNos($workNos,$payerId=1)
    {
        $where = [
            'status'   => 1,
            'order_no' => $workNos,
            'payer_id' => $payerId
        ];
        $query = self::find()
            ->where($where)
            ->select("order_no,sum(cost_real_amount) as amount")
            ->groupBy('order_no')
            ->asArray()
            ->all();

        if($query)
        {
            return array_column($query,'amount','order_no');
        }

        return [];
    }
    //获取技师使用/回收备件 记录
    public static function getCostStorage($workNo)
    {
        $db = self::find()
            ->from(self::tableName().' as a')
            ->leftJoin(WorkCostDetail::tableName().' as b','a.id = b.work_cost_id')
            ->where(['a.work_no'=>$workNo,'a.status'=>1,'a.cost_type'=>2])
            ->select("a.process_user_id,a.work_no,a.order_no,b.prod_id,b.prod_num,b.cost_price,b.recycle_prod_id")
            ->asArray()->all();
        if($db)
        {
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                foreach ($db as $v)
                {
                    //查询产品信息
                    $prod = Product::findOne(['id' => $v['prod_id']]);
                    //查询技师信息
                    $tecArr = Technician::findOne(['id' => $v['process_user_id']]);
                    //查询部门信息
                    $department = Department::findOne(['id' => $tecArr->store_id]);
                    $model = new TechProdUseRecord();
                    $model->direct_company_id = $department->direct_company_id;
                    $model->department_top_id = $department->parent_id;
                    $model->tech_id         = $v['process_user_id'];
                    $model->prod_id         = $v['prod_id'];
                    $model->used_num        = $v['prod_num'];
                    $model->create_time     = date("Y-m-d H:i:s");
                    $model->price           = $prod->sale_price;
                    $model->sale_price      = $prod->sale_price * $v['prod_num'];
                    $model->amount          = $v['cost_price'];
                    $model->total_amount   = $v['cost_price'] * $v['prod_num'];
                    $model->work_no         = $v['work_no'];
                    $model->order_no        = $v['order_no'];
                    if ($model->save(false))
                    {
                        $storage = TechStorage::findOne(['prod_id' => $v['prod_id'], 'tech_id' => $v['process_user_id'],'type'=>1]);
                        if ($storage)
                        {
                            //修改良品数量
                            $storage->num = $storage->num - $v['prod_num'];
                            $storage->total_num = $storage->total_num - $v['prod_num'];
                            $storage->freeze_num = $storage->freeze_num - $v['prod_num'];
                            $storage->save(false);
                            //添加回收件(非良品)
                            if($v['recycle_prod_id'])
                            {
                                $storageBad = TechStorage::findOne(['prod_id' => $v['recycle_prod_id'], 'tech_id' => $v['process_user_id'],'type'=>0]);
                                if(isset($storageBad->id) && $storageBad->id)
                                {
                                    $storageBad->total_num = $storageBad->total_num+$v['prod_num'];
                                    $storageBad->num = $storageBad->num+$v['prod_num'];
                                    $storageBad->save();
                                }else{
                                    $storageModel = new TechStorage();
                                    $storageModel->direct_company_id = $department->direct_company_id;;
                                    $storageModel->department_top_id = $department->parent_id;
                                    $storageModel->tech_id            = $v['process_user_id'];
                                    $storageModel->prod_id            = $v['recycle_prod_id'];
                                    $storageModel->total_num          = $v['prod_num'];
                                    $storageModel->num                 = $v['prod_num'];
                                    $storageModel->type                = 0;
                                    $storageModel->time_stamp         = date("Y-m-d H:i:s",time());
                                    $storageModel->save();
                                }
                            }
                        }
                    }
                }
                $transaction->commit();
                return true;
            }catch (\Exception $e)
            {
                //echo $e->getMessage();exit;
                $transaction->rollBack();
                return false;
            }
        }
        return false;
    }
}