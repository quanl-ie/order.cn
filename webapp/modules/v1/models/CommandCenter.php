<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class CommandCenter extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }
    public static function tableName()
    {
        return 'command_center';
    }
}