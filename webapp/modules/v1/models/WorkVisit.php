<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class WorkVisit extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_visit';
    }

    /**
     * 回访
     * @param $orderNo
     * @param $workNo
     * @param $srcType
     * @param $srcId
     * @param $content
     * @param $userId
     * @return array
     */
    public static function visit($orderNo,$workNo,$srcType,$srcId,$content,$userId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();

        try
        {
            $model = new self();
            $model->src_type    = $srcType;
            $model->src_id      = $srcId;
            $model->user_id     = $userId;
            $model->order_no    = $orderNo;
            $model->work_no     = $workNo;
            $model->content     = $content;
            $model->status      = 1;
            $model->create_time = time();
            $model->update_time = time();
            if($model->save()){
                $res = Work::updateAll(['visit_status'=>1,'update_time'=>time()],['work_no'=>$workNo]);
                if($res)
                {
                    $workArr = Work::findOneByAttributes(['work_no'=>$workNo]);
                    //动态填加
                    $params = [
                        'accountId' => $userId,
                        'fwsId'     => $srcId,
                        'reason'    => $content,
                        'workStage' => isset($workArr['work_stage'])?$workArr['work_stage']:0
                    ];
                    WorkOrderDynamic::add(WorkOrderDynamic::TYPE_FWS,20,$orderNo,$params,$workNo);

                    $transaction->commit();
                    return [
                        'order_no' => $orderNo,
                        'work_no'  => $workNo
                    ];
                }
                else {
                    $transaction->rollBack();
                    return ModelBase::errorMsg('回访失败',20011);
                }
            }
            else {
                $transaction->rollBack();
                return ModelBase::errorMsg('回访失败',20010);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('回访失败',20009);
        }
        $transaction->rollBack();
        return ModelBase::errorMsg('回访失败',20008);
    }
}