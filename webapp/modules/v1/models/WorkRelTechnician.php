<?php
namespace webapp\modules\v1\models;

use common\models\Common;
use webapp\models\ModelBase;
use Yii;

class WorkRelTechnician extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_rel_technician';
    }

    /**
     * 工单指派技师
     * @param $technicianId
     * @return bool
     */
    public static function add($workNo,$technicianId,$isSelf,$opUserType,$opUserId)
    {
        if($technicianId>0 && $workNo!='')
        {
            $model = new self();
            $model->work_no       = $workNo;
            $model->technician_id = $technicianId;
            $model->is_self       = $isSelf;
            $model->create_time   = time();
            $model->update_time   = time();
            $model->op_user_type  = $opUserType;
            $model->op_user_id    = $opUserId;
            return $model->save();
        }
        return false;
    }

    /**
     * 改派
     * @param $workNo
     * @param $technicianId
     * @return bool
     */
    public static function reAssign($workNo,$technicianId)
    {
        self::updateAll(['type'=>2,'update_time'=>time()],['work_no'=>$workNo]);
        return self::add($workNo,$technicianId);
    }

    /**
     * 根据工单信息查技师id
     * @param array $workNos
     * @return array
     */
    public static function findTechnicianIdsByWorkNos(array $workNos,$andWhere = '1=1')
    {
        $query = self::find()
            ->where(['work_no'=>$workNos,'type'=>1])
            ->andWhere($andWhere)
            ->select('technician_id,work_no')
            ->asArray()->all();
        if($query)
        {
            $result = [];
            foreach ($query as $val){
                $result[$val['work_no']][] = $val['technician_id'];
            }
            return $result;
        }
        return [];
    }

    /**
     * 根据工单号查出技师组长跟负责人
     * @param array $workNos
     * @return array
     */
    public static function findTechnicianLeader(array $workNos)
    {
        $query = self::find()
            ->where(['work_no'=>$workNos,'type'=>1])
            ->select('technician_id,work_no,is_self')
            ->asArray()->all();
        if($query)
        {
            $result = [];
            foreach ($query as $val){
                $result[$val['work_no']][$val['is_self']] = $val;
            }
            return $result;
        }
        return [];
    }

    /**
     * 根据工单号查出技师并排序
     * @param $workNo
     * @return array
     */
    public static function findTechnicianIdsByWorkNo($workNo)
    {
        $query = self::find()
            ->where(['work_no'=>$workNo,'type'=>1])
            ->select('technician_id,is_self,work_no')
            ->orderBy('`is_self`<>2 and `is_self`<>1')
            ->asArray()->all();
        if($query)
        {
            $result = [];
            foreach ($query as $val){
                $result[$val['work_no']][] = $val;
            }
            return $result;
        }
        return [];
    }

    /**
     * 查出技师示完成工单数及总数，用于工单指派改派
     * @param $technicianIds
     * @return array
     * @author xi
     */
    public static function getTechnicianWorkTongji($technicianIds,$plantime)
    {
        $plantime = strtotime(date('Y-m-d',strtotime($plantime)));
        $result = [];
        $where = [
            'b.technician_id' => $technicianIds,
            'b.type' => 1
        ];
        $query = self::find()
            ->from(Work::tableName() . ' as a ')
            ->innerJoin(['`'. self::tableName() .'` as b'],'a.work_no = b.work_no')
            ->where($where)
            ->andWhere(['>','a.status',1])
            ->andWhere(['>','a.plan_time', $plantime])
            ->andWhere(['<','a.plan_time', $plantime+86400])
            ->select('b.technician_id,a.status')
            ->groupBy('b.work_no')
            ->asArray()
            ->all();

        if($query)
        {
            foreach ($query as $val)
            {
                if(!isset($result[$val['technician_id']]['finsh'])){
                    $result[$val['technician_id']]['finsh'] = 0;
                }
                if($val['status']==4){
                    $result[$val['technician_id']]['finsh']+=1;
                }
                if(!isset($result[$val['technician_id']]['total'])){
                    $result[$val['technician_id']]['total'] = 0;
                }
                if($val['status']!=6){
                    $result[$val['technician_id']]['total']+=1;
                }
            }
        }
        return $result;
    }

    /**
     * 检查技师与否与服务时间有冲突
     * @param $technicianId
     * @param $plantime
     * @return array
     */
    public static function hasPlantimeConflict($technicianId ,$plantime,$workNo)
    {
        $where = [
            'b.technician_id' => $technicianId,
            'b.type'          => 1,
            'a.status'        => [2,3,5]
        ];
        $query = self::find()
            ->from(Work::tableName() . ' as a')
            ->innerJoin(['`'. self::tableName() .'` as b'],'a.work_no = b.work_no')
            ->where($where)
            ->andWhere(['<>','a.work_no',$workNo])
            ->andWhere(['>','a.plan_time',$plantime-1800])
            ->andWhere(['<','a.plan_time',$plantime+1800])
            ->count();
        return [
            'conflict' => $query >0?1:0
        ];
    }

    /**
     * 技师未完成工单数
     * @param $technicianId
     * @return int|string
     * @author xi
     * @date 2018-6-30
     */
    public static function technicianNoFinshWork($technicianId)
    {
        $count = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.Work::tableName().'` as b'],'a.work_no = b.work_no')
            ->where("a.technician_id = $technicianId and a.type = 1 and b.`status`<>4")
            ->count();

        return $count;
    }

    /**
     * 填加组长
     * @param $workNo
     * @param $technicianId
     * @param $opUserType
     * @param $opUserId
     */
    public static function addLeaderTechnician($workNo,$technicianId,$opUserType,$opUserId)
    {
        //查出技师的组长
        $leaderTechnicianId = Common::getGroupingLeaderByTechnicianId($technicianId);
        if($leaderTechnicianId > 0)
        {
            //查一下工单是否有指派了组长
            $query = self::findOneByAttributes(['work_no'=>$workNo,'is_self'=>2,'type'=>1]);
            if(!$query){
                self::add($workNo,$leaderTechnicianId,2,$opUserType,$opUserId);
            }
        }
    }

    public static function noFinish($technicianId,$workNos,$status)
    {
        $where = [
            'b.work_no'       => $workNos,
            'b.status'        => $status,
            'b.cancel_status' => 1,
            'a.technician_id' => $technicianId,
            'a.type'          => 1,
            'a.is_self'       => 1

        ];
        $count = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.Work::tableName().'` as b'], 'a.work_no = b.work_no')
            ->where($where)
            ->count();

        return $count > 0;
    }
}