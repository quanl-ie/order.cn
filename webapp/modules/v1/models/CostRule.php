<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class CostRule extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'cost_rule';
    }


    /*
     * 获取服务商的合作商家规则
     * @param $sj_id  商家id
     * @sp_id 服务商id
     * */
    public static function getRuleInfoBySj($sj_id,$sp_id)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a' );
        $db->innerJoin(['cost_rule_relation  as b'],'b.cost_rule_id = a.id');
        $db->innerJoin(['cost_rule_detail as c'],'c.cost_rule_id = a.id');
        $db->innerJoin(['cost_rule_item as d'],'d.cost_rule_id = a.id');
        $db->where(['b.service_provider_id'=>$sp_id,'b.src_id'=>$sj_id,'a.status'=>1]);
        $db->select('a.service_area as service_area,a.real_cost_date  as cost_date,c.brand_id,c.class_id,c.type_id,d.cost_item_id,d.cost_type,d.fixed_amount,d.pecent');
        //$sql = $db->createCommand()->getRawSql();
        //print_r($sql);exit;
        $query = $db->asArray()->all();
        return $query;
    }

}