<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v1\models\Work;
use Yii;

class WorkOrderAssign extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_assign';
    }

    /**
     * 指配机构
     * @param $orderNo
     * @param $serviceProvideId
     * @return bool
     */
    public static function add($orderNo,$type,$departmentId,$serviceDepartmentId,$userId,$assignType)
    {
        $fwsAssignModel = new WorkOrderAssign();
        $fwsAssignModel->order_no    = $orderNo;
        $fwsAssignModel->type        = $type;
        $fwsAssignModel->user_id     = $userId;
        $fwsAssignModel->reason      = '';
        $fwsAssignModel->create_time = time();
        $fwsAssignModel->update_time = time();
        $fwsAssignModel->assign_type = $assignType;
        $fwsAssignModel->department_id         = $departmentId;
        $fwsAssignModel->service_department_id = $serviceDepartmentId;
        if($fwsAssignModel->save()){
            return true;
        }
        throw new \Exception(self::className() ."保存失败",30002);
    }

    /**
     * 驳回
     * @param $orderNo
     * @param $serviceDepartmentId
     * @param $processUserId
     * @param $rejectReason
     * @return bool
     * @throws \Exception
     * @author xi
     */
    public static function reject($orderNo,$serviceDepartmentId,$processUserId,$rejectReason)
    {
        $where = [
            'order_no' => $orderNo,
            'type'     => 1,
            'service_department_id' => $serviceDepartmentId,
        ];
        $model = self::findOne($where);
        if($model)
        {
            $model->type = 4;
            $model->user_id = $processUserId;
            $model->reason  = $rejectReason;
            $model->update_time = time();
            if($model->save()){
                return true;
            }
        }
        throw new \Exception(self::className() ."操作失败",30002);
    }

    /**
     * 改派机构
     * @param $orderNo
     * @param $reason
     * @param $userId
     * @return array
     * @author xi
     * @date 2018-1-31
     */
    public static function reAssignFws($orderNo,$departmentId,$serviceDepartmentId,$reason,$userId,$orderSourceId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();

        try
        {
            $res = self::find()->where(['order_no'=>$orderNo,'department_id'=>$orderSourceId,'type'=>1])->orderBy('id desc')->asArray()->one();


            if($res)
            {
                if($res['service_department_id']==$serviceDepartmentId)
                {
                    return ModelBase::errorMsg('改派时不可指向同一机构',20009);
                }
                $attr = ['reason'=>$reason,'type'=>2,'update_time'=>time()];
                self::updateAll($attr,['id'=>$res['id']]);

            }

            if($serviceDepartmentId > 0)
            {
                if(self::assignInstitution($orderNo,$departmentId,$serviceDepartmentId,$userId,1,$orderSourceId,false))
                {
                    //把指类型改成机构指派
                    self::updateAll(['assign_type'=>1],['order_no'=>$orderNo,'assign_type'=>2]);

                    $transaction->commit();
                    //改派机构时，第一次指派技师的信息删除
                    $workInfo = Work::findOne(['order_no'=>$orderNo,'is_first'=>1]);
                    $workInfo->status = 1;
                    $workInfo->save(false);
                    if($workInfo){
                        WorkRelTechnician::deleteAll(['work_no'=>$workInfo->work_no]);
                    }

                    //添加改派订单给机构动态填加
                    $params = [
                        'accountId' => $userId,
                        'departmentId' => $departmentId,
                        'fwsId'      => $serviceDepartmentId,
                        'reason' => $reason
                    ];

                    WorkOrderDynamic::add(7,$orderNo,$params);

                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
        }
        $transaction->rollBack();
        return ModelBase::errorMsg('改派失败',20009);
    }

    /**
     * 指派机构
     * @param $orderNo
     * @param $departmentId
     * @param $serviceDepartmentId
     * @param $userId
     * @return array
     * @author xi
     */
    public static function assignInstitution($orderNo,$departmentId,$serviceDepartmentId,$userId,$assignType,$orderSourceId,$hasAddDynamic = true)
    {
        $transaction = Yii::$app->order_db->beginTransaction();

        try
        {
            //如果以前是指派技师，把他改成1
            self::updateAll(['assign_type'=>1,'update_time'=>time()],['order_no'=>$orderNo,'assign_type'=>2]);

            //更新之前的指派机构状态为2
            self::updateAll(['type'=>2,'update_time'=>time()],['order_no'=>$orderNo,'type'=>1]);

            $res = self::add($orderNo,1,$departmentId,$serviceDepartmentId,$userId,$assignType);
            if($res)
            {
                $transaction->commit();
                //指派机构时，第一次指派技师的信息删除
                $workInfo = Work::findOne(['order_no'=>$orderNo,'status'=>2]);
                if($workInfo){
                    $workInfo->status = 1;
                    $workInfo->save(false);
                    WorkRelTechnician::deleteAll(['work_no'=>$workInfo->work_no]);
                }

                //添加指派机构的动态
                if($hasAddDynamic == true)
                {
                    $params = [
                        'accountId' => $userId,
                        'jgId'     => $serviceDepartmentId
                    ];
                    WorkOrderDynamic::add(4,$orderNo,$params);
                }


                //如果改派机构，上次的机构为取消状态，本次的指派的机构为待服务状态

                //查看订单
                $orderModel = WorkOrder::findOne(['order_no'=>$orderNo]);

                //如果指派的机构为原下单机构,改派机构时之前指派的机构订单为取消状态
                if($orderModel->department_id==$departmentId)
                {
                    //查询最新的一条数据，不管操作机构是否是下单人
                    $orderStatus = WorkOrderStatus::find()->where(['order_no'=>$orderNo])->orderBy(['id'=> SORT_DESC])->one();
                    if($orderStatus)
                    {
                        if($orderStatus->department_id != $serviceDepartmentId)
                        {
                            WorkOrderStatus::changeStatus($orderStatus->department_id,$orderNo,6,$departmentId);
                            WorkOrderStatus::changeStatus($serviceDepartmentId,$orderNo,1,$departmentId);
                        }
                        else{
                            WorkOrderStatus::changeStatus($serviceDepartmentId,$orderNo,1,$departmentId,'');
                        }
                        $workOrderAssign = WorkOrderAssign::findOne(['order_no'=>$orderNo,'type'=>1,'service_department_id'=>$serviceDepartmentId]);
                        if($workOrderAssign)
                        {
                            $workOrderAssign->type        = 1;
                            $workOrderAssign->assign_type = 1;
                            $workOrderAssign->save(false);
                        }
                    }
                    else
                    {
                        WorkOrderStatus::changeStatus($serviceDepartmentId,$orderNo,1,$departmentId,'');
                    }
                    WorkOrderStatus::changeStatus($departmentId,$orderNo,2,$departmentId,'');


                }
                else
                {
                    //其他机构时订单改成待服务
                    WorkOrderStatus::changeStatus($departmentId,$orderNo,2,$userId);
                    WorkOrderStatus::add($orderNo,$serviceDepartmentId,1,$departmentId);
                }
                
                return [
                    'order_no' => $orderNo
                ];
            }
            else
            {
                $transaction->rollBack();
                return ModelBase::errorMsg('指派失败',20010);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('指派失败'.$e->getMessage(),20011);
        }
        return ModelBase::errorMsg('指派失败',20009);
    }

    /**
     * 更改 assign_type 为指派技师
     * @param $orderNo
     * @param $serviceDepartmentId
     * @return bool
     * @throws \Exception
     */
    public static function changeAssignType($orderNo,$serviceDepartmentId,$opUserType = 13,$userId = 0)
    {
        $model = self::findOne(['order_no'=>$orderNo,'service_department_id'=>$serviceDepartmentId,'type'=>[1,3]]);
        if($model)
        {
            $model->assign_type = 2;
            $model->update_time = time();
            if($model->save(false)){
                return true;
            }
        }
        else if($opUserType == 13)
        {
            if(self::add($orderNo,1,$serviceDepartmentId,$serviceDepartmentId,$userId,2)){
                return true;
            }
        }

        throw new \Exception('Change assign_type failure!',30002);
    }

    /**
     * 查出服务机构
     * @param $orderNo
     * @return int|mixed
     * @author xi
     * @date 2018-6-30
     */
    public static function getServiceDepartmentId($orderNos)
    {
        $query = self::find()
            ->where(['order_no'=>$orderNos,'type'=>[1,3]])
            ->orderBy('id desc')
            ->select('service_department_id')
            ->asArray()
            ->one();

        if($query){
            return $query['service_department_id'];
        }
        return 0;
    }

    /**
     * 把其他指派改成改派
     * @param $orderNo
     * @param $serviceDepartmentId
     */
    public static function reAssignChangeType($orderNo,$serviceDepartmentId)
    {
        $attr = [
            'type'        => 2,
            'update_time' => time()
        ];
        $where = "order_no = '$orderNo' and service_department_id<>$serviceDepartmentId and type = 1";
        self::updateAll($attr,$where);
    }

    /**
     * 查出订单下所有服务机构id
     * @param $orderNos
     * @return array
     * @author xi
     * @date 2018-7-5
     */
    public static function getAllServiceDepartmentIds($orderNos)
    {
        $result = [];
        $where = [
            'order_no' => $orderNos,
            'type'     => [1,3]
        ];
        $query = self::find()
            ->where($where)
            ->select('order_no,service_department_id')
            ->asArray()->all();

        if($query)
        {
            foreach ($query as $val){
                $result[$val['order_no']][] = $val['service_department_id'];
            }
        }

        return $result;
    }
}