<?php
namespace webapp\modules\v1\models;

use Yii;
use webapp\models\ModelBase;
use common\models\Region;

class WorkOrderXcx extends WorkOrder
{

    /**
     * 订单状态信息 (1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getStatus($index=false)
    {
        $data = [
            1 => '待接单',
            2 => '待接单',
            3 => '待服务',
            4 => '服务中',
            5 => '已完成',
            6 => '已取消',
            7 => '待验收',
            8 => '待审核',
            9 => '审核未通过'
        ];
        if($index!=false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }

    /**
     * 订单列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(["`".WorkOrderDetail::tableName() . '` as b '],' a.order_no = b.order_no');
        $db->groupBy('a.order_no');


        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('a.order_no,a.plan_time,a.plan_time_type,a.work_type,a.status ,b.sale_order_id');

            $db->orderBy('a.plan_time asc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            foreach ($list as $key=>$val)
            {
                if($val['status'] == 7){
                    $list[$key]['status'] = 4;
                }
                $list[$key]['status_desc'] = self::getStatus($list[$key]['status']);
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 取消订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $cancelReason 取消原因
     * @author xi
     * @date 2017-12-15
     */
    public static function cancelOrder($orderNo,$accountId,$cancelReason,$cancelStatus)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no'   => $orderNo,
                'account_id' => $accountId,
                'status'     => [1,2,3,8]
            ];
            $model = self::findOne($where);
            if($model){
                $model->status        = 6;
                $model->cancel_status = $cancelStatus;
                $model->update_time   = time();
                if($model->save(false))
                {
                    //任务大厅删除
                    WorkOrderTaskHall::updateAll(['status'=>0],['order_no'=> $orderNo]);

                    $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo]);
                    foreach($workDetailArr as $val){
                        WorkOrderProcess::add(ModelBase::SRC_YH,$accountId,$orderNo,$val['id'], 22 ,$accountId,0,0,$cancelReason,$model->amount,'');
                    }

                    //填加动态
                    $params = [
                        'accountId' => $accountId,
                        'sjId'      => $model->src_id,
                        'reason'    => $cancelReason,
                    ];
                    WorkOrderDynamic::add(WorkOrderDynamic::TYPE_SJ,19,$orderNo,$params);

                    //发消息
                    Message::send(22,$orderNo);

                    $transaction->commit();
                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
            else {
                return ModelBase::errorMsg('未找到要取消的订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage().$e->getTraceAsString(), 20006);
        }
    }

    /**
     * 订单详情
     * @param $orderNo 订单号
     * @param $srcType 查询端
     * @param $src_id 查询端id(如，商家id)
     * @param $accountId 用户id
     * @return array|null|\yii\db\ActiveRecord
     * @author xi
     * @date 2018-03-21
     */
    public static function getOrderDetail($orderNo,$srcType,$src_id,$accountId)
    {
        $where = [
            'order_no'   => $orderNo,
            'src_type'   => $srcType,
            'src_id'     => $src_id,
            'account_id' => $accountId,
            'del_status' => 1
        ];

        $query = self::find()
            ->where($where)
            ->asArray()
            ->one();

        if($query)
        {
            $select = 'id,sale_order_id,problem_info,fault_img,status';
            $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],$select);
            if($workDetailArr){
                foreach ($workDetailArr as $key=>$val){
                    if($val['fault_img']!=''){
                        $workDetailArr[$key]['fault_img_arr'] = explode(',',$val['fault_img']);
                    }
                    else {
                        $workDetailArr[$key]['fault_img_arr'] = [];
                    }
                }
                $query['workDetail'] = $workDetailArr;
            }
            else{
                $query['workDetail'] = [];
            }

            if(trim($query['scope_img'])!=''){
                $query['scope_img'] = explode(',',$query['scope_img']);
            }else {
                $query['scope_img'] = [];
            }

            if($query['status'] == 7){
                $query['status'] = 5;
            }
            //状态中文描述
            $query['status_desc'] = self::getStatus($query['status']);

            //订单取消
            if($query['status'] == 6){
                $processArr = WorkOrderProcess::getCancelOneData($query['order_no']);
                if($processArr){
                    $query['cancel_reason'] = $processArr['problem_reason'];
                }
                else {
                    $query['cancel_reason'] = '';
                }
            }
            //审核未通过原因
            else if($query['status'] == 9){
                $query['audit_faild_reason'] = WorkOrderAudit::getAuditFailedReason($query['order_no']);
            }
            //服务流程（工单流程，只有待服务，服务中，已完成才有）
            else if(in_array($query['status'],[3,4,5]))
            {
                $query['work_list'] = WorkXcx::getListByOrderNo($orderNo);
            }
        }
        return $query;
    }

    /**
     * 服务记录及用户收费项目
     * @param $workNo
     * @param $accountId
     * @return array
     * @author xi
     */
    public static function serviceLogAndCost($workNo,$accountId)
    {
        $where = [
            'work_no' => $workNo,
        ];
        $select = 'work_no,order_no,status,plan_time,work_img,scene_img,service_record,reason,technician_id';
        $query = Work::findOneByAttributes($where,$select);
        if($query)
        {
            //查出订单
            $where = [
                'order_no'   => $query['order_no'],
                'account_id' => $accountId
            ];
            $workOrderArr = WorkOrder::findOneByAttributes($where,'is_scope,scope_img');
            if($workOrderArr)
            {
                //查出产品记录
                $productArr = WorkProduct::findAllByWorkNos($workNo);
                //查出收费项目(只查用户付费)
                $costArr = WorkCost::getListByWorkNos([$workNo],['=','payer_id',3]);
                //开始时间
                $startTime = WorkProcess::getTypeTimeData($workNo,4);
                $startTime = isset($startTime[$workNo])?date('Y-m-d H:i',$startTime[$workNo]):'';
                //完成时间
                $finshTime = WorkProcess::getTypeTimeData($workNo,5);
                $finshTime = isset($finshTime[$workNo])?date('Y-m-d H:i',$finshTime[$workNo]):'';

                $totalCostAmount = 0;
                $costArr = isset($costArr[$workNo])?$costArr[$workNo]:[];
                if($costArr){
                    $totalCostAmount = array_sum(array_column($costArr,'cost_amount'));
                }

                return [
                    'work_no'        => $query['work_no'],
                    'order_no'       => $query['order_no'],
                    'status'         => $query['status'],
                    'plan_time'      => date('Y-m-d H:i',$query['plan_time']),
                    'start_time'     => $startTime,
                    'finsh_time'     => $finshTime,
                    'reason'         => $query['reason'],
                    'work_img'       => $query['work_img']!=''?explode(',',$query['work_img']) :[],
                    'scene_img'      => $query['scene_img']!=''?explode(',',$query['scene_img']) :[],
                    'is_scope'       => $workOrderArr['is_scope'] == 1?'保内':'保外',
                    'scope_img'      => $workOrderArr['scope_img']!=''?array_filter(explode(',',$workOrderArr['scope_img'])):[],
                    'service_record' => $query['service_record'],
                    'technician_id'  => $query['technician_id'],
                    'productArr'     => isset($productArr[$workNo])?$productArr[$workNo]:[],
                    'costArr'        => [
                        'list' => $costArr,
                        'totalAmount' => $totalCostAmount
                    ]
                ];
            }
        }

        return [];
    }

    /**
     * 删除订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @author xi
     * @date 2018-03-22
     */
    public static function del($orderNo,$accountId)
    {
        $where = [
            'order_no'   => $orderNo,
            'account_id' => $accountId,
            'status'     => [5,6,9],
            'del_status' => 1
        ];
        $model = self::findOne($where);
        if($model){
            $model->del_status = 2;
            $model->update_time   = time();
            if($model->save(false))
            {
                return [
                    'order_no' => $orderNo
                ];
            }
        }
        else {
            return ModelBase::errorMsg('未找到要删除的订单数据',20004);
        }
    }


    /**
     * 根据订单号查询订单信息
     * @param $orderNo  订单号
     *
     */
    public static function getOrderData($orderNo) {
        $orderArr = WorkOrder::find()->where(['order_no' => $orderNo])->asArray()->one();
        if($orderArr) {

            return [
                'plan_time'  => $orderArr['plan_time'],
                'address_id' => $orderArr['address_id'],
                'src_type'   => $orderArr['src_type'],
                'src_id'     => $orderArr['src_id']
            ];
        }
        return ModelBase::errorMsg('未找到要删除的订单数据',20004);
    }
}