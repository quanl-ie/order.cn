<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class CommandCenterStatistics extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }
    public static function tableName()
    {
        return 'command_center_statistics';
    }

    //首页数据
    public static function homeData($addressIds)
    {
        $db = self::find();
        $db->where(['account_id' => $addressIds]);
        $db->select("sum(order_total_num) as order_total,sum(work_total_num) as work_total,sum(work_finish_num) as fnum,sum(work_cancel_num) as cancel_num,sum(work_abnormal_num) as abn,sum(work_finish_abnormal_num) as abn_finish,sum(work_in_server_num) as in_server,sum(work_wait_appraise_num) as wait_pj,sum(spare_parts_consumption_num) as spare");
        $list = $db->asArray()->all();
        $yc['max_value'] = intval($list[0]['work_total']) - intval($list[0]['fnum']) - intval($list[0]['cancel_num']);
        $yc['min_value'] = 0;
        $yc['yAxis']['value'] = intval($list[0]['abn']-$list[0]['abn_finish']);
        $yc['yAxis']['name'] = "服务异常";

        $ins['max_value'] = intval($list[0]['work_total']) - intval($list[0]['fnum']) - intval($list[0]['cancel_num']);
        $ins['min_value'] = 0;
        $ins['yAxis']['value'] = intval($list[0]['in_server']);
        $ins['yAxis']['name'] = "服务中";

        $wap['max_value'] = intval($list[0]['fnum']);
        $wap['min_value'] = 0;
        $wap['yAxis']['value'] = intval($list[0]['wait_pj']);
        $wap['yAxis']['name'] = "待评价";

        $data['abnormal'] = $yc;
        $data['ins'] = $ins;
        $data['wap'] = $wap;
        $data['store'] = count($addressIds);
        $data['abnormal_work'] = $list[0]['abn']-$list[0]['abn_finish'];
        $data['order_date'] = intval($list[0]['order_total']);
        $data['order_store'] = intval($list[0]['order_total']);
        $data['spare_reduce'] = intval($list[0]['spare']);
        return $data;
    }
    //门店分布
    public static function storeMap($addressIds)
    {
        $db = self::find();
        $db->where(['account_id' => $addressIds,'date'=>date("Y-m-d",time())]);
        $db->select("account_id,work_finish_num");
        $today = $db->asArray()->all();
        $data = array_column($today,'work_finish_num','account_id');

        $db2 = self::find();
        $db2->where(['account_id' => $addressIds]);
        $db2->select("account_id,sum(work_total_num) as work_total,sum(work_finish_num) as fnum,sum(work_cancel_num) as cancel_num");
        $db2->groupBy("account_id");
        $history = $db2->asArray()->all();
        foreach ($history as $k=>$v)
        {
            $new[$v['account_id']]['unfinish_num']= intval($v['work_total'])-intval($v['fnum'])-intval($v['cancel_num']);
        }
        foreach($new as $key => $item) {
            $exist = array_key_exists($key, $data);
            $t = $exist ? $data[$key] : 0;
            $new[$key]['today_finish_num'] = $t;
            $new[$key]['unfinish_num'] = $t+ $new[$key]['unfinish_num'];
        }
        return $new;
    }
    //派单概览
    public static function orderDate($addressIds,$time,$type=1)
    {
        $db = self::find();
        $db->select("sum(order_total_num) as order_total_num,sum(order_finish_num) as order_finish_num,sum(order_cancel_num) as order_cancel_num,date,account_id");
        $db->where(['account_id' => $addressIds]);
        if(is_array($time)){
            $db->andWhere(['>=','date',$time[0]]);
            $db->andWhere(['<=','date',$time[1]]);
        }else{
            $db->andWhere(['date'=>$time]);
        }
        if($type == 2){
            $db->groupBy('account_id');
            $db->orderBy('order_total_num desc');
        }else{
            $db->groupBy('date');
        }
        $list = $db->asArray()->all();
        $data = [];
        if($list)
        {
            $data['legend']= ['报修数','完成量','已取消'];
            foreach ($list as $k=>$v)
            {
                if($v['order_total_num'] == 0 && $v['order_finish_num'] ==0 && $v['order_cancel_num'] ==0)
                {
                    continue;
                }
                if($type == 2){
                    $data['xAxis'][] = $v['account_id'];
                }else{
                    $data['xAxis'][] = date("m-d",strtotime($v['date']));
                }
                $order_num[] = $v['order_total_num'];
                $finish_num[] = $v['order_finish_num'];
                $cancel_num[] = $v['order_cancel_num'];
            }
            $data['yAxis'] = array($order_num,$finish_num,$cancel_num);
        }
        return $data;
    }
    //异常工单
    public static function abnormalWork($addressIds,$time)
    {
        $db = self::find();
        $db->select("sum(work_abnormal_num) as abn,sum(work_finish_abnormal_num) as f_abn");
        $db->where(['account_id' => $addressIds]);
        if(is_array($time)){
            $db->andWhere(['>=','date',$time[0]]);
            $db->andWhere(['<=','date',$time[1]]);
        }else{
            $db->andWhere(['date'=>$time]);
        }
        $list = $db->asArray()->one();
        $data= [];
        if($list)
        {
            $data[] = [
                'value'=>$list['abn']-$list['f_abn'],
                'pix_value'=> round(($list['abn']-$list['f_abn'])/$list['abn']*100),
                'name'=>'未处理'
            ];
            $data[] = [
                'value'=>$list['f_abn'],
                'pix_value'=> round($list['f_abn']/$list['abn']*100),
                'name'=>'已处理'
            ];
        }
        return $data;
    }
}