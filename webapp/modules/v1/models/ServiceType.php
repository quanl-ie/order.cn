<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use webapp\modules\v1\models\SaleOrder;

class ServiceType extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->o2odb;
    }

    public static function tableName()
    {
        return 'tp_service_type';
    }
    
    public static function getServiceId($id) {
        $service_type = self::find()->where(['id' => $id])->asArray()->one();
        return isset($service_type['id']) ? $service_type['id'] : '';
    }
    
    /**
     * 获取订单服务类型
     */
    public static function getServiceTitle($id) {
        $service_type = self::find()->where(['id' => $id])->asArray()->one();
        return isset($service_type['title']) ? $service_type['title'] : '';
    }
}