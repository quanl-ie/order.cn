<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class SaleOrder extends ModelBase
{
    public static function tableName()
    {
        return 'sale_order';
    }
    
    public static function getClassId($id) {
        $saleOrder = self::find()->where(['id' => $id])->asArray()->one();
        return isset($saleOrder['class_id']) ? $saleOrder['class_id'] : '';
    }
        
    /**
     * 获取订单服务产品名称
     */
    public static function getClassName($id) {
        $saleOrder = self::find()->where(['id' => $id])->asArray()->one();
        return isset($saleOrder['class_name']) ? $saleOrder['class_name'] : '';
    }
    
    /**
     * 获取订单服务品牌名称
     */
    public static function getBrandName($id) {
        $saleOrder = self::find()->where(['id' => $id])->asArray()->one();
        return isset($saleOrder['brand_name']) ? $saleOrder['brand_name'] : '';
    }
    public static function getBrandId($id) {
        $saleOrder = self::find()->where(['id' => $id])->asArray()->one();
        return isset($saleOrder['brand_id']) ? $saleOrder['brand_id'] : '';
    }
    
    /**
     * 获取服务内容
     */
    public static function getServiceContent($saleOrderIds) {
        $saleOrder = self::find()->where(['id' => $saleOrderIds])->asArray()->all();
        $saleArr = [];
        foreach ($saleOrder as $k => $v) {
            $saleArr[$v['id']]['id'] = $v['id'];
            $saleArr[$v['id']]['content'] = $v['brand_name'].$v['class_name'].$v['type_name'];
        }
        return $saleArr;
    }

    /**
     * 获取用户产品详情
     */
    public static function getServiceInfo($saleOrderIds) {
        $saleOrder = self::find()->where(['id' => $saleOrderIds])->asArray()->all();
        $saleArr = [];
        foreach ($saleOrder as $k => $v) {
            $saleArr[$v['id']]['id'] = $v['id'];
            $saleArr[$v['id']]['brand_id'] = $v['brand_id'];
            $saleArr[$v['id']]['class_id'] = $v['class_id'];
            $saleArr[$v['id']]['type_id'] = $v['type_id'];
            $saleArr[$v['id']]['type_name'] = $v['type_name'];
            $saleArr[$v['id']]['class_name'] = $v['class_name'];
            $saleArr[$v['id']]['brand_name'] = $v['brand_name'];
        }
        return $saleArr;
    }

}