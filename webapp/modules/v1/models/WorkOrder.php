<?php
namespace webapp\modules\v1\models;

use common\models\CostPayType;
use common\models\CostItem;
use common\models\Department;
use common\models\PushMessage;
use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use webapp\modules\v1\models\CostRule;
use webapp\modules\v1\models\SaleOrder;
use webapp\modules\v1\models\AccountAddress;
use Yii;
use yii\helpers\ArrayHelper;

class WorkOrder extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }
    
    /**
     * 取消类型
     * @param bool $index
     * @return array|mixed|string
     */
    public static function getCancelStatus($index='')
    {
        $data = [
            1 => '商家取消',
            2 => '服务商取消',
            3 => '门店取消',
            4 => '微信客户取消'
        ];
        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 取消类型
     * @param bool $index
     * @return array|mixed|string
     */
    public static function getSourceDesc($index='')
    {
        $data = [
            1 => '商家创建',
            2 => '商家创建',
            4 => '微信小程序',
            5 => '美标导入'
        ];
        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 质保状态中文描述
     * @param string $index
     * @return mixed|string
     */
    public static function getIsScopeDesc($index='')
    {
        $data = [
            1 => '保内',
            2 => '保外',
        ];
        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 订单列表
     * @param string/array $where
     * @param string $select
     * @param number $page
     * @param number $pageSize
     * @return array
     * @author xi
     * @date 2017-12-14
     */
    public static function getList ($where,$sqlWhere,$srcDepartmentId,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(["`". WorkOrderDetail::tableName() . '` as b '],' a.order_no = b.order_no');
        $db->leftJoin(["`" . Work::tableName() . "` as work "] , 'a.order_no = work.order_no');
        $db->leftJoin(["`" . WorkOrderAssign::tableName() . "` as assign "] , 'a.order_no = assign.order_no');
        $db->innerJoin(["`". WorkOrderStatus::tableName() . '` as c '],' a.order_no = c.order_no and assign.service_department_id = c.department_id');
        //$db->leftJoin(["`" . WorkAppraisal::tableName() . "` as d "] , 'a.order_no = d.order_no');
        $db->groupBy('a.order_no');

        if($sqlWhere){
            $db->andWhere($sqlWhere);
        }

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->select('a.*, a.direct_company_id,a.department_id as `order_department_id` ,b.sale_order_id, c.status ,assign.assign_type,c.remarks as `cancel_reason`,c.remarks,c.operation_department_id');
            $db->orderBy('a.update_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            $orderNo = array_column($list,'order_no');

            //服务中（开始服务时间）
            $inServerArr = WorkOrderProcess::findAllByOrderNoAndStatus($orderNo,3);
            //已完成时间
            $finishArr = WorkOrderProcess::findAllByOrderNoAndStatus($orderNo,5);
            //技师信息
            $technicianArr = Work::findTechnicianIdsByOrderNosNew($orderNo);

            //把订单状态修正一下
            $workOrderStatusArr = WorkOrderStatus::findAllByAttributes(['order_no'=>$orderNo,'department_id'=>$srcDepartmentId],'order_no,department_id,status,is_show_btn','order_no');

            //把指派类型修正一下
            $workOrderAssignArr = WorkOrderAssign::findAllByAttributes(['order_no'=>$orderNo,'service_department_id'=>$srcDepartmentId],'order_no,assign_type','order_no');

            //订单有多少工单
            $workCountArr = Work::totalWorkOrderCount($orderNo);
            //当前的机构下的工单数
            $selfWorkCountArr = Work::totalWorkOrderCount($orderNo,['department_id'=>$srcDepartmentId]);
            //查出订单下的所有工单
            $workNoArr = Work::findWorkIdsByOrderNos($orderNo);
            //把订单所有服务机构查出，为了列表上下级操作项目
            $allServiceDepartmentArr = WorkOrderAssign::getAllServiceDepartmentIds($orderNo);

            //查询订单下工单的服务流程
            $workStageArr = work::workStageArr($orderNo);

            foreach ($list as $key=>$val)
            {
                $list[$key]['source_status'] = $val['status'];
                if(isset($workOrderStatusArr[$val['order_no']]))
                {
                    $list[$key]['status'] = $workOrderStatusArr[$val['order_no']]['status'];
                    $list[$key]['is_show_btn'] = $workOrderStatusArr[$val['order_no']]['is_show_btn'];
                }
                else {
                    $list[$key]['is_show_btn'] = 0;
                }

                if(isset($workOrderAssignArr[$val['order_no']])){
                    $list[$key]['assign_type'] = $workOrderAssignArr[$val['order_no']]['assign_type'];
                }


                $list[$key]['status_desc']           = WorkOrderStatus::getStatus($list[$key]['status']);
                $list[$key]['start_service_time']    = isset($inServerArr[$val['order_no']])?$inServerArr[$val['order_no']]['create_time']:'';
                $list[$key]['finish_time']           = isset($finishArr[$val['order_no']])?$finishArr[$val['order_no']]['create_time']: date('Y-m-d H:i:s',$val['update_time']);
                $list[$key]['technician_id']         = isset($technicianArr[$val['order_no']])?$technicianArr[$val['order_no']]['technician_id']:0;
                $list[$key]['technician_arr']        = isset($technicianArr[$val['order_no']])?$technicianArr[$val['order_no']]:[];
                $list[$key]['source_desc']           = self::getSourceDesc($val['source']);
                $list[$key]['is_scope_desc']         = self::getIsScopeDesc($val['is_scope']);
                $list[$key]['service_department_id'] = WorkOrderAssign::getServiceDepartmentId($val['order_no']);
                $list[$key]['service_department_ids']= isset($allServiceDepartmentArr[$val['order_no']])?$allServiceDepartmentArr[$val['order_no']]:[];
                $list[$key]['total_work']            = isset($workCountArr[$val['order_no']])?$workCountArr[$val['order_no']]:0;
                $list[$key]['total_self_work']       = isset($selfWorkCountArr[$val['order_no']])?$selfWorkCountArr[$val['order_no']]:0;
                $list[$key]['work_stage_arr']        = isset($workStageArr[$val['order_no']])?$workStageArr[$val['order_no']]:[];
                $list[$key]['work_no_arr']           = isset($workNoArr[$val['order_no']])?$workNoArr[$val['order_no']]:[];


                if($val['status'] == 9){
                    $list[$key]['notThroughReason']  = WorkOrderAudit::getAuditFailedReason($val['order_no']);
                }
            }

            foreach ($list as $key=>$val)
            {
                if($srcDepartmentId == $val['service_department_id']){
                    $list[$key]['source_status'] = $val['status'];
                }
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 取消订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $cancelReason 取消原因
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function cancel($orderNo,$cancelReason,$departmentId = 0,$processUserId = 0)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //订单状态变成已取消
            WorkOrderStatus::batchChangeStatus($orderNo,6,$cancelReason);
            //取消工单
            WorkProcess::cancelOrderAddProcess($orderNo,$departmentId,$processUserId,$cancelReason);
            Work::batchCancelWork($orderNo);

            //操作过程
            WorkOrderProcess::add($orderNo,6,$cancelReason);
            //工单操作过程

            //技师排班
            $workArr = Work::findOneByAttributes(['order_no'=>$orderNo],'work_no,plan_time');
            if($workArr){
                ScheduleTechnicianQueue::push($workArr['work_no'],5,date('Y-m-d'), date('Y-m-d',$workArr['plan_time']));
            }

            $transaction->commit();
            @PushMessage::push($orderNo,"qx");
            return [
                'order_no' => $orderNo
            ];
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage().$e->getTraceAsString(), 20006);
        }
    }

    /**
     * 取消订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $cancelReason 取消原因
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function ignore($orderNo,$processUserId,$ignoreReason,$srcType,$srcId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo
            ];
            $model = self::findOne($where);
            if($model){
                $model->ignore_status = 2;
                $model->update_time   = time();
                if($model->save(false))
                {
                    $type = 0;
                    if($srcType == 14){
                        $type = 18;
                    }
                    else if($srcType == 13){
                        $type = 19;
                    }
                    else if($srcType == 12){
                        $type = 20;
                    }

                    if( $type > 0 ){
                        $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo]);
                        foreach($workDetailArr as $val){
                            WorkOrderProcess::add($srcType,$srcId,$orderNo,$val['id'],$type,$processUserId,0,0,$ignoreReason,$model->amount,'');
                        }
                    }
                    unset($type);
                    $transaction->commit();
                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
            else {
                return ModelBase::errorMsg('未找到要取消的订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 订单详情
     * @param $orderNo 订单号
     * @param $srcType 查询端
     * @param $src_id 查询端id(如，商家id)
     * @return array|null|\yii\db\ActiveRecord
     * @author xi
     * @date 2017-12-15
     */

    public static function getDetail($orderNo,$directCompanyId,$departmentId,$serviceDepartmentId,$srcDepartmentId)
    {
        $where = [
            'a.order_no' => $orderNo,
            'a.department_id' => $departmentId
        ];
        if($serviceDepartmentId>0){
            $where['b.department_id'] = $serviceDepartmentId;
            $where['c.service_department_id'] = $serviceDepartmentId;
        }

        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(["`".WorkOrderStatus::tableName() . '` as b '],' a.order_no = b.order_no');
        $db->innerJoin(['`'.WorkOrderAssign::tableName() . '` as c '],' a.order_no = c.order_no');
        $db->where($where);
        $db->select('a.* , b.status , b.remarks , c.service_department_id , c.assign_type');
        $query = $db->asArray()->one();

        if($query)
        {
            $select = 'id,sale_order_id,sale_order_num,fault_img,is_scope,scope_img';
            $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],$select);

            if($workDetailArr)
            {
                foreach ($workDetailArr as $key=>$val){
                    if($val['fault_img']!=''){
                        $workDetailArr[$key]['fault_img_arr'] = explode(',',$val['fault_img']);
                    }
                    else {
                        $workDetailArr[$key]['fault_img_arr'] = [];
                    }
                    if($val['scope_img']!=''){
                        $workDetailArr[$key]['scope_img_arr'] = explode(',',$val['scope_img']);
                    }
                    else {
                        $workDetailArr[$key]['scope_img_arr'] = [];
                    }
                }
                $query['workDetail'] = $workDetailArr;
            }
            else{
                $query['workDetail'] = [];
            }

            if(trim($query['scope_img'])!=''){
                $query['scope_img'] = explode(',',$query['scope_img']);
            }
            else {
                $query['scope_img'] = [];
            }

            //修正订单状态
            $workOrderStatusArr = WorkOrderStatus::findOneByAttributes(['order_no'=>$orderNo,'department_id'=>$srcDepartmentId],'status,remarks');
            if($workOrderStatusArr){
                $query['status'] = $workOrderStatusArr['status'];
            }

            //状态中文描述
            $query['status_desc'] = WorkOrderStatus::getStatus($query['status']);
            $query['process'] = WorkOrderDynamic::getDynamic( $query['order_no'],$directCompanyId);
        }
        return $query;
    }

    /**
     * 订单详情
     * @param $orderNo 订单号
     * @return array|null|\yii\db\ActiveRecord
     * @author xi
     * @date 2017-12-15
     */
    public static function getDetailFws($orderNo)
    {
        $where = [
            'order_no' => $orderNo,
        ];

        $query = self::find()
            ->where($where)
            ->asArray()
            ->one();

        if($query)
        {
            $select = 'id,sale_order_id,problem_info,fault_img,status';
            $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],$select);
            if($workDetailArr){
                foreach ($workDetailArr as $key=>$val){
                    if($val['fault_img']!=''){
                        $workDetailArr[$key]['fault_img_arr'] = explode(',',$val['fault_img']);
                    }
                    else {
                        $workDetailArr[$key]['fault_img_arr'] = [];
                    }
                }
                $query['workDetail'] = $workDetailArr;
            }
            else{
                $query['workDetail'] = [];
            }

            $query['pay_type_desc'] = self::getPayType($query['pay_type']);
            $query['process'] = WorkOrderProcess::getDataByOrderNo($query['order_no']);
            $query['work'] = Work::getListByOrderNo($query['order_no']);
        }
        return $query;
    }

    /**
     * 指派技师
     * @param $orderNo 订单号
     * @param $detailId 订单详情
     * @param $srcType 指配来源（服务商、商家、门店）
     * @param $src_id 指配来源id(服务商id、门店id)
     * @param $processUserId 指派人id
     * @param $technicianId 技师id
     */
    public static function assignTechnician($orderNo,$srcType,$srcId,$processUserId,$technicianId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {

            $where = [
                'order_no' => $orderNo,
                'status' => 2 //待指派
            ];
            $orderModel = self::find()->where($where)->one();
            if($orderModel){

                $orderDetail = WorkOrderDetail::find()
                        ->where(['order_no'=>$orderNo])
                        ->asArray()->one();
                if($orderDetail)
                {
                    $orderModel->status = 3; //改成待服务
                    if($orderModel->save(false))
                    {
                        //订单详情把技师id加上
                       $res = WorkOrderDetail::updateTechnician($orderNo, $technicianId);
                       if($res)
                       {
                           if(WorkOrderAssign::assign($orderNo,$technicianId))
                           {
                                //服务商指派技师
                                $res1 = WorkOrderProcess::add($srcType,$srcId,$orderNo,$orderDetail['id'],8,$processUserId,0,0,'',$orderModel->amount,'');
                                if($res1){
                                    $transaction->commit();
                                    return [
                                        'order_no' => $orderNo,
                                    ];
                                }
                           }
                       }
                    }
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('指派失败，错误信息：'.$e->getMessage(), 20008);
        }
        return ModelBase::errorMsg('指派失败',20009);
    }

    /**
     * 重新指派技师
     * @param $orderNo 订单号
     * @param $detailId 订单详情
     * @param $srcType 指配来源（服务商、商家、门店）
     * @param $src_id 指配来源id(服务商id、门店id)
     * @param $processUserId 指派人id
     * @param $technicianId 技师id
     */
    public static function reAssignTechnician($orderNo,$srcType,$srcId,$processUserId,$technicianId,$reason)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo,
                'status'   => 3
            ];
            $orderModel = self::find()->where($where)->one();
            if($orderModel){

                $orderDetail = WorkOrderDetail::find()
                    ->where(['order_no'=>$orderNo])
                    ->asArray()->one();
                if($orderDetail)
                {
                    //更新技师信息
                    $res = WorkOrderDetail::updateTechnician($orderNo, $technicianId);
                    if($res)
                    {
                        if(WorkOrderAssign::reAssign($orderNo,$technicianId,$reason)) {
                            //服务商指派技师
                            $res1 = WorkOrderProcess::add($srcType, $srcId, $orderNo, $orderDetail['id'], 9, $processUserId, 0, 0, '', $orderModel->amount, '');
                            if ($res1) {
                                $transaction->commit();
                                return [
                                    'order_no' => $orderNo,
                                ];
                            }
                        }
                    }
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('重新指派失败，错误信息：'.$e->getMessage(), 20008);
        }
        return ModelBase::errorMsg('重新指派失败',20009);
    }

    /**
     * 服务中
     * @param $orderNo 订单号
     * @param $processUserId 操作人员
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function inService($orderNo,$processUserId,$srcType,$srcId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo,
                'status'   => 3
            ];
            $model = self::findOne($where);
            if($model){
                $model->status        = 4;
                $model->update_time   = time();
                if($model->save(false))
                {
                    $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo]);
                    foreach($workDetailArr as $val){
                        if($val['technician_id'] == $processUserId){
                            WorkOrderProcess::add($srcType,$srcId,$orderNo,$val['id'],23,$processUserId,0,0,'服务中',$model->amount,'');
                        }
                        else {
                            $transaction->rollBack();
                            return ModelBase::errorMsg('非法操作',20008);
                        }
                    }
                    $transaction->commit();
                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
            else {
                return ModelBase::errorMsg('未找到要更新的订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 服务完成
     * @param $orderNo 订单号
     * @param $processUserId 操作人员
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function finish($orderNo,$processUserId,$srcType,$srcId,$imgUrl,$problemReason)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo,
                'status'   => 4
            ];
            $model = self::findOne($where);
            if($model){
                $model->status        = 5;
                $model->update_time   = time();
                $model->visit_status  = 2;
                if($model->save(false))
                {
                    $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo]);
                    foreach($workDetailArr as $val){
                        if($val['technician_id'] == $processUserId){
                            WorkOrderProcess::add($srcType,$srcId,$orderNo,$val['id'],24,$processUserId,0,0,$problemReason,$model->amount,'',$imgUrl);
                        }
                        else {
                            $transaction->rollBack();
                            return ModelBase::errorMsg('非法操作',20008);
                        }
                    }
                    $transaction->commit();
                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
            else {
                return ModelBase::errorMsg('未找到要更新的订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 商家首页统计
     * @return array
     * @author xi
     * @date 2017-12-25
     */
    public static function tongji($srcType,$srcId)
    {
        $result = [
            'waitAudit'   => 0,
            'wait'        => 0,
            'waitService' => 0,
            'inService'   => 0,
            'finish'      => 0,
            'responseTimeout' => 0,
            'assignTimeout'   => 0,
            'serviceTimeout'  => 0,
            'serviceProvidersReject' => 0
        ];
        $query = self::find()
            ->where(['src_type'=>$srcType,'src_id'=>$srcId])
            ->select('status,cancel_status,count(1) as count')
            ->groupBy('status,cancel_status,order_no')
            ->asArray()->all();
        if($query)
        {
            foreach ($query as $val)
            {
                if($val['status'] == 1 && $val['cancel_status']==0){
                    $result['wait']+=$val['count'];
                }
                else if( in_array($val['status'],[2,3])  && in_array($val['cancel_status'],[0,8,9])){
                    $result['waitService']+=$val['count'];
                }
                else if(in_array($val['status'],[4,7]) && $val['cancel_status']==0){
                    $result['inService']+=$val['count'];
                }
                else if($val['status'] == 5 && intval($val['cancel_status']) == 0 ){
                    $result['finish']+=$val['count'];
                }
                else if($val['status'] == 8){
                    $result['waitAudit']+=$val['count'];
                }
            }
        }

        //异常订单数据
        $result['responseTimeout'] = self::getResponseTimeout($srcType,$srcId);
        $result['assignTimeout']   = self::getAssignTimeout($srcType,$srcId);
        $result['serviceTimeout']  = self::getServiceTimeout($srcType,$srcId);
        $result['serviceProvidersReject'] = self::getServiceProvidersReject($srcType,$srcId);

        return $result;
    }

    /**
     * 响应超时间数据
     * @param $srcType
     * @param $srcId
     * @return int
     * @author xi
     * @date 2017-12-25
     */
    private static function getResponseTimeout($srcType,$srcId)
    {
        $where = [
            'src_type' => $srcType,
            'src_id'   => $srcId,
            'ignore_status' => 1,
            'cancel_status' => 6
        ];
        $query = self::find()
            ->where($where)
            ->count();
        return (int)$query;
    }

    /**
     * 指派超时间数据
     * @param $srcType
     * @param $srcId
     * @return int
     * @author xi
     * @date 2017-12-25
     */
    private static function getAssignTimeout($srcType,$srcId)
    {
        $where = [
            'a.src_type' => $srcType,
            'a.src_id'   => $srcId,
            'a.status'   => 2,
            'a.cancel_status' => 8
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->count();
        return (int)$query;
    }

    /**
     * 上门超时间数据
     * @param $srcType
     * @param $srcId
     * @return int
     * @author xi
     * @date 2017-12-25
     */
    private static function getServiceTimeout($srcType,$srcId)
    {
        $where = [
            'a.src_type' => $srcType,
            'a.src_id'   => $srcId,
            'a.status'   => 3,
            'a.cancel_status' => 9
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->count();
        return (int)$query;
    }

    /**
     * 服务商拒接数据
     * @param $srcType
     * @param $srcId
     * @return int
     * @author xi
     * @date 2017-12-25
     */
    private static function getServiceProvidersReject($srcType,$srcId)
    {
        $where = [
            'src_type' => $srcType,
            'src_id'   => $srcId,
            'is_receive'   => 2,
            'ignore_status' => 1
        ];
        $query = self::find()
            ->where($where)
            ->andWhere(['<>','status',6])
            ->count();
        return (int)$query;
    }


    /**
     * 服务商抢单
     * @param $orderNo 订单号
     * @param $processUserId 操作人员
     * @param $srcType 从哪端来的
     * @author xi
     * @date 2017-12-25
     */
    public static function serviceProviderOrder($orderNo,$processUserId,$srcType,$srcId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo,
                'status'   => 1
            ];
            $taskHallModel = WorkOrderTaskHall::findOne($where);

            $where = [
                'order_no' => $orderNo,
                'status'   => 1,
                'service_provide_id' => 0,
                'has_service_provide'=>0
            ];
            $model = self::findOne($where);
            if($model && $taskHallModel){
                $model->status        = 3;
                $model->update_time   = time();
                $model->service_provide_id  = $srcId;
                $model->has_service_provide = 1;

                if($model->save(false))
                {
                    //指派给自已
                    WorkOrderAssign::add($orderNo,$taskHallModel['src_type'],$taskHallModel['src_id'],$srcId,0);

                    $workDetailModels = WorkOrderDetail::find()->where(['order_no'=>$orderNo])->all();
                    if($workDetailModels)
                    {
                        foreach ($workDetailModels as $detalModel) {
                            WorkOrderProcess::add($srcType, $srcId, $orderNo, $detalModel->id, 28, $processUserId, 0, 0, '服务商接单', $model->amount, '');
                        }
                        $attr = [
                            'status'      => 0,
                            'update_time' => time(),
                            'version'     => $taskHallModel->version+1
                        ];
                        $where = ['order_no'=>$orderNo,'status'=>1,'version'=>$taskHallModel->version];
                        if(WorkOrderTaskHall::updateAll($attr,$where))
                        {
                            //生成工单
                            $workNo = Work::autoCreate($srcId,$orderNo,$processUserId,$model->plan_time,$model->plan_time_type,1,$model->work_type);
                            if($workNo)
                            {
                                //动态填加
                                $params = [
                                    'accountId' => $processUserId,
                                    'fwsId'     => $srcId,
                                ];
                                WorkOrderDynamic::add(WorkOrderDynamic::TYPE_SJ,3,$orderNo,$params,$workNo);
                                WorkOrderDynamic::add(WorkOrderDynamic::TYPE_FWS,4,$orderNo,$params,$workNo);

                                $transaction->commit();
                                return [
                                    'order_no' => $orderNo
                                ];
                            }
                        }
                        else
                        {
                            $transaction->rollBack();
                            return ModelBase::errorMsg('抢单失败', 20009);
                        }
                    }
                    else {
                        $transaction->rollBack();
                        return ModelBase::errorMsg('抢单失败', 20008);
                    }
                }
            }
            else {
                return ModelBase::errorMsg('未找到订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('抢单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 技师抢单
     * @param $orderNo 订单号
     * @param $srcType 从哪端来的
     * @author xi
     * @date 2017-12-25
     */
    public static function technicianOrder($orderNo,$srcType,$srcId,$storeId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo,
                'status'   => 1
            ];
            $taskHallModel = WorkOrderTaskHall::findOne($where);
            $where = [
                'order_no' => $orderNo,
//                'status'   => 1,
                'has_service_provide' => 0,
                'service_provide_id'  => 0
            ];
            $model = self::findOne($where);
            if(!$model) {
                return ModelBase::errorMsg('很抱歉，工单被取消了！', 20013);
            }

            if($model->status == 6) {
                return ModelBase::errorMsg('很抱歉，工单被取消了！', 20011);
            }

            if($model->status != 1) {
                return ModelBase::errorMsg('很抱歉，工单被抢走了！', 20012);
            }
            if($model && $taskHallModel)
            {
                $model->has_service_provide = 1;
                $model->service_provide_id  = $storeId;
                $model->status        = 3;
                $model->update_time   = time();
                if($model->save(false))
                {
                    //自动加一条数据
                    WorkOrderAssign::add($orderNo,$model->src_type,$model->src_id,$storeId,0);

                    $workDetailModels = WorkOrderDetail::find()->where(['order_no'=>$orderNo])->all();
                    if($workDetailModels)
                    {
                        foreach($workDetailModels as $detalModel)
                        {
                            WorkOrderProcess::add($model->src_type,$model->src_id,$orderNo,$detalModel->id,25,$srcId,0,0,'技师接单',$model->amount,'');
                        }

                        $attr = [
                            'status'      => 0,
                            'update_time' => time(),
                            'version'     => $taskHallModel->version+1
                        ];
                        $where = ['order_no'=>$orderNo,'status'=>1,'version'=>$taskHallModel->version];
                        if(WorkOrderTaskHall::updateAll($attr,$where))
                        {
                            //生成工单
                            $workStage = ServiceFlow::getFirstOne($storeId);
                            $workNo = Work::autoCreate($storeId,$orderNo,$srcId,$model->plan_time,$model->plan_time_type,2,$workStage);
                            if($workNo)
                            {
                                //动态填加
                                $params = [
                                    'jsId' => $srcId,
                                ];
                                WorkOrderDynamic::add(WorkOrderDynamic::TYPE_SJ,6,$orderNo,$params,$workNo);
                                WorkOrderDynamic::add(WorkOrderDynamic::TYPE_FWS,7,$orderNo,$params,$workNo);

                                //指配技师
                                if(WorkRelTechnician::add( $workNo ,$srcId, 1, 11, $srcId)){
                                    $transaction->commit();
                                    return [
                                        'order_no' => $orderNo
                                    ];
                                }
                                else
                                {
                                    $transaction->rollBack();
                                    return ModelBase::errorMsg('抢单失败', 20010);
                                }
                            }
                            else
                            {
                                $transaction->rollBack();
                                return ModelBase::errorMsg('抢单失败', 20011);
                            }
                        }
                        else
                        {
                            $transaction->rollBack();
                            return ModelBase::errorMsg('抢单失败', 20009);
                        }
                    }
                    else {
                        $transaction->rollBack();
                        return ModelBase::errorMsg('抢单失败', 20008);
                    }
                }
            }
            else {
                return ModelBase::errorMsg('未找到订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('抢单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 查出用户最近服务的产品
     * @param $accountId
     * @param $saleOrderId
     * @return array
     */
    public static function getAccountProduct($accountId,$saleOrderId)
    {
        $result = [];
        $where = [
            'a.account_id'   => $accountId,
            'b.sale_order_id' => $saleOrderId
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->select('b.sale_order_id,a.plan_time,a.work_type')
            ->asArray()->all();
        if($query)
        {
            foreach ($query as $val)
            {
                $result[$val['sale_order_id']] = [
                    'plan_time' => $val['plan_time'],
                    'work_type' => $val['work_type']
                ];
            }
        }
        return $result;
    }

    /**
     * 根据用户id 查出订单数
     * @param $srcType
     * @param $accountIds
     * @return array
     */
    public static function getUserOrders($accountIds)
    {
        $where = [
            'account_id' => $accountIds
        ];
        $query = self::find()
            ->where($where)
            ->select('account_id,count(1) as count')
            ->groupBy('account_id')
            ->asArray()
            ->all();

        if($query){
            return array_column($query,'count','account_id');
        }
        return [];
    }

    /**
     * 根据订单号查出订单信息
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findOneByOrderNo($orderNo)
    {
        $result = [];
        $where = [
            'a.order_no'   => $orderNo,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->select('a.*,b.sale_order_id,b.fault_img')
            ->asArray()->one();
        if($query)
        {
            return $query;
        }
        return $result;
    }
    /*
     * 获取待接全部订单
     * @author  li
     * @date    2018-01-04
     * @return array
     */
    public static function getNewOrderList() {
        $order = self::find()->where(['status' => 1])->orderBy('create_time desc')->asArray()->all();
        $orderNos = $newOrderNos = [];
        foreach($order as $v) {
            $orderNos[] = $v['order_no'];
        }
        //查询可以抢的订单
        $orderDetail = WorkOrderDetail::find()->where(['order_no' => $orderNos])->asArray()->all();
        foreach ($orderDetail as $k => $v) {
            if($v['status'] == 1) {
                $newOrderNos[] = $v['order_no'];
            }
        }
        foreach ($order as $k => $v) {
            if(!in_array($v['order_no'], $newOrderNos)) {
                unset($order[$k]);
            }
        }
        return $order;
    } 
    
    /**
     * 获取待接订单详情
     * @author li
     * @date   2018-01-08
     * @param  string  $order_no
     * @return array
     */
    public static function getNewOrderDetail($order_no) {
        $order = self::find()->where(['order_no' => $order_no])->asArray()->one();
        //获取订单的服务类型
        $order['goods_class_name'] = WorkOrderDetail::getClassName($order_no);
        $order['service_title']    = ServiceType::getServiceTitle($order['work_type']);
        //获取服务地址
        $address = AccountAddress::getOrderAddress($order['address_id']);
        foreach ($address as $v) {
            $order['address'] = $v['address'];
            $order['lon']     = $v['lon'];
            $order['lat']     = $v['lat'];
        }
        return $order;
    }


    /**
     * 查询当前订单在当天是否有上一个订单
     * @date 2017-11-20
     * @param $time
     * @param int $id 技师ID
     */
    public static function getFrontOrder($time, $id) {
        //得到当前时间的年月日
        $day = date("Y-m-d",$time);
        //起始时间
        $start_time = strtotime($day."00:00:00");
        //结束时间
        $end_time   = strtotime($day."23:59:59");
        $where = "plan_time>=$start_time and plan_time<=$end_time and status=1 and plan_time<$time";
        $order = self::find()->where($where)->andWhere(['technician_id' => $id])->orderBy('plan_time desc')->asArray()->one();
        if(!empty($order)) {
            $frontOrder = WorkOrder::getOrder($order['order_no']);
            //获取服务地址
            $address = AccountAddress::getOrderAddress($frontOrder['address_id']);
            foreach ($address as $v) {
                $frontOrder['address'] = $v['address'];
                $frontOrder['lon']     = $v['lon'];
                $frontOrder['lat']     = $v['lat'];
            }
            return $frontOrder;
        }
        return [];
    }

    /**
     * 按日期查询派单总量
     * liquan
     */
    public static function getOrderNum($where){
        $db = self::find();
        $db->from(self::tableName());
        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        //当有结果时进行组合数据
        $db->select("account_id,count(id) as order_num,from_unixtime(create_time,'%Y-%m-%d') as `date`");
        $db->groupBy('account_id,date');
        $db->asArray();
        echo $db->createCommand()->rawSql;exit;
        $list = $db->all();

    }
    /**
     * 查询当前订单在当天是否有上一个订单
     * @date 2017-11-20
     * @param $time
     * @param int $id 技师IDiddi
     */
    public static function getAfterOrder($time, $id) {
        //得到当前时间的年月日
        $day = date("Y-m-d",$time);
        //起始时间
        $start_time = strtotime($day."00:00:00");
        //结束时间
        $end_time   = strtotime($day."23:59:59");
        $where = "plan_time>=$start_time and plan_time<=$end_time and status=1 and plan_time>$time";
        $order = self::find()->where($where)->andWhere(['technician_id' => $id])->orderBy('plan_time asc')->asArray()->one();
        if(!empty($order)) {
            $afterOrder= WorkOrder::getOrder($order['order_no']);
            //获取服务地址
            $address = AccountAddress::getOrderAddress($afterOrder['address_id']);
            foreach ($address as $v) {
                $afterOrder['address'] = $v['address'];
                $afterOrder['lon']     = $v['lon'];
                $afterOrder['lat']     = $v['lat'];
            }
            return $afterOrder;
        }
        return [];
    }


    
    /**
     * 根据订单号获取订单信息
     */
    public static function getOrder($order_no) {
        return self::find()->where(['order_no' => $order_no])->asArray()->one();
    }

    /**
     * 订单列表
     * @param string/array $where
     * @param string $select
     * @param number $page
     * @param number $pageSize
     * @return array
     * @author xi
     * @date 2017-12-14
     */
    public static function getTechnicianList ($where,$sqlWhere,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');
        $db->innerJoin(['`'.WorkOrderAssign::tableName() . '` as c '],' a.order_no = c.order_no');

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        if($sqlWhere!=''){
            $db->andWhere($sqlWhere);
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('a.order_no,a.plan_time,a.create_time,a.update_time,a.status,a.account_id,a.address_id,a.amount,b.sale_order_id,a.service_provide_id,c.type');

            $db->orderBy('a.plan_time desc ');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            //取消订单信息
            $orderNo = array_column($list,'order_no');

            //服务中（开始服务时间）
            $inServerArr = WorkOrderProcess::getTypeTimeData($orderNo,23);
            //已完成时间
            $finishArr = WorkOrderProcess::getTypeTimeData($orderNo,24);

            foreach ($list as $key=>$val){
                $list[$key]['status_desc']        = $val['type'] == 2?'已改派': self::getStatus($val['status']);
                $list[$key]['start_service_time'] = isset($inServerArr[$val['order_no']])?$inServerArr[$val['order_no']]:'';
                $list[$key]['finish_time']        = isset($finishArr[$val['order_no']])?$finishArr[$val['order_no']]:'';
                $list[$key]['amount']             = $val['amount']>0?$val['amount']/100:'0.00';
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 订单详情
     * @param $orderNo 订单号
     * @param $technicianId 技师id
     * @return array|null|\yii\db\ActiveRecord
     * @author xi
     * @date 2017-12-15
     */
    public static function getTechnicianDetail($orderNo,$technicianId)
    {
        $where = [
            'a.order_no' => $orderNo,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderAssign::tableName() . " as b"],'a.order_no = b.order_no')
            ->where($where)
            ->andWhere(['=','b.technician_id',$technicianId])
            ->select('a.*,b.type')
            ->asArray()
            ->one();
        if($query)
        {
            $select = 'id,sale_order_id,problem_info,fault_img,status';
            $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],$select);
            if($workDetailArr){
                foreach ($workDetailArr as $key=>$val){
                    if($val['fault_img']!=''){
                        $workDetailArr[$key]['fault_img_arr'] = explode(',',$val['fault_img']);
                    }
                    else {
                        $workDetailArr[$key]['fault_img_arr'] = [];
                    }
                }
                $query['workDetail'] = $workDetailArr;
            }
            else{
                $query['workDetail'] = [];
            }

            $query['status_desc']   = self::getStatus($query['status']);
            $query['pay_type_desc'] = self::getPayType($query['pay_type']);
            $query['amount']        = $query['amount']>0?$query['amount']/100:'0.00';

            $reAssignArr = WorkOrderAssign::getReAssign($orderNo);

            //取消信息
            $query['cancel'] = WorkOrderProcess::getCancelOneData($query['order_no']);
            //改派信息
            $query['reAssign'] = WorkOrderAssign::getReAssign($query['order_no']);
            //已完成信息
            $query['finish'] = WorkOrderProcess::getFinishOneData($query['order_no']);
            //开始时间
            $query['startService'] = WorkOrderProcess::getStartServiceOneData($query['order_no']);
        }
        return $query;
    }

    /**
     * 判断技师是否可以服务
     * @param $technicianId
     * @param $planTime
     * @return bool
     * @author xi
     * @date 2018-19
     */
    public static function hasTechnicianService($technicianId,$planTime)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');

        $db->where(['b.technician_id'=>$technicianId]);
        $db->andWhere( ($planTime - 7200).' < a.plan_time  && a.plan_time < '.($planTime+7200));
        $count = $db->count();

        return [
            'services_status' => $count>0?0:1,
            'order_count'     => $count
        ];
    }

    /**
     * 判断技师是否可以服务
     * @param $technicianId
     * @param $planTime
     * @return bool
     * @author xi
     * @date 2018-19
     */
    public static function hasTechnicianServiceByStatus($technicianId)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');

        $db->where(['b.technician_id'=>$technicianId,'a.status'=>[4]]);
        $count = $db->count();

        return $count>0?0:1;
    }

    /**
     * 确认订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function confirmOrder($orderNo,$processUserId,$srcType,$srcId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $where = [
                'order_no' => $orderNo
            ];
            $model = self::findOne($where);
            if($model){
                $model->status        = 3;
                $model->is_receive    = 1;
                $model->cancel_status = 0;
                $model->update_time   = time();
                if($model->save(false))
                {
                    $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo]);
                    foreach($workDetailArr as $val){
                        WorkOrderProcess::add($srcType,$srcId,$orderNo,$val['id'],6,$processUserId,0,0,"服务商已确认",$model->amount,'');
                    }

                    //删除任务大厅数据
                    WorkOrderTaskHall::updateAll(['status'=>0,'update_time'=>time()],['order_no'=>$orderNo]);

                    //生成工单
                    $workNo = Work::autoCreate($srcId,$orderNo,$processUserId,$model->plan_time,$model->plan_time_type,1,$model->work_type);
                    if($workNo)
                    {
                        //动态填加
                        $params = [
                            'accountId' => $processUserId,
                            'fwsId'     => $srcId,
                        ];
                        WorkOrderDynamic::add(WorkOrderDynamic::TYPE_SJ,4,$orderNo,$params,$workNo);
                        WorkOrderDynamic::add(WorkOrderDynamic::TYPE_FWS,5,$orderNo, $params,$workNo);

                        $transaction->commit();
                        return [
                            'order_no' => $orderNo
                        ];
                    }
                }
            }
            else {
                return ModelBase::errorMsg('未找到要确认的订单数据',20007);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('确认订单失败，错误信息：'.$e->getMessage().$e->getTraceAsString(), 20006);
        }
    }

    /**
     * 驳回
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $srcType 从哪端取消的
     * @author xi
     * @date 2017-12-15
     */
    public static function rejectOrder($orderNo,$processUserId,$serviceDepartmentId,$rejectReason)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //修改自已的订单状态
            WorkOrderStatus::reject($orderNo,$serviceDepartmentId,$rejectReason);
            //把指派状态改驳回
            WorkOrderAssign::reject($orderNo,$serviceDepartmentId,$processUserId,$rejectReason);

            $transaction->commit();

            //驳回订单动态
            $params = [
                'accountId' => $processUserId,
                'fwsId'      => $serviceDepartmentId,
                'reason'      => $rejectReason,
            ];
            WorkOrderDynamic::add(10,$orderNo,$params);

            return [
                'order_no' => $orderNo
            ];
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('拒绝订单失败，错误信息：'.$e->getMessage().$e->getTraceAsString(), 20006);
        }
    }

    /**
     * 技师服务状态（根据status = 4 查询）
     * @return array|bool
     * @author xi
     * @date 2018-1-11
     */
    public static function technicianServiceStatus($technicianIds)
    {
        if($technicianIds)
        {
            $db = self::find();
            $db->from(self::tableName() . ' as a');
            $db->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');

            $db->where(['b.technician_id'=>$technicianIds,'a.status'=>4]);
            $db->groupBy('b.technician_id');
            $db->select('b.technician_id,count(1) as count');

            $query = $db->asArray()->all();
            $technicianIdsArr = [];
            if($query){
                $technicianIdsArr = array_column($query,'count','technician_id');
            }

            $result = [];
            foreach ($technicianIds as $val){
                $result[$val] = [
                    'status'      => isset($technicianIdsArr[$val])?1:0,
                    'status_desc' => isset($technicianIdsArr[$val])?'服务中':'空闲中'
                ];
            }
            return $result;
        }
        return ModelBase::errorMsg('未找到要查询的技师信息',20003);
    }

    /**
     * 查询订单是否指派
     * @return array|bool
     * @author xi
     * @date 2018-1-11
     */
    public static function hasOrderAssign($orderNo,$type)
    {
        $count = WorkOrderAssign::find()
            ->where(['order_no'=>$orderNo,'type'=>$type])
            ->count();
        return [
            'status' => $count>0?1:0
        ];
    }

    /**
     * 根据订单号查出订单信息
     * @param $orderNos
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findAllByOrderNos($orderNos,$limit=1000)
    {
        $result = [];
        $where = [
            'a.order_no'   => $orderNos,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->select('a.*,b.sale_order_id')
            ->limit($limit)
            ->asArray()->all();
        if($query)
        {
            return ArrayHelper::index($query,'order_no');
        }
        return $result;
    }

    /**
     * 订单列表
     * @param string/array $where
     * @param string $select
     * @param number $page
     * @param number $pageSize
     * @return array
     * @author xi
     * @date 2017-12-14
     */
    public static function getReAssignList ($serviceProviderId,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');
        $db->innerJoin([ '`'.WorkOrderAssign::tableName() . '` as c ' ],'a.order_no = c.order_no');

        $db->where(['c.type'=>2,'c.service_provider_id'=>$serviceProviderId]);

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('a.order_no,a.plan_time,a.work_type,b.sale_order_id,c.reason,c.create_time');

            $db->orderBy('a.id desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 验收通过
     * @param $orderNo 订单号
     * @param $processUserId 操作人员
     * @author xi
     * @date 2017-12-15
     */
    public static function confirmFinish($orderNo,$processUserId,$departmentId,$flag=1)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            if($flag == 1){
                //自已的机构变成 等待上级验收
                WorkOrderStatus::changeStatus($departmentId,$orderNo,13,$departmentId);
                WorkOrderStatus::acceptanceCheck($orderNo,$departmentId);
                //上一级机构变成待验收
                $assignArr = WorkOrderAssign::findOneByAttributes(['order_no'=>$orderNo,'service_department_id'=>$departmentId,'type'=>[1,3]],'type,department_id,service_department_id');
                if($assignArr)
                {
                    //如果是顶级处理
                    if($assignArr['type'] == 3 && $assignArr['service_department_id'] == $assignArr['department_id'])
                    {
                        //订单过程表加上数据
                        WorkOrderProcess::add($orderNo,5,'完成服务');
                        //所有订单变成已完成
                        WorkOrderStatus::batchChangeStatus($orderNo,5,'验收完成');

                        //顶级机构验收通过动态填加
                        $params = [
                            'accountId' => $processUserId,
                        ];
                        WorkOrderDynamic::add(19,$orderNo,$params);
                    }
                    else
                    {
                        //把上面所有级别都改成待验收
                        WorkOrderStatus::updateAll(['status'=>4,'operation_department_id'=>$departmentId,'update_time'=>time()],['order_no'=>$orderNo,'status'=>[3,7]]);
                        //订单过程表加上数据
                        WorkOrderProcess::add($orderNo,4,'待验收');

                        //验收通过动态填加
                        $params = [
                            'accountId' => $processUserId,
                        ];
                        WorkOrderDynamic::add(18,$orderNo,$params);
                    }
                }
            }else{
                //订单过程表加上数据
                WorkOrderProcess::add($orderNo,5,'完成服务');
                //所有订单变成已完成
                WorkOrderStatus::batchChangeStatus($orderNo,5,'验收完成');

                //顶级机构验收通过动态填加
                $params = [
                    'accountId' => $processUserId,
                ];
                WorkOrderDynamic::add(19,$orderNo,$params);
            }


            $transaction->commit();
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('验收失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 验收驳回
     * @param $orderNo
     * @param $processUserId
     * @param $departmentId
     * @param $reason
     * @return array
     */
    public static function acceptanceFailure($orderNo,$processUserId,$departmentId,$reason)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //自已的机构变成待验收驳回
            WorkOrderStatus::changeStatus($departmentId,$orderNo,7,$departmentId,$reason);
            WorkOrderStatus::updateAll(['status'=>7,'remarks'=>$reason,'update_time'=>time()], ['order_no'=>$orderNo]);

            //订单过程表加上数据
            WorkOrderProcess::add($orderNo,7,$reason);

            //验收驳回动态填加
            $params = [
                'accountId' => $processUserId,
                'reason'    => $reason
            ];
            WorkOrderDynamic::add(20,$orderNo,$params);

            $transaction->commit();
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage(), 20006);
        }
    }

    /**
     * 服务商首页统计
     * @return array
     * @author xi
     * @date 2017-12-25
     */
    public static function fwsTongji($serviceProviderId)
    {
        $result = [
            'finishOrder'      => 0,
            'waitConfirmOrder' => 0,
            'waitCheckOrder'   => 0,
            'waitAssignWork'   => 0,
            'waitServiceWork'  => 0,
            'noFirstWork'      => 0,
            'waitSettlement'   => 0

        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkOrderAssign::tableName() . '` as b'],'a.order_no = b.order_no')
            ->where(['a.src_type'=> ModelBase::SRC_FWS,'b.service_provider_id'=>$serviceProviderId,'b.type'=>1,'a.status'=>[1,5,7]])
            ->select('a.status,count(1) as count')
            ->groupBy('a.status')
            ->asArray()->all();
        if($query)
        {
            foreach ($query as $val)
            {
                if($val['status'] == 1){
                    $result['waitConfirmOrder']+=$val['count'];
                }
                else if($val['status'] == 5){
                    $result['finishOrder']+=$val['count'];
                }
                else if($val['status'] == 7){
                    $result['waitCheckOrder']+=$val['count'];
                }
            }
        }

        //查工单数据
        $query = Work::find()
            ->where(['src_type'=>ModelBase::SRC_FWS,'src_id'=>$serviceProviderId,'status'=>[1,2]])
            ->orWhere(['is_first'=>1])
            ->orWhere(['settlement_status'=>1])
            ->select('status,is_first,settlement_status')
            ->asArray()->all();
        if($query)
        {
            foreach($query as $val)
            {
                if($val['status'] == 1){
                    $result['waitAssignWork']+=1;
                }
                else if($val['status'] == 2){
                    $result['waitServiceWork']+=1;
                }
                if($val['is_first'] == 2){
                    $result['noFirstWork']+=1;
                }
                if($val['settlement_status'] == 1){
                    $result['waitSettlement']+=1;
                }
            }
        }

        return $result;
    }


    /*
    * 获取指定时间段内所有没有结算的工单
    * */
    public static function getOrderForSettlement($start_time,$end_time,$src_type,$src_id,$sj_id)
    {

        //获取服务商对应商家的结算规则
        $cost_rules = CostRule::getRuleInfoBySj($sj_id,$src_id);
        if($cost_rules)
        {
            //订单完成后多少天可以进行结算，开始时间和结束时间统一往后推
            $cost_date = $cost_rules[0]['cost_date'];
            $start_time +=$cost_date;
            $end_time +=$cost_date;
            foreach ($cost_rules as $v)
            {
                //拼接成统一可Key Value形式，方便后续匹配
                $commonKey = $v['brand_id'].'_'.$v['class_id'].'_'.$v['type_id'].'_'.$v['cost_item_id'];
                $costRules[$commonKey] = ['cost_type'=>$v['cost_type'],'fixed_amount'=>$v['fixed_amount'],'pecent'=>$v['pecent'],'area'=>$v['service_area']];
            }
        }else{
            //如果不存在结算规则，则无法进行结算，数据为空
            return [];
        }

        //print_r($costRules);
        //测试数据
      //  $costRules['2_0_0_2_16_32_1'] = ['cost_type'=>1,'fixed_amount'=>30,'pecent'=>0];
       // $costRules['_0_0_2_16_32_76'] = ['cost_type'=>2,'fixed_amount'=>30,'pecent'=>40];
       // $costRules['2_38_40_4_15_32_2'] = ['cost_type'=>2,'fixed_amount'=>30,'pecent'=>40];



        $where = "p.create_time + ".$cost_date*86400 . " < ".time()."  and p.create_time>=$start_time and p.create_time<=$end_time";


        $db = self::find();

        $db->from(self::tableName() . ' as a' );
        $db->innerJoin([WorkOrderProcess::tableName().' as p'],'p.order_no = a.order_no');
        $db->innerJoin([WorkCost::tableName().' as c'],'c.order_no = a.order_no');
        $db->innerJoin([WorkOrderAssign::tableName().' as e'],'e.order_no = a.order_no');
        $db->innerJoin([WorkOrderDetail::tableName().' as d'],'d.order_no = a.order_no');
        $db->where($where);
        $db->andWhere(['p.type' => 24, 'a.settlement_status' =>1]);
        $db->andWhere(['a.src_type'=>14,'a.src_id'=>$sj_id]);
        $db->andWhere(['c.payer_id'=>1]);
        $db->andWhere(['c.status'=>1]);
        $db->andWhere(['e.service_provider_id'=>$src_id]);
        $db->select('a.id as order_id,a.order_no,a.work_type,a.account_id,a.address_id,d.sale_order_id,c.cost_id,cost_number,cost_price,cost_amount,cost_real_amount,p.create_time');
        $db->orderBy('a.id asc');

        // $sql = $db->createCommand()->getRawSql();
        // print_r($sql);exit;
        $query = $db->asArray()->all();
        $result = [];
        if($query)
        {
            //所有的订单编号
            $orderNos = array_unique(array_column($query,'order_no'));
            //订单总数
            $result['order_number'] = count($orderNos);
            //所有的客户服务产品
            $saleOrderIds = array_unique(array_column($query,'sale_order_id'));

            //所有的客户地址
            $addressIds = array_unique(array_column($query,'address_id'));

            //获取所有的服务产品中对应的产品信息（品牌 brand_id，分类 class，服务类型 type）
            $saleOrdersTmp = SaleOrder::getServiceInfo($saleOrderIds);

            //获取所有的客户地址对应的省市区信息
            $accountAddress = AccountAddress::getOrderAddress($addressIds);


            //获取费用name
            $costIds = array_unique(array_column($query,'cost_id'));
            $costArr = CostItem::getCostNameByIds($costIds);

            $totalAmount = 0;
            $totalOrderNumber =0;

            //遍历所有的订单对应的费用信息。将不满足匹配条件的数据过滤掉
            foreach ($query as $data)
            {
                $orderId = $data['order_id'];
                $saleOrderId = $data['sale_order_id'];
                $addressId = $data['address_id'];
                $costId = $data['cost_id'];
                $brandId = 0;
                $typeId = 0;
                $classId = 0;
                $brandName = '';
                $typeName = '';
                $className = '';
                $provinceId = 0;
                $cityId = 0;
                $districtId = 0;
                $divideAmount =0;
                // 客户产品存在
                if(key_exists($saleOrderId,$saleOrdersTmp))
                {
                    $brandId = $saleOrdersTmp[$saleOrderId]['brand_id'];
                    $typeId = $saleOrdersTmp[$saleOrderId]['type_id'];
                    $classId = $saleOrdersTmp[$saleOrderId]['class_id'];
                    $brandName = $saleOrdersTmp[$saleOrderId]['brand_name'];
                    $typeName = $saleOrdersTmp[$saleOrderId]['type_name'];
                    $className = $saleOrdersTmp[$saleOrderId]['class_name'];
                    $typeId = $data['work_type'];

                    //客户地址存在
                    if(key_exists($addressId,$accountAddress))
                    {
                        $provinceId = $accountAddress[$addressId]['province_id'];
                        $cityId = $accountAddress[$addressId]['city_id'];
                        $districtId = $accountAddress[$addressId]['district_id'];
                        //拼接校验结算规则的key
                        $key = $brandId.'_'.$classId.'_'.$typeId.'_'.$costId;
                        $area_key = $provinceId.'_'.$cityId.'_'.$districtId;
                        if(key_exists($key,$costRules))
                        {
                            $rules = $costRules[$key];
                            $area_str = $rules['area'];
                            $position = strpos($area_str,$area_key);
                            if($position!==false)
                            {
                                $costRule = $costRules[$key];
                                //['cost_type'=>$v['cost_type'],'fixed_amount'=>$v['fixed_amount'],'pecent'=>$v['pecent']]
                                //结算方式为定额的，结算金额等于 数量乘以固定的金额
                                if($costRule['cost_type'] == 1)
                                {
                                    $divideAmount = $costRule['fixed_amount']*$data['cost_number'];
                                }elseif ($costRule['cost_type'] == 2){
                                    //结算方式为百分比的，结算金额等于百分比乘以实际收费金额
                                    $divideAmount = $data['cost_real_amount']*$costRule['pecent']/100;
                                }
                                $costName = isset($costArr[$data['cost_id']])?$costArr[$data['cost_id']]:'';
                                $result ['order_info'][$orderId]['cost_info'][]=['cost_id'=>$data['cost_id'],'cost_name'=>$costName,'divide_amount'=>$divideAmount];
                                $result ['order_info'][$orderId]['detail_info']=['order_no'=>$data['order_no'],'class_id'=>$classId,'class_name'=>$className,'brand_id'=>$brandId,'brand_name'=>$brandName,'type_id'=>$typeId,'type_name'=>$typeName,'finish_time'=>$data['create_time']];
                                if(is_array($result))
                                {
                                    if(key_exists('order_info',$result))
                                    {
                                        $tmp = $result['order_info'];
                                        if(key_exists($orderId,$tmp))
                                        {
                                            $tmp1 = $tmp[$orderId];
                                            if(is_array($tmp1))
                                            {
                                                if(key_exists('total_amount',$tmp1))
                                                {
                                                    //订单的结算金额
                                                    $result['order_info'][$orderId]['total_amount'] += $divideAmount;
                                                }else{
                                                    $result['order_info'][$orderId]['total_amount'] = $divideAmount;
                                                }
                                            }
                                        }
                                    }
                                    //汇总当前订单的预计结算总金额
                                    if(key_exists('total_amount',$result))
                                    {
                                        //总的结算金额
                                        $result['total_amount'] +=$divideAmount;
                                    }else{
                                        $result['total_amount'] =$divideAmount;
                                    }
                                }
                        }
                        }
                    }
                }//汇总订单金额
                if(key_exists('order_total_amount',$result))
                {
                    $result['order_total_amount'] += $data['cost_real_amount'];
                }else{
                    $result['order_total_amount'] = $data['cost_real_amount'];
                }
            }
        }
        //费用总计
        $costItems =[];
        if($result)
        {
            if(key_exists('order_info',$result))
            {
                foreach ($result['order_info'] as $v)
                {
                    if(is_array($v))
                    {
                        if(key_exists('cost_info',$v))
                        {
                            $cost_info = $v['cost_info'];
                            if(is_array($cost_info))
                            {
                                foreach ($cost_info as $info)
                                {
                                    //将相同cost_id的做累加操作
                                    if(!isset($costItems[$info['cost_id']]['amount'])){
                                        $costItems[$info['cost_id']]['amount'] =$info['divide_amount'];
                                        $costItems[$info['cost_id']]['cost_name'] =$info['cost_name'];
                                        $costItems[$info['cost_id']]['cost_id'] =$info['cost_id'];
                                    }
                                    else
                                    {
                                        $costItems[$info['cost_id']]['amount'] +=$info['divide_amount'];
                                        $costItems[$info['cost_id']]['cost_name'] =$info['cost_name'];
                                        $costItems[$info['cost_id']]['cost_id'] =$info['cost_id'];

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if($costItems)
        {
            $result['cost_info'] = $costItems;
        }else{
            return [];
        } 
        return $result;
    }


    /**
     *
     * @param $srcType
     * @param $srcId
     * @param $accountId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getLatelyOrder($srcType,$srcId,$accountId)
    {
        $where = [
            'src_type'   => $srcType,
            'src_id'     => $srcId,
            'account_id' => $accountId,
            'status'     => [1,2,3,4,7,8]
        ];
        $query = self::find()
            ->where($where)
            ->select('order_no,status')
            ->orderBy('plan_time asc')
            ->limit(1)
            ->asArray()
            ->one();

        if($query){
            return $query;
        }
        return [];
    }

    /**
     * 审核不通过
     * @param $orderNo
     * @param $srcId
     * @param $reason
     * @param $processUserId
     * @return array
     * @author xi
     * @date 2018-6-23
     */
    public static function auditNotThrough($orderNo,$departmentId,$reason,$processUserId)
    {

        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //加上审核日志
            WorkOrderAudit::add($departmentId,$orderNo,2,$processUserId,$reason);
            //改成未通过
            WorkOrderStatus::changeStatus($departmentId,$orderNo,11,$departmentId,$reason);

            $params = [
                'department_id' => $departmentId,
                'user_id'   => $processUserId,
                'reason'    => $reason,
                'accountId' => $processUserId
            ];
            WorkOrderDynamic::add(24,$orderNo,$params);

            $transaction->commit();
            return ['order_no' => $orderNo];
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('操作失败'.$e->getMessage(), 20006);
        }
        $transaction->rollBack();
        return ModelBase::errorMsg('操作失败', 20005);
    }

    /**
     * 修改订单服务时间
     * @param $orderNo
     * @param $plantime
     * @param $planTimeType
     * @return bool
     * @throws \Exception
     * @author xi
     * @date 2018-7-2
     */
    public static function changePlantime($orderNo,$plantime,$planTimeType)
    {
        $model = self::findOne(['order_no'=>$orderNo]);
        if($model)
        {
            $model->plan_time      = $plantime;
            $model->plan_time_type = $planTimeType;
            $model->update_time    = time();
            if($model->save(false)){
                return true;
            }
        }

        throw new \Exception('修改订单服务时间失败',30002);
    }
}
