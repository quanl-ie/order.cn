<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use yii\helpers\ArrayHelper;

class WorkAppraisalDict extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_appraisal_dict';
    }

    /**
     * 星级中文描述
     * @param $star
     * @return mixed|string
     */
    public static function getStarDesc($star)
    {
        $data = [
            1 => '好评',
            2 => '中评',
            3 => '差评'
        ];

        return isset($data[$star])?$data[$star]:'';
    }

    /**
     * 获取好评星级
     * @param $starNum
     * @return int|mixed
     */
    public static function getStar($starNum)
    {
        $data = [
            1 => 3,
            2 => 3,
            3 => 2,
            4 => 2,
            5 => 1
        ];

        return isset($data[$starNum])?$data[$starNum]:0;
    }

    /**
     * 重组技师的勋章数据
     * @param $technicianId
     * @param $directCompanyId
     * @return array
     * @author xi
     */
    public static function getTechnicianIdMedalList($technicianId,$directCompanyId)
    {
        $medalList = WorkAppraisalTechnician::getTechnicianIdMedalList($technicianId);

        $where = [
            'direct_company_id' => $directCompanyId,
            'status' => 1,
            'star'   => 1
        ];
        $allMedalArr = self::findAllByAttributes($where , "id,title,icon,gray_icon");

        $result = [];
        foreach ($allMedalArr as $val)
        {
            if(isset($medalList[$val['id']]))
            {
                $temp = $medalList[$val['id']];
                $result [] = [
                    "title"     => $temp['title'],
                    "icon"      => $temp['icon'],
                    "gray_icon" => $temp['gray_icon'],
                    "count"     => $temp['count']
                ];
            }
            else
            {
                $result [] = [
                    "title"     => $val['title'],
                    "icon"      => $val['icon']!=''?Yii::$app->params['js.uservices.cn'].$val['icon']:'',
                    "gray_icon" => $val['gray_icon'],
                    "count"     => "0"
                ];
            }
        }

        return $result;
    }

    /**
     * 根据星级查标签
     * @param $directCompanyId
     * @param $star
     * @return array
     * @author xi
     */
    public static function getTagByStar($directCompanyId,$star)
    {
        $where = [
            'star'   => $star,
            'status' => 1,
            'direct_company_id' => $directCompanyId
        ];
        $query = self::findAllByAttributes($where,'id,title');
        if($query)
        {
            foreach ($query as $key=>$val){
                $query[$key]['checked'] = false;
            }
            return $query;
        }

        return [];
    }
    /**
     * 根据标签id查询文字描述
     * @param $star
     * @return mixed|string
     */
    public static function getTagDesc($dictId)
    {
        $where = [
            'id'   => $dictId,
            'status' => 1,
        ];
        $query = self::findOneByAttributes($where,'id,title');
        return isset($query['title'])?$query['title']:'';
    }
}