<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderStandard extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_standard';
    }

    /**
     * 标准填加
     * @param $orderNo
     * @param $type
     * @param $typeId
     * @param $standardId
     * @return bool
     */
    public static function add($orderNo,$type,$typeId,$standardId)
    {
        $model = new WorkOrderStandard();
        $model->order_no    = $orderNo;
        $model->type        = $type;
        $model->type_id     = $typeId;
        $model->standard_id = $standardId;
        $model->create_time = time();
        $model->update_time = time();
        if($model->save()){
            return true;
        }
        throw new \Exception(self::className() ."保存失败");
    }
}