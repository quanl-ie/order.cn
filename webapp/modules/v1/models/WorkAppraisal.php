<?php
namespace webapp\modules\v1\models;

use common\models\Department;
use webapp\models\ModelBase;
use Yii;

class WorkAppraisal extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_appraisal';
    }

    /**
     * 星级中文描述
     * @param $star
     * @return mixed|string
     */
    public static function getStarDesc($star)
    {
        $data = [
            1 => '好评',
            2 => '中评',
            3 => '差评'
        ];

        return isset($data[$star])?$data[$star]:'';
    }

    /**
     * 订单评价状态  2 已评价  1 未评价
     * @param $orderNo
     * @return bool
     * @author xi
     */
    public static function appraisalStatus($orderNo)
    {
        $where = [
            'order_no'         => $orderNo,
            'appraisal_status' => 1,
            'status'           => 1
        ];
        $query = self::findOneByAttributes($where,'id');
        if($query){
            return 1;
        }
        return 2;
    }

    /**
     * 获取待评价工单
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getWaitAppraisalWork($orderNo)
    {
        $where = [
            'a.order_no'       => $orderNo,
            'a.appraisal_status' => 1,
            'b.status'         => 1,
            'a.cancel_status'  => 1,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as b')
            ->innerJoin(['`'.Work::tableName().'` as a'], 'a.work_no = b.work_no')
            ->where($where)
            ->select("a.work_no,a.work_stage,b.work_finish_time")
            ->asArray()->all();

        if($query){
            return $query;
        }

        return [];
    }

    /**
     * 我的评价
     * @param $userIds
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getMyAppraisal($userIds,$page,$pageSize)
    {
        $where = [
            'a.user_id' => $userIds,
            'a.user_type' => 1
        ];
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(['`'.Work::tableName().'` as b'], 'a.work_no = b.work_no');
        $db->where($where);

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum > 0)
        {
            if ($pageSize <= 0) {
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum / $pageSize);
            if ($page < 1) {
                $page = 1;
            }

            $db->select(" a.order_no,b.work_stage,a.star_num,a.star,a.create_time,a.`desc`");
            $db->orderBy("a.create_time desc");
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $query = $db->asArray()->all();

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $query
            ];
        }

        return [
            'page'       => $page,
            'totalCount' => $totalNum,
            'totalPage'  => 0,
            'list'       => []
        ];
    }
    /**
     * 获取工单评价状态
     * @param $workNo
     * @return array
     * @author  sxz
     * @date 2018-10-12
     */
    public static function getAppraisalStatus($workNo)
    {
        $where = [
            'work_no' => $workNo,
            'status'  => 1,
        ];
        $query = self::findAllByAttributes($where,'work_no,appraisal_status','work_no');
        if($query){
            foreach ($query as $key=>$val){
                $query[$key] = $val['appraisal_status'];
            }
        }
        return $query;
    }
    /**
     * 获取工单评价信息
     * @param $workNo
     * @return array
     * @author  sxz
     * @date 2018-10-12
     */
    public static function getAppraisalData($workNo)
    {
        $where = [
            'work_no'           => $workNo,
            'status'            => 1,
            'appraisal_status'  => 2
        ];
        $query = self::findAllByAttributes($where,'*','work_no');
        $workNos = array_column($query,'work_no');
        $tagArr =  WorkAppraisalTag::find()->where(['work_no'=>$workNos])->asArray()->all();
        $tags= [];
        foreach ($tagArr as $key => $val) {
            $tagArr[$key]['tag_desc'] = WorkAppraisalDict::getTagDesc($val['tag_id']);
        }
        foreach ($tagArr as $key => $val) {
            $tags[$val['work_no']][] = isset($val['tag_desc'])?$val['tag_desc']:'';
        }
        foreach($query as $k => $v){
            $query[$k]['tag_arr'] = isset($tags[$v['work_no']])?$tags[$v['work_no']]:[];
            $query[$k]['star_desc'] = WorkAppraisalDict::getStarDesc($v['star']);
            $query[$k]['create_time'] = isset($v['create_time'])?date('Y-m-d H:i:s',$v['create_time']):'';
        }
        return $query;
    }

    /**
     * 填加未评价数据
     * @param $workNo
     * @param $orderNo
     * @param $finishTime
     * @param $departmentId
     * @author xi
     */
    public static function add($orderNo,$workNo,$finishTime,$departmentId)
    {
        $count = WorkAppraisal::find()->where(['work_no'=>$workNo])->count();
        if($count == 0)
        {
            $model = new WorkAppraisal();
            $model->department_id     = $departmentId;
            $model->direct_company_id = Department::getDirectCompanyId($departmentId);
            $model->order_no          = $orderNo;
            $model->work_no           = $workNo;
            $model->star_num          = 0;
            $model->star              = 0;
            $model->desc              = '';
            $model->user_id           = 0;
            $model->user_type         = 0;
            $model->wechat_openid     = '';
            $model->wechat_nickname   = '';
            $model->create_time       = time();
            $model->work_finish_time  = $finishTime;
            $model->status            = 1;
            $model->appraisal_status  = 1;
            if($model->save())
            {
                $technicianQuery = WorkRelTechnician::find()
                    ->where(['work_no'=>$workNo,'is_self'=>[1,3],'type'=>1])
                    ->select('technician_id')
                    ->asArray()->all();

                if($technicianQuery)
                {
                    foreach ($technicianQuery as $technicianVal)
                    {
                        $technicianDepartmentId = 0;
                        $technicianArr = Technician::find()->where(['id'=>$technicianVal['technician_id']])->select('store_id')->asArray()->one();
                        if($technicianArr){
                            $technicianDepartmentId = $technicianArr['store_id'];
                        }

                        $technicianModel = new WorkAppraisalTechnician();
                        $technicianModel->work_no       = $workNo;
                        $technicianModel->technician_id = $technicianVal['technician_id'];
                        $technicianModel->technician_department_id = $technicianDepartmentId;
                        $technicianModel->save();
                    }
                }
            }
        }
    }

}