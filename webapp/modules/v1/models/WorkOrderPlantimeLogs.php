<?php
namespace webapp\modules\v1\models;

use common\models\PushMessage;
use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use Yii;

class WorkOrderPlantimeLogs extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_plantime_logs';
    }

    /**
     * 修改服务时间
     * @param $orderNo
     * @param $srcType
     * @param $srcId
     * @param $plantime
     * @param $userId
     * @return bool
     * @author  xi
     * @date 2018-1-31
     */
    public static function changePlantime($orderNo,$departmentId,$plantime,$plantimeType,$userId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();

        try
        {
            $model = new self();
            $model->department_id = $departmentId;
            $model->order_no      = $orderNo;
            $model->plan_time     = $plantime;
            $model->user_id       = $userId;
            $model->create_time   = time();
            $model->update_time   = time();
            if($model->save())
            {
                $orderInfo = WorkOrder::findOne(['order_no'=>$orderNo]);
                $oldPlantime = $orderInfo->plan_time;
                $orderInfo->plan_time = $plantime;
                $orderInfo->plan_time_type = $plantimeType;
                $orderInfo->update_time = time();
                $res = $orderInfo->save(false);
                if($res)
                {
                    //修改工单的时间
                    $workModel = Work::findOne(['order_no'=>$orderNo,'status'=>[1,2,6],'cancel_status'=>1]);
                    if($workModel)
                    {
                        $workModel->plan_time = $plantime;
                        $workModel->plan_time_type = $plantimeType;
                        $workModel->update_time = time();
                        $workModel->save(false);
                    }

                    $transaction->commit();
                    //修改时间动态填加
                    $params = [
                        'accountId' => $userId,
                        'departmentId' => $departmentId,
                        'planTime'      => $plantime,
                        'planTimeType' => $plantimeType
                    ];
                    WorkOrderDynamic::add(11,$model->order_no,$params);
                    @PushMessage::push($model->order_no,"ed");

                    //技师排班
                    if($workModel){
                        ScheduleTechnicianQueue::push($workModel->work_no,6, date('Y-m-d',$plantime), date('Y-m-d',$oldPlantime ));
                    }

                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
        }
        return ModelBase::errorMsg('修改服务时间失败',20008);
    }
}