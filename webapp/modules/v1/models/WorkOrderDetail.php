<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use webapp\modules\v1\models\SaleOrder;
use yii\helpers\ArrayHelper;

class WorkOrderDetail extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_detail';
    }
    
    /**
     * 更新技师 
     * @param string $orderNo 订单号
     * @param int $technicianId 技师id
     * @return boolean
     * @author xi
     * @date 2017-12-19
     */
    public static function updateTechnician($orderNo,$technicianId)
    {
        $attr = [
            'technician_id' => $technicianId,
            'update_time'   => time()
        ];
        return WorkOrderDetail::updateAll($attr,['order_no'=>$orderNo]);
    }
    
    public static function getClassId($order_no) {
        $orderDetail = self::find()->where(['order_no' => $order_no])->asArray()->one();
        $classId = SaleOrder::getClassId($orderDetail['sale_order_id']);
        return $classId;
    }
    
    /**
     * 获取订单服务产品
     */
    public static function getClassName($order_no) {
        $orderDetail = self::find()->where(['order_no' => $order_no])->asArray()->one();
        $className = SaleOrder::getClassName($orderDetail['sale_order_id']);
        return $className;
    }
    
    /**
     * 获取品牌名称
     */
    public static function getBrandName($order_no) {
        $orderDetail = self::find()->where(['order_no' => $order_no])->asArray()->one();
        $brandName = SaleOrder::getBrandName($orderDetail['sale_order_id']);
        return $brandName;
    }
    public static function getBrandId($order_no) {
        $orderDetail = self::find()->where(['order_no' => $order_no])->asArray()->one();
        $brandName = SaleOrder::getBrandId($orderDetail['sale_order_id']);
        return $brandName;
    }
    
    /**
     * 获取订单根据订单号
     */
    public static function getOrderData($orderNos, $tec_id) {
        $order = self::find()->where(['order_no' => $orderNos])->asArray()->all();
        $orderNo = '';
        foreach ($order as $k => $v) {
            if($v['technician_id'] == $tec_id) {
                $orderNo = $v['order_no'];
                break;
            }
        }
        return $orderNo;
    }
    

    


    /**
     * 根据订单号查出订单信息
     * @param $orderNos
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findAllSaleOrderId($orderNos,$limit=1000)
    {
        $result = [];
        $where = [
            'order_no'   => $orderNos,
        ];
        $query = self::find()
            ->where($where)
            ->select('order_no,sale_order_id')
            ->limit($limit)
            ->asArray()->all();
        if($query)
        {
            return ArrayHelper::index($query,'order_no');
        }
        return $result;
    }
    
    
    /**
     * 根据订单号查询服务内容
     * @author  li
     * @param   array  $orderIds
     */
    public static function getSaleOrder($orderNos) {
        $saleOrder = self::find()->where(['order_no' => $orderNos])->asArray()->all();
        $serviceContentArr = $saleOrderArr = $saleOrderIds = [];
        foreach ($saleOrder as $v) {
            $saleOrderIds[] = $v['sale_order_id'];
        }
        $serviceContent = SaleOrder::getServiceContent($saleOrderIds);
        foreach ($saleOrder as $k => $v) {
            $saleOrderArr[$v['order_no']] = $v;
        }
        foreach($saleOrderArr as $k => $v) {
            if(isset($serviceContent[$v['sale_order_id']])) {
                $saleOrderArr[$k]['service_content'] = $serviceContent[$v['sale_order_id']]['content'];
            }
        }
        return $saleOrderArr;
    }

    /**
     * 查出订单详情的id
     * @param $orderNo
     * @return int|mixed
     */
    public static function getDetailId($orderNo)
    {
        $query = self::findOneByAttributes(['order_no'=>$orderNo,'is_del'=>1],'id');
        if($query){
            return $query['id'];
        }
        return 0;
    }
}