<?php
namespace webapp\modules\v1\models;

use Yii;
use webapp\models\ModelBase;
class ServiceFlow extends ModelBase
{


    public static function tableName()
    {
        return 'service_flow';
    }

    /**
     * 获取第一个服务流程
     * @param $serviceProvideId
     * @return int|mixed
     */
    public static function getFirstOne($serviceProvideId)
    {
        $where = [
            'src_type' => ModelBase::SRC_FWS,
            'src_id'   => $serviceProvideId,
            'status'   => 1
        ];
        $query = self::find()
            ->where($where)
            ->select('*')
            ->orderBy('id asc')
            ->asArray()->one();
        if($query)
        {
            return $query['id'];
        }
        return 0;
    }
    /**
     * 获取服务流程
     * @param $serviceProvideId
     * @return int|mixed
     * 2018-5-8
     */
    public static function getFlowOne($serviceProvideId,$workType)
    {
        $where = [
            'src_type' => ModelBase::SRC_FWS,
            'src_id'   => $serviceProvideId,
            'status'   => 1,
            'type_id'  =>$workType
        ];
        $query = self::find()
            ->where($where)
            ->select('*')
            ->orderBy('id asc')
            ->asArray()->one();
        if($query)
        {
            return $query['id'];
        }
        return 0;
    }

}