<?php
namespace webapp\modules\v1\models;

use webapp\models\BaseModel;
use webapp\models\ModelBase;
use Yii;

class WorkProcess extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_process';
    }

    /**
     * 获取服务中、已完成的时间
     * @param $workNo
     * @return array
     * @author  xi
     * @date 2017-12-22
     */
    public static function getTypeTimeData($workNo,$type)
    {
        $where = [
            'work_no' => $workNo,
            'type' => $type
        ];
        $query = self::findAllByAttributes($where,'work_no,create_time','work_no');
        if($query){
            foreach ($query as $key=>$val){
                $query[$key] = $val['create_time'];
            }
        }
        return $query;
    }

    /**
     * 获取原因
     * @param $workNo
     * @param $type
     * @return array
     */
    public static function getReason($workNo,$type)
    {
        $where = [
            'work_no' => $workNo,
            'type' => $type
        ];
        $query = self::findAllByAttributes($where,'work_no,comment','work_no');
        if($query){
            foreach ($query as $key=>$val){
                $query[$key] = $val['comment'];
            }
        }
        return $query;
    }

    /**
     * 工单过程添加
     * @param $workNo
     * @param $departmentId
     * @param $userId
     * @param $type
     * @param $comment
     * @param string $img1
     * @param string $img2
     * @return bool
     * @author xi
     * @date 2018-2-4
     */
    public static function add($workNo,$departmentId,$operatorUserId,$operatorUserType,$type,$comment,$img1='',$img2='')
    {
        $model = new self();
        $model->work_no            = $workNo;
        $model->department_id      = $departmentId;
        $model->operator_user_id   = $operatorUserId;
        $model->operator_user_type = $operatorUserType;
        $model->type               = $type;
        $model->comment            = $comment;
        $model->img1               = $img1;
        $model->img2               = $img2;
        $model->create_time        = time();
        $model->update_time        = time();
        if($model->save()){
            return true;
        }

        throw new \Exception("WorkProcess Add failure ",30002);
    }

    /**
     * 取消订单加上过程
     * @param $orderNo
     * @param $departmentId
     * @param $processUserId
     * @throws \Exception
     */
    public static function cancelOrderAddProcess($orderNo,$departmentId,$processUserId,$cancelReason)
    {
        $where = [
            'order_no'      => $orderNo,
            'cancel_status' => 1
        ];
        $query = Work::findAllByAttributes($where);
        foreach ($query as $val) {
            self::add($val['work_no'], $departmentId, $processUserId, 1, 10, $cancelReason);
        }
    }

    public static function getOne($where,$select="*",$group='',$order=''){
        $db = self::find();
        $db->from(self::tableName());
        $db->select($select);
        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        if($group){
            $db->groupBy($group);
        }
        if($order){
            $db->orderBy($order);
        }
        $db->asArray();
        return $db->all();
    }
}