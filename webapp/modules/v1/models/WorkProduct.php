<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class WorkProduct extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_product';
    }

    /**
     * 根据工单查出产品记录
     * @param $workNos
     * @return array
     */
    public static function findAllByWorkNos($workNos)
    {
        $query = self::findAllByAttributes(['work_no'=>$workNos,'status'=>1]);
        if($query)
        {
            $result = [];
            foreach ($query as $val)
            {
                $result[$val['work_no']][] = [
                    'product_name' => $val['product_name'],
                    'sn' => $val['sn']
                ];
            }
            return $result;
        }
        return [];
    }

    /**
     * 根据工单号查出产品记录
     * @param $workNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($workNo)
    {
        $db = self::find();
        $db->where(['work_no'=>$workNo,'status'=>1]);
        $db->select('id,work_no,product_name,sn');
        $db->orderBy('create_time asc');
        $query = $db->asArray()->all();
        return $query;
    }

    /**
     * 根据工单号查出产品记录
     * @param $workNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListByWorkNos(array $workNos)
    {
        $result = [];

        $db = self::find();
        $db->where(['work_no'=>$workNos,'status'=>1]);
        $db->select('id,work_no,product_name,sn');
        $db->orderBy('create_time asc');
        $query = $db->asArray()->all();

        if($query)
        {
            foreach ($query as $val)
            {
                $result[$val['work_no']][] = $val;
            }
        }

        return $result;
    }

    /**
     * 填加
     * @param $workNo
     * @param $productName
     * @param $sn
     * @return bool
     */
    public static function add($workNo,$productName,$sn)
    {
        $model = new self();
        $model->work_no      = $workNo;
        $model->product_name = $productName;
        $model->sn           = $sn;
        $model->status       = 1;
        $model->create_time  = time();
        $model->update_time  = time();
        if($model->save()){
            return [
                'id' => $model->id
            ];
        }
        return ModelBase::errorMsg('填加失败',20005);
    }

    /**
     * 修改
     * @param $id
     * @param $technicianId
     * @param $productName
     * @param $sn
     * @return array
     */
    public static function edit($id,$technicianId,$productName,$sn)
    {
        $model = self::findOne(['id'=>$id]);
        if($model)
        {
            $workJs = WorkRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'work_no'=>$model->work_no,'type'=>1]);
            if(!$workJs){
                return ModelBase::errorMsg('非法操作',20007);
            }

            $model->product_name = $productName;
            $model->sn           = $sn;
            $model->status       = 1;
            $model->update_time  = time();
            if($model->save())
            {
                return [
                    'id' => $model->id
                ];
            }
        }
        return ModelBase::errorMsg('编辑失败',20008);
    }

    /**
     * 删除
     * @param $id
     * @param $technicianId
     * @return array
     */
    public static function del($id,$technicianId)
    {
        $model = self::findOne(['id'=>$id]);
        if($model)
        {
            $workJs = WorkRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'work_no'=>$model->work_no,'type'=>1]);
            if(!$workJs){
                return ModelBase::errorMsg('非法操作',20007);
            }

            $model->status       = 0;
            $model->update_time  = time();
            if($model->save())
            {
                return [
                    'id' => $model->id
                ];
            }
        }
        return ModelBase::errorMsg('删除失败',20008);
    }

}