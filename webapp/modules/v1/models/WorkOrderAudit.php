<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderAudit extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_audit';
    }

    /**
     * 查出审核未通过原因
     * @param $orderNo
     * @return mixed|string
     * @author xi
     */
    public static function getAuditFailedReason($orderNo)
    {
        $where = [
            'order_no'     => $orderNo,
            'audit_status' => 2
        ];
        $query = self::findOneByAttributes($where,'reason');
        if($query){
            return $query['reason'];
        }

        return '';
    }

    /**
     * 填加
     * @param $departmentId
     * @param $orderNo
     * @param $status
     * @param $processUserId
     * @param $reason
     * @return bool
     * @author xi
     */
    public static function add($departmentId,$orderNo,$status,$processUserId,$reason)
    {
        $auditModel = new WorkOrderAudit();
        $auditModel->department_id = $departmentId;
        $auditModel->order_no      = $orderNo;
        $auditModel->audit_status  = $status;
        $auditModel->audit_user_id = $processUserId;
        $auditModel->reason        = $reason;
        $auditModel->create_time   = time();
        if($auditModel->save()){
            return true;
        }

        throw new \Exception(self::className() ." 填加失败");
    }
}