<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class Technician extends ModelBase
{
    public static function tableName()
    {
        return 'technician';
    }
    

    /**
     * 获取技师信息
     */
    public static function getTechnicianNames($techIds) {
        $techInfos = self::find()->where(['id' => $techIds])->asArray()->all();
        $techArr = [];
        foreach ($techInfos as $k => $v) {
            $techArr[$v['id']] = $v['name'];
        }
        return $techArr;
    }
}