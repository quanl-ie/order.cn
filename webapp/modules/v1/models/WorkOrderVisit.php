<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderVisit extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_visit';
    }

    /**
     * 回访
     * @param $orderNo
     * @param $srcType
     * @param $srcId
     * @param $content
     * @param $userId
     * @return array
     */
    public static function visit($orderNo,$srcType,$srcId,$content,$userId)
    {
        $transaction = Yii::$app->order_db->beginTransaction();

        try
        {
            $model = new self();
            $model->src_type    = $srcType;
            $model->src_id      = $srcId;
            $model->user_id     = $userId;
            $model->order_no    = $orderNo;
            $model->content     = $content;
            $model->status      = 1;
            $model->create_time = time();
            $model->update_time = time();
            if($model->save()){
                $res = WorkOrder::updateAll(['visit_status'=>1,'update_time'=>time()],['order_no'=>$orderNo]);
                if($res)
                {
                    //动态填加
                    $params = [
                        'accountId' => $userId,
                        'sjId'      => $srcId,
                        'reason'    => $content,
                    ];
                    WorkOrderDynamic::add(WorkOrderDynamic::TYPE_SJ,17,$orderNo,$params);

                    $transaction->commit();
                    return [
                        'order_no' => $orderNo
                    ];
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
        }
        $transaction->rollBack();
        return ModelBase::errorMsg('回访失败',20008);
    }
}