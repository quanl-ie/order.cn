<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class WorkXcx extends Work
{



    /**
     * 获取工单列表
     * @param $orderNo
     * @return array
     * @author xi
     * @date 2018-2-11
     */
    public static function getListByOrderNo($orderNo)
    {
        $result = [];
        $query = self::findAllByAttributes(['order_no' => $orderNo]);
        if($query)
        {
            $workNos = array_column($query,'work_no');
            //开始服务时间
            $startServiceTime = WorkProcess::getTypeTimeData($workNos,4);
            //用户总付费金额
            $userPayArr = WorkCost::getUserPayByWorkNos($workNos);
            //技师信息
            $technicianArr = WorkRelTechnician::findTechnicianIdsByWorkNos($workNos);

            $workStageIds = array_column($query,'work_stage');
            $workStageArr = ServiceFlow::findAllByAttributes(['id'=>$workStageIds],'id,service_flow_name','id');


            foreach ($query as $val)
            {
                $result[] = [
                    'order_no'           => $val['order_no'],
                    'work_no'            => $val['work_no'],
                    'status'             => $val['status'],
                    'status_desc'        => $val['cancel_status']==2?'已关闭':self::getStatusDesc($val['status']),
                    'work_stage'         => isset($workStageArr[$val['work_stage']])?$workStageArr[$val['work_stage']]['service_flow_name']:'',
                    'plan_time'          => $val['plan_time'],
                    'start_service_time' => isset($startServiceTime[$val['work_no']])?$startServiceTime[$val['work_no']]:'',
                    'technician_id'      => isset($technicianArr[$val['work_no']])?$technicianArr[$val['work_no']]:0,
                    'user_pay_sum'       => isset($userPayArr[$val['work_no']])?sprintf('%.2f',round($userPayArr[$val['work_no']],2)):0,
                ];
            }
        }
        return $result;
    }
}