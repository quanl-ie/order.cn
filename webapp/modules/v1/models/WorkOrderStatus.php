<?php
namespace webapp\modules\v1\models;

use webapp\models\ModelBase;
use Yii;
use yii\base\Exception;

class WorkOrderStatus extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_status';
    }

    /**
     * 订单状态信息 (1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getStatus($index=false)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '待验收',
            5 => '已完成',
            6 => '已取消',
            7 => '验收未通过',
            8 => '待指派',
            9 => '待服务',
            10 => '待审核',
            11 => '审核未通过',
            12 => '待收款',
            13 => '等待上级验收'
        ];
        if($index!=false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }

    /**
     * 添加
     * @param $orderNo
     * @param $departmentId
     * @param $status
     * @return bool
     * @author xi
     * @date 2018-6-15
     */
    public static function add($orderNo,$departmentId,$status,$operationDepartmentId=0)
    {
        $where = [
            'order_no'      => $orderNo,
            'department_id' => $departmentId
        ];
        $model = self::findOne($where);
        if($model)
        {
            $model->status = 1;
            $model->update_time   = time();
            $model->operation_department_id = $operationDepartmentId;
            if($model->save()){
                return true;
            }
        }
        else
        {
            $model = new self();
            $model->department_id = $departmentId;
            $model->order_no      = $orderNo;
            $model->status        = $status;
            $model->create_time   = time();
            $model->update_time   = time();
            $model->operation_department_id = $operationDepartmentId;
            if($model->save()){
                return true;
            }
        }
        throw new \Exception(self::className() ."保存失败");
    }

    /**
     * 更改订单状态
     * @param $departmentId
     * @param $orderNo
     * @param $status
     * @return bool
     * @throws \Exception
     * @author xi
     */
    public static function changeStatus($departmentId,$orderNo,$status,$operationDepartmentId,$remarks = '')
    {
         $model = self::find()->where(['department_id' => $departmentId,'order_no' => $orderNo])->orderBy(['id'=> SORT_DESC])->one();
         if($model)
         {
             $model->status      = $status;
             $model->update_time = time();
             $model->remarks     = $remarks;
             $model->operation_department_id = $operationDepartmentId;
             if($model->save()){
                 return true;
             }
         }
         else
         {
             $model = new self();
             $model->department_id = $departmentId;
             $model->order_no      = $orderNo;
             $model->status        = $status;
             $model->create_time   = time();
             $model->update_time   = time();
             $model->remarks       = $remarks;
             $model->operation_department_id = $operationDepartmentId;
             if($model->save()){
                 return true;
             }
         }

        throw new \Exception(self::className() ."更改状态失败");
    }

    /**
     * 驳回
     * @param $orderNo
     * @param $serviceDepartmentId
     * @return bool
     * @throws \Exception
     * @author xi
     */
    public static function reject($orderNo,$serviceDepartmentId,$remarks)
    {
        $where = [
            'order_no' => $orderNo,
            'type'     => 1,
            'service_department_id' => $serviceDepartmentId,
        ];
        $assignArr = WorkOrderAssign::findOneByAttributes($where,'department_id');
        if($assignArr)
        {
            self::changeStatus($assignArr['department_id'],$orderNo,8,$serviceDepartmentId,$remarks);
            self::changeStatus($serviceDepartmentId,$orderNo,6,$serviceDepartmentId,$remarks);

            return true;
        }

        throw new \Exception(self::className() ."操作失败",30002);
    }

    /**
     * 批量更改订单状态
     * @param $orderNo
     * @param $status
     * @return bool
     * @throws \Exception
     * @author xi
     */
    public static function batchChangeStatus($orderNo,$status,$remarks = '')
    {

        //如果已取消的 不需要改变成已完成

        if(self::updateAll(['status'=> $status ,'remarks' => $remarks ,'update_time' => time()],"order_no =".$orderNo." and status != 6")){
            return true;
        }
    }

    /**
     * 把下一级的也变成待上级验收
     * @param $orderNo
     * @param $departmentId
     * @throws \Exception
     */
    public static function acceptanceCheck($orderNo,$departmentId,$id = 0)
    {
        $where = "order_no = '$orderNo' and department_id = $departmentId and type = 1 and id <> $id";
        $orderAssignArr = WorkOrderAssign::findOneByAttributes($where);
        if($orderAssignArr)
        {
            $where = [
                'department_id' => $orderAssignArr['service_department_id'],
                'order_no'      => $orderNo,
                'status'        => 4
            ];
            $attr = [
                'status' => 13,
                'operation_department_id'=>$departmentId,
                'update_time' => time()
            ];
            self::updateAll($attr,$where);
            self::acceptanceCheck($orderNo,$orderAssignArr['service_department_id'],$orderAssignArr['id']);
        }
    }

    /**
     * 把其他机构状态改成不可以操作
     * @param $orderNo
     * @param $excludeDepartmentId
     * @return bool
     */
    public static function batchChangeShowBtn($orderNo,$excludeDepartmentId)
    {
        if(self::updateAll(['is_show_btn'=> 2 ,'update_time' => time()], "order_no = '$orderNo' and department_id <> $excludeDepartmentId")){
            return true;
        }
        return false;
    }

    /**
     * 查出订单的状态
     * @param $departmentId
     * @param $orderNos
     * @return array
     * @author xi
     */
    public static function getOrderStatus($departmentId,$orderNos)
    {
        $result = [];
        if($departmentId && $orderNos)
        {
            $where = [
                'department_id' => $departmentId,
                'order_no' => $orderNos
            ];
            $query = self::find()
                    ->where($where)
                    ->select('order_no,status')
                    ->asArray()->all();

            if($query){
                $result = array_column($query,'status','order_no');
            }

        }

        return $result;
    }
    /**
     * 查出订单的状态
     * @param $status
     * @param $is_department
     * @param $department_ids
     * @return array
     * @author q
     */
    public static function getOrderCount($status,$is_department,$department_ids)
    {
        if($status && !empty($department_ids))
        {
            if($is_department == 0){
                $where = [
                    'department_id' => $department_ids,
                    'status' => $status
                ];
            }else{
                $where = [
                    'create_user_id' => $department_ids,
                    'status' => $status
                ];
            }
            //var_dump($where);exit;
            $query = self::find()
                ->where($where)
                ->select('id')
                ->groupBy('order_no')
                ->asArray()->count();

            return $query;
        }
        return 0;
    }
}
