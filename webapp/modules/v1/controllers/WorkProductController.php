<?php
namespace webapp\modules\v1\controllers;

use webapp\models\ModelBase;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkOrder;
use webapp\modules\v1\models\WorkProduct;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v1\models\WorkVisit;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkProductController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Work';

    /**
     * 列表
     * @return array
     */
    public function actionSearch()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }

        return WorkProduct::getList($workNo);
    }

    /**
     * 填加
     * @author xi
     * @date 2018-2-2
     */
    public function actionAdd()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        $sn     = trim(Yii::$app->request->post('sn',''));
        $productName = trim(Yii::$app->request->post('product_name',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($workNo == 0){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($sn == ''){
            return ModelBase::errorMsg('参数 sn 不能为空',20003);
        }
        if($productName == ''){
            return ModelBase::errorMsg('参数 product_name 不能为空',20004);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('参数technician_id不能为空 ',20006);
        }
        $workJs = WorkRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'work_no'=>$workNo,'type'=>1]);
        if(!$workJs){
            return ModelBase::errorMsg('非法操作',20007);
        }

        return WorkProduct::add($workNo,$productName,$sn);
    }

    /**
     * 修改
     * @author xi
     * @date 2018-2-2
     */
    public function actionEdit()
    {
        $id     = intval(Yii::$app->request->post('id'));
        $sn     = trim(Yii::$app->request->post('sn',''));
        $productName = trim(Yii::$app->request->post('product_name',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($id == 0){
            return ModelBase::errorMsg('参数 id 不能为空',20002);
        }
        if($sn == ''){
            return ModelBase::errorMsg('参数 sn 不能为空',20003);
        }
        if($productName == ''){
            return ModelBase::errorMsg('参数 product_name 不能为空',20004);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('参数technician_id不能为空 ',20006);
        }

        return WorkProduct::edit($id,$technicianId,$productName,$sn);
    }

    /**
     * 删除
     * @author xi
     * @date 2018-2-2
     */
    public function actionDel()
    {
        $id     = intval(Yii::$app->request->post('id'));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($id == 0){
            return ModelBase::errorMsg('参数 id 不能为空',20002);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('参数technician_id不能为空 ',20006);
        }

        return WorkProduct::del($id,$technicianId);
    }

}