<?php
namespace webapp\modules\v1\controllers;

use common\models\CostRule;
use common\models\CostRuleRelation;
use common\models\Product;
use webapp\models\ModelBase;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkCost;
use webapp\modules\v1\models\WorkCostDetail;
use webapp\modules\v1\models\WorkOrder;
use webapp\modules\v1\models\WorkProduct;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v1\models\WorkVisit;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkCostController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Work';

    /**
     * 列表
     * @return array
     */
    public function actionSearch()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }

        return WorkCost::getList($workNo);
    }
    /**
     * 填加
     * @author xi
     * @date 2018-2-2
     */
    public function actionAdd()
    {
        $processUserId = intval(Yii::$app->request->post('process_user_id', 0));
        $workNo = trim(Yii::$app->request->post('work_no', ''));
        $payerId = intval(Yii::$app->request->post('payer_id', ''));
        $costType = intval(Yii::$app->request->post('cost_type', 0));
        $comment = trim(Yii::$app->request->post('comment', ''));
        $voucherImg = Yii::$app->request->post('voucher_img', '');
        $product        = Yii::$app->request->post('product',[]);
        if ($processUserId == 0) {
            return ModelBase::errorMsg('操作人不能为空', 20012);
        }
        if ($workNo == '') {
            return ModelBase::errorMsg('参数 work_no 不能为空', 20002);
        }
        if ($payerId == 0) {
            return ModelBase::errorMsg('参数 payer_id 不能为空', 20003);
        }
        if(empty($product)){
            return ModelBase::errorMsg('参数 product 不能为空', 20004);
        }
        //开启事务
        $transaction = Work::getDb()->beginTransaction();
        try {
            $workArr = Work::findOneByAttributes(['work_no' => $workNo], 'department_id,order_no');
            if (!$workArr) {
                return ModelBase::errorMsg('工单号不存在', 20010);
            }
            $model = new WorkCost();
            $model->department_id = $workArr['department_id'];
            $model->process_user_id = $processUserId;
            $model->order_no = $workArr['order_no'];
            $model->work_no = $workNo;
            $model->payer_id = $payerId;
            $model->cost_id = $product[0]['prod_id'];
            $model->cost_number = $product[0]['prod_num'];
            $model->cost_type = $costType;
            $model->cost_price =  $product[0]['cost_price'];
            $model->cost_amount = $product[0]['prod_num']*$product[0]['cost_price'];
            $model->cost_real_amount = $product[0]['prod_num']*$product[0]['cost_price'];
            $model->comment = $comment;
            $model->voucher_img = $voucherImg ? (is_array($voucherImg) ? implode(',', $voucherImg) : $voucherImg) : '';
            $model->create_time = time();
            $model->update_time = time();

            //如果是商家付费
            if ($payerId == 1) {
                $workOrderArr = WorkOrder::findOneByAttributes(['order_no' => $workArr['order_no']]);
                if ($workOrderArr) {
                    $ruleItemArr = CostRuleRelation::getCostRuleItemInfo($workArr['department_id'], $workOrderArr['department_id'], $workArr['order_no'], $product[0]['prod_id']);
                    if ($ruleItemArr) {
                        $model->cri_cost_type = $ruleItemArr['cost_type'];
                        $model->cri_fixed_amount = $ruleItemArr['fixed_amount'];
                        $model->cri_pecent = $ruleItemArr['pecent'];
                        $model->cri_id = $ruleItemArr['id'];
                    }
                }
            }
            if ($model->save()) {
                foreach ($product as $key=>$val)
                {
                    $value[$key][] = $model->id;
                    $value[$key][] = $val['prod_id'];
                    $value[$key][] = isset($val['prod_name'])? $val['prod_name']:'';
                    $value[$key][] = isset($val['serial_number'])? $val['serial_number']:'';
                    $value[$key][] = $val['prod_num'];
                    $value[$key][] = $val['cost_price'];
                    $value[$key][] = $val['cost_price']*$val['prod_num'];
                    $value[$key][] = $val['cost_price']*$val['prod_num'];
                    $value[$key][] = isset($val['prod_unit'])?$val['prod_unit']:'';
                    if(isset($val['isRecycle']) && $val['isRecycle']){
                        $value[$key][] = $val['recycle_prod_id'];
                        $value[$key][] = $val['recycle_prod_name'];
                    }else{
                        $value[$key][] = 0;
                        $value[$key][] = '';
                    }
                }
                $keys = ['work_cost_id','prod_id','prod_name','serial_number','prod_num','cost_price','cost_amount','cost_real_amount','prod_unit','recycle_prod_id','recycle_prod_name'];
                $connection = \Yii::$app->order_db;
                $res = $connection->createCommand()->batchInsert(WorkCostDetail::tableName(), $keys, $value)->execute();
                if($res){
                    $transaction->commit();
                    return [
                        'id' => $model->id
                    ];
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            //echo $e->getMessage();exit
            return ModelBase::errorMsg('添加失败', 20012);
        }
        return ModelBase::errorMsg('添加失败', 20011);
    }
        /**
     * 修改
     * @author xi
     * @date 2018-2-2
     */
    public function actionEdit()
    {
        $id             = intval(Yii::$app->request->post('work_cost_id'));
        $payerId        = intval(Yii::$app->request->post('payer_id',''));
        $comment        = trim(Yii::$app->request->post('comment',''));
        $voucherImg     = trim(Yii::$app->request->post('voucher_img',''));
        $costType      = intval(Yii::$app->request->post('cost_type','0'));
        $product        = Yii::$app->request->post('product',[]);

        if($id == 0){
            return ModelBase::errorMsg('参数 id 不能为空',20013);
        }
        if($payerId == 0){
            return ModelBase::errorMsg('参数 payer_id 不能为空',20003);
        }
        if(empty($product)){
            return ModelBase::errorMsg('参数 product 不能为空 ',20008);
        }
        //开启事务
        $transaction = Work::getDb()->beginTransaction();
        try
        {
            $model = WorkCost::findOne(['id'=>$id]);
            if($model)
            {
                $model->payer_id         = $payerId;
                $model->cost_id = $product[0]['prod_id'];
                $model->cost_number = $product[0]['prod_num'];
                $model->cost_price =  $product[0]['cost_price'];
                $model->cost_amount = $product[0]['prod_num']*$product[0]['cost_price'];
                $model->cost_real_amount = $product[0]['prod_num']*$product[0]['cost_price'];
                $model->cost_type        = $costType;
                if($comment)
                    $model->comment      = $comment;
                $model->voucher_img      = $voucherImg;
                $model->update_time      = time();
                //如果是商家付费
                if ($payerId == 1) {
                    $workArr = Work::findOneByAttributes(['work_no' => $model->work_no], 'department_id,order_no');
                    $workOrderArr = WorkOrder::findOneByAttributes(['order_no' => $workArr['order_no']]);
                    if ($workOrderArr) {
                        $ruleItemArr = CostRuleRelation::getCostRuleItemInfo($workArr['department_id'], $workOrderArr['department_id'], $workArr['order_no'], $product[0]['prod_id']);
                        if ($ruleItemArr) {
                            $model->cri_cost_type = $ruleItemArr['cost_type'];
                            $model->cri_fixed_amount = $ruleItemArr['fixed_amount'];
                            $model->cri_pecent = $ruleItemArr['pecent'];
                            $model->cri_id = $ruleItemArr['id'];
                        }
                    }
                }
                if($model->save()){
                    WorkCostDetail::deleteAll(['work_cost_id'=>$id]);
                    foreach ($product as $key=>$val)
                    {
                        $value[$key][] = $model->id;
                        $value[$key][] = $val['prod_id'];
                        $value[$key][] = isset($val['prod_name'])? $val['prod_name']:'';
                        $value[$key][] = isset($val['serial_number'])? $val['serial_number']:'';
                        $value[$key][] = $val['prod_num'];
                        $value[$key][] = $val['cost_price'];
                        $value[$key][] = $val['cost_price']*$val['prod_num'];
                        $value[$key][] = $val['cost_price']*$val['prod_num'];
                        $value[$key][] = isset($val['prod_unit'])?$val['prod_unit']:'';
                        if(isset($val['isRecycle']) && $val['isRecycle']){
                            $value[$key][] = $val['recycle_prod_id'];
                            $value[$key][] = $val['recycle_prod_name'];
                        }else{
                            $value[$key][] = 0;
                            $value[$key][] = '';
                        }
                    }
                    $keys = ['work_cost_id','prod_id','prod_name','serial_number','prod_num','cost_price','cost_amount','cost_real_amount','prod_unit','recycle_prod_id','recycle_prod_name'];
                    $connection = \Yii::$app->order_db;
                    $connection->createCommand()->batchInsert(WorkCostDetail::tableName(), $keys, $value)->execute();
                    $transaction->commit();
                    return [
                        'id' => $id
                    ];
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            //echo $e->getMessage();exit
            return ModelBase::errorMsg('编辑失败',20010);
        }
        return ModelBase::errorMsg('编辑失败',20011);
    }

    /**
     * 删除
     * @author xi
     * @date 2018-2-2
     */
    public function actionDel()
    {
        $id      = intval(Yii::$app->request->post('id'));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));

        if($id == 0){
            return ModelBase::errorMsg('参数 id 不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('参数 process_user_id 不能为空 ',20005);
        }

        $model = WorkCost::findOne(['id'=>$id,'status'=>1]);
        if($model)
        {
            $model->status       = 0;
            $model->update_time  = time();
            if($model->save())
            {
                return [
                    'id' => $model->id
                ];
            }
            else {
                return ModelBase::errorMsg('删除失败',20007);
            }
        }
        return ModelBase::errorMsg('删除失败',20006);
    }

    /**
     * 收费项目详情
     * @return array|NULL|string|\yii\db\ActiveRecord
     */
    public function actionDetail()
    {
        $id = intval(Yii::$app->request->post('id'));
        if ($id == 0) {
            return ModelBase::errorMsg('参数 id 不能为空', 20002);
        }

        $query = WorkCost::findOneByAttributes(['id'=>$id]);
        if($query)
        {
            if(trim($query['voucher_img'])!=''){
                $query['voucher_img'] = explode(',',$query['voucher_img']);
            }
            unset($query['create_time'],$query['update_time'],$query['status']);

            //如果是备件查出信息
            if($query['cost_type'] == 2)
            {
                $detailArr = WorkCostDetail::findAllByAttributes(['work_cost_id' => $query['id']]);
                $query['partsArr'] = $detailArr;
            }

            return $query;
        }
        return [];
    }

    /**
     * 统计收费总金额
     * @return array
     */
    public function actionTotalAmount()
    {
        $orderNo = Yii::$app->request->post('order_no');
        $payerId = Yii::$app->request->post('payer_id',1);
        if ($orderNo == '') {
            return ModelBase::errorMsg('参数 order_no 不能为空', 20002);
        }

        $result = WorkCost::totalAmountByWorkNos($orderNo,$payerId);
        return $result;
    }

}
