<?php
namespace webapp\modules\v1\controllers;

use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkOrder;
use Yii;
use webapp\controllers\ApiBaseController;
use webapp\modules\v1\models\WorkOrderXcx;

class OrderXcxController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Order';



    public function actionList()
    {
        $accountId = intval(Yii::$app->request->post('account_id',0));
        $srcType   = intval(Yii::$app->request->post('src_type',0));
        $srcId     = intval(Yii::$app->request->post('src_id',0));

        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pagesize',10));

        if($accountId == 0){
            return ModelBase::errorMsg('参数 account_id 不能为空',20003);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 不能为空',20004);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('参数 src_id 不能为空',20005);
        }

        $where =[
            ['a.account_id', '=', $accountId],
            ['a.src_type', '=', $srcType],
            ['a.src_id','=',$srcId],
            ['a.del_status','=',1]
        ];

        return WorkOrderXcx::getList($where,$page,$pageSize);
    }

    /**
     * 取消订单
     * @author xi
     * @date 2018-03-21
     */
    public static function actionCancel()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $cancelReason = trim(Yii::$app->request->post('cancel_reason',''));
        $accountId    = intval(Yii::$app->request->post('account_id',0));
        $cancelStatus = intval(Yii::$app->request->post('cancel_status',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($cancelReason == ''){
            return ModelBase::errorMsg('取消订单原因不能为空',20003);
        }
        if($accountId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if(!in_array($cancelStatus,[1,2,3,4,5,6,7,8,9])){
            return ModelBase::errorMsg('取消类型取值范围只能为(1,2,3,4,5,6,7,8,9)', 20005);
        }

        return WorkOrderXcx::cancelOrder($orderNo,$accountId,$cancelReason,$cancelStatus);
    }

    /**
     * 订单详情
     * @author xi
     * @date 2018-03-21
     */
    public function actionDetail()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $srcType   = intval(Yii::$app->request->post('src_type',0));
        $srcId     = intval(Yii::$app->request->post('src_id',0));
        $accountId = intval(Yii::$app->request->post('account_id',0));

        if($accountId == 0){
            return ModelBase::errorMsg('用户id不能为空',20005);
        }
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if($srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }

        $result = WorkOrderXcx::getOrderDetail($orderNo,$srcType,$srcId,$accountId);

        return $result;
    }

    /**
     * 查出服务记录及收费项目
     * @author xi
     */
    public function actionViewServiceLogAndCost()
    {
        $workNo    = trim(Yii::$app->request->post('work_no',''));
        $accountId = intval(Yii::$app->request->post('account_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20003);
        }
        if($accountId == 0){
            return ModelBase::errorMsg('帐户id不能为空',20004);
        }

        return WorkOrderXcx::serviceLogAndCost($workNo,$accountId);
    }

    /**
     * 订单删除
     * @author xi
     */
    public function actionDel()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $accountId = intval(Yii::$app->request->post('account_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($accountId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }

        return WorkOrderXcx::del($orderNo,$accountId);
    }

    /**
     * 根据订单号查询订单信息
     */
    public function actionGetOrderData() {
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        return WorkOrderXcx::getOrderData($orderNo) ;
    }

}