<?php
namespace webapp\modules\v1\controllers;

use common\helpers\QueuePush;
use common\models\Department;
use common\models\ScheduleTechnician;
use common\models\ScheduleTechnicianQueue;
use webapp\models\ModelBase;
use webapp\modules\v1\models\Message;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkAppraisal;
use webapp\modules\v1\models\WorkCostDivide;
use webapp\modules\v1\models\WorkOrder;
use webapp\modules\v1\models\WorkOrderDynamic;
use webapp\modules\v1\models\WorkOrderStatus;
use webapp\modules\v1\models\WorkOrderTrade;
use webapp\modules\v1\models\WorkProcess;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v1\models\WorkVisit;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkPcController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Work';

    /**
     * 全部订单，未完成、已完成订单列表
     * @return array
     */
    public function actionSearch()
    {
        $departmentId  = Yii::$app->request->post('department_id',0);
        $workNo        = trim(Yii::$app->request->post('work_no',''));
        $accountId     = Yii::$app->request->post('account_ids',0);
        $workType      = intval(Yii::$app->request->post('work_type',0));
        $saleOrderId   = Yii::$app->request->post('sale_order_id');
        $planStartTime = intval(Yii::$app->request->post('plan_start_time',0));
        $planEndTime   = intval(Yii::$app->request->post('plan_end_time',0));
        $technicianId  = Yii::$app->request->post('technician_id');
        $status        = Yii::$app->request->post('status',0);
        $cancelStatus  = intval(Yii::$app->request->post('cancel_status',1));
        $visitStatus   = intval(Yii::$app->request->post('visit_status',0));
        $isFirst       = Yii::$app->request->post('is_first',-1);
        $settlementStatus = intval(Yii::$app->request->post('settlement_status',0));
        $confirmStatus = intval(Yii::$app->request->post('confirm_status',0));
        $sort          = intval(Yii::$app->request->post('sort',0));
        $page          = intval(Yii::$app->request->post('page',1));
        $pageSize      = intval(Yii::$app->request->post('pageSize',10));

        if( !$departmentId ){
            return ModelBase::errorMsg('参数 department_id 不能为空',20003);
        }

        if(is_array($departmentId)){
            $where []= ['a.department_id','in',$departmentId];
        }
        else {
            $where []= ['a.department_id','=',$departmentId];
        }

        if($workNo != ''){
            $where [] = ['a.work_no','=' , $workNo];
        }
        if($accountId){
            if(is_array($accountId)){
                $where [] = ['b.account_id','in',$accountId];
            }
            else {
                $where [] = ['b.account_id','=',$accountId];
            }
        }
        if($workType){
            $where[] = ['b.work_type','=',$workType];
        }
        if($saleOrderId)
        {
            if(is_array($saleOrderId)){
                $where[] = ['c.sale_order_id','in',$saleOrderId];
            }
            else {
                $where[] = ['c.sale_order_id','=',$saleOrderId];
            }
        }
        if($planStartTime){
            $where[] = ['a.plan_time','>=',$planStartTime];
        }
        if($planEndTime){
            $where[] = ['a.plan_time','<=',$planEndTime];
        }
        if($technicianId)
        {
            $workTelArr = WorkRelTechnician::findAllByAttributes(['technician_id'=>$technicianId,'type'=>1],'work_no');
            if($workTelArr){
                $workNos = array_column($workTelArr,'work_no');
                $workNos = array_unique($workNos);
                $where [] = ['a.work_no','in' , $workNos];
            }
            else {
                $where [] = ['a.work_no','=' , -1];
            }
        }
        if($status){
            if(is_array($status)){
                $where[] = ['a.status','in',$status];
            }
            else {
                $where[] = ['a.status','=',$status];
            }
        }
        if($visitStatus){
            $where[] = ['a.visit_status','=',$visitStatus];
        }
        if($isFirst && $isFirst !=-1){
            if(is_array($isFirst)){
                $where[] = ['a.is_first','in',$isFirst];
            }
            else {
                $where[] = ['a.is_first','=',$isFirst];
            }
        }
        if($settlementStatus>0){
            $where[] = ['a.settlement_status','=',$settlementStatus];
        }
        if($cancelStatus){
            $where[] = ['a.cancel_status','=',$cancelStatus];
        }
        if($confirmStatus>0){
            $where[] = ['a.confirm_status','=',$confirmStatus];
        }

        return Work::getPcList($where,$page,$pageSize,$sort);
    }

    /**
     * 修改工单时间
     * @return array
     * @author xi
     * @date 2018-2-3
     */
    public function actionChangePlantime()
    {
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $departmentId        = intval(Yii::$app->request->post('department_id',0));
        $plantime     = intval(Yii::$app->request->post('plantime',0));
        $plantimeType = intval(Yii::$app->request->post('plan_time_type',5));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }

        if($departmentId == 0){
            return ModelBase::errorMsg('下单源departmentId不能为空!', 20005);
        }
        if($plantime == 0){
            return ModelBase::errorMsg('服务时间不能为空',20006);
        }
        return Work::changePlantime($workNo,$departmentId,$processUserId,$plantime,$plantimeType);
    }

    /**
     * 关闭工单
     * @author xi
     * @date 2018-2-4
     */
    public function actionClose()
    {
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $reason       = trim(Yii::$app->request->post('reason',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20005);
        }
        if($reason == ''){
            return ModelBase::errorMsg('关闭原因不能为空',20006);
        }

        return Work::close($workNo,$departmentId,$processUserId,$reason);
    }

    /**
     * 指派
     * @author xi
     * @date 2018-2-4
     */
    public function actionAssign()
    {
        $workNo               = trim(Yii::$app->request->post('work_no',''));
        //是谁操作的这个订单
        $processUserId        = intval(Yii::$app->request->post('process_user_id',0));
        $departmentTopId      = intval(Yii::$app->request->post('department_top_id',0));
        $departmentId         = intval(Yii::$app->request->post('department_id',0));
        $plantime             = intval(Yii::$app->request->post('plantime',0));
        $plantimeType         = intval(Yii::$app->request->post('plan_time_type',5));

        //操作类型  13 服务商 11 技师
        $opUserType          = intval(Yii::$app->request->post('op_user_type',0));
        $technicianId        = Yii::$app->request->post('technician_id',0);
        //technician_type 1技师 2组长
        $technicianType      = intval(Yii::$app->request->post('technician_type',0));
        $technicianLeader    = intval(Yii::$app->request->post('technician_leader',0));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }
   /*     if($departmentTopId == 0){
            return ModelBase::errorMsg('参数 departmentTopId 传入不正确',20004);
        }*/
        if($departmentId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20005);
        }
        if($opUserType == ModelBase::SRC_FWS && $plantime == 0){
            return ModelBase::errorMsg('服务时间不能为空',20006);
        }
        if($opUserType == ModelBase::SRC_FWS && $plantime <= time()){
            return ModelBase::errorMsg('服务时间不能小于当前时间',20007);
        }
        if(!$technicianId){
            return ModelBase::errorMsg('技师id不能为空',20008);
        }
        if(!in_array($technicianType,[1,2])){
            return ModelBase::errorMsg('参数technician_type取范围不正确',20012);
        }
        if($technicianType == 1 && $technicianLeader == 0){
            return ModelBase::errorMsg('工单负责人不能为空',20013);
        }
        if($technicianType == 2 && count($technicianId)!=1){
            return ModelBase::errorMsg('只能指派一位组长',20014);
        }

        return Work::assign($workNo,$departmentTopId,$departmentId,$opUserType,$processUserId,$plantime,$plantimeType,$technicianId,$technicianType,$technicianLeader);
    }

    /**
     * 改指派
     * @author xi
     * @date 2018-2-4
     */
    public function actionReAssign()
    {
        $workNo               = trim(Yii::$app->request->post('work_no',''));
        $orderNo              = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId        = intval(Yii::$app->request->post('process_user_id',0));
        $departmentTopId      = intval(Yii::$app->request->post('department_top_id',0));
        $departmentId         = intval(Yii::$app->request->post('department_id',0));
        $plantime             = intval(Yii::$app->request->post('plantime',0));
        $plantimeType         = intval(Yii::$app->request->post('plan_time_type',5));
        $reason               = trim(Yii::$app->request->post('reason',''));

        //操作类型  13 服务商 11 技师
        $opUserType       = intval(Yii::$app->request->post('op_user_type',0));
        $technicianId     = Yii::$app->request->post('technician_id',0);
        $technicianType   = intval(Yii::$app->request->post('technician_type',0));
        $technicianLeader = intval(Yii::$app->request->post('technician_leader',0));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }

        if($departmentId == 0){
            return ModelBase::errorMsg('下单源departmentId不能为空!', 20005);
        }
        if($opUserType == ModelBase::SRC_FWS && $plantime == 0){
            return ModelBase::errorMsg('服务时间不能为空',20006);
        }
        if($opUserType == ModelBase::SRC_FWS && $plantime <= time()){
            return ModelBase::errorMsg('服务时间不能小于当前时间',20007);
        }
        if(!$technicianId){
            return ModelBase::errorMsg('技师id不能为空',20008);
        }
//        if($reason == ''){
//            return ModelBase::errorMsg('改派原因不能为空',20009);
//        }
        if(!in_array($technicianType,[1,2])){
            return ModelBase::errorMsg('参数technician_type取范围不正确',20013);
        }
        if($technicianType == 1 && $technicianLeader == 0){
            return ModelBase::errorMsg('工单负责人不能为空',20014);
        }
        if($technicianType == 2 && count($technicianId)!=1){
            return ModelBase::errorMsg('只能指派一位组长',20015);
        }

        return Work::reAssign($workNo,$departmentTopId,$departmentId,$opUserType,$processUserId,$plantime,$plantimeType,$technicianId,$reason,$technicianType,$technicianLeader);
    }

    /**
     * 工单回访
     */
    public function actionVisit()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $srcType      = intval(Yii::$app->request->post('src_type',0));
        $srcId        = intval(Yii::$app->request->post('src_id',0));
        $content      = trim(Yii::$app->request->post('content',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20003);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 传入不正确',20005);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20006);
        }

        return WorkVisit::visit($orderNo,$workNo,$srcType,$srcId,$content,$processUserId);
    }

    /**
     * 完成服务 finsh-service
     * @author xi
     * @date 2018-2-2
     */
    public function actionFinshService()
    {
        //workNo有可能是订单有可能是工单( 1 是工单 2 订单 )
        $type         = intval(Yii::$app->request->post('type',1));
        $flag         = intval(Yii::$app->request->post('flag',1));
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $departmentIds = Yii::$app->request->post('department_ids','');

        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));

        $workImg      = Yii::$app->request->post('work_img','');
        $sceneImg     = Yii::$app->request->post('scene_img','');
        $serviceRecord= trim(Yii::$app->request->post('service_record',''));

        //如果传过来的是订单，查出进行中的工单
        if($type == 2)
        {
            $workArr = Work::findOneByAttributes(['order_no'=>$workNo,'status'=>3],'work_no');
            if(!$workArr){
                return ModelBase::errorMsg('未找到进行中的工单',20003);
            }
            else {
                $workNo = $workArr['work_no'];
            }
        }

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20012);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20005);
        }

        $transaction = Work::getDb()->beginTransaction();
        try
        {
            $where = [
                'work_no'       => $workNo,
                //'department_id' => $departmentId,
                'cancel_status' => 1
            ];
            $model = Work::findOne($where);

            if($model)
            {
                $model->work_img    = is_array($workImg)?implode(',',$workImg):$workImg;
                $model->scene_img   = is_array($sceneImg)?implode(',',$sceneImg):$sceneImg;
                $model->status      = 4;
                $model->update_time = time();
                $model->service_record = $serviceRecord;
                $model->visit_status   = 2;
                $model->confirm_status   = 1;
                if($model->save(false))
                {
                    //工单过程加上
                    WorkProcess::add($workNo,$departmentId,$processUserId,1,5,'完成服务');
                    
                    //调队列处理技师排版
                    $query = WorkRelTechnician::findTechnicianIdsByWorkNos([$workNo]);
                    if($query)
                    {
                        $tid = $query[$workNo];
                        QueuePush::sendPush('increase','tech-schedule','increase',[$tid,4,Department::getDepartmentTopId($departmentId),$departmentId]);
                    }

                    //如果是上级  改变下级的订单状态
                    if(!empty($departmentIds))
                    {
                        //查出已取消的机构
                        $cancelOrderArr = WorkOrderStatus::findAllByAttributes(['order_no'=>$model->order_no,'department_id'=>$departmentIds,'status'=>6],'department_id','department_id');

                        foreach ($departmentIds as $val)
                        {
                            if(!isset($cancelOrderArr[$val])){
                                WorkOrderStatus::changeStatus($val,$model->order_no,4,$departmentId);
                            }
                        }
                    }else{
                        WorkOrderStatus::changeStatus($departmentId,$model->order_no,4,$departmentId);
                    }
                    //下级机构代替技师完成服务动态填加
                    $workRelTechId = WorkRelTechnician::findOneByAttributes(['is_self'=>[1,3],'work_no'=>$workNo,'type'=>1],'technician_id');
                    $params = [
                        'accountId' => $processUserId,
                    ];
                    WorkOrderDynamic::add(14,$model->order_no,$params,$workNo);

                    //如果是pc 后台操作完成服务，把支付方式改成现金支付
                    if($type == 2){
                        WorkOrderTrade::savePayInfo(3,$workNo,0,date('Y-m-d H:i:s'));
                    }

                    //技师排班
                    ScheduleTechnicianQueue::push($workNo,4,date('Y-m-d'), date('Y-m-d', $model->plan_time));

                    //给评价表加上一条未评价的数据
                    WorkAppraisal::add($model->order_no,$model->work_no,$model->update_time,$model->department_id);

                    $transaction->commit();
                    return [
                        'work_no' => $workNo
                    ];
                }
                else {
                    $transaction->commit();
                    return ModelBase::errorMsg('操作失败',20007);
                }
            }
            else {
                $transaction->commit();
                return ModelBase::errorMsg('操作失败',20008);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('操作失败',20009);
        }
        return ModelBase::errorMsg('操作失败',20010);
    }

    /**
     * 工单详情
     * @author xi
     * @date 2018-2-5
     */
    public function actionDetail()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $departmentId = intval(Yii::$app->request->post('department_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20004);
        }

        return Work::getDetail($workNo,$departmentId);
    }

    /**
     * 工单回访查看信息
     * @return array
     */
    public function actionViewVisit()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        $srcType      = intval(Yii::$app->request->post('src_type',0));
        $srcId        = intval(Yii::$app->request->post('src_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 传入不正确',20003);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20004);
        }

        $where = [
            'src_type' => $srcType,
            'src_id'   => $srcId,
            'work_no'  => $workNo,
            'status'   => 1
        ];
        $visitArr = WorkVisit::findOneByAttributes($where,'content');
        if($visitArr){
            return $visitArr;
        }
        return [];
    }

    /**
     * 统计
     * @author xi
     * @date 2018-2-12
     */
    public function actionTongji()
    {
        $serviceProviderId = intval(Yii::$app->request->post('service_provider_id',0));
        if($serviceProviderId == 0){
            return ModelBase::errorMsg('服务商id 不能为空!');
        }

        return WorkOrder::fwsTongji($serviceProviderId);
    }

    /**
     * 技师分成修改
     * @author xi
     */
    public function actionCostDivideEdit()
    {
        $id           = intval(Yii::$app->request->post('id',0));
        $divideAmount = intval(Yii::$app->request->post('divide_amount',0));
        $comment      = trim(Yii::$app->request->post('comment',''));

        if($id <=0){
            return ModelBase::errorMsg('参数 id 不能为空',20002);
        }
        if($divideAmount <=0){
            return ModelBase::errorMsg('参数 divide_amount 不能为空',20003);
        }

        $model = WorkCostDivide::findOne(['id'=>$id]);
        if($model){
            $model->divide_amount = $divideAmount;
            $model->comment = $comment;
            $model->update_time = time();
            if($model->save()){
                return ['id'=>$id];
            }
        }
        return ModelBase::errorMsg('修改失败',20005);
    }

    /**
     * 结算确认
     * @return array
     */
    public function actionConfirmSettlement()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }

        $where = [
            'work_no' => $workNo,
            'confirm_status' => 1
        ];
        $model = Work::findOne($where);
        if($model)
        {
            $model->confirm_status =2;
            $model->settlement_status = 1;
            $model->update_time = time();
            if($model->save()){
                return [
                    'work_no' => $workNo
                ];
            }
        }
        return ModelBase::errorMsg('结算确认失败', 20003);
    }

    /**
     * 查出技师未完成工单及总数
     * @return array
     * @author xi
     */
    public function actionGetTechnicianWork()
    {
        $technicianIds = Yii::$app->request->post('technician_ids');
        $plantime      = Yii::$app->request->post('plantime');
        if(!$technicianIds){
            return ModelBase::errorMsg('技师id 不能为空', 20002);
        }
        if(!$plantime){
            return ModelBase::errorMsg('服务时间不能为空', 20003);
        }

        return WorkRelTechnician::getTechnicianWorkTongji($technicianIds,$plantime);
    }

    /**
     * 判断服务时间是否冲突
     * @author xi
     * @date 2018-4-28
     */
    public function actionHasPlantimeConflict()
    {
        $technicianId = intval(Yii::$app->request->post('technician_id'));
        $plantime     = intval(Yii::$app->request->post('plantime',0));
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        if(!$technicianId){
            return ModelBase::errorMsg('技师id 不能为空', 20002);
        }
        if(!$plantime){
            return ModelBase::errorMsg('服务时间不能为空', 20003);
        }
        if(!$workNo){
            return ModelBase::errorMsg('工单号不能为空', 20004);
        }

        return WorkRelTechnician::hasPlantimeConflict($technicianId,$plantime,$workNo);
    }

    /**
     * 查询分组下未完成的工单
     * @param  int  $group_id   分组ID
     * @return  json
     * @date  2018-05-08
     * @author  li
     */
    public function actionSreachGroupWork()
    {
        $group_id = Yii::$app->request->post('techician_ids', '');
        if(!$group_id) {
            return ModelBase::errorMsg('分组id 不能为空', 20002);
        }
        return Work::getGroupWork($group_id);
    }

    /**
     * 开始服务
     * @author xi
     * @date 2018-6-21
     */
    public function actionBeginService()
    {
        $type           = intval(Yii::$app->request->post('type',1));
        $workNo         = trim(Yii::$app->request->post('work_no',''));
        $departmentId   = intval(Yii::$app->request->post('department_id',0));
        $operatorUserId = intval(Yii::$app->request->post('operator_user_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20002);
        }
        if($operatorUserId == ''){
            return ModelBase::errorMsg('参数 operator_user_id 不能为空',20002);
        }

        //如查 type == 2 说明他传过来的是工单
        if($type == 2)
        {
            $workArr = Work::findOneByAttributes(['order_no'=>$workNo,'status' => [2,6],'cancel_status'=>1],'work_no,status');
            if(!$workArr) {
                return ModelBase::errorMsg('未找到工单不能操作', 20005);
            }
            else
            {
                $workNo = $workArr['work_no'];
                //如果指派给组长，查一下组长是否指派了技师
                if($workArr['status'] == 6){
                    $workRelTechRes = WorkRelTechnician::findOneByAttributes(['is_self'=>[1,3],'work_no'=>$workNo],'id');
                    if(!$workRelTechRes){
                        return ModelBase::errorMsg('组长未指派技师不可以开始服务!', 20006);
                    }
                }
            }
        }
        else
        {
            //根据工单查询订单号
            $workArr = Work::findOneByAttributes(['work_no' => $workNo]);
            if(!$workArr) {
                return ModelBase::errorMsg('未找到工单不能操作', 20003);
            }
            else if($workArr['status'] !=2) {
                return ModelBase::errorMsg('只有待服务中的工单才可以开始', 20004);
            }
        }
        
        $workRelTechId = WorkRelTechnician::findOneByAttributes(['is_self'=>[1,3],'work_no'=>$workNo,'type'=>1],'technician_id');

        return Work::pcBeginService($workNo,$departmentId,$operatorUserId,$workRelTechId);
    }

    /**
     * 查出技师未完成的工单
     * @author xi
     * @date 2018-6-30
     */
    public function actionTechnicianNoFinshWork()
    {
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        if($technicianId <= 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空');
        }

        return WorkRelTechnician::technicianNoFinshWork($technicianId);
    }
}