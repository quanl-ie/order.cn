<?php
namespace webapp\modules\v1\controllers;

use Codeception\Lib\Generator\Helper;
use common\helpers\QueuePush;
use common\models\BaseModel;
use common\models\Common;
use common\models\Department;
use common\models\Order;
use common\models\ScheduleTechnicianQueue;
use PhpParser\Node\Expr\AssignOp\Mod;
use webapp\models\ModelBase;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkCost;
use webapp\modules\v1\models\WorkOrder;
use webapp\modules\v1\models\WorkOrderAssign;
use webapp\modules\v1\models\WorkOrderDynamic;
use webapp\modules\v1\models\WorkOrderStandard;
use webapp\modules\v1\models\WorkOrderStatus;
use webapp\modules\v1\models\WorkOrderTrade;
use webapp\modules\v1\models\WorkProcess;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v1\models\WorkVisit;
use common\models\Technician;
use common\models\Manufactor;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Work';

    /**
     * 全部订单，未完成、已完成订单列表
     * @param  int   $type      1、我的工单  2、小组工单
     * @param  int   $status    1、全部  2、待指派  3、服务中   4、已完成  5、待收款   6、已关闭
     * @param  int   $grouping_id  所属小组ID
     * @param  int   $sort         排序方式  asc 正序   desc倒序
     * @return array
     */
    public function actionSearch()
    {
        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pageSize',10));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $type         = intval(Yii::$app->request->post('type',1));
        $status       = intval(Yii::$app->request->post('status',0));
        $sort         = trim(Yii::$app->request->post('sort',''));

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }

        if($type == 1 && $status == 1) {
            return ModelBase::errorMsg('参数 status 与 type 不匹配');
        }

        if(!$status){
            $where = "(b.technician_id = $technicianId and b.is_self <> 2) ";
            if($type == 2) {
                $where = "(b.technician_id = $technicianId and b.is_self = 2) ";
            }
        }else if($status == 1) {//组长待指派
            $where = "(b.technician_id = $technicianId and a.status =6 and a.cancel_status = 1 and b.type = 1) and b.is_self = 2";
        }
        else if($status == 6){//关闭（取消）工单
            if($type == 2) {
                $where = " b.technician_id = $technicianId and (a.cancel_status = 2 or b.type = 2 ) and b.is_self = 2 ";
            }
            else {
                $where = " b.technician_id = $technicianId and (a.cancel_status = 2 or b.type = 2 ) and b.is_self <> 2 ";
            }
        }
        else {
            $where = "(b.technician_id = $technicianId and a.status in( $status) and a.cancel_status = 1 and b.type = 1) and b.is_self <> 2";
            if($type == 2) {
                $where = "(b.technician_id = $technicianId and a.status in( $status) and a.cancel_status = 1 and b.type = 1) and b.is_self = 2";
            }
        }
//        if($status != 1) {
//            $where.= " and a.plan_time > 0";
//        }
        return Work::getList($where,$page,$pageSize,$type,$technicianId,$sort);
    }


    /**
     * 已完成且使用配件的订单
     * @return array
     */
    public function actionSearchWork()
    {
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }
        $where = "(a.technician_id = $technicianId and a.status =4 and a.cancel_status = 1 and b.type = 1)  and a.plan_time > 0";
        return Work::getWorkList($where);
    }


    /**
     * 工作台
     * @author xi
     * @date 2018-2-1
     */
    public function actionWorkbench()
    {
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $service_time = Yii::$app->request->post('service_time','');
        $work_no      = trim(Yii::$app->request->post('work_no',''));

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }
        if($service_time == ''){
            return ModelBase::errorMsg('参数 $service_time 不能为空',20003);
        }
        if($work_no == ''){
            return ModelBase::errorMsg('参数 $work_no 不能为空',20003);
        }
        return Work::getWorkbenchList($technicianId,$service_time,$work_no);
    }

    /**
     * 填加
     * @author xi
     * @date 2018-2-1
     */
    public function actionAdd()
    {
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $workStage    = intval(Yii::$app->request->post('work_stage',0)); //服务阶段
        $planTime     = intval(Yii::$app->request->post('plan_time',0));
        $planTimeType = intval(Yii::$app->request->post('plan_time_type',5));
        $reason       = trim(Yii::$app->request->post('reason',''));
        $reasonDetail = trim(Yii::$app->request->post('reason_detail',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $assignTechnicianType = intval(Yii::$app->request->post('assign_technician_type',0));
        $assignPlantimeType   = intval(Yii::$app->request->post('assign_plantime_type',0));
        $processUserId        = intval(Yii::$app->request->post('process_user_id',0));

        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20002);
        }
        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20003);
        }
        if($workStage == 0){
            return ModelBase::errorMsg('参数 work_stage 不能为空',20004);
        }
        if($planTime == 0){
            return ModelBase::errorMsg('参数 plan_time 不能为空',20005);
        }
        /* if($reason == ''){
             return ModelBase::errorMsg('参数 reason 不能为空',20006);
         }
         if($assignTechnicianType == 0){
             return ModelBase::errorMsg('参数 assign_technician_type 不能为空',20011);
         }*/
        if($assignPlantimeType == 0){
            return ModelBase::errorMsg('参数 assign_plantime_type 不能为空',20012);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('参数 process_user_id 不能为空',200013);
        }

        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $countWork = Work::find()->where(['order_no'=>$orderNo])->count();

            $model = new Work();
            $model->department_id   = $departmentId;
            $model->order_no        = $orderNo;
            $model->work_no         = Work::getWorkNo($orderNo);
            $model->process_user    = $departmentId;
            $model->process_time    = time();
            $model->work_stage      = $workStage;
            $model->plan_time       = $planTime;
            $model->plan_time_type  = $planTimeType;
            $model->name            = '';
            $model->type            = 1;
            $model->level           = 1;
            $model->status          = 1;//先生成工单，再指派技师
            $model->notice_type     = 0;
            $model->reason          = $reason;
            $model->reason_detail   = $reasonDetail;
            $model->create_type     = 1;
            $model->visit_status    = 0;
            $model->create_time     = time();
            $model->update_time     = time();
            $model->is_first        = $countWork == 0?1:2;
            $model->assign_technician_type = $assignTechnicianType;
            $model->assign_plantime_type   = $assignPlantimeType;
            $model->cancel_status   = 1;

            if($model->save())
            {
                //下级机构预约下次上门动态填加
                $params = [
                    'accountId' => $processUserId,
                    'reason'    => (trim($reason) == '其他')?$reasonDetail:$reason,
                    'workStage' => $workStage,
                    'planTime'  => $planTime,
                    'planTimeType' => $planTimeType,
                    'jsId'      => $technicianId
                ];
                WorkOrderDynamic::add(16,$orderNo,$params,$model->work_no);

                //把订单状态改成待指派
                if($model->is_first == 2)
                {
                    //WorkOrderStatus::changeStatus($departmentId,$orderNo,1,$departmentId);
                    WorkOrderStatus::batchChangeStatus($orderNo,1);
                    WorkOrder::changePlantime($orderNo,$planTime,$planTimeType);

                    //把其他机构的操作状态改成不可操作
                    WorkOrderStatus::batchChangeShowBtn($orderNo,$departmentId);
                }

                if(intval($technicianId) >0 ){
                    WorkRelTechnician::add($model->work_no,$technicianId);
                }

                $transaction->commit();
                return [
                    'work_no' => $model->work_no
                ];
            }
            $transaction->rollBack();
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('预约失败'. $e->getMessage(),20009);
        }
        return ModelBase::errorMsg('预约失败',20010);
    }

    /**
     * 工单详情
     * @author xi
     * @date 2018-2-1
     */
    public function actionDetail()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == ''){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }

        return Work::detail($workNo, $technicianId);
    }

    /**
     * 历史服务记录 history-service-list
     * @return array
     * @author xi
     * @date 2018-2-1
     */
    public function actionHistoryServiceList()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20002);
        }

        return Work::historyServiceList($orderNo);
    }

    /**
     * 确认完成 confim-finsh
     * @author xi
     * @date 2018-2-2
     */
    public function actionConfimFinsh()
    {
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        $nextService = intval(Yii::$app->request->post('next_service',0));//是否需要下次上门服务  1 不需要  2 需要
        $workImg     = Yii::$app->request->post('work_img',''); //服务工单图片
        $sceneImg    = Yii::$app->request->post('scene_img',''); //现场拍照图片
        $isScope     = intval(Yii::$app->request->post('is_scope',1)); //质保状态 1 保内 2 保外
        $scopeImg    = Yii::$app->request->post('scope_img',''); //质保凭证图片
        $serviceRecord = trim(Yii::$app->request->post('service_record','')); //工单表（服务记录）
        $assign_technician_type = intval(Yii::$app->request->post('assign_technician_type',1));
        $assign_plantime_type = intval(Yii::$app->request->post('assign_plantime_type',1));
        $process_user = intval(Yii::$app->request->post('process_user',0));
        //生成下次工单参数
        $technicianId = Yii::$app->request->post('technician_id',0);
        $workStage    = intval(Yii::$app->request->post('work_stage',0)); //服务阶段
        $planTime     = intval(Yii::$app->request->post('plan_time',0));
        $planTimeType = intval(Yii::$app->request->post('plan_time_type',5));
        $reason       = trim(Yii::$app->request->post('reason',''));
        $reasonDetail = trim(Yii::$app->request->post('reason_detail',''));
        $demartmentId = intval(Yii::$app->request->post('department_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($nextService == 0){
            return ModelBase::errorMsg('参数next_service 不能为空',20003);
        }
        if($demartmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20009);
        }
        if($nextService == 2)
        {
            if($workStage == 0){
                return ModelBase::errorMsg('服务阶段不能为空',20004);
            }
            if($reason == ''){
                return ModelBase::errorMsg('下次上门原因不能空',20005);
            }
        }

        if(is_array($workImg) && $workImg) {
            $workImg = implode(',', $workImg);
        }
        if(is_array($sceneImg) && $sceneImg) {
            $sceneImg = implode(',', $sceneImg);
        }
        if(is_array($scopeImg) && $scopeImg) {
            $scopeImg = implode(',', $scopeImg);
        }

        if(!$technicianId){
            $technicianId = 0;
        }

        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $workModel = Work::findOne(['work_no'=>$workNo]);
            if($workModel)
            {
                $workModel->work_img  = $workImg;
                $workModel->scene_img = $sceneImg;
                $workModel->service_record = $serviceRecord;
                $workModel->update_time = time();
                $workModel->status = 5;
                $workModel->assign_technician_type = $assign_technician_type;
                $workModel->assign_plantime_type = $assign_plantime_type;
                $workModel->reason          = $reason;
                $workModel->reason_detail   = $reasonDetail;


                if($workModel->save())
                {
                    $orderModel = WorkOrder::findOne(['order_no'=>$workModel->order_no]);
                    if($orderModel)
                    {
                        if($isScope != 0) {
                            $orderModel->is_scope = $isScope;
                        }

                        $orderModel->scope_img = $scopeImg;
                        $orderModel->update_time = time();
                        if($orderModel->save())
                        {
                            //过程表加上完成服务
                            WorkProcess::add($workNo,$demartmentId,$technicianId,2,6,'待收款');

                            //生成工单
                            if($nextService == 2)
                            {
                                $newWorkModel = new Work();
                                $newWorkModel->department_id   = $demartmentId;
                                $newWorkModel->order_no        = $orderModel->order_no;
                                $newWorkModel->work_no         = Work::getWorkNo($orderModel->order_no);
                                $newWorkModel->process_user    = $process_user;
                                $newWorkModel->process_time    = time();
                                $newWorkModel->work_stage      = $workStage;
                                $newWorkModel->plan_time       = $planTime;
                                $newWorkModel->plan_time_type  = $planTimeType;
                                $newWorkModel->name            = '';
                                $newWorkModel->type            = 1;
                                $newWorkModel->level           = 1;
                                $newWorkModel->status          = $technicianId?2:1;
                                $newWorkModel->notice_type     = 0;
                                $newWorkModel->create_type     = 1;
                                $newWorkModel->create_time     = time();
                                $newWorkModel->update_time     = time();
                                $newWorkModel->cancel_status   = 1;
                                $newWorkModel->is_first        = 2;

                                if($assign_technician_type == 3) {
                                    $newWorkModel->status = 6;
                                    $newWorkModel->assign_type = 2;
                                }
                                if($assign_technician_type == 1) {
                                    $newWorkModel->assign_type = 1;
                                }
                                if($newWorkModel->save())
                                {
                                    //如果指派指师改成已指派
                                    if($technicianId > 0){
                                        WorkOrder::updateAll(['is_assign_technician'=>1,'update_time'=>time()] ,['order_no'=>$orderModel->order_no]);
                                    }

                                    //修改订单服务时间
                                    WorkOrder::changePlantime($orderModel->order_no,$planTime,$planTimeType);
                                    //把订单改成服务中

                                    $tmpStatus = $technicianId?2:1;
                                    if($assign_technician_type == 3) {
                                        $tmpStatus = 2;
                                    }
                                    WorkOrderStatus::batchChangeStatus($orderModel->order_no,$tmpStatus);


                                    $reasonArr = [
                                        1 => '技师原因',
                                        2 => '客户需求',
                                        3 => '其他',
                                    ];

                                    //技师预约下次上门动态填加
                                    $params = [
                                        'reason'    => isset($reasonArr[$reason])?$reasonArr[$reason]:$reason,
                                        'workStage' => $workStage,
                                        'planTime'  => $planTime,
                                        'planTimeType' => $planTimeType,
                                        'jsId'      => $process_user
                                    ];
                                    WorkOrderDynamic::add(17,$orderModel->order_no,$params);

                                    if($assign_technician_type == 3) {
                                        $res = WorkRelTechnician::add($newWorkModel->work_no,$process_user,2,ModelBase::SRC_JS,$process_user);
                                    }

                                    if($technicianId)
                                    {
                                        $tempTechnicianId = 0;
                                        foreach ($technicianId as $k => $v)
                                        {
                                            if($v['id'] > 0 ){
                                                $tempTechnicianId = $v['id'];
                                            }

                                            $res = WorkRelTechnician::add($newWorkModel->work_no,$v['id'],$v['is_self'],ModelBase::SRC_JS,$process_user);
                                            if(!$res)
                                            {
                                                $transaction->rollBack();
                                                break;
                                            }
                                        }
                                        if($tempTechnicianId > 0){
                                            WorkRelTechnician::addLeaderTechnician($newWorkModel->work_no,$tempTechnicianId,ModelBase::SRC_JS,$process_user);
                                        }

                                        //查询操作技师的服务商ID
                                        $technicianArr = Technician::find()->where(['id'=>$process_user])->asArray()->one();
                                        $store = Manufactor::find()->where(['id'=>$technicianArr['store_id']])->asArray()->one();
                                        //查询当前技师所在分组的组长
                                        $leader = Common::getGroupingLeader(ModelBase::SRC_FWS, $store['id'], $process_user);
                                        $leader_res = WorkRelTechnician::add($newWorkModel->work_no,$leader,2,ModelBase::SRC_JS,$process_user);
                                        if($res)
                                        {

                                            //技师排班
                                            ScheduleTechnicianQueue::push($newWorkModel->work_no,1,date('Y-m-d',$planTime),date('Y-m-d',$planTime));

                                            $transaction->commit();
                                            return [
                                                'work_no' => $newWorkModel->work_no
                                            ];
                                        }
                                    }
                                    else {
                                        $transaction->commit();
                                        return [
                                            'work_no' => $newWorkModel->work_no
                                        ];
                                    }
                                }
                            }
                            else
                            {
                                WorkOrderStatus::changeStatus($demartmentId,$orderModel->order_no,12,$demartmentId);

                                $transaction->commit();
                                return [
                                    'work_no' => $workNo
                                ];
                            }
                        }
                    }
                }
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('操作失败',20007);
        }
        $transaction->rollBack();
        return ModelBase::errorMsg('操作失败',20008);
    }

    /**
     * 开始服务
     * @author xi
     * @date 2018-2-1
     */
    public function actionBeginService()
    {
        $workNo             = trim(Yii::$app->request->post('work_no',''));
        $technicianId       = intval(Yii::$app->request->post('technician_id',0));
        $department_id      = intval(Yii::$app->request->post('department_id',0));
        $operator_user_type = intval(Yii::$app->request->post('operator_user_type',0));
        $type               = intval(Yii::$app->request->post('type',0));

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }
        if($department_id == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20003);
        }
        if($operator_user_type == 0){
            return ModelBase::errorMsg('参数 operator_user_type 不能为空',20003);
        }
        if($type == 0){
            return ModelBase::errorMsg('参数 type 不能为空',20003);
        }

        //根据工单查询订单号
        $workobj = Work::findOneByAttributes(['work_no' => $workNo]);
        $orderArr = WorkOrderStatus::findOneByAttributes(['order_no' => $workobj['order_no']]);
        if($orderArr['status'] == 6) {
            return ModelBase::errorMsg('工单已关闭，无法继续操作！', 20005);
        }

        //判断用户是否有已经开始的服务
        $workArr = WorkRelTechnician::find()->where(['technician_id' => $technicianId, 'type' => 1])->andWhere('is_self <> 2')->asArray()->all();

        if(!$workArr) {
            return ModelBase::errorMsg('工单已改派，无法继续操作！', 20005);
        }

        $workNos = array_column($workArr,'work_no');
        if(WorkRelTechnician::noFinish($technicianId,$workNos,3) == true) {
            return ModelBase::errorMsg('您还有服务中的工单，请完成后再开始！',20005);
        }

        if(WorkRelTechnician::noFinish($technicianId,$workNos,5) == true) {
            return ModelBase::errorMsg('您还有待收款的工单，请完成后再开始！',20005);
        }

        $workJs = WorkRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'work_no'=>$workNo,'type'=>1]);
        if(!$workJs){
            return ModelBase::errorMsg('非法操作',20004);
        }

        return Work::beginService($workNo,$technicianId,$department_id,$operator_user_type,$type);
    }

    /**
     * 完成服务
     * @author xi
     * @date 2018-2-2
     */
    public function actionFinshService()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        $technicianId   = intval(Yii::$app->request->post('technician_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }

        $workJs = WorkRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'work_no'=>$workNo,'type'=>1]);
        if(!$workJs){
            return ModelBase::errorMsg('非法操作',20004);
        }

        return Work::finshService($workNo,$technicianId);
    }

    /**
     * 编辑服务记录
     * @author xi
     * @date 2018-2-2
     */
    public function actionEditServiceRecord()
    {
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        $isScope     = intval(Yii::$app->request->post('is_scope',0));
        $scopeImg    = trim(Yii::$app->request->post('scope_img',''));
        $workImg     = trim(Yii::$app->request->post('work_img',''));
        $sceneImg    = trim(Yii::$app->request->post('scene_img',''));
        $serviceRecord = trim(Yii::$app->request->post('service_record',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($isScope == 0){
            return ModelBase::errorMsg('质保状态不能为空',20003);
        }

        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            $workModel = Work::findOne(['work_no'=>$workNo,'cancel_status'=>1]);
            if($workModel)
            {
                $workModel->work_img  = $workImg;
                $workModel->scene_img = $sceneImg;
                $workModel->service_record = $serviceRecord;
                $workModel->update_time = time();
                if($workModel->save())
                {
                    $orderModel = WorkOrder::findOne(['order_no'=>$workModel->order_no]);
                    if($orderModel)
                    {
                        if($isScope > 0) {
                            $orderModel->is_scope = $isScope;
                        }

                        $orderModel->scope_img = $scopeImg;
                        $orderModel->update_time = time();
                        if($orderModel->save()){
                            return [
                                'work_order' => $workNo
                            ];
                        }
                    }
                }
            }

            $transaction->rollBack();
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
        }
        return ModelBase::errorMsg('提交失败',20004);
    }

    /**
     * 收费标准信息
     * @return array
     * @author xi
     * @date 2018-2-1
     */
    public function actionStandard()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20002);
        }

        $query = WorkOrderStandard::findOneByAttributes(['order_no'=>$orderNo],'type,type_id,standard_id');
        if($query)
        {
            return $query;
        }
        return [];
    }

    /*
     * 获取技师待结算数据
     * */
    public function actionSettlementWork()
    {
        $begin_time = strtotime(trim(Yii::$app->request->post('begin_time','')));
        $end_time = strtotime(trim(Yii::$app->request->post('end_time','')));
        $src_type = trim(Yii::$app->request->post('src_type',0));
        $src_id = trim(Yii::$app->request->post('src_id',0));

        if($begin_time == ''){
            return ModelBase::errorMsg('开始时间不能为空',20002);
        }
        if($end_time == ''){
            return ModelBase::errorMsg('结束时间不能为空',20002);
        }
        if($src_type == 0){
            return ModelBase::errorMsg('来源类型不能为空',20002);
        }
        if($src_id == 0){
            return ModelBase::errorMsg('来源id不能为空',20002);
        }

        $result = Work::getWorkForSettlement($begin_time,$end_time,$src_type,$src_id);
        return $result;
    }

    /**
     * 添加收费签名
     * @param  string   work_no        工单号
     * @param  int      process_user   操作人ID
     * @param  string   signature      签名图片路径
     * @date    2018-06-22
     * @author  li
     * @return json
     */
    public function actionAddSignature()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $process_user = intval(Yii::$app->request->post('process_user',0));
        $signature    = trim(Yii::$app->request->post('signature',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($process_user == ''){
            return ModelBase::errorMsg('操作人ID不能为空',20002);
        }
        if($signature == ''){
            return ModelBase::errorMsg('签名图片不能为空',20002);
        }

        return Work::addSignature($workNo, $process_user, $signature);
    }

    /**
     * 保存支付信息
     * @author lwy
     */
    public function actionSavePayInfo()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $pay_type     = intval(Yii::$app->request->post('pay_type',0));
        $totalAmount  = trim(Yii::$app->request->post('totalAmount',0));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if(!in_array($pay_type, [0,1])){
            return ModelBase::errorMsg('支付方式不能为空',20003);
        }
        if($totalAmount == 0){
            return ModelBase::errorMsg('支付金额不能为空',20004);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('技师id不能为空',20005);
        }

        return WorkOrderTrade::savePayInfo($pay_type,$workNo,$totalAmount,$technicianId);
    }

    /**
     * 修改支付状态 edit-pay-status
     * @author lwy
     */
    public function actionEditPayStatus()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $trade_no     = trim(Yii::$app->request->post('trade_no',''));
        $trade_status = intval(Yii::$app->request->post('trade_status',0));
        $pay_time     = trim(Yii::$app->request->post('pay_time',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($trade_no == ''){
            return ModelBase::errorMsg('交易流水号不能为空',20002);
        }
        if($pay_time == 0){
            return ModelBase::errorMsg('支付时间不能为空',20002);
        }

        return WorkOrderTrade::editPayInfo($workNo, $trade_no, $trade_status, $pay_time);
    }

    /**
     * 获取工单状态
     * @author xi
     * @date 2018-7-1
     */
    public function actionGetWorkStatus()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }

        $query = Work::findOneByAttributes(['work_no'=>$workNo],'status');
        if($query){
            return $query['status'];
        }

        return ModelBase::errorMsg('未找到工单',20003);
    }
}
