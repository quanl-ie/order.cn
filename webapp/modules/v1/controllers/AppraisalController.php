<?php
namespace webapp\modules\v1\controllers;

use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkAppraisalDict;
use webapp\modules\v1\models\WorkAppraisalTechnician;
use Yii;
use webapp\controllers\ApiBaseController;

class AppraisalController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v1\models\Work';

    /**
     * 获取技师服务评价
     * @return array
     */
    public function actionGetCustomerAppraisalList()
    {
        $technicianId    = intval(Yii::$app->request->post('technician_id',0));
        $page            = intval(Yii::$app->request->post('page',0));
        $pageSize        = intval(Yii::$app->request->post('pagesize',20));
        $showDay         = intval(Yii::$app->request->post('show_day',0));

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }

        //客户评价列表
        $appraisalList = WorkAppraisalTechnician::getCustomerAppraisalList($technicianId,$showDay,$page,$pageSize);

        return $appraisalList;
    }
}