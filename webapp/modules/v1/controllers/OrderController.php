<?php
namespace webapp\modules\v1\controllers;

use common\models\BaseModel;
use common\models\CooporationServiceProvider;
use common\models\CostLevel;
use common\models\Department;
use common\models\User;
use webapp\models\ModelBase;
use webapp\modules\v1\models\CommandCenter;
use webapp\modules\v1\models\CommandCenterStatistics;
use webapp\modules\v1\models\Message;
use webapp\modules\v1\models\WorkOrder;
use webapp\modules\v1\models\WorkOrderAudit;
use webapp\modules\v1\models\WorkOrderDetail;
use webapp\modules\v1\models\WorkOrderDynamic;
use webapp\modules\v1\models\WorkOrderAssign;
use webapp\modules\v1\models\WorkOrderPlantimeLogs;
use webapp\modules\v1\models\WorkOrderProcess;
use webapp\modules\v1\models\WorkOrderStandard;
use webapp\modules\v1\models\WorkOrderStatus;
use webapp\modules\v1\models\WorkOrderTaskHall;
use webapp\modules\v1\models\WorkOrderTaskHallFws;
use webapp\modules\v1\models\WorkOrderVisit;
use webapp\modules\v1\models\WorkProcess;
use webapp\modules\v1\models\WorkRelTechnician;
use Yii;
use webapp\controllers\ApiBaseController;
use webapp\modules\v1\models\AccountAddress;
use webapp\modules\v1\models\ServiceType;
use webapp\modules\v1\models\Work;


class OrderController extends ApiBaseController
{
    public $modelClass = 'webapp\modules\v1\models\Order';

    public function verbs()
    {
        return [
            'search' => ['GET','POST','OPTIONS'],
        ];
    }

    public function actionDy()
    {
        //指派技师参数
        $params = [
            'accountId' => 178,
            'fwsId'     => 29
        ];

        //新建订单动态，不指派给机构或技师
        WorkOrderDynamic::add(1,'1362201806255670',$params);
    }
    /**
     * 列表
     * @return array
     */
    public function actionSearch()
	{
	    $page      = intval(Yii::$app->request->post('page',1));
        $pageSize  = intval(Yii::$app->request->post('pageSize',10));
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $status    = Yii::$app->request->post('status',0);
        $accountId = Yii::$app->request->post('account_ids',0);
        $tecId      = Yii::$app->request->post('tec_ids',0);
        $workStatus = Yii::$app->request->post('work_status',[]);
        $workType  = intval(Yii::$app->request->post('work_type',0));
        $saleOrderId   = Yii::$app->request->post('sale_order_id');
        $planStartTime = intval(Yii::$app->request->post('plan_start_time',0));
        $planEndTime   = intval(Yii::$app->request->post('plan_end_time',0));
        $createStartTime = intval(Yii::$app->request->post('create_start_time',0));
        $createEndTime   = intval(Yii::$app->request->post('create_end_time',0));
        $cancelStatus  = Yii::$app->request->post('cancel_status',-1);
        $ignoreStatus  = intval(Yii::$app->request->post('ignore_status',0));
        $departmentId  = Yii::$app->request->post('department_id',0);
        $technicianId  = Yii::$app->request->post('technician_id',0);
        $serviceTimeType = intval(Yii::$app->request->post('service_time_type',0));
        $visitStatus   = intval(Yii::$app->request->post('visit_status',0));
        $isReceive     = intval(Yii::$app->request->post('is_receive',0));
        $settlementStatus = intval(Yii::$app->request->post('settlement_status',0));
        $createUserId     = intval(Yii::$app->request->post('create_user_id',0));

        $isScope        = intval(Yii::$app->request->post('is_scope',0));
        $addressIds     = Yii::$app->request->post('address_ids');
        $contractId     = intval(Yii::$app->request->post('contract_id',0));  //合同关联id

        //哪个机构搜索
        $srcDepartmentId = Yii::$app->request->post('src_department_id',0);
        //所属公司
        $orderDepartmentId = Yii::$app->request->post('order_department_id',0);
        //评价状态 2018-10-12 新增
        $appraisalStatus   = intval(Yii::$app->request->post('appraisal_status',0));  //1 未评价 2 已评价
        $finishStartTime   = intval(Yii::$app->request->post('finish_start_time',0));
        $finishEndTime     = intval(Yii::$app->request->post('finish_end_time',0));

        $where = [];
        $sqlWhere = '';

        if($departmentId)
        {
            if(is_array($departmentId)){
                $where [] = ['assign.service_department_id','in',$departmentId];
            }
            else {
                $where [] = ['assign.service_department_id','=',$departmentId];
            }
        }
        if($tecId != 0 && !empty($tecId)){
            $res = WorkRelTechnician::findAllByAttributes(['technician_id'=>$tecId,'is_self'=>[1,3],"type"=>1],"work_no");
            if($res){
                $workNo = array_column($res,'work_no');
                if(!empty($workStatus)){
                    $orderNos = Work::findAllByAttributes(['work_no'=>$workNo,'status'=>$workStatus],'order_no');
                }else{
                    $orderNos = Work::findAllByAttributes(['work_no'=>$workNo],'order_no');
                }
                $orderNoArr = array_column($orderNos,'order_no');
                if($orderNo){
                    $orderNoArr[] = $orderNo;
                }
                $orderNo = $orderNoArr;
            }else{
                $orderNo =  $tecId;
            }
        }
        if($orderNo)
        {
            if(is_array($orderNo)){
                $where [] = ['a.order_no','in',$orderNo];
            }
            else {
                $where [] = ['a.order_no','=',$orderNo];
            }
        }
        if($status )
        {
            if(is_array($status)){
                $where[] = ['c.status','in',$status];
            }
            else {
                $where[] = ['c.status','=',$status];
            }
        }
        if($accountId){
            $where[] = ['a.account_id','in',$accountId];
        }

        if($workType){
            $where[] = ['a.work_type','=',$workType];
        }
        if($saleOrderId)
        {
            if(is_array($saleOrderId)){
                $where[] = ['b.sale_order_id','in',$saleOrderId];
            }
            else {
                $where[] = ['b.sale_order_id','=',$saleOrderId];
            }
        }
        if($planStartTime){
            $where[] = ['a.plan_time','>=',$planStartTime];
        }
        if($planEndTime){
            $where[] = ['a.plan_time','<=',$planEndTime];
        }
        if($createStartTime){
            $where[] = ['a.create_time','>=',$createStartTime];
        }
        if($createEndTime){
            $where[] = ['a.create_time','<=',$createEndTime];
        }
        if($cancelStatus !=-1){
            $where[] = ['a.cancel_status','in',$cancelStatus];
        }
        if($ignoreStatus>0){
            $where[] = ["a.ignore_status",'=', $ignoreStatus];
        }
        if($technicianId){
            $where[] = ["work.technician_id",'in',$technicianId];
        }
        if($serviceTimeType>0){
            $where[] = ['a.service_time_type','=',$serviceTimeType];
        }
        if($visitStatus>0){
            $where[] = ['a.visit_status','=',$visitStatus];
        }
        if($isReceive>0){
            $where[] = ['a.is_receive','=',$isReceive];
        }
        if($settlementStatus > 0){
            $where[] = ['a.settlement_status','=',$settlementStatus];
        }
        if($isScope>0){
            $where[] = ['a.is_scope','=',$isScope];
        }
        if($addressIds){
            if(is_array($addressIds)){
                $where[] = ['a.address_id','in',$addressIds];
            }
            else {
                $where[] = ['a.address_id','=',$addressIds];
            }
        }
        //创建人用户id
        if($createUserId){
            $where[] = ['a.create_user_id','=',$createUserId];
        }
        //订单所属公司ids
        if($orderDepartmentId)
        {
            if(is_array($orderDepartmentId)){
                $where[] = ['a.department_id','in',$orderDepartmentId];
            }
            else {
                $where[] = ['a.department_id','=',$orderDepartmentId];
            }
        }
        //合同关联id
        if($contractId){
            $where[] = ['a.contract_id','=',$contractId];
        }
        //评价状态  2018-10-12 新增
        if($appraisalStatus )
        {
            if(is_array($appraisalStatus)){
                $where[] = ['work.appraisal_status','in',$appraisalStatus];
            }
            else {
                $where[] = ['work.appraisal_status','=',$appraisalStatus];
            }
        }
        if($finishStartTime){
            $where[] = ['work.work_finish_time','>=',$finishStartTime];
        }
        if($finishEndTime){
            $where[] = ['work.work_finish_time','<=',$finishEndTime];
        }
        return WorkOrder::getList($where,$sqlWhere,$srcDepartmentId,$page,$pageSize);
    }

    /**
     * 添加订单
     * @return array
     * @author xi
     * @date 2018-6-15
     */
    public function actionAdd()
    {
        $accountId    = intval(Yii::$app->request->post('account_id',0));
        $orderList  = Yii::$app->request->post('orderList',[]);
        $judge  = intval(Yii::$app->request->post('judge',0));
        $addressId    = intval(Yii::$app->request->post('address_id',0));
        $workType     = Yii::$app->request->post('work_type',0);
        $workStage    = Yii::$app->request->post('work_stage',0);
        $isScope      = intval(Yii::$app->request->post('is_scope',0));
        $planTime     = Yii::$app->request->post('plan_time',0);
        $operatorId    = intval(Yii::$app->request->post('operator_user_id',0));
        $contractId    = intval(Yii::$app->request->post('contract_id',0));
        $subjectName     = Yii::$app->request->post('subject_name','');
        $faultImg     = trim(Yii::$app->request->post('fault_img',''));

        //服务机构
        $serviceDepartId  = intval(Yii::$app->request->post('service_depart_id',0));
        $description      = trim(Yii::$app->request->post('description',''));
        $createUserId     = intval(Yii::$app->request->post('create_user_id',0));
        $standardId       = intval(Yii::$app->request->post('standard_id',0));
        $standardType     = intval(Yii::$app->request->post('standard_type',0));
        $standardTypeId   = intval(Yii::$app->request->post('standard_type_id',0));
        $departmentId     = Yii::$app->request->post('department_id');

        //订单来源
        $source       = intval(Yii::$app->request->post('source',0));
        $amount       = Yii::$app->request->post('amount',0);
        $planTimeType = intval(Yii::$app->request->post('plan_time_type',5));
        if($accountId == 0){
            return ModelBase::errorMsg('客户不能为空!',20002);
        }
        if($judge == 0){
            return ModelBase::errorMsg('产品类型错误',20016);
        }
        if(empty($orderList)){
            return ModelBase::errorMsg('服务产品不能为空!',20003);
        }
        if($addressId == 0){
            return ModelBase::errorMsg('联系地址不能为空!',20004);
        }
        if($planTime == ''){
            return ModelBase::errorMsg('服务时间不能为空',20007);
        }
        if($departmentId <= 0){
            return ModelBase::errorMsg('机构id不能为空!', 20010);
        }
        if($createUserId == 0){
            return ModelBase::errorMsg('下单源用户id不能为空',20015);
        }
        if(!in_array($source,[1,2,3,4])){
            return ModelBase::errorMsg('参数 source 取值范围不正确');
        }
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //不拆单
            if($judge == 1)
            {
                $orderNo = ModelBase::createOrderNo(13,$workType);
                $model = new WorkOrder();
                $model->order_no       = $orderNo;
                $model->department_id  = $departmentId;
                $model->direct_company_id = Department::getDirectCompanyId($departmentId);
                $model->operator_user_id   = $operatorId;
                $model->contract_id   = $contractId;
                $model->subject_name   = $subjectName;
                $model->is_scope       = $isScope;
                $model->work_type      = $workType;
                $model->work_stage     = $workStage;
                $model->amount         = $amount;
                $model->account_id     = $accountId;
                $model->address_id     = $addressId;
                $model->create_user_id = $createUserId; //哪位登录用户下的单
                $model->description    = $description;
                $model->plan_time      = $planTime;
                $model->plan_time_type = $planTimeType;
                $model->create_time    = time();
                $model->update_time    = time();
                $model->settlement_status = 0;
                $model->source         = $source;
                $model->customer_name  = '';
                $model->customer_mobile= '';
                $model->external_order_no = $orderNo;
                if($model->save())
                {
                    //多产品添加
                    $data = [];
                    foreach ($orderList as $key=>$val)
                    {
                        $data[$key]= array($model->order_no,$val['product_id'],$faultImg,($val['num'] != null && $val['num'] != '' && $val['num'] != 0) ? $val['num'] : 1,time(),time(),1,$isScope);
                    }

                    $connection = \Yii::$app->order_db;
                    //数据批量入库
                    $res = $connection->createCommand()->batchInsert(
                        WorkOrderDetail::tableName(),
                        ['order_no','sale_order_id','fault_img','sale_order_num','create_time','update_time','is_del','is_scope'],//字段
                        $data
                    )->execute();
                    if($res)
                    {
                        //生成工单
                        $workNo = Work::autoCreate($departmentId,$model->order_no,$workStage,$planTime,$planTimeType);
                        //默认生成一条当前机构的数据
                        WorkOrderAssign::add($orderNo,3,$departmentId,$departmentId,$createUserId,1);

                        //指派技师参数
                        $params = [
                            'accountId' => $createUserId,
                            'fwsId'     => $serviceDepartId
                        ];

                        //如果没有指派服务商，那他的状态是 待指派
                        if($serviceDepartId == 0)
                        {
                            //小程序
                            if($source == 4)
                            {
                                //订单状态 待审核
                                WorkOrderStatus::add($orderNo,$departmentId,10,$departmentId);
                                //小程序新建订单动态
                                WorkOrderDynamic::add(21,$orderNo,$params);
                            }
                            else
                            {
                                //订单状态 待指派
                                WorkOrderStatus::add($orderNo,$departmentId,1,$departmentId);
                                //新建订单动态，不指派给机构或技师
                                WorkOrderDynamic::add(1,$orderNo,$params);
                            }
                            //订单的创建时间
                            WorkOrderProcess::add($orderNo,1,'');
                        }
                        else
                        {
                            //新建订单动态，指派给机构/部门
                            WorkOrderDynamic::add(2,$orderNo,$params);

                            WorkOrderStatus::add($orderNo,$departmentId,2,$departmentId);
                            WorkOrderStatus::add($orderNo,$serviceDepartId,1,$departmentId);

                            //订单的创建时间
                            WorkOrderProcess::add($orderNo,1,'');
                            //订单的指派时间
                            WorkOrderProcess::add($orderNo,2,'');

                            //指派机构/部门
                            WorkOrderAssign::add($orderNo,1,$departmentId,$serviceDepartId,$createUserId,1);

                            //如果有收费标准就填一个
                            if($standardType > 0 && $standardId > 0 && $standardTypeId > 0){
                                WorkOrderStandard::add($orderNo,$standardType,$standardTypeId,$standardId);
                            }
                        }
                        $transaction->commit();
                        return [
                            'order_no' => $model->order_no,
                            'work_no'  => $workNo
                        ];
                    }
                    else {
                        $transaction->rollBack();
                        return ModelBase::errorMsg('下单保存失败!', 20011);
                    }
                }
                else {
                    $transaction->rollBack();
                    return ModelBase::errorMsg('下单保存失败!', 20012);
                }
            }
            //拆单
            if($judge == 2){
                $flag = 0;
                foreach ($orderList as $val){
                    $orderNo = ModelBase::createOrderNo(13,$workType);
                    $model = new WorkOrder();
                    $model->order_no       = $orderNo;
                    $model->department_id  = $departmentId;
                    $model->direct_company_id = Department::getDirectCompanyId($departmentId);
                    $model->contract_id   = $contractId;
                    $model->subject_name   = $subjectName;
                    $model->is_scope       = $isScope;
                    $model->work_type      = $workType;
                    $model->work_stage     = $workStage;
                    $model->amount         = $amount;
                    $model->account_id     = $accountId;
                    $model->address_id     = $addressId;
                    $model->operator_user_id = $operatorId;
                    $model->create_user_id = $createUserId; 
                    $model->description    = $description;
                    $model->plan_time      = $planTime;
                    $model->plan_time_type = $planTimeType;
                    $model->create_time    = time();
                    $model->update_time    = time();
                    $model->settlement_status = 0;
                    $model->source         = $source;
                    $model->external_order_no = $orderNo;
                    if($model->save())
                    {
                        //多产品添加
                        $data = [];
                        foreach ($val as $key=>$v)
                        {
                            $data[$key]= array($model->order_no,$v['product_id'],$faultImg,($v['num'] != null && $v['num'] != '' && $v['num'] != 0) ? $v['num'] : 1,time(),time(),1,$isScope);
                        }

                        $connection = \Yii::$app->order_db;
                        //数据批量入库
                        $res = $connection->createCommand()->batchInsert(
                            WorkOrderDetail::tableName(),
                            ['order_no','sale_order_id','fault_img','sale_order_num','create_time','update_time','is_del','is_scope'],//字段
                            $data
                        )->execute();
                        if($res)
                        {
                            //生成工单
                            $workNo = Work::autoCreate($departmentId,$model->order_no,$workStage,$planTime,$planTimeType);
                            //默认生成一条当前机构的数据
                            WorkOrderAssign::add($orderNo,3,$departmentId,$departmentId,$createUserId,1);

                            //指派技师参数
                            $params = [
                                'accountId' => $createUserId,
                                'fwsId'     => $serviceDepartId
                            ];

                            //如果没有指派服务商，那他的状态是 待指派
                            if($serviceDepartId == 0)
                            {
                                //新建订单动态，不指派给机构或技师
                                WorkOrderDynamic::add(1,$orderNo,$params);

                                //订单状态 待指派
                                WorkOrderStatus::add($orderNo,$departmentId,1,$departmentId);
                                //订单的创建时间
                                WorkOrderProcess::add($orderNo,1,'');
                            }
                            else
                            {
                                //新建订单动态，指派给机构/部门
                                WorkOrderDynamic::add(2,$orderNo,$params);

                                WorkOrderStatus::add($orderNo,$departmentId,2,$departmentId);
                                WorkOrderStatus::add($orderNo,$serviceDepartId,1,$departmentId);

                                //订单的创建时间
                                WorkOrderProcess::add($orderNo,1,'');
                                //订单的指派时间
                                WorkOrderProcess::add($orderNo,2,'');

                                //指派机构/部门
                                WorkOrderAssign::add($orderNo,1,$departmentId,$serviceDepartId,$createUserId,1);

                                //如果有收费标准就填一个
                                if($standardType > 0 && $standardId > 0 && $standardTypeId > 0){
                                    WorkOrderStandard::add($orderNo,$standardType,$standardTypeId,$standardId);
                                }
                            }

                        }
                        else {
                            $flag =1;
                        }
                    }
                    else
                        $flag =1;
                    }
                }
                if($flag){
                    $transaction->rollBack();
                    return ModelBase::errorMsg('下单保存失败!', 20010);
                }else{
                    $transaction->commit();
                    if(count($orderList) == 1){
                        return [
                            'order_no' => $model->order_no,
                            'work_no'  => $workNo
                        ];
                    }
                    return [
                        'order_no' => '',
                        'work_no'  => ''
                    ];
                }
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('下单保存失败，错误信息：'.$e->getMessage() . $e->getLine(), 20013);
        }
    }

    /**
     * 取消订单
     * @author xi
     * @date 2017-12-14
     */
    public static function actionCancel()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $cancelReason = trim(Yii::$app->request->post('cancel_reason',''));
        //是谁取消的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($cancelReason == ''){
            return ModelBase::errorMsg('取消订单原因不能为空',20003);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20006);
        }

        //添加取消订单的动态
        $params = [
            'accountId' => $processUserId,
            'reason'     => $cancelReason
        ];
        WorkOrderDynamic::add(9,$orderNo,$params);

        return WorkOrder::cancel($orderNo,$cancelReason,$departmentId,$processUserId);
    }

    /**
     * 忽略订单
     * @author xi
     * @date 2017-12-14
     */
    public static function actionIgnore()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $ignoreReason = trim(Yii::$app->request->post('ignore_reason',''));
        //是谁忽略的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        //取消订单来原 14商家,12服务商 11门店 20用户
        $srcType      = intval(Yii::$app->request->post('src_type',0));
        $srcId        = intval(Yii::$app->request->post('src_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($ignoreReason == ''){
            return ModelBase::errorMsg('忽略订单原因不能为空',20003);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if(!in_array($srcType,[11,12,13,14])){
            return ModelBase::errorMsg('忽略订单来源取值范围只能为(11,12,13,14)',20005);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20006);
        }

        return WorkOrder::ignore($orderNo,$processUserId,$ignoreReason,$srcType,$srcId);
    }

    /**
     * 订单详情(商家用)
     * @author xi
     * @date 2017-12-15
     */
    public function actionDetail()
    {
        $orderNo             = trim(Yii::$app->request->post('order_no',''));
        $departmentId        = intval(Yii::$app->request->post('department_id',0)); //订单来源机构id
        $directCompanyId     = intval(Yii::$app->request->post('direct_company_id',0));//直属公司ID
        $showWork            = intval(Yii::$app->request->post('show_work',0));
        $serviceDepartmentId =  intval(Yii::$app->request->post('service_department_id',0));//服务机构id
        $srcDepartmentId     =  intval(Yii::$app->request->post('src_department_id',0));//查询机构id

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if( $departmentId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }

        $result = WorkOrder::getDetail($orderNo,$directCompanyId,$departmentId,$serviceDepartmentId,$srcDepartmentId);

        if($result)
        {
            $workArr = [];
            if($showWork == 1){
                $workArr = Work::getDetailByOrderNo($orderNo);
            }
            $result['workArr'] = $workArr;
        }
        return $result;
    }

    /**
     * 订单详情(服务商)
     * @author xi
     * @date 2017-12-15
     */
    public function actionDetailFws()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }

        return WorkOrder::getDetailFws($orderNo);
    }

    /**
     * 指派
     * @author xi
     * @date 2017-12-15
     */
    public static function actionAssignTechnician()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $srcType = intval(Yii::$app->request->post('src_type',0));
        $srcId   = intval(Yii::$app->request->post('src_id',0));
        $technicianId  = intval(Yii::$app->request->post('technician_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('技师id不能为空',20005);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20007);
        }

        return WorkOrder::assignTechnician($orderNo,$srcType,$srcId,$processUserId,$technicianId);
    }

    /**
     * 改派
     * @author xi
     * @date 2017-12-15
     */
    public static function actionReAssignTechnician()
    {
        $orderNo       = trim(Yii::$app->request->post('order_no',''));
        $srcType       = intval(Yii::$app->request->post('src_type',0));
        $srcId         = intval(Yii::$app->request->post('src_id',0));
        $technicianId  = intval(Yii::$app->request->post('technician_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        $reason        = trim(Yii::$app->request->post('reason','不在服务范围或服务类型不符合'));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($technicianId == 0){
            return ModelBase::errorMsg('技师id不能为空',20005);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20007);
        }

        return WorkOrder::reAssignTechnician($orderNo,$srcType,$srcId,$processUserId,$technicianId,$reason);
    }


    /**
     * 过程操作记录
     * @return array
     * @author  xi
     * @date 2017-12-21
     */
    public function actionProcess()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $srcType = intval(Yii::$app->request->post('src_type',0));
        $srcId   = intval(Yii::$app->request->post('src_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        $type    = intval(Yii::$app->request->post('type',0));
        $processResult = intval(Yii::$app->request->post('process_result',0));
        $problemReason = trim(Yii::$app->request->post('problem_reason',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20005);
        }
        if($type == 0){
            return ModelBase::errorMsg('过程类型不能为空',20006);
        }

        $order = WorkOrder::findOneByAttributes(['order_no'=>$orderNo],'amount');
        $orderDetail = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],'id,');
        if($order && $orderDetail){
            $num = 0;
            foreach ($orderDetail as $val){
                /*$res = WorkOrderProcess::add($orderNo,$val['id'],$type,$processUserId,0,$processResult,$problemReason,$order['amount'],'');
                if($res){
                    $num++;
                }*/
            }
            if($num == count($orderDetail)){
                return [];
            }
        }
        return ModelBase::errorMsg('操作失败',20007);
    }

    /**
     * 服务中
     * @author  xi
     * @date 2017-12-22
     */
    public function actionInService()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $srcType = intval(Yii::$app->request->post('src_type',0));
        $srcId   = intval(Yii::$app->request->post('src_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20005);
        }
        if(!WorkOrder::hasTechnicianServiceByStatus($processUserId)){
            return ModelBase::errorMsg('有订单未关闭',20006);
        }

        return WorkOrder::inService($orderNo,$processUserId,$srcType,$srcType);
    }

    /**
     * 完成操作
     * @author  xi
     * @date 2017-12-22
     */
    public function actionFinish()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $srcType = intval(Yii::$app->request->post('src_type',0));
        $srcId   = intval(Yii::$app->request->post('src_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        $imgUrl        = Yii::$app->request->post('img_url');
        $problemReason = trim(Yii::$app->request->post('problem_reason',''));


        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20005);
        }
        if(!$imgUrl){
            return ModelBase::errorMsg('维修凭证不能为空',20006);
        }
        if($problemReason == ''){
            return ModelBase::errorMsg('维修记录不能为空',20007);
        }

        if($imgUrl && is_array($imgUrl)){
            $imgUrl = implode(',',$imgUrl);
        }

        return WorkOrder::finish($orderNo,$processUserId,$srcType,$srcId,$imgUrl,$problemReason);
    }

    /**
     * 统计
     * @author xi
     * @date 2017-12-25
     */
    public function actionTongji()
    {
        $srcType = intval(Yii::$app->request->post('src_type',0));
        $srcId   = intval(Yii::$app->request->post('src_id',0));

        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }

        return WorkOrder::tongji($srcType,$srcId);
    }

    /**
     * 服务商页面统计
     * @return array
     */
    public function actionFwsTongji()
    {
        $srcId   = intval(Yii::$app->request->post('src_id',0));

        if( $srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }

        return WorkOrder::fwsTongji($srcId);
    }

    /**
     * 客户服务过的产品信息
     * @author xi
     * @date 2017-12-25
     */
    public function actionAccountProduct()
    {
        $accountId   = intval(Yii::$app->request->post('account_id',0));
        $saleOrderId = Yii::$app->request->post('sale_order_id','');

        if( $accountId == 0 ){
            return ModelBase::errorMsg('客户id不能为空',20003);
        }
        if(!$saleOrderId){
            return ModelBase::errorMsg('产品id不能为空',20004);
        }

        return WorkOrder::getAccountProduct($accountId,$saleOrderId);
    }

    /**
     * 根据用户查出订单数
     * @return array
     */
    public function actionUserOrders()
    {

        $accountIds = Yii::$app->request->post('account_ids');
        if( !$accountIds){
            return ModelBase::errorMsg('客户account_ids不能为空',20005);
        }
        return WorkOrder::getUserOrders($accountIds);
    }

    /**
     * 技师端根据订单号获取订单信息及上一单/下一单信息 
     */
    public function actionGetNewOrderDetail() {
        $order_no = Yii::$app->request->post('order_no');
        $tec_id   = Yii::$app->request->post('tec_id');
        //查询当前订单
        $order['now']   = WorkOrder::getNewOrderDetail($order_no);
        //查询上一单
        $order['front'] = Work::getFrontOrder($order['now']['plan_time'], $tec_id);
        //查询下一单
        $order['after'] = Work::getAfterOrder($order['now']['plan_time'], $tec_id);
        return $order;
    }
    
    /**
     * 待结算工单
     * @author  li
     * @date    2018-02-05
     * @return  json
     */
    public function actionWaitCost() {
        $id = Yii::$app->request->post('id',0);
        $page = Yii::$app->request->post('page',0);
        if($id == 0) {
            return ModelBase::errorMsg('用户ID不能为空', 20002);
        }
        if($page == 0) {
            return ModelBase::errorMsg('页数不能为空', 20002);
        }
        return Work::getWaitCostOrder($id, $page);
    }






    /*************私有方法*******************/
    
    /**
     * 获取所有待接订单
     */
    protected function getNewOrder() {
        $order = WorkOrder::getNewOrderList();
        return $order;
    }
    
    /**
     * 获取订单的服务地址
     */
    protected function getOrderAddress($newOrder, $addressIds) {
        $address = AccountAddress::getOrderAddress($addressIds);
        //数组拼接
        foreach ($newOrder as $k => $v) {
            if(isset($address[$v['address_id']])) {
                $newOrder[$k]['lon']     = $address[$v['address_id']]['lon'];
                $newOrder[$k]['lat']     = $address[$v['address_id']]['lat'];
                $newOrder[$k]['address'] = $address[$v['address_id']]['address'];
            }
        }
        return $newOrder;
    }
    
    /**
     * 获取订单的服务项目  服务类型/产品类型/品牌
     */
    protected function getOrderService($newOrder) {
        foreach ($newOrder as $k => $v) {
            $weekday = array("周日","周一","周二","周三","周四","周五","周六");
            $newOrder[$k]['goods_class_name'] = WorkOrderDetail::getClassName($v['order_no']);
            $newOrder[$k]['goods_class_id']   = WorkOrderDetail::getClassId($v['order_no']);
            $newOrder[$k]['service_id']       = ServiceType::getServiceId($v['work_type']);
            $newOrder[$k]['service_name']     = ServiceType::getServiceTitle($v['work_type']);
            $newOrder[$k]['brand_name']       = WorkOrderDetail::getBrandName($v['order_no']);
            $newOrder[$k]['brand_id']         = WorkOrderDetail::getBrandId($v['order_no']);
            $newOrder[$k]['week']             = $weekday[date('w',$v['plan_time'])];
            $newOrder[$k]['service_time']     = date("H:i", $v['plan_time']);
        }
        return $newOrder;
    }

    /**
     * 技师搜索
     * @author xi
     * @date 2018-1-8
     */
    public function actionTechnicianSearch()
    {
        $page      = intval(Yii::$app->request->post('page',1));
        $pageSize  = intval(Yii::$app->request->post('pageSize',10));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $status       = intval(Yii::$app->request->post('status',0));

        if($technicianId<=0){
            return ModelBase::errorMsg("参数 technician_id 不能为空",20002);
        }

        $where = [];
        $sqlWhere = '';
        if($technicianId > 0){
            $where[] = ['c.technician_id','=',$technicianId];
        }

        if($status == 1){
            $sqlWhere = '(a.status in(3,4) and c.type = 1)';
        }
        else if($status == 2){
            $sqlWhere = '(a.status = 5 and c.type = 1)';
        }
        else if($status == 3){
            $sqlWhere = '(a.status = 6 or c.type = 2)';
        }
        return WorkOrder::getTechnicianList($where,$sqlWhere,$page,$pageSize);
    }

    /**
     * 技师订单详情
     * @author xi
     * @date 2018-1-8
     */
    public function actionTechnicianDetail()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }

        return WorkOrder::getTechnicianDetail($orderNo,$technicianId);
    }

    /**
     * 技师服务情况
     * @author xi
     * @date 2018-1-9
     */
    public function actionHasTechnicianService()
    {
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $planTime     = intval(Yii::$app->request->post('plan_id',0));

        if($technicianId <= 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }
        if($planTime <= 0){
            return ModelBase::errorMsg('参数 plan_id 不能为空',20003);
        }
        return WorkOrder::hasTechnicianService($technicianId,$planTime);
    }

    /**
     * 发送消息
     * @return array
     * @date 2018-1-9
     */
    public function actionSendMessage()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $workNo = trim(Yii::$app->request->post('work_no',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20003);
        }
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20004);
        }

        return Message::send(200,$orderNo,'','',$workNo);
    }

    /**
     * 机构驳回订单
     * @author xi
     * @date 2017-12-14
     */
    public static function actionReject()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $rejectReason = trim(Yii::$app->request->post('reject_reason',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20006);
        }
        return WorkOrder::rejectOrder($orderNo,$processUserId,$departmentId,$rejectReason);
    }

    /**
     * 技师服务状态（根据status = 4 查询）
     * @return array|bool
     * @author xi
     * @date 2018-1-11
     */
    public function actionTechnicianServiceStatus()
    {
        $technicianIds = Yii::$app->request->post('technician_ids');

        if(!$technicianIds){
            return ModelBase::errorMsg('参数 technician_ids 不能为空',20002);
        }

        return WorkOrder::technicianServiceStatus($technicianIds);
    }

    /**
     * 查询订单是否指派
     * @return array|bool
     * @author xi
     * @date 2018-1-11
     */
    public function actionHasOrderAssign()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $type    = intval(Yii::$app->request->post('type',1));

        if(!$orderNo){
            return ModelBase::errorMsg('参数 order_no 不能为空',20002);
        }
        if(!in_array($type,[1,2])){
            return ModelBase::errorMsg('参数 type 不能为空',20003);
        }

        return WorkOrder::hasOrderAssign($orderNo,$type);
    }

    /**
     * 修改服务时间 change-plantime
     * @author xi
     * @date 2018-1-31
     */
    public function actionChangePlantime()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $plantime     = intval(Yii::$app->request->post('plantime',0));
        $plantimeType = intval(Yii::$app->request->post('plan_time_type',5));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20006);
        }
        if($plantime == 0){
            return ModelBase::errorMsg('服务时间不能为空',20007);
        }

        return WorkOrderPlantimeLogs::changePlantime($orderNo,$departmentId,$plantime,$plantimeType,$processUserId);
    }

    /**
     * 指派机构 assign-institution
     * @return array
     * @author xi
     */
    public function actionAssignInstitution()
    {
        $orderNo             = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId       = intval(Yii::$app->request->post('process_user_id',0));
        $departmentId        = intval(Yii::$app->request->post('department_id',0));
        $serviceDepartmentId = intval(Yii::$app->request->post('service_department_id',0));
        $orderSourceId       = intval(Yii::$app->request->post('order_source_id',0)); //下单源id

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数department_id不能为空!', 20006);
        }
        if($serviceDepartmentId == 0){
            return ModelBase::errorMsg('服务机构不能为空',20007);
        }

        return WorkOrderAssign::assignInstitution($orderNo,$departmentId,$serviceDepartmentId,$processUserId,1,$orderSourceId);
    }

    /**
     * 改派机构 re-assign-institution
     * @return array
     */
    public function actionReAssignInstitution()
    {
        $orderNo             = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId       = intval(Yii::$app->request->post('process_user_id',0));
        $departmentId        = intval(Yii::$app->request->post('department_id',0));
        $serviceDepartmentId = intval(Yii::$app->request->post('service_department_id',0)); //被指派的机构id
        $orderSourceId       = intval(Yii::$app->request->post('order_source_id',0)); //下单源id
        $reason              = trim(Yii::$app->request->post('reason',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20006);
        }
        if($serviceDepartmentId == 0){
            return ModelBase::errorMsg('参数 service_department_id 不能为空!', 20007);
        }
        if($reason == ''){
            return ModelBase::errorMsg('改派原因不能为空',20008);
        }

        $orderArr = WorkOrder::findOneByAttributes(['order_no'=>$orderNo]);
        $orderStatusArr = WorkOrderStatus::findOneByAttributes(['order_no'=>$orderNo,'department_id'=>$orderSourceId]);
        if(!$orderArr || !$orderStatusArr){
            return ModelBase::errorMsg('未找到订单数据',20011);
        }
        else if(!in_array($orderStatusArr['status'],[1,2])){
            return ModelBase::errorMsg('订单进行中不可以改派',20012);
        }


        return WorkOrderAssign::reAssignFws($orderNo,$departmentId,$serviceDepartmentId,$reason,$processUserId,$orderSourceId);
    }

    /**
     * 回访
     * @return array
     */
    public function actionVisit()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId= intval(Yii::$app->request->post('process_user_id',0));
        $srcType      = intval(Yii::$app->request->post('src_type',0));
        $srcId        = intval(Yii::$app->request->post('src_id',0));
        $content      = trim(Yii::$app->request->post('content',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 传入不正确',20005);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20006);
        }
        if($content == ''){
            return ModelBase::errorMsg('回访记录不能为空',20007);
        }

        return WorkOrderVisit::visit($orderNo,$srcType,$srcId,$content,$processUserId);
    }

    /**
     * 质保状态修改
     * @author xi
     * @date 2018-2-4
     */
    public function actionEditScope()
    {
        $department_id   = intval(Yii::$app->request->post('department_id',0));
        $orderNo         = trim(Yii::$app->request->post('order_no',''));
        $isScope         = intval(Yii::$app->request->post('is_scope',0));
        $scopeImg        = Yii::$app->request->post('scope_img','');

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if( $department_id == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }
        if($isScope == 0){
            return ModelBase::errorMsg('质保状态不能为空',20005);
        }

        $model = WorkOrder::findOne(['order_no'=>$orderNo,'department_id'=>$department_id]);
        if($model)
        {
            $model->is_scope = $isScope;
            $model->scope_img = $scopeImg?(is_array($scopeImg)?implode(',',$scopeImg):$scopeImg):'';
            $model->update_time = time();
            if($model->save())
            {
                return [
                    'order_no' => $orderNo
                ];
            }
            else {
                return ModelBase::errorMsg('修改失败',20007);
            }
        }
        return ModelBase::errorMsg('修改失败',20006);
    }

    /**
     * 改派列表
     * @return array
     */
    public function actionReAssignSearch()
    {
        $page      = intval(Yii::$app->request->post('page',1));
        $pageSize  = intval(Yii::$app->request->post('pageSize',10));
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $workType  = intval(Yii::$app->request->post('work_type',0));
        $saleOrderId   = Yii::$app->request->post('sale_order_id');
        $planStartTime = intval(Yii::$app->request->post('plan_start_time',0));
        $planEndTime   = intval(Yii::$app->request->post('plan_end_time',0));
        $createStartTime = intval(Yii::$app->request->post('create_start_time',0));
        $createEndTime   = intval(Yii::$app->request->post('create_end_time',0));
        $srcId         = intval(Yii::$app->request->post('src_id',0));

        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20002);
        }

        $where = [];

        if($orderNo){
            $where [] = ['a.order_no','=',$orderNo];
        }
        if($workType){
            $where[] = ['a.work_type','=',$workType];
        }
        if($saleOrderId)
        {
            if(is_array($saleOrderId)){
                $where[] = ['b.sale_order_id','in',$saleOrderId];
            }
            else {
                $where[] = ['b.sale_order_id','=',$saleOrderId];
            }
        }
        if($planStartTime){
            $where[] = ['a.plan_time','>=',$planStartTime];
        }
        if($planEndTime){
            $where[] = ['a.plan_time','<=',$planEndTime];
        }
        if($createStartTime){
            $where[] = ['a.create_time','>=',$createStartTime];
        }
        if($createEndTime){
            $where[] = ['a.create_time','<=',$createEndTime];
        }

        return WorkOrder::getReAssignList($srcId,$where,$page,$pageSize);
    }

    /**
     * 验收通过
     * @author  xi
     * @date 2018-2-5
     */
    public function actionConfirmFinish()
    {
        $orderNo       = trim(Yii::$app->request->post('order_no',''));
        $flag          = intval(Yii::$app->request->post('flag',1));
        $departmentId  = intval(Yii::$app->request->post('department_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if( $departmentId == 0 ){
            return ModelBase::errorMsg('参数 department_id 不能为空',20004);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20005);
        }

        return WorkOrder::confirmFinish($orderNo,$processUserId,$departmentId,$flag);
    }

    /**
     * 验收不通过
     * @author xi
     * @date 2018-6-22
     */
    public function actionAcceptanceFailure()
    {
        $orderNo       = trim(Yii::$app->request->post('order_no',''));
        $departmentId  = intval(Yii::$app->request->post('department_id',0));
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        $reason        = trim(Yii::$app->request->post('reason',''));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if( $departmentId == 0 ){
            return ModelBase::errorMsg('参数 department_id 不能为空',20004);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20005);
        }

        WorkOrder::acceptanceFailure($orderNo,$processUserId,$departmentId,$reason);
    }

    /**
     * 工单回访查看信息
     * @return array
     */
    public function actionViewVisit()
    {
        $orderNo  = trim(Yii::$app->request->post('order_no',''));
        $srcType  = intval(Yii::$app->request->post('src_type',0));
        $srcId    = intval(Yii::$app->request->post('src_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 传入不正确',20003);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('下单源id不能为空!', 20004);
        }

        $where = [
            'src_type' => $srcType,
            'src_id'   => $srcId,
            'order_no' => $orderNo,
            'status'   => 1
        ];
        $visitArr =WorkOrderVisit::findOneByAttributes($where,'content');
        if($visitArr){
            return $visitArr;
        }
        return [];
    }

    /*
     * 获取服务商和商家的结算单数据
     * */
    public function actionSettlementOrder()
    {
        $begin_time = strtotime(trim(Yii::$app->request->post('begin_time','')));
        $end_time = strtotime(trim(Yii::$app->request->post('end_time','')));
        $src_type = trim(Yii::$app->request->post('src_type',0));
        $src_id = trim(Yii::$app->request->post('src_id',0));
        $sj_id = trim(Yii::$app->request->post('sj_id'));

        if($begin_time == ''){
            return ModelBase::errorMsg('开始时间不能为空',20002);
        }
        if($end_time == ''){
            return ModelBase::errorMsg('结束时间不能为空',20003);
        }
        if($src_type == 0){
            return ModelBase::errorMsg('来源类型不能为空',20004);
        }
        if($src_id == 0){
            return ModelBase::errorMsg('来源id不能为空',20005);
        }
        if($sj_id == 0){
            return ModelBase::errorMsg('商家id不能为空',20006);
        }

        $result = WorkOrder::getOrderForSettlement($begin_time,$end_time,$src_type,$src_id,$sj_id);
        return $result;
    }

    /**
     * 查出最近未完成订单
     * @author xi
     * @date 2018-3-20
     */
    public function actionGetLatelyOrder()
    {
        $accountId = intval(Yii::$app->request->post('account_id',0));
        $srcType   = intval(Yii::$app->request->post('src_type',0));
        $srcId     = intval(Yii::$app->request->post('src_id',0));

        if($accountId == 0){
            return ModelBase::errorMsg('参数 account_id 不能为空',20003);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('参数 src_type 不能为空',20004);
        }
        if($srcId == 0){
            return ModelBase::errorMsg('参数 src_id 不能为空',20005);
        }

        return WorkOrder::getLatelyOrder($srcType,$srcId,$accountId);
    }

    /**
     * 审核不通过
     * @author xi
     */
    public function actionAuditNotThrough()
    {
        $orderNo       = Yii::$app->request->post('order_no','');
        $departmentId  = intval(Yii::$app->request->post('department_id',0));
        $reason        = Yii::$app->request->post('reason','');
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));


        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20003);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人id不能为空',20004);
        }

        return WorkOrder::auditNotThrough($orderNo,$departmentId,$reason,$processUserId);
    }

    /**
     * 审核通过
     * @author xi
     * @date 2018-2-23
     */
    public function actionThroughAudit()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        //是谁操作的这个订单
        $processUserId = intval(Yii::$app->request->post('process_user_id',0));
        $departmentId  = intval(Yii::$app->request->post('department_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($processUserId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空!', 20006);
        }

        $params = [
            'department_id' => $departmentId,
            'user_id' => $processUserId,
            'accountId' => $processUserId
        ];
        WorkOrderDynamic::add(23,$orderNo,$params);


        WorkOrderStatus::changeStatus($departmentId,$orderNo,1,$departmentId,'审核通过');
        WorkOrderAudit::add($departmentId,$orderNo,1,$processUserId,'审核通过');


        return [
            'order_no' => $orderNo
        ];
    }

    /**
     * 获取订单状态
     * @return array
     * @author xi
     */
    public function actionGetOrderStatus()
    {
        $departmentId  = Yii::$app->request->post('department_id');
        $orderNos      = Yii::$app->request->post('order_nos');

        if(!$departmentId){
            return ModelBase::errorMsg('参数 department_id 不能为空');
        }
        if(!$orderNos){
            return ModelBase::errorMsg('参数 order_nos 不能为空');
        }

        return WorkOrderStatus::getOrderStatus($departmentId,$orderNos);
    }
    /**
     * 订单状态统计
     * @param status 状态 is_department 机构0  本人1   department_ids array
     * 2018-7-18
     * quan
     */
    public function actionCount()
    {
        $status = Yii::$app->request->post('status','');
        $is_department     = intval(Yii::$app->request->post('is_department',0));
        $department_ids     = Yii::$app->request->post('department_ids',[]);

        if($status == '' || empty($department_ids)){
            return ModelBase::errorMsg('参数错误');
        }

        return WorkOrderStatus::getOrderCount($status,$is_department,$department_ids);
    }
    /**
     * 工单确认收费列表
     */
    public function actionFeeList()
    {
        $workNo         = Yii::$app->request->post('work_no','');
        $department_ids = Yii::$app->request->post('department_ids',[]);
        $tec_ids        = Yii::$app->request->post('tec_ids',[]);
        $serviceType    = intval(Yii::$app->request->post('service_type',0));
        $startFeeTime   = Yii::$app->request->post('start_fee_time','');
        $endFeeTime     = Yii::$app->request->post('end_fee_time','');
        $status         = intval(Yii::$app->request->post('status',0));
        $page           = intval(Yii::$app->request->post('page',1));
        $pageSize       = intval(Yii::$app->request->post('pageSize',10));
        if($status === '' || empty($department_ids)){
            return ModelBase::errorMsg('参数错误');
        }
        $where[] = ['a.status','=',4];
        $where[] = ['a.fee_status','=',$status];
        $where[] = ['a.department_id','in',$department_ids];
        if($serviceType)
        {
            $res = WorkOrder::findAllByAttributes(['work_type'=>$serviceType],"order_no");
            if($res)
            {
                $orderNo = array_column($res,"order_no");
                $where[] = ['a.order_no','in',$orderNo];
            }
        }
        if($startFeeTime)
        {
            $res = WorkProcess::getOne([['create_time','>=',strtotime($startFeeTime)],['type','in',[5,7]]],"work_no","work_no");
            if($res)
            {
                $workNos = array_column($res,"work_no");
                if($workNo == ''){
                    $workNo = $workNos;
                }else{
                    if(!in_array($workNo,$workNos)){
                        $workNo = '-1';
                    }
                }
            }
        }
        if($endFeeTime)
        {
            $res = WorkProcess::getOne([['create_time','<=',strtotime($endFeeTime." 23:59:59")],['type','in',[5,7]]],"work_no","work_no");
            if($res)
            {
                $workNos = array_column($res,"work_no");
                if(is_array($workNo) && !empty($workNo)){
                    $workNo = array_intersect($workNo,$workNos);
                }else{
                    if($workNo == ''){
                        $workNo = $workNos;
                    }else{
                        if(!in_array($workNo,$workNos)){
                            $workNo = '-1';
                        }
                    }
                }
            }
        }
        if(!empty($tec_ids))
        {
            $res = WorkRelTechnician::findAllByAttributes(['technician_id'=>$tec_ids,'is_self'=>[1,3]],"work_no");
            if($res)
            {
                $workNos = array_column($res,'work_no');
                if(is_array($workNo) && !empty($workNo)){
                    $workNo = array_intersect($workNo,$workNos);
                }else{
                    if($workNo == ''){
                        $workNo = $workNos;
                    }else{
                        if(!in_array($workNo,$workNos)){
                            $workNo = '-1';
                        }
                    }
                }
            }else{
                $workNo = '-1';
            }
        }
        if($workNo)
        {
            if(is_array($workNo)){
                $where [] = ['a.work_no','in',$workNo];
            }
            else {
                $where [] = ['a.work_no','=',$workNo];
            }
        }
        return Work::feeList($where,$page,$pageSize);
    }

    /**
     * 确认收费
     * @return array|int
     */
    public function actionFeeConfirm()
    {
        $workNo = Yii::$app->request->post('work_no', '');
        if($workNo === ''){
            return ModelBase::errorMsg('参数错误');
        }
        return Work::updateAll(['fee_status'=>1],['work_no'=>$workNo]);
    }

    /**
     * 修改备注
     * @return array|int
     */
    public function actionEditRemark()
    {
        $workNo = Yii::$app->request->post('work_no', '');
        $remark = Yii::$app->request->post('remark', '');
        if($workNo === ''){
            return ModelBase::errorMsg('参数错误');
        }
        return Work::updateAll(['fee_remark'=>$remark],['work_no'=>$workNo]);
    }
    /**
     *  根据客户 门店查询 订单工单数量
     */
    public function actionStastic(){
        $acount_ids = Yii::$app->request->post('account_ids', '');
        $where[] = ['account_id','in',$acount_ids];
        //var_dump($where);exit;
        return WorkOrder::getOrderNum($where);
    }
    /**
     * 查询指挥中心账号是否存在
     */
    public function actionIssetCommand(){
        $mobile = Yii::$app->request->post('mobile', '');
        return CommandCenter::findAllByAttributes(['mobile'=>$mobile]);
    }
    /**
     * 根据id 获取 可查看部门
     */
    public function actionGetAccount()
    {
        $id = Yii::$app->request->post('id', 0);
        return CommandCenter::findOneByAttributes(['id'=>$id],"account_ids");
    }

    /**
     *  获取战略指挥心中数据
     */
    public function actionCommand()
    {
        $acount_ids = Yii::$app->request->post('account_ids', '');
        $method = Yii::$app->request->post('method', 1);
        $dateType = Yii::$app->request->post('date_type', 1);
        if($dateType == 1){
            $time = date("Y-m-d",time());
        }else if($dateType == 2){
            $time = date("Y-m-d",strtotime("-1 day"));
        }else if($dateType == 3){
            $time[0] = date("Y-m-d",strtotime("-1 week"));
            $time[1] = date("Y-m-d");
        }else if($dateType == 4){
            $time[0] = date("Y-m-d",strtotime("-1 month"));
            $time[1] = date("Y-m-d");
        }else{
            $time[0] = date("Y-m-01",strtotime($dateType));
            $time[1] = date("Y-m-t",strtotime($dateType));
        }
        switch ($method)
        {
            case 1: //首页
                return CommandCenterStatistics::homeData($acount_ids);
                break;
            case 2: //门店分布
                return CommandCenterStatistics::storeMap($acount_ids);
                break;
            case 3: //派单概览(日期)
                return CommandCenterStatistics::OrderDate($acount_ids,$time,1);
                break;
            case 4: //派单概览(门店)
                return CommandCenterStatistics::OrderDate($acount_ids,$time,2);
                break;
            case 5: //异常工单
                return CommandCenterStatistics::abnormalWork($acount_ids,$time);
                break;
            case 6: //备件消耗
                return CommandCenterStatistics::partReduce($acount_ids,$method,$date);
                break;
            default:
                return CommandCenterStatistics::homeData($acount_ids,$method,$date);
        }

    }
}
