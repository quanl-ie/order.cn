<?php
namespace webapp\modules\v2\models;

use Yii;

class WorkRelTechnician extends \webapp\modules\v1\models\WorkRelTechnician
{

    /**
     * 获取工单关联的技师
     * @param $workNo
     * @return array
     */
    public static function getTechnicianInfoByWorkNo($workNo)
    {
        $where = [
            'work_no' => $workNo,
            'type'    => 1
        ];
        $query = self::find()
            ->where($where)
            ->select('technician_id,is_self')
            ->orderBy(" `is_self`<>2,`is_self`<>1 ")
            ->asArray()
            ->all();
        if($query){
            return $query;
        }
        return [];
    }

    /**
     * 获取工单信息
     * @param $workNo
     * @param $technicianId
     * @return array
     */
    public static function getTechnicianAndWork($workNo,$technicianId)
    {
        $result = [];
        $where = [
            'b.work_no'         => $workNo,
            'b.type'            => 1,
            'a.`cancel_status`' => 1,
            'a.`status`'        => 3,
            'b.`is_self`'       => [1,2,3]
        ];
        $db = self::find();
        $db->from(self::tableName() . ' as b');
        $db->innerJoin(['`'.\webapp\modules\v1\models\Work::tableName().'` as a'] , ' a.work_no = b.work_no');
        $db->where($where);
        $db->select("a.department_id,b.work_no,b.technician_id,b.is_self,a.order_no,a.plan_time");
        $db->orderBy("b.`is_self`<>1");
        $query = $db->asArray()->all();

        if($query)
        {
            $orderNo        = '';
            $departmentId   = 0;
            $ownWork        = 0;
            $workRelTechnicianArr = [];
            $plantime = 0;
            foreach ($query as $val)
            {
                if($departmentId == 0){
                    $departmentId = $val['department_id'];
                }
                if($orderNo == ''){
                    $orderNo = $val['order_no'];
                }
                if($technicianId == $val['technician_id']){
                    $ownWork = 1;
                }
                $workRelTechnicianArr[$val['is_self']][] = $val['technician_id'];
                $plantime = $val['plan_time'];
            }

            //查出订单信息
            $workType = 0;
            $addressId = 0;
            $saleOrderIds = [];
            if($orderNo)
            {
                $workOrderArr = WorkOrder::findOneByAttributes(['order_no'=>$orderNo],'work_type,address_id');
                if($workOrderArr){
                    $workType  = $workOrderArr['work_type'];
                    $addressId = $workOrderArr['address_id'];
                }

                $orderDetailRes = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],'sale_order_id');
                if($orderDetailRes){
                    $saleOrderIds = array_column($orderDetailRes,'sale_order_id');
                }
            }

            $result = [
                'work_no'        => $workNo,
                'order_no'       => $orderNo,
                'address_id'     => $addressId,
                'work_type'      => $workType,
                'sale_order_ids' => $saleOrderIds,
                'plantime'       => $plantime,
                'department_id'  => $departmentId,
                'rel_technician' => $workRelTechnicianArr,
                'own_work'       => $ownWork
            ];
        }

        return $result;
    }

    /**
     * 检查技师与否与服务时间有冲突
     * @param $technicianId
     * @param $plantime
     * @return array
     */
    public static function getPlantimeConflict($technicianIds ,$plantime,$workNo)
    {
        $result = [];
        $where = [
            'b.technician_id' => $technicianIds,
            'b.type'          => 1,
            'a.status'        => [2,3,5]
        ];
        $query = self::find()
            ->from(\webapp\modules\v1\models\Work::tableName() . ' as a')
            ->innerJoin(['`'. self::tableName() .'` as b'],'a.work_no = b.work_no')
            ->where($where)
            ->andWhere(['<>','a.work_no',$workNo])
            ->andWhere(['>','a.plan_time',$plantime-1800])
            ->andWhere(['<','a.plan_time',$plantime+1800])
            ->select('count(1) as count, b.`technician_id`')
            ->groupBy('b.`technician_id`')
            ->asArray()->all();
        if($query)
        {
            $technicianCountArr = array_column($query,'count','technician_id');
            foreach ($technicianIds as $val)
            {
                $result[$val] = 0;
                if(isset($technicianCountArr[$val]) && $technicianCountArr[$val] > 0){
                    $result[$val] = 1;
                }
            }
        }
        else
        {
            foreach ($technicianIds as $val){
                $result[$val] = 0;
            }
        }
        return $result;
    }

    /**
     * 获取服务技师
     * @param $workNos
     * @return array
     * @author xi
     */
    public static function getServiceTechnician($workNos)
    {
        $result = [];

        $where = [
            'work_no' => $workNos,
            'is_self' => [1,3],
            'type'    => 1
        ];
        $query = self::findAllByAttributes($where,'work_no,technician_id,is_self');
        if($query)
        {
            foreach ($query as $val)
            {
                $result [$val['work_no']][] = [
                    'technician_id' => $val['technician_id'],
                    'is_self'       => $val['is_self']
                ];
            }
        }

        return $result;
    }

}