<?php
namespace webapp\modules\v2\models;

use common\models\Technician;
use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkCost;
use webapp\modules\v1\models\WorkOrderAssign;
use webapp\modules\v2\models\WorkOrder;
use webapp\modules\v1\models\WorkOrderTrade;
use webapp\modules\v1\models\WorkProcess;
use webapp\modules\v1\models\WorkRelTechnician;
use webapp\modules\v2\models\WorkRelTechnician as WorkRelTechnicianV2 ;
use Yii;

class Work extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work';
    }

    /**
     * 状态中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getStatusDesc($index)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '已完成',
            5 => '待收款',
            6 => '组长待指派',
            7 => '已改派'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 下次服务中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getNextServiceDesc($index)
    {
        $data = [
            1 => '由公司指派',
            2 => '本次服务技师',
            3 => '由组长指派'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 下次服务时间中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getNextServiceTimeDesc($index)
    {
        $data = [
            1 => '由公司指定',
            2 => '负责人指定',
            3 => '由组长指定'
        ];

        return isset($data[$index])?$data[$index]:'';
    }


    /**
     * 订单详情
     * @param $workNo
     * @return array
     */
    public static function detail($workNo, $technicianId)
    {
        $query = self::findOneByAttributes(['work_no'=>$workNo]);
        if($query)
        {
            $orderNo = $query['order_no'];
            $workOrderArr = \webapp\modules\v1\models\WorkOrder::findOneByOrderNo($orderNo);
            if(!$workOrderArr){
                return ModelBase::errorMsg('未找到工单信息',20003);
            }

            $finshArr = $beginServicesArr = $chargeArr = $cancelArr = [];
            //开始服务时间
            if($query['status'] >= 3){
                $beginServicesArr = WorkProcess::getTypeTimeData($workNo,4);
            }
            //完成时间
            if($query['status'] >= 4){
                $beginServicesArr = WorkProcess::getTypeTimeData($workNo,4);
                $finshArr = WorkProcess::getTypeTimeData($workNo,5);
            }
            //收费时间
            if($query['status'] == 7 || $query['status'] == 4){
                $chargeArr = WorkProcess::getTypeTimeData($workNo,7);
            }

            //关闭信息
            if($query['cancel_status'] == 2){
                $cancelArr = WorkProcess::findOneByAttributes(['work_no'=>$workNo,'type'=>10],'comment,create_time');
            }
            //收费项目
            $workCostData = WorkCost::getList($workNo);
            $workCostList = $workCostData['list'];
            //判断是否是改派
            $workRelTechnician = WorkRelTechnician::findAllByAttributes(['work_no' => $workNo, 'technician_id' => $technicianId]);

            if($query['status'] == 6) {
                $query['status'] = 1;
            }

            //如果改派
            if($workRelTechnician)
            {
                $hasReAssign = true;
                foreach ($workRelTechnician as $tval){
                    if($tval['type'] == 1){
                        $hasReAssign = false;
                    }
                }

                if($hasReAssign == true)
                {
                    $workRelTechnician = WorkRelTechnician::findOne(['work_no' => $workNo, 'technician_id' => $technicianId,'type'=>2]);
                    $query['status'] = 6;
                    $cancelArr = [
                        'create_time' => $workRelTechnician->create_time,
                        'comment'     => $workRelTechnician->reason
                    ];
                }
            }

            //订单详情数据
            $orderDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],'sale_order_id,sale_order_num');
            //技师信息
            $technicianArr = WorkRelTechnicianV2::getTechnicianInfoByWorkNo($workNo);

            //支付类型
            $paymentMethod = '';
            if($query['status'] == 4){
                $paymentMethod = WorkOrderTrade::getPaymentMethod($workNo);
            }

            $result = [
                'work_department_id' => $query['department_id'],
                'signature'          => $query['signature'],
                'work_no'            => $query['work_no'],
                'order_no'           => $query['order_no'],
                'subject_name'       => $workOrderArr['subject_name']?$workOrderArr['subject_name']:'',
                'account_id'         => $workOrderArr['account_id'],
                'department_id'      => $workOrderArr['department_id'],
                'address_id'         => $workOrderArr['address_id'],
                'work_type'          => $workOrderArr['work_type'],
                'is_scope'           => $workOrderArr['is_scope'],
                'scope_img'          => $workOrderArr['scope_img'] != '' ? explode(',',$workOrderArr['scope_img']) : [],
                'contract_id'        => $workOrderArr['contract_id'],
                'plan_time'          => $query['plan_time'],
                'plan_time_type'     => $query['plan_time_type'],
                'work_stage'         => $query['work_stage'],
                'fault_img'          => $workOrderArr['fault_img']!=''?explode(',',$workOrderArr['fault_img']):[],
                'remark'             => $workOrderArr['description'],
                'status'             => $query['status'],
                'status_desc'        => Work::getStatusDesc($query['status']),
                'cancel_status'      => $query['cancel_status'],
                'cancel_status_desc' => $query['cancel_status']==1?'未关闭':'已关闭',
                'begin_service_time' => isset($beginServicesArr[$query['work_no']])?$beginServicesArr[$query['work_no']]:0,
                'finsh_time'         => isset($finshArr[$query['work_no']])?$finshArr[$query['work_no']]:0,
                'create_time'        => $query['create_time'],
                'charge_time'        => isset($chargeArr[$query['work_no']])?$chargeArr[$query['work_no']]:'',
                'cancel_time'        => isset($cancelArr['create_time'])?$cancelArr['create_time']:0,
                'cancel_season'      => isset($cancelArr['comment'])?$cancelArr['comment']:'',
                'work_cost_list'     => $workCostList,
                'assign_type'        => $query['assign_type'],
                'order_detail_arr'   => $orderDetailArr,
                'technicianArr'      => $technicianArr,
                'payment_method'     => $paymentMethod,
                'is_service_history' => self::hasHistoryService($orderNo)
            ];

            return $result;
        }
        return ModelBase::errorMsg('未找到工单信息',20002);
    }

    /**
     * 是否有服务记录
     * @param $orderNo
     * @return bool
     * @author xi
     */
    public static function hasHistoryService($orderNo)
    {
        $where = [
            'order_no'      => $orderNo,
            'status'        => 4,
            'cancel_status' => 1
        ];
        $count = self::find()->where($where)->count();
        return $count>0?1:0;
    }

    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($where,$page=1,$pageSize=10,$type,$technicianId,$sort,$andWhere = [])
    {
        $departmentId = Technician::getDepartmentIdByTechnicianId($technicianId);

        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkRelTechnician::tableName(). ' as b'],' a.work_no = b.work_no');
        $db->innerJoin(['`'.WorkOrder::tableName().'` as c'], 'a.order_no = c.order_no' );
        $db->innerJoin(['`'. WorkOrderAssign::tableName() .'` as d'] , 'a.order_no = d.order_no and d.type in (1,3) and d.assign_type = 2 and d.service_department_id = '.$departmentId);
        $db->where($where);

        if($andWhere){
            foreach ($andWhere as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        $db->groupBy('a.work_no');

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }

            $db->select('a.order_no,a.work_no,a.plan_time,a.plan_time_type,a.status,a.cancel_status,a.work_stage,b.technician_id,b.type,b.is_self,a.assign_type');

            $db->orderBy("a.plan_time $sort");
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $orderNos = array_column($list,'order_no');
            $orderArr = WorkOrder::findAllByOrderNos($orderNos);

            $workNos = array_column($list,'work_no');
            //开始服务时间
            $beginServicesArr = WorkProcess::getTypeTimeData($workNos,4);

            $workRelTechRes = WorkRelTechnician::find()
                ->where("type != 2 and is_self != 2")
                ->andWhere(['technician_id' => $technicianId,'work_no' => $workNos])
                ->asArray()->all();
            $workRelTechArr = array_column($workRelTechRes,'work_no','work_no');

            //订单详情数据
            $orderDetailArr = [];
            $orderDetailRes = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNos,'is_del'=>1],'order_no,sale_order_id,sale_order_num');
            if($orderDetailRes){
                foreach ($orderDetailRes as $val){
                    $orderDetailArr[$val['order_no']][] = $val;
                }
            }

            //如果组长，把服务技师查出来
            $serviceTechnicianArr = [];
            if($type == 2 && $workNos){
                $serviceTechnicianArr = \webapp\modules\v2\models\WorkRelTechnician::getServiceTechnician($workNos);
            }



            foreach ($list as $key=>$val)
            {
                $accountId = $addressId = $workType = 0;
                $subjectName = '';
                if(isset($orderArr[$val['order_no']]))
                {
                    $accountId   = $orderArr[$val['order_no']]['account_id'];
                    $addressId   = $orderArr[$val['order_no']]['address_id'];
                    $workType    = $orderArr[$val['order_no']]['work_type'];
                    $subjectName = $orderArr[$val['order_no']]['subject_name'];
                }

                if($type == 2 && isset($workRelTechArr[$val['work_no']]))
                {
                    unset($list[$key]);
                    continue;
                }

                //组长显示服务技师
                if($type == 2)
                {
                    if(isset($serviceTechnicianArr[$val['work_no']])){
                        $list[$key]['serviceTechnicianArr'] = $serviceTechnicianArr[$val['work_no']];
                    }
                    else {
                        $list[$key]['serviceTechnicianArr'] = [];
                    }
                }

                $list[$key]['status']        = $val['type'] == 2?7:$val['status'];
                $list[$key]['status_desc']   = self::getStatusDesc($list[$key]['status']);
                if($val['status'] == 6)
                {
                    $list[$key]['status']        = $val['type'] == 2?7:1;
                    $list[$key]['status_desc']   = self::getStatusDesc(1);
                }

                $list[$key]['cancel_status_desc'] = $val['cancel_status']==1?'未关闭':'已关闭';
                $list[$key]['account_id']         = $accountId;
                $list[$key]['address_id']         = $addressId;
                $list[$key]['order_detail_arr']   = isset($orderDetailArr[$val['order_no']])?$orderDetailArr[$val['order_no']]:[];
                $list[$key]['work_type']          = $workType;
                $list[$key]['begin_service_time'] = isset($beginServicesArr[$val['work_no']])?$beginServicesArr[$val['work_no']]:'';
                $list[$key]['subject_name']       = $subjectName?$subjectName:'';
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

}
