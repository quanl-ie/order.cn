<?php
namespace webapp\modules\v2\models;

use common\models\Technician;
use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkOrderTrade;
use Yii;

class WorkProcess extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_process';
    }


    /**
     * 线下收款加上流水及过程
     * @param $workNo
     * @param $technicianId
     * @author xi
     */
    public static function addOfflineReceivables($workNo,$technicianId)
    {
        $technicianArr = Technician::findOneByAttributes(['id'=>$technicianId] , 'store_id');

        $model = new self();
        $model->department_id       = isset($technicianArr['store_id'])?$technicianArr['store_id']:0;
        $model->work_no             = $workNo;
        $model->operator_user_id    = $technicianId;
        $model->operator_user_type  = 2;
        $model->type                = 7;
        $model->comment             = '线下收款';
        $model->img1                = '';
        $model->img2                = '';
        $model->create_time         = time();
        $model->update_time         = time();
        if($model->save())
        {
            $tradeModel = new WorkOrderTrade();
            $tradeModel->trade_no       = '';
            $tradeModel->work_no        = $workNo;
            $tradeModel->pay_amount     = 0;
            $tradeModel->trade_status   = 1;
            $tradeModel->trade_pay_code = 3;
            $tradeModel->pay_time       = date('Y-m-d H:i:s');
            if($tradeModel->save()){
                return true;
            }
        }

        return false;
    }

}