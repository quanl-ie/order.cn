<?php
namespace webapp\modules\v2\models;

use webapp\models\ModelBase;
use Yii;
use yii\helpers\ArrayHelper;

class WorkOrder extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }

    /**
     * 根据订单号查出订单信息
     * @param $orderNos
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findAllByOrderNos($orderNos,$limit=1000)
    {
        $result = [];
        $where = [
            'a.order_no'   => $orderNos,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->where($where)
            ->select('a.*')
            ->limit($limit)
            ->asArray()->all();
        if($query)
        {
            return ArrayHelper::index($query,'order_no');
        }
        return $result;
    }
}
