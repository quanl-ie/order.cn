<?php
namespace webapp\modules\v2\models;

use webapp\models\ModelBase;
use Yii;
use webapp\modules\v1\models\SaleOrder;
use yii\helpers\ArrayHelper;

class WorkOrderDetail extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_detail';
    }

    /**
     * 查出订单的产品ids
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSaleOrderIdsByOrderNo($orderNo)
    {
        $result = [];

        $where = [
            'order_no'   => $orderNo,
        ];
        $query = self::findAllByAttributes($where,'sale_order_id');
        if($query){
            $result = array_column($query,'sale_order_id');
        }

        return $result;
    }

}