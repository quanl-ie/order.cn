<?php
//入口
namespace webapp\modules\v2;
use Yii;
use yii\web\Response;
use yii\web\Request;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'webapp\modules\v2\controllers';
    public $requestedRoute = '';

    public function init()
    {
        parent::init();  
    }
}
