<?php
namespace webapp\modules\v2\controllers;

use webapp\models\ModelBase;
use webapp\modules\v2\models\Work;
use webapp\modules\v2\models\WorkProcess;
use webapp\modules\v2\models\WorkRelTechnician ;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v2\models\Work';

    /**
     * 全部订单，未完成、已完成订单列表
     * @param  int   $type      1、我的工单  2、小组工单
     * @param  int   $status    1、全部  2、待指派  3、服务中   4、已完成  5、待收款   6、已关闭
     * @param  int   $grouping_id  所属小组ID
     * @param  int   $sort         排序方式  asc 正序   desc倒序
     * @return array
     */
    public function actionSearch()
    {
        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pageSize',10));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $type         = intval(Yii::$app->request->post('type',1));
        $status       = intval(Yii::$app->request->post('status',0));
        $sort         = trim(Yii::$app->request->post('sort',''));
        $accountAddressIds = Yii::$app->request->post('account_address_ids');

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }

        if($type == 1 && $status == 1) {
            return ModelBase::errorMsg('参数 status 与 type 不匹配');
        }

        if(!$status){
            $where = "(b.technician_id = $technicianId and b.is_self <> 2) and type_status = 1 ";
            if($type == 2) {
                $where = "(b.technician_id = $technicianId and b.is_self = 2) and type_status = 1 ";
            }
        }else if($status == 1) {//组长待指派
            $where = "(b.technician_id = $technicianId and a.status =6 and a.cancel_status = 1 and b.type = 1) and b.is_self = 2";
        }
        else if($status == 6){//关闭（取消）工单
            if($type == 2) {
                $where = " b.technician_id = $technicianId and (a.cancel_status = 2 or (b.type = 2 and type_status = 1) ) and b.is_self = 2 ";
            }
            else {
                $where = " b.technician_id = $technicianId and (a.cancel_status = 2 or (b.type = 2 and type_status = 1) ) and b.is_self <> 2 ";
            }
        }
        else {
            $where = "(b.technician_id = $technicianId and a.status in( $status) and a.cancel_status = 1 and b.type = 1) and b.is_self <> 2";
            if($type == 2) {
                $where = "(b.technician_id = $technicianId and a.status in( $status) and a.cancel_status = 1 and b.type = 1) and b.is_self = 2";
            }
        }

        $andWhere = [];
        if($accountAddressIds){
            $andWhere [] = ['c.address_id','in', $accountAddressIds];
        }

        return Work::getList($where,$page,$pageSize,$type,$technicianId,$sort,$andWhere);
    }


    /**
     * 工单详情
     * @author xi
     * @date 2018-2-1
     */
    public function actionDetail()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == ''){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }

        return Work::detail($workNo, $technicianId);
    }

    /**
     * 技师线下收款加流水及过程
     * @author xi
     */
    public function actionOfflineReceivables()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == ''){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }

        $res = WorkProcess::addOfflineReceivables($workNo,$technicianId);
        if($res){
            return [];
        }
        return ModelBase::errorMsg('操作失败',20004);
    }

    /**
     * 获取技师及工单信息
     * @return array
     * @author xi
     */
    public function actionGetTechnicianAndWork()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $technicianId = intval(Yii::$app->request->post('technician_id',''));
        if($workNo == ''){
            return ModelBase::errorMsg('参数 work_no 不能为空',20002);
        }
        if($technicianId == ''){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20003);
        }

        $res = WorkRelTechnician::getTechnicianAndWork($workNo,$technicianId);
        if($res){
            return $res;
        }
        return ModelBase::errorMsg('获取失败',20004);
    }

    /**
     * 判断服务时间是否冲突
     * @author xi
     * @date 2018-4-28
     */
    public function actionGetPlantimeConflict()
    {
        $technicianIds= Yii::$app->request->post('technician_ids');
        $plantime     = intval(Yii::$app->request->post('plantime',0));
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        if(!$technicianIds){
            return ModelBase::errorMsg('技师id 不能为空', 20002);
        }
        if(!$plantime){
            return ModelBase::errorMsg('服务时间不能为空', 20003);
        }
        if(!$workNo){
            return ModelBase::errorMsg('工单号不能为空', 20004);
        }

        return WorkRelTechnician::getPlantimeConflict($technicianIds,$plantime,$workNo);
    }

    /**
     * 查询工单的服务类型
     * @author xi
     */
    public function actionGetWorkWorkstage()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }

        $query = Work::findOneByAttributes(['work_no'=>$workNo],'work_stage');
        if($query){
            return $query['work_stage'];
        }
        return ModelBase::errorMsg('未找到数据',20003);
    }
}
