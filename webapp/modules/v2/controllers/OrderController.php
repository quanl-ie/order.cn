<?php
namespace webapp\modules\v2\controllers;

use webapp\controllers\ApiBaseController;
use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkOrderTechnician;
use webapp\modules\v2\models\WorkOrderDetail;
use Yii;


class OrderController extends ApiBaseController
{
    public $modelClass = 'webapp\modules\v2\models\Order';

    public function verbs()
    {
        return [
            'get-sale-order-num' => ['POST','OPTIONS'],
        ];
    }

    /**
     * 获取售后产品数量
     * @author xi
     */
    public function actionGetSaleOrderNum()
    {
        $productId = Yii::$app->request->post('product_id',0);
        $orderNo   = trim(Yii::$app->request->post('order_no',''));

        if($productId <= 0){
            return ModelBase::errorMsg('参数 product_id 不能为空',20002);
        }
        if($orderNo == ''){
            return ModelBase::errorMsg('参数 order_no 不能为空',20003);
        }

        $where = [
            'order_no'      => $orderNo,
            'sale_order_id' => $productId
        ];
        $orderDetailArr = WorkOrderDetail::findAllByAttributes($where,'sale_order_id,sale_order_num');
        if($orderDetailArr){
            return array_column($orderDetailArr,'sale_order_num','sale_order_id');
        }
        return [];
    }

    /**
     * 检查一下技师是否可以操作产品
     * @author xi
     */
    public function actionHasCrudProduct()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',0));
        $workNo    = trim(Yii::$app->request->post('work_no',0));
        $productId = intval(Yii::$app->request->post('product_id',0));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));

        if($workNo == ''){
            return ModelBase::errorMsg("参数 work_no 不能为空",20001);
        }
        if($orderNo == ''){
            return ModelBase::errorMsg("参数 order_no 不能为空",20002);
        }
        if($productId <= 0){
            return ModelBase::errorMsg("参数 product_id 不能为空",20003);
        }
        if($technicianId <= 0){
            return ModelBase::errorMsg("参数 technician_id 不能为空",20004);
        }

        $crud = 0;
        //查出技师是否被指派给了这个工单
        $workRelTechArr = WorkOrderTechnician::findOneByAttributes(['work_no'=>$workNo,'technician_id'=>$technicianId,'type'=>1] , 'id');
        $orderDetailArr = WorkOrderDetail::findOneByAttributes(['order_no'=>$orderNo,'sale_order_id'=>$productId,'is_del'=>1], 'id');
        if($workRelTechArr && $orderDetailArr){
            $crud = 1;
        }

        return [
            'crud' => $crud
        ];
    }

    /**
     * 查出订单的产品质保状态跟质保凭证
     * @return array|NULL|string|\yii\db\ActiveRecord
     * @author xi
     */
    public function actionGetOrderProduct()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',0));
        $productId = intval(Yii::$app->request->post('product_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg("参数 order_no 不能为空",20002);
        }
        if($productId <= 0){
            return ModelBase::errorMsg("参数 product_id 不能为空",20003);
        }

        $where = [
            'order_no'      => $orderNo,
            'sale_order_id' => $productId,
            'is_del'        => 1
        ];
        $select = "is_scope,scope_img";
        $orderDetailArr = WorkOrderDetail::findOneByAttributes($where,$select);
        if($orderDetailArr)
        {
            return $orderDetailArr;
        }
        return ModelBase::errorMsg("未找到数据",20004);
    }

    /**
     * 更新订单产品质保信息
     * @return array
     * @author xi
     */
    public function actionUpdateOrderScope()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',0));
        $productId = intval(Yii::$app->request->post('product_id',0));
        $isScope   = intval(Yii::$app->request->post('is_scope',0));
        $scopeImg  = Yii::$app->request->post('scope_img','');

        if($orderNo == ''){
            return ModelBase::errorMsg("参数 order_no 不能为空",20002);
        }
        if($productId <= 0){
            return ModelBase::errorMsg("参数 product_id 不能为空",20003);
        }

        $where = [
            'order_no'      => $orderNo,
            'sale_order_id' => $productId,
            'is_del'        => 1
        ];
        $model = WorkOrderDetail::findOne($where);
        if($model)
        {
            if($isScope){
                $model->is_scope = $isScope;
            }
            $model->scope_img   = $scopeImg;
            $model->update_time = time();
            if($model->save()){
                return [
                    'id' => $model->id
                ];
            }
        }
        return ModelBase::errorMsg("未找到数据",20004);
    }

}
