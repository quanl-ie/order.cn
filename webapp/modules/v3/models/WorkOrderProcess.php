<?php
namespace webapp\modules\v3\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderProcess extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_process';
    }

    /**
     * 获取类型
     * @param bool $index
     * @return array|mixed|string
     * @author  xi
     * @date 2017-12-21
     */
    public static function getStatus($index = false)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '待验收',
            5 => '已完成',
            6 => '已取消',
            7 => '待审核',
            8 => '审核不通过'
        ];
        if($index !==false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }

    /**
     * 订单号
     * @param $orderNo
     * @param $type
     * @return bool
     * @author xi
     * @date 2018-6-15
     */
    public static function add($orderNo,$status,$remark)
    {
        try
        {
            self::updateAll(['current'=>0],['order_no'=>$orderNo]);

            $model = new self();
            $model->order_no    = $orderNo;
            $model->status      = $status;
            $model->current     = 1;
            $model->remark      = $remark;
            $model->create_time = time();
            if($model->save()){
                return true;
            }
            else {
                throw new \Exception("订单过程保存失败",30004);
            }
        }
        catch (\Exception $e)
        {
            throw new \Exception("订单过程保存失败",30003);
        }
    }

    /**
     * 获取订单处理过程
     * @param $orderNo
     * @return array
     */
    public static function findAllByOrderNoAndStatus($orderNo,$status)
    {
        $result = [];
        $query = self::find()
            ->where(['order_no'=>$orderNo,'status'=>$status])
            ->select('*')
            ->orderBy('id desc')
            ->asArray()
            ->all();
        if($query)
        {
            foreach ($query as $val)
            {
                $result[$val['order_no']] = [
                    'status'      => $val['status'],
                    'status_desc' => self::getStatus($val['status']),
                    'remark'      => $val['remark'],
                    'create_time' => date('Y-m-d H:i:s',$val['create_time'])
                ];
            }
        }
        return $result;
    }

    /**
     * 获取订单状态信息
     * @param $order_no
     * @return array
     * @author  xi
     * @date 2017-12-22
     */
    public static function getOneByOrderNo($orderNo,$status)
    {
        $where = [
            'order_no' => $orderNo,
            'statu'    => $status
        ];
        $query = static::find()
            ->where($where)
            ->select('*')
            ->orderBy('id desc')
            ->asArray()
            ->one();
        if($query)
        {
            $query['status_desc'] = self::getStatus($query['status']);
            return $query;
        }
        return [];
    }
}