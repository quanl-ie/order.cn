<?php
namespace webapp\modules\v3\models;

use common\models\CostItemSet;
use common\models\CostPayType;
use common\models\DictEnum;
use Yii;

class WorkCost extends \webapp\modules\v1\models\WorkCost
{


    /**
     * 查出配件数据
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getPartsUseLog($orderNo)
    {
        $where = [
            'a.order_no'  => $orderNo,
            'a.status'    => 1,
            'a.cost_type' => 2

        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkCostDetail::tableName().'` as b'] , 'a.id = b.work_cost_id')
            ->where($where)
            ->select(["a.order_no","a.work_no","b.prod_id","b.prod_num","b.recycle_prod_id","b.serial_number"])
            ->asArray()
            ->all();

        return $query;
    }

    /**
     * 获取收费项目
     * @param $orderNo
     * @return array
     * @author xi
     */
    public static function getWorkServicesLog($orderNo)
    {
        $result = [];

        $where = [
            'a.order_no'  => $orderNo,
            'a.status'    => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkCostDetail::tableName().'` as b'] , 'a.id = b.work_cost_id')
            ->where($where)
            ->select([
                "a.order_no",
                "a.work_no",
                "a.cost_type",
                "a.payer_id",
                "a.cost_id",
                "b.prod_num",
                "b.cost_price",
                "b.cost_amount",
                "b.prod_unit",
                "a.voucher_img",
                "a.comment",
                "b.recycle_prod_id",
                "b.prod_id",
                "b.cost_real_amount"
            ])
            ->asArray()
            ->all();

        if($query)
        {
            //服务类型（上门费，安装费等）
            $costIds = array_column($query,'cost_id');
            $serviceTypeArr = CostItemSet::getName($costIds);

            //付费方
            $payerIds = array_column($query,'payer_id');
            $payTypeArr = CostPayType::getNameRel($payerIds);

            foreach ($query as $val)
            {
                $unitDesc = '次';
                if($val['prod_unit']){
                    $unitDesc = DictEnum::getDesc('enum_unit_id',$val['prod_unit']);
                }
                $price = '';
                if($val['cost_price'] > 0 && $val['prod_num'] > 0 && $val['cost_amount'] > 0){
                    $price = "￥".$val['cost_price']."/$unitDesc * " . $val['prod_num'] . "/$unitDesc = ￥" . $val['cost_amount'];
                }


                if($val['cost_type'] == 2){
                    $serviceType = '配件费';
                }
                else {
                    $serviceType = isset($serviceTypeArr[$val['cost_id']]) ? $serviceTypeArr[$val['cost_id']] : '';
                }

                $result[$val['work_no']][] = [
                    "type"           => $val['cost_type'],
                    "serviceType"    => $serviceType,
                    "payType"        => isset($payTypeArr[$val['payer_id']]) ? $payTypeArr[$val['payer_id']] : '',
                    "price"          => $price,
                    "certificateArr" => $val['voucher_img'] ? explode(',',$val['voucher_img']) : [],
                    "remark"         => $val['comment'],
                    "prod_id"        => $val['prod_id'],
                    'recycle_prod_id'=> $val['recycle_prod_id'],
                    "real_total_amount" => $val['cost_real_amount'],
                    "total_amount"      => $val['cost_amount']
                ];
            }
        }

        return $result;
    }

}