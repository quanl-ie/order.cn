<?php
namespace webapp\modules\v3\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderStatus extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_status';
    }

    /**
     * 订单状态信息 (1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getStatus($index=false)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '待验收',
            5 => '已完成',
            6 => '已取消',
            7 => '验收未通过',
            8 => '待指派',
            9 => '待服务',
            10 => '待审核',
            11 => '审核未通过',
            12 => '待收款',
            13 => '等待上级验收'
        ];
        if($index!=false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }
}