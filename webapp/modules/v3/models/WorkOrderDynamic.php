<?php
namespace webapp\modules\v3\models;

use webapp\models\ModelBase;
use Yii;

class WorkOrderDynamic extends ModelBase
{

    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_dynamic';
    }

    /**
     * 获取动态标题中文描述
     * @param $typeStatus 状态
     * @return string
     */
    public static function getStatusDesc($typeStatus)
    {
        $data = [
                1=>'新建', //创建订单（不指派机构或技师）
                2=>'新建', //创建订单（指派给机构）
                3=>'新建', //创建订单（指派给技师）
                4=>'指派', //指派订单给机构
                5=>'指派', //指派订单给技师
                6=>'指派', //技师将订单指派给技师
                7=>'改派', //改派订单给机构
                8=>'改派', //改派订单给技师
                9=>'取消', //下单人取消订单
                10=>'驳回', //驳回订单
                11=>'修改服务时间', //修改服务时间
                12=>'开始服务', //下级机构代替技师开始服务
                13=>'开始服务', //技师开始服务
                14=>'完成服务', //下级机构代替技师完成服务
                15=>'完成服务', //技师完成服务及收款
                16=>'预约下次上门', //下级机构预约下次上门
                17=>'预约下次上门', //技师预约下次上门
                18=>'通过验收', //验收通过
                19=>'通过验收', //最顶部机构验收通过
                20=>'验收不通过', //上级机构验收驳回
                21 =>'新建',
                22 => '取消',
                23 => '审核通过',
                24 => '审核未通过',
                25 => '改约',
                26 => '拉单建单',
                27 => '拉单建单',
                28 => '导入更新'


        ];

        return isset($data[$typeStatus])?$data[$typeStatus]:'';
    }


    /**
     * 填加动态
     * @param int $typeStatus 状态
     * @param string $orderNo 订单ID
     * @param string $workNo 工单ID
     * @param array $params 参数数组
     * @return bool
     */
    public static function add($typeStatus,$orderNo,$params,$workNo='')
    {
        try
        {
            $model = new self();
            $model->type_status = $typeStatus;
            $model->title       = self::getStatusDesc($typeStatus);
            $model->content     = '';
            $model->order_no    = $orderNo;
            $model->work_no     = $workNo;
            $model->status      = 2;
            $model->params      = json_encode($params);
            $model->create_time = time();
            $model->update_time = time();
            if($model->save()){
                return true;
            }
            else {
                throw new \Exception("动态添加失败",30005);
            }
        }
        catch (\Exception $e)
        {
            throw new \Exception("动态添加失败",30004);
        }
    }

    /**
     * 获取动态
     * @param string $orderNo 订单ID
     * @param integer $directCompanyId 直属公司ID
     * @return bool
     * @author xi
     */
    public static function getDynamic($orderNo,$directCompanyId)
    {
        $result = [];

        $where = [
            'order_no' => $orderNo,
            'status'   => 1
        ];
        $data = static::find()
            ->where($where)
            ->select('id,type_status,direct_company_id,direct_company_name,account_name,order_no,work_no,title,content,create_time')
            ->limit(1000)
            ->orderBy('create_time DESC, id DESC')
            ->asArray()
            ->all();

        if($data)
        {
            foreach ($data as $key=>$val)
            {
                if($directCompanyId == $val['direct_company_id']){
                    $content = sprintf($val['content'],$val['account_name']);
                }
                else{
                    $content = sprintf($val['content'],$val['direct_company_name']);
                }

                $result[] = [
                    'type'    => $val['title'],
                    'content' => $content,
                    'date'    => date('m-d',$val['create_time']),
                    'time'    => date('H:i',$val['create_time'])
                ];
            }
        }

        return $result;
    }
}