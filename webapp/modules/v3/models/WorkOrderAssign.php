<?php
namespace webapp\modules\v3\models;

use Yii;

class WorkOrderAssign extends \webapp\modules\v1\models\WorkOrderAssign
{


    /**
     * 更新服务机构指派类型 为指派技师
     * @param $orderNo
     * @throws \Exception
     * @author xi
     */
    public static function pullChangeAssignType($orderNo)
    {
        $where = [
            'order_no' => $orderNo,
            'type'     => [1,3],
            'assign_type' => 2
        ];
        $query = self::findOneByAttributes($where);
        if(!$query)
        {
            $where = [
                'order_no' => $orderNo,
                'type'     => [1,3]
            ];
            $model = self::find()
                    ->where($where)
                    ->orderBy('id desc')
                    ->limit(1)
                    ->one();
            if($model)
            {
                $model->assign_type = 2;
                if(!$model->save()){
                    throw new \Exception("更新失败",30006);
                }
            }
        }
    }
}