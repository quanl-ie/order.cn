<?php
namespace webapp\modules\v3\models;

use common\models\Department;
use common\models\Technician;
use webapp\modules\v1\models\WorkProcess;
use Yii;
use webapp\modules\v1\models\WorkOrderStatus as WorkOrderStatusV1;

class WorkRelTechnician extends \webapp\modules\v2\models\WorkRelTechnician
{

    /**
     * 获取订单服务公司直属id
     * @param $workNo
     * @return int|mixed
     * @author xi
     */
    public static function getDirectCompanyIdByWorkNo($workNo)
    {
        $query = self::findOneByAttributes(['work_no'=>$workNo,'is_self'=>1,'type'=>1],'technician_id');
        if($query)
        {
            $technicianId = $query['technician_id'];

            $queryDep = Technician::find()
                ->from(Technician::tableName() . ' as a')
                ->innerJoin(['`'.Department::tableName().'` as b'], 'a.store_id = b.id')
                ->select('b.direct_company_id')
                ->where(['a.id'=>$technicianId])
                ->asArray()->one();
            if($queryDep)
            {
                return $queryDep['direct_company_id'];
            }
        }

        return 0;
    }

    /**
     * 获取工单技师数据
     * @param $workNo
     * @return array
     * @author xi
     */
    public static function getTechnicianIdsByWorkNo($workNo,$isSelf = [1,2,3])
    {
        $technicianRes = WorkRelTechnician::findAllByAttributes(['work_no'=>$workNo,'type'=>1,'is_self'=>$isSelf],'technician_id');
        if($technicianRes){
            return array_column($technicianRes,'technician_id');
        }

        return [];
    }

    /**
     * 指派技师
     * @param $technicianArr
     * @param $workNo
     * @param $createUserId
     * @throws \Exception
     */
    public static function addTechnician($technicianArr,$orderNo,$workNo,$createUserId,$directCompanyId)
    {
        if($technicianArr)
        {
            foreach ($technicianArr as $val)
            {
                if(!WorkRelTechnician::add($workNo,$val['id'],$val['is_self'],13,$createUserId)){
                    throw new \Exception("指派技师失败",30003);
                }
            }

            //更新工单状态
            Work::updateAll(['status'=>2,'update_time'=>time()], ['work_no'=>$workNo]);
            //工单过程表加一条数据
            WorkProcess::add($workNo,$directCompanyId,$createUserId,1,2,'已指派');
        }
    }

    /**
     * 更新指派技师
     * @param $technicianArr
     * @param $workNo
     * @param $createUserId
     * @throws \Exception
     * @author xi
     */
    public static function pullChange($technicianArr,$workNo,$createUserId)
    {
        if($technicianArr && $workNo)
        {
            self::updateAll(['type'=>2,'update_time'=>time()],['work_no'=>$workNo]);
            
            foreach ($technicianArr as $val)
            {
                if(!WorkRelTechnician::add($workNo,$val['id'],$val['is_self'],13,$createUserId)){
                    throw new \Exception("指派技师失败",300041);
                }
            }

            return true;
        }
    }
}