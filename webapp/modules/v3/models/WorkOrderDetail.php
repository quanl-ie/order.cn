<?php
namespace webapp\modules\v3\models;

use webapp\models\ModelBase;
use Yii;
use webapp\modules\v3\models\SaleOrder;
use yii\helpers\ArrayHelper;

class WorkOrderDetail extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_detail';
    }

    /**
     * 查出订单的产品ids
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSaleOrderIdsByOrderNo($orderNo)
    {
        $result = [];

        $where = [
            'order_no'   => $orderNo,
        ];
        $query = self::findAllByAttributes($where,'sale_order_id');
        if($query){
            $result = array_column($query,'sale_order_id');
        }

        return $result;
    }

    /**
     * 查出订单的产品ids
     * @param $orderNo
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSaleOrderIdsByOrderNos(array $orderNos)
    {
        $result = [];

        $where = [
            'order_no' => $orderNos,
            'is_del'   => 1
        ];
        $query = self::findAllByAttributes($where,'order_no,sale_order_id');
        if($query)
        {
            foreach ($query as $val){
                $result[$val['order_no']][] = $val['sale_order_id'];
            }
        }

        return $result;
    }

    /**
     * 拉取更新订单产品
     * @param $orderNo
     * @param $faultImgs
     * @param $scope
     * @param $saleOrderArr
     * @throws \Exception
     * @author xi
     */
    public static function pullChange($orderNo,$faultImgs,$scope,$saleOrderArr)
    {
        if($saleOrderArr)
        {
            $oldSaleOrderIds = [];
            $query = self::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1], 'sale_order_id');
            if($query){
                $oldSaleOrderIds = array_column($query,'sale_order_id');
            }

            //新的产品数组
            $newSaleOrderIds = array_column($saleOrderArr,'id');

            //删除的
            $delArr = array_diff($oldSaleOrderIds,$newSaleOrderIds);
            //新加的
            $addArr = array_diff($newSaleOrderIds,$oldSaleOrderIds);
            //无变化的
            $unchangedArr = array_intersect($oldSaleOrderIds,$newSaleOrderIds);

            if($delArr){
                self::updateAll(['is_del'=>2], ['order_no'=>$orderNo,'sale_order_id'=>$delArr]);
            }
            if($addArr)
            {
                foreach ($saleOrderArr as $val)
                {
                    if(in_array($val['id'], $addArr))
                    {
                        $detailModel = new WorkOrderDetail();
                        $detailModel->order_no       = $orderNo;
                        $detailModel->sale_order_id  = $val['id'];
                        $detailModel->sale_order_num = $val['num'];
                        $detailModel->fault_img      = $faultImgs ? implode(',' , $faultImgs):'';
                        $detailModel->create_time    = time();
                        $detailModel->update_time    = time();
                        $detailModel->is_scope       = $scope;
                        if(!$detailModel->save()){
                            throw new \Exception("更新订单详情失败",300032);
                        }
                    }
                }
            }

            //更新无变化的产品
            if($unchangedArr)
            {
                foreach ($saleOrderArr as $val)
                {
                    if(in_array($val['id'], $unchangedArr))
                    {
                        $detailModel = WorkOrderDetail::findOne(['order_no'=>$orderNo,'sale_order_id'=>$val['id']]);
                        $detailModel->sale_order_num = $val['num'];
                        $detailModel->fault_img      = $faultImgs ? implode(',' , $faultImgs):'';
                        $detailModel->update_time    = time();
                        $detailModel->is_scope       = $scope;
                        if(!$detailModel->save()){
                            throw new \Exception("更新订单详情失败",300033);
                        }
                    }
                }
            }
        }
    }

    /**
     * 获取订单产品信息
     * @param $orderNo
     * @return array
     * @author xi
     */
    public static function listAllByOrderNo($orderNo , $select = 'sale_order_id,is_scope,sale_order_num,scope_img')
    {
        $where = [
            'order_no' => $orderNo,
            'is_del'   => 1
        ];
        $query = self::findAllByAttributes($where,$select);
        if($query){
            return $query;
        }

        return [];
    }
}