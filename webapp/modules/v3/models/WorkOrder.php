<?php
namespace webapp\modules\v3\models;

use common\helpers\Helper;
use common\models\Common;
use webapp\models\ModelBase;
use Yii;
use yii\helpers\ArrayHelper;

class WorkOrder extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }

    /**
     * 订单基础信息
     * @param $orderNo
     * @param $departmentId
     * @param $directCompanyId
     * @return array
     * @author xi
     */
    public static function getWorkBasics($orderNo)
    {
        $where = [
            'a.order_no' => $orderNo,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkOrderDetail::tableName().'` as b'], 'a.order_no = b.order_no')
            ->where($where)
            ->select([
                "a.order_no",
                "a.subject_name",
                "a.customer_name",
                "a.customer_mobile",
                "a.customer_address",
                "a.work_type",
                "a.plan_time",
                "a.plan_time_type",
                "a.create_time",
                "a.description",
                "b.fault_img"
            ])
            ->groupBy("a.order_no")
            ->asArray()->one();
        if($query)
        {
            $basicsInfo =  [
                "orderNo"       => $query['order_no'],
                "subject"       => $query['subject_name'],
                "customerName"  => $query['customer_name'],
                "customerPhone" => $query['customer_mobile'],
                "address"       => $query['customer_address'],
                "workType"      => Common::getWorktypeName($query['work_type']),
                "planTime"      => Helper::setDate($query['plan_time'], $query['plan_time_type']),
                "faultImgs"     => $query['fault_img'] ? explode(',', $query['fault_img']):[],
                "createTime"    => date('Y-m-d H:i:s', $query['create_time']),
                "remark"        => $query['description']
            ];

            return $basicsInfo;
        }
        return [];
    }

}
