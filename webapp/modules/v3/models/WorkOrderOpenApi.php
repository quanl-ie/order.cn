<?php
namespace webapp\modules\v3\models;

use common\helpers\Helper;
use common\models\Common;
use webapp\models\ModelBase;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class WorkOrderOpenApi extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }

    /**
     * 订单来源
     * @param $key
     * @return mixed|string
     * @author xi
     */
    public static function getSource($key)
    {
        $data = [
            1 => '商家创建',
            4 => '小程序',
            5 => '导入'
        ];

        return isset($data[$key])?$data[$key]:'';
    }

    /**
     * 订单列表
     * @param string/array $where
     * @param string $select
     * @param number $page
     * @param number $pageSize
     * @return array
     * @author xi
     * @date 2017-12-14
     */
    public static function getList ($where, $srcDirectCompanyId ,$srcDepartmentId,$page=1,$pageSize=10)
    {

        $select = [
            'a.order_no',
            'a.external_order_no',
            'a.subject_name',
            'a.customer_name',
            'a.customer_mobile',
            'a.customer_address',
            'a.source',
            'a.create_time',
            'b.status'
        ];

        $subQuery = (new Query());
        $subQuery->select($select);
        $subQuery->from(self::tableName() . " as a");
        $subQuery->innerJoin(['`'.WorkOrderStatus::tableName().'` as b'] , 'a.order_no = b.order_no');

        if($where)
        {
            foreach ($where as $key=>$val)
            {
                if(is_array($val))
                {
                    if(!is_numeric($key)){
                        $subQuery->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $subQuery->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $subQuery->andWhere([$key=>$val]);
                }
            }
        }

        $subQuery->orderBy("b.`department_id` <> $srcDepartmentId, b.`department_id` <>  $srcDirectCompanyId");

        $db = (new Query());
        $db->from(['t'=>$subQuery]);
        $db->groupBy("order_no");


        //总数
        $totalNum = $db->count('*',self::getDb());

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }

            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->orderBy('create_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);

            $query = $db->all(self::getDb());

            $list = [];

            foreach ($query as $key=>$val)
            {
                $list[] = [
                    'orderNo'        => $val['order_no'],
                    'subject'        => $val['subject_name'],
                    'customerName'   => $val['customer_name'],
                    'customerMobile' => $val['customer_mobile'],
                    'status'         => WorkOrderStatus::getStatus($val['status']),
                    'source'         => self::getSource($val['source']),
                    'address'        => $val['customer_address'],
                    'createTime'     => date('Y-m-d H:i:s', $val['create_time']),
                    'external_order_no' => $val['external_order_no'],
                ];
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 获取订单状态
     * @param $where
     * @param $srcDirectCompanyId
     * @param $srcDepartmentId
     * @return array
     * @author xi
     */
    public static function getStatus ($where, $srcDirectCompanyId ,$srcDepartmentId)
    {
        $select = [
            'a.external_order_no',
            'b.status',
            'a.order_no'
        ];

        $subQuery = (new Query());
        $subQuery->select($select);
        $subQuery->from(self::tableName() . " as a");
        $subQuery->innerJoin(['`'.WorkOrderStatus::tableName().'` as b'] , 'a.order_no = b.order_no');
        $subQuery->where($where);
        $subQuery->orderBy("b.`department_id` <> $srcDepartmentId, b.`department_id` <>  $srcDirectCompanyId");

        $db = (new Query());
        $db->from(['t'=>$subQuery]);
        $db->groupBy("external_order_no");
        $query = $db->all(self::getDb());

        $list = [];
        foreach ($query as $key=>$val)
        {
            $list[] = [
                'order_no'   => $val['order_no'],
                'status'     => $val['status'],
                'statusDesc' => WorkOrderStatus::getStatus($val['status']),
                'external_order_no'   => $val['external_order_no'],
            ];
        }

        return $list;
    }

    /**
     * 订单基础信息
     * @param $orderNo
     * @param $departmentId
     * @param $directCompanyId
     * @return array
     * @author xi
     */
    public static function getWorkBasics($orderNo,$departmentId,$directCompanyId)
    {
        $where = [
            'a.order_no' => $orderNo,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkOrderDetail::tableName().'` as b'], 'a.order_no = b.order_no')
            ->where($where)
            ->select([
                "a.order_no",
                "a.subject_name",
                "a.customer_name",
                "a.customer_mobile",
                "a.customer_address",
                "a.work_type",
                "a.plan_time",
                "a.plan_time_type",
                "a.create_time",
                "a.description",
                "b.fault_img"
            ])
            ->groupBy("a.order_no")
            ->asArray()->one();
        if($query)
        {

            $statusKeyArr = [];
            $statusArr = self::getStatus($where,$directCompanyId,$departmentId);
            if($statusArr){
                $statusKeyArr = array_column($statusArr,'statusDesc','order_no');
            }

            $basicsInfo =  [
                "status"        => isset($statusKeyArr[$query['order_no']]) ? $statusKeyArr[$query['order_no']]: '',
                "orderNo"       => $query['order_no'],
                "subject"       => $query['subject_name'],
                "customerName"  => $query['customer_name'],
                "customerPhone" => $query['customer_mobile'],
                "address"       => $query['customer_address'],
                "workType"      => Common::getWorktypeName($query['work_type']),
                "planTime"      => Helper::setDate($query['plan_time'], $query['plan_time_type']),
                "faultImgs"     => $query['fault_img'] ? explode(',', $query['fault_img']):[],
                "createTime"    => date('Y-m-d H:i:s', $query['create_time']),
                "remark"        => $query['description']
            ];

            return $basicsInfo;
        }
        return [];
    }

    /**
     * 根据外部订单号查出自已系统订单号
     * @param $externalOrderNo
     * @return array|mixed
     * @author xi
     */
    public static function getOrderNoByExt($externalOrderNo,$source)
    {
        $query = self::findOneByAttributes(['external_order_no' => $externalOrderNo,'source'=>$source],'order_no');
        if($query){
            return $query['order_no'];
        }

        return '';
    }
}
