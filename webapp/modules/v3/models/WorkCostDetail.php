<?php
namespace webapp\modules\v3\models;

use Yii;
use yii\db\ActiveRecord;

class WorkCostDetail extends ActiveRecord
{

    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost_detail';
    }

}