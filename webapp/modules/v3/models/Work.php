<?php
namespace webapp\modules\v3\models;

use common\helpers\Helper;
use common\models\Common;
use common\models\ScheduleTechnicianQueue;
use common\models\Technician;
use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkOrderAssign;
use webapp\modules\v1\models\WorkProcess;
use Yii;

class Work extends ModelBase
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work';
    }

    /**
     * 状态中文描述
     * @param $index
     * @return mixed|string
     */
    public static function getStatusDesc($index)
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '已完成',
            5 => '待收款',
            6 => '组长待指派',
            7 => '已改派'
        ];

        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 获取服务流程 + 服务产品名称
     * @param $workNo
     * @return string
     * @author xi
     */
    public static function getTitle($workNo)
    {
        $query = self::findOneByAttributes(['work_no'=>$workNo],'work_stage,order_no');
        if($query)
        {
            $flow = $query['work_stage'];
            //服务流程
            $flowTitle = Common::getServiceFlow($flow);
            //服务产品名称
            $prodName = '';

            $detailArr = WorkOrderDetail::find()
                ->where(['order_no' => $query['order_no'],'is_del' => 1])
                ->select('sale_order_id')
                ->orderBy('id asc')
                ->limit(1)
                ->asArray()->one();
            if($detailArr)
            {
                $saleOrderNameArr = Common::getSaleOrderName($detailArr['sale_order_id']);
                if($saleOrderNameArr){
                    $prodName = $saleOrderNameArr[$detailArr['sale_order_id']];
                }
            }

            return $flowTitle . ' ' . $prodName;
        }

        return '';
    }

    /**
     * 获取服务地址id
     * @param $workNo
     * @return int|mixed
     * @author xi
     */
    public static function getAddressId($workNo,$departmentId)
    {
        $where = [
            'a.work_no' => $workNo
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.WorkOrder::tableName().'` as b'] , 'a.order_no = b.order_no')
            ->innerJoin(['`'.WorkOrderAssign::tableName().'` as c'], 'c.order_no = b.order_no and c.type in(1,3) and c.assign_type = 2 and c.service_department_id = '.$departmentId )
            ->where($where)
            ->select('b.address_id')
            ->asArray()->one();

        if($query)
        {
            return $query['address_id'];
        }

        return 0;
    }

    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($technicianId,$accountAddressIds,$page=1,$pageSize=10)
    {
        $departmentId = Technician::getDepartmentIdByTechnicianId($technicianId);

        $where = [
            'b.technician_id' => $technicianId,
            'b.type' => 1,
            'a.status' => [2,3,5],
            'a.cancel_status' => 1
        ];

        $orWhere = '';
        if($accountAddressIds){
            $where['c.address_id'] = $accountAddressIds;
            $orWhere = " and c.address_id in(".implode(',',$accountAddressIds).")";
        }

        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin([WorkRelTechnician::tableName(). ' as b'],' a.work_no = b.work_no');
        $db->innerJoin(['`'.WorkOrder::tableName().'` as c'], 'a.order_no = c.order_no' );
        $db->innerJoin(['`'. WorkOrderAssign::tableName() .'` as d'] , 'a.order_no = d.order_no and d.type in (1,3) and d.assign_type = 2 and d.service_department_id = '.$departmentId);
        $db->where($where);
        $db->andWhere(['<>','b.is_self',2]);


        $db->orWhere("(a.status = 4 and a.cancel_status = 1 and a.update_time > ".strtotime('today')." and a.update_time < ".strtotime('tomorrow'). $orWhere . " ) ");
        $db->groupBy('a.work_no');

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->select('
                a.order_no,a.work_no,a.plan_time,a.plan_time_type,a.status,a.cancel_status,a.work_stage,
                b.technician_id,b.type,b.is_self,
                c.account_id,c.address_id,c.subject_name,c.work_type
            ');

            $db->orderBy("a.plan_time desc");
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $orderNos = array_column($list,'order_no');

            //订单详情数据
            $orderDetailArr = [];
            $orderDetailRes = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNos,'is_del'=>1],'order_no,sale_order_id,sale_order_num');
            if($orderDetailRes){
                foreach ($orderDetailRes as $val){
                    $orderDetailArr[$val['order_no']][] = $val;
                }
            }

            foreach ($list as $key=>$val)
            {
                $list[$key]['status']        = $val['type'] == 2?7:$val['status'];
                $list[$key]['status_desc']   = Work::getStatusDesc($list[$key]['status']);
                $list[$key]['order_detail_arr']   = isset($orderDetailArr[$val['order_no']])?$orderDetailArr[$val['order_no']]:[];
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 获取工单的 服务时间，服务技师
     * @param $workNo
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getPlantimeAndTechnician($workNo)
    {
        if($workNo)
        {
            $where = [
                'a.work_no'       => $workNo,
                'a.cancel_status' => 1,
                'b.type'          => 1,
                'b.is_self'       => [1,3]
            ];
            $query = self::find()
                ->from(self::tableName() . ' as a')
                ->innerJoin(['`'.WorkRelTechnician::tableName().'` as b'], 'a.work_no = b.work_no')
                ->where($where)
                ->select("a.work_no,a.plan_time,a.plan_time_type,b.technician_id,b.is_self")
                ->orderBy("b.is_self asc")
                ->asArray()->all();

            if($query)
            {
                $planTime = $planTimeType = 0;
                $technicianArr = [];
                foreach ($query as $val)
                {
                    if($planTime == 0){
                        $planTime = $val['plan_time'];
                    }
                    if($planTimeType == 0){
                        $planTimeType = $val['plan_time_type'];
                    }
                    $technicianArr [] = [
                        'id'      => $val['technician_id'],
                        'is_self' => $val['is_self']
                    ];
                }

                return [
                    'workNo'        => $workNo,
                    'planTime'      => $planTime,
                    'planTimeType'  => $planTimeType,
                    'technicianArr' => $technicianArr
                ];
            }
        }

        return [];
    }


    /**
     * 改约操作
     * @param $workNo
     * @param $assignType
     * @param $plantime
     * @param $plantimeType
     * @param $technicianId
     * @param $serviceTechnicianArr
     * @param $reason
     * @return array
     * @throws \yii\db\Exception
     * @author xi
     */
    public static function changeReschedule($workNo,$assignType,$plantime,$plantimeType,$technicianId,$serviceTechnicianArr,$reason)
    {
        $transaction = self::getDb()->beginTransaction();
        try
        {

            $opTypeDy = 'changePlantime';

            $where = [
                'work_no'       => $workNo,
                'status'        => 2,
                'cancel_status' => 1
            ];
            $model = self::find()
                ->where($where)
                ->one();
            if($model)
            {
                //记录旧的服务时间,排班日历用
                $oldPlantime = $model->plan_time;

                if($plantime >0 && in_array($plantimeType,[1,2,3,4,5]))
                {
                    $model->plan_time      = $plantime;
                    $model->plan_time_type = $plantimeType;

                    //修改工单的服务时间
                    $changeOrderPlantimeFun = function( $model){
                        WorkOrder::updateAll([
                            'plan_time'      => $model->plan_time,
                            'plan_time_type' => $model->plan_time_type
                        ],
                        [
                            'order_no' => $model->order_no
                        ]);
                    };
                }
                else if($oldPlantime < time())
                {
                    throw new \Exception("服务时间不能小于当前时间", 30005);
                }

                $model->update_time = time();

                //如果暂无指派，把工单状态改成，待指配
                if( $assignType == 2 )
                {
                    //如果以前指派的是多技师变成 1 如果是组长变成 6
                    $model->status = 6;
                    /*if($model->assign_type == 1){
                        $model->status = 1;
                    }
                    else {
                        $model->status = 6;
                    }*/
                }

                if($model->save())
                {
                    if(isset($changeOrderPlantimeFun)){
                        $changeOrderPlantimeFun($model);
                    }

                    //如果指派了指师 （1指派技师  2 暂不指定）
                    if($assignType == 1)
                    {
                        //查出工单负责人及协助
                        $oldTechnicianIds = WorkRelTechnician::getTechnicianIdsByWorkNo($workNo,[1,3]);
                        $newTechnicianIds = array_column($serviceTechnicianArr,'id');

                        //算出新加的
                        $newAddJsIds = array_diff($newTechnicianIds,$oldTechnicianIds);
                        //算出要删除的
                        $delJsIds = array_diff($oldTechnicianIds,$newTechnicianIds);

                        //要删除技师
                        if($delJsIds)
                        {
                            $attr = [
                                'type'         => 2,
                                'update_time'  => time(),
                                'op_user_type' => 11,
                                'op_user_id'   => $technicianId,
                                'reason'       => $reason
                            ];
                            if(!WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'technician_id'=>$delJsIds,'type'=>1,'is_self'=>[1,3]])){
                                throw new \Exception("改约失败", 30001);
                            }
                        }

                        //新加的技师
                        if($newAddJsIds)
                        {
                            foreach ($serviceTechnicianArr as $val)
                            {
                                if(in_array($val['id'] , $newAddJsIds))
                                {
                                    //如果新加的技师是负责人，把旧的负责人改成绡
                                    if($val['is_self'] == 1){
                                        WorkRelTechnician::updateAll([ 'is_self'=>3 ],['work_no' =>$workNo,'type'=>1,'is_self'=>1]);
                                    }
                                    if(!WorkRelTechnician::add($workNo,$val['id'],$val['is_self'],11,$technicianId)){
                                        throw new \Exception("改约失败", 30002);
                                    }
                                }
                            }

                            //如果以前有改派过的，给了 type_status 改 0，以免一个单子会在 待服务，已关闭出现
                            $where = [
                                'work_no'       => $workNo,
                                'type'          => 2,
                                'type_status'   => 1,
                                'technician_id' => $newAddJsIds
                            ];
                            WorkRelTechnician::updateAll(['type_status'=>0],$where);
                            unset($relTechArr,$where);
                        }

                        //如果新加，删除都没有，看看负责人改了没有
                        if(!$newAddJsIds && !$delJsIds)
                        {
                            $lenderJs = $oldLenderJs = 0;
                            $newServiceTechnicianArr = [];
                            foreach ($serviceTechnicianArr as $val)
                            {
                                if($val['is_self'] == 1){
                                    $lenderJs = $val['id'];
                                    $newServiceTechnicianArr[] = $val;
                                    break;
                                }
                            }

                            //查出旧的工单负责人
                            $relTechArr = WorkRelTechnician::findOneByAttributes(['work_no'=>$workNo,'type'=>1,'is_self'=>1],'technician_id');
                            if($relTechArr){
                                $oldLenderJs = $relTechArr['technician_id'];
                            }

                            //如果更换了负责人，把旧的改成协助
                            if($lenderJs != $oldLenderJs)
                            {
                                WorkRelTechnician::updateAll(['is_self'=>3],['work_no'=>$workNo,'type'=>1,'is_self'=>1,'technician_id'=>$oldLenderJs]);
                                WorkRelTechnician::updateAll(['is_self'=>1],['work_no'=>$workNo,'type'=>1,'is_self'=>3,'technician_id'=>$lenderJs]);

                                $opTypeDy = 'assignJs';
                            }
                        }

                        //以下代码为了解决，可能出现没有负责人情况
                        if($serviceTechnicianArr)
                        {
                            foreach ($serviceTechnicianArr as $val)
                            {
                                //如果新加的技师是负责人，把旧的负责人改成绡
                                if($val['is_self'] == 1){
                                    WorkRelTechnician::updateAll([ 'is_self'=>1 ],['work_no' =>$workNo,'type'=>1,'technician_id'=>$val['id'],'is_self'=>3]);
                                    break;
                                }
                            }
                        }

                        if($newAddJsIds || $delJsIds){
                            $opTypeDy = 'assignJs';
                        }
                    }
                    //暂不指定
                    else
                    {
                        $opTypeDy = 'noAssignJs';

                        //如果不指定，把以前的技师查出，把排班数据给删除服
                        $technicianIdRes = WorkRelTechnician::findAllByAttributes(['work_no' =>$workNo,'type'=>1,'is_self'=>[1,3]],'technician_id');
                        if($technicianIdRes){
                            $delJsIds = array_column($technicianIdRes,'technician_id');
                        }


                        $attr = [
                            'type'         => 2,
                            'update_time'  => time(),
                            'op_user_type' => 11,
                            'op_user_id'   => $technicianId,
                            'reason'       => $reason
                        ];
                        if(!WorkRelTechnician::updateAll($attr,['work_no' =>$workNo,'type'=>1,'is_self'=>[1,3]])){
                            throw new \Exception("改约失败", 30003);
                        }
                    }

                    //动态
                    $params = [
                        'jsId'         => $technicianId,
                        'planTime'     => $plantime,
                        'planTimeType' => $plantimeType,
                        'serviceJsArr' => $serviceTechnicianArr,
                        'reason'       => $reason,
                        'opType'       => $opTypeDy
                    ];
                    WorkOrderDynamic::add(25,$model->order_no,$params,$workNo);

                    //技师排班
                    $planTimePb = '';
                    if($model->plan_time >0){
                        $planTimePb = date('Y-m-d',$model->plan_time);
                    }
                    $params = [
                        'newAddJsIds' => isset($newAddJsIds)?$newAddJsIds:[],
                        'delJsIds'    => isset($delJsIds)?$delJsIds:[]
                    ];
                    ScheduleTechnicianQueue::push($workNo,2,$planTimePb, date('Y-m-d',$oldPlantime), json_encode($params));

                    $transaction->commit();
                    return [
                        'work_no' => $workNo
                    ];
                }
                else {
                    throw new \Exception("改约失败", 30006);
                }
            }
            else {
                throw new \Exception("改约失败", 30005);
            }
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg($e->getMessage(),$e->getCode());
        }
    }

    /**
     * 更新工单
     * @param $orderNo
     * @param $workStage
     * @param $planTime
     * @param $planTimeType
     * @throws \Exception
     * @author xi
     */
    public static function pullChange($orderNo,$workStage,$planTime,$planTimeType)
    {

        $where = [
            'order_no' => $orderNo,
            'status'   => [1,2,3,5,6],
            'cancel_status' => 1
        ];
        $model = self::findOne($where);
        if($model)
        {
            $model->work_stage     = $workStage;
            $model->plan_time      = $planTime;
            $model->plan_time_type = $planTimeType;
            $model->update_time    = time();
            if($model->save()){
                return $model->work_no;
            }
        }

        throw new \Exception("改约失败", 300051);
    }

    /**
     * 获取工单服务记录
     * @param $orderNo
     * @return array
     * @author xi
     */
    public static function getWorkServicesLog($orderNo)
    {
        $result = [];

        $where = [
            'order_no'      => $orderNo,
            'cancel_status' => 1
        ];
        $query = self::find()->where($where)->orderBy('id desc')->asArray()->all();
        if($query)
        {
            //服务产品
            $saleOrderArr = WorkOrderDetail::listAllByOrderNo($orderNo,"sale_order_id,sale_order_num");
            $workNos = array_column($query,'work_no');
            $technicianArr = WorkRelTechnician::getServiceTechnician($workNos);

            //收费项目
            $payServiceArr = WorkCost::getWorkServicesLog($orderNo);

            foreach ($query as $val)
            {
                $startTime = WorkProcess::getTypeTimeData($val['work_no'], 4 );

                $result [] = [
                    "status"            => self::getStatusDesc($val['status']),
                    "workNo"            => $val['work_no'],
                    "workStage"         => Common::getServiceFlow($val['work_stage']),
                    "productArr"        => $saleOrderArr,
                    "plantime"          => $val['plan_time'] > 0 ? Helper::setDate($val['plan_time'], $val['plan_time_type']) : '',
                    "startTime"         => isset($startTime[$val['work_no']]) ? date('Y-m-d H:i:s', $startTime[$val['work_no']]) : '',
                    "technician"        => isset($technicianArr[$val['work_no']]) ? $technicianArr[$val['work_no']] : [],
                    "serviceWorkPicArr" => $val['work_img'] !='' ? explode(',', $val['work_img']) : [],
                    "scenePhotoArr"     => $val['scene_img'] !='' ? explode(',', $val['scene_img']) : [],
                    "serviceRecord"     => $val['service_record'],
                    "customerSignature" => $val['signature'] ? $val['signature'] : '',
                    "payServiceArr"     => isset($payServiceArr[$val['work_no']]) ? $payServiceArr[$val['work_no']]: []
                ];
            }
        }

        return $result;
    }


}
