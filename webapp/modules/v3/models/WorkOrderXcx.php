<?php
namespace webapp\modules\v3\models;

use common\models\PushMessage;
use common\models\ScheduleTechnician;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkAppraisal;
use webapp\modules\v1\models\WorkCost;
use webapp\modules\v1\models\WorkOrderTrade;
use webapp\modules\v1\models\WorkProcess;
use webapp\modules\v1\models\WorkProduct;
use Yii;
use webapp\models\ModelBase;
use common\models\Region;

class WorkOrderXcx extends WorkOrder
{

    /**
     * 订单状态信息 (1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消)
     * @param int $index
     * @return string
     * @author xi
     * @date 2017-12-19
     */
    public static function getStatus($index=false)
    {
        $data = [
            1 => '待服务',
            2 => '待服务',
            3 => '服务中',
            4 => '服务中',
            5 => '已完成',
            6 => '已取消',
            7 => '验收未通过',
            8 => '被驳回',
            9 => '上门超时',
            10 => '待审核',
            11 => '审核未通过',
            12 => '服务中',
            13 => '服务中',
        ];
        if($index!=false){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }

    /**
     * 订单列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(["`".WorkOrderStatus::tableName() . '` as c '],' a.order_no = c.order_no');
//        $db->where(['a.source'=>4]);
        $db->groupBy('a.order_no');
        
        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('a.order_no,a.plan_time,a.plan_time_type,a.work_type,a.address_id,c.status');

            $db->orderBy('a.create_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            
            $db->asArray();
            $list = $db->all();

            //查出多产品
            $orderRelProductArr = WorkOrderDetail::getSaleOrderIdsByOrderNos(array_column($list,'order_no'));

            foreach ($list as $key=>$val)
            {
                $list[$key]['status_desc'] = self::getStatus($list[$key]['status']);
                if(isset($orderRelProductArr[$val['order_no']])){
                    $list[$key]['sale_order_arr'] =  $orderRelProductArr[$val['order_no']];
                }
                else {
                    $list[$key]['sale_order_arr'] = [];
                }
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 待评价列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     */
    public static function getWaitAppraisalList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->innerJoin(["`".WorkOrderStatus::tableName() . '` as c '],' a.order_no = c.order_no');
        $db->innerJoin(['`'.WorkAppraisal::tableName().'` as b'], 'a.order_no = b.order_no');
        $db->groupBy('a.order_no');

        $db->where([
            'b.appraisal_status' => 1,
            'b.status' => 1
        ]);

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }

                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('a.order_no,a.plan_time,a.plan_time_type,a.work_type,a.address_id,c.status');

            $db->orderBy('a.create_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);

            $db->asArray();
            $list = $db->all();

            //查出多产品
            $orderRelProductArr = WorkOrderDetail::getSaleOrderIdsByOrderNos(array_column($list,'order_no'));

            foreach ($list as $key=>$val)
            {
                $list[$key]['status_desc'] = self::getStatus($list[$key]['status']);
                if(isset($orderRelProductArr[$val['order_no']])){
                    $list[$key]['sale_order_arr'] =  $orderRelProductArr[$val['order_no']];
                }
                else {
                    $list[$key]['sale_order_arr'] = [];
                }
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 取消订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @param $cancelReason 取消原因
     * @author xi
     * @date 2017-12-15
     */
    public static function cancelOrder($orderNo,$accountId,$departmentId,$cancelReason)
    {
        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            
            //取消动态
            $params = [
                'accountId' => $accountId,
                'sjId'      => $departmentId,
                'reason'    => $cancelReason,
            ];
            WorkOrderDynamic::add(22,$orderNo,$params);
    
            //订单状态变成已取消
            \webapp\modules\v1\models\WorkOrderStatus::batchChangeStatus($orderNo,6,$cancelReason);
            //取消工单
            WorkProcess::cancelOrderAddProcess($orderNo,$departmentId,$accountId,$cancelReason);
            Work::batchCancelWork($orderNo);
    
            //操作过程
            WorkOrderProcess::add($orderNo,6,$cancelReason);

            //技师排班
            $workArr = Work::findOneByAttributes(['order_no'=>$orderNo],'work_no,plan_time');
            if($workArr){
                ScheduleTechnicianQueue::push($workArr['work_no'],5,date('Y-m-d'), date('Y-m-d',$workArr['plan_time']));
            }
    
            $transaction->commit();
            @PushMessage::push($orderNo,"qx");
            return [
                'order_no' => $orderNo
            ];
        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg('取消订单失败，错误信息：'.$e->getMessage().$e->getTraceAsString(), 20006);
        }
    }

    /**
     * 订单详情
     * @param $orderNo 订单号
     * @param $srcType 查询端
     * @param $src_id 查询端id(如，商家id)
     * @param $accountId 用户id
     * @return array|null|\yii\db\ActiveRecord
     * @author xi
     * @date 2018-03-21
     */
    public static function getOrderDetail($orderNo,$srcType,$src_id,$accountId)
    {
        $where = [
            'order_no'   => $orderNo,
            'src_type'   => $srcType,
            'src_id'     => $src_id,
            'account_id' => $accountId,
            'del_status' => 1
        ];

        $query = self::find()
            ->where($where)
            ->asArray()
            ->one();

        if($query)
        {
            $select = 'id,sale_order_id,problem_info,fault_img,status';
            $workDetailArr = WorkOrderDetail::findAllByAttributes(['order_no'=>$orderNo,'is_del'=>1],$select);
            if($workDetailArr){
                foreach ($workDetailArr as $key=>$val){
                    if($val['fault_img']!=''){
                        $workDetailArr[$key]['fault_img_arr'] = explode(',',$val['fault_img']);
                    }
                    else {
                        $workDetailArr[$key]['fault_img_arr'] = [];
                    }
                }
                $query['workDetail'] = $workDetailArr;
            }
            else{
                $query['workDetail'] = [];
            }

            if(trim($query['scope_img'])!=''){
                $query['scope_img'] = explode(',',$query['scope_img']);
            }else {
                $query['scope_img'] = [];
            }

            if($query['status'] == 7){
                $query['status'] = 5;
            }
            //状态中文描述
            $query['status_desc'] = self::getStatus($query['status']);

            //订单取消
            if($query['status'] == 6){
                $processArr = WorkOrderProcess::getCancelOneData($query['order_no']);
                if($processArr){
                    $query['cancel_reason'] = $processArr['problem_reason'];
                }
                else {
                    $query['cancel_reason'] = '';
                }
            }
            //审核未通过原因
            else if($query['status'] == 9){
                $query['audit_faild_reason'] = WorkOrderAudit::getAuditFailedReason($query['order_no']);
            }
            //服务流程（工单流程，只有待服务，服务中，已完成才有）
            else if(in_array($query['status'],[3,4,5]))
            {
                $query['work_list'] = WorkXcx::getListByOrderNo($orderNo);
            }
        }
        return $query;
    }

    /**
     * 服务记录及用户收费项目
     * @param $workNo
     * @param $accountId
     * @return array
     * @author xi
     */
    public static function serviceLogAndCost($workNo)
    {
        $where = [
            'work_no' => $workNo,
        ];
        $select = 'work_no,order_no,status,plan_time,work_img,scene_img,service_record,reason,technician_id';
        $query = Work::findOneByAttributes($where,$select);
        if($query)
        {
    
            //查出产品记录
            $productArr = WorkProduct::findAllByWorkNos($workNo);
            //查出收费项目(只查用户付费)
            $costArr = WorkCost::getListByWorkNos([$workNo],['=','payer_id',3]);
            //开始时间
            $startTime = WorkProcess::getTypeTimeData($workNo,4);
            $startTime = isset($startTime[$workNo])?date('Y-m-d H:i',$startTime[$workNo]):'';
            //完成时间
            $finshTime = WorkProcess::getTypeTimeData($workNo,5);
            $finshTime = isset($finshTime[$workNo])?date('Y-m-d H:i',$finshTime[$workNo]):'';
            //收费时间
            $costTime = WorkOrderTrade::find()->where(['work_no'=>$workNo,'trade_status'=>1])->asArray()->one();
            $costTime = isset($costTime['pay_time']) ? date('Y-m-d H:i',strtotime($costTime['pay_time'])) : '';
    
            $totalCostAmount = 0;
            $costArr = isset($costArr[$workNo])?$costArr[$workNo]:[];
            if($costArr){
                $totalCostAmount = array_sum(array_column($costArr,'cost_amount'));
            }
    
            return [
                'work_no'        => $query['work_no'],
                'order_no'       => $query['order_no'],
                'status'         => $query['status'],
                'plan_time'      => date('Y-m-d H:i',$query['plan_time']),
                'start_time'     => $startTime,
                'finsh_time'     => $finshTime,
                'cost_time'      => $costTime,
                'reason'         => $query['reason'],
                'work_img'       => $query['work_img']!=''?explode(',',$query['work_img']) :[],
                'scene_img'      => $query['scene_img']!=''?explode(',',$query['scene_img']) :[],
                'service_record' => $query['service_record'],
                'technician_id'  => $query['technician_id'],
                'productArr'     => isset($productArr[$workNo])?$productArr[$workNo]:[],
                'costArr'        => [
                    'list' => $costArr,
                    'totalAmount' => $totalCostAmount
                ]
            ];
        }

        return [];
    }

    /**
     * 删除订单
     * @param $orderNo 订单号
     * @param $processUserId 取消人员
     * @author xi
     * @date 2018-03-22
     */
    public static function del($orderNo,$accountId)
    {
        $where = [
            'order_no'   => $orderNo,
            'account_id' => $accountId,
            'status'     => [5,6,9],
            'del_status' => 1
        ];
        $model = self::findOne($where);
        if($model){
            $model->del_status = 2;
            $model->update_time   = time();
            if($model->save(false))
            {
                return [
                    'order_no' => $orderNo
                ];
            }
        }
        else {
            return ModelBase::errorMsg('未找到要删除的订单数据',20004);
        }
    }


    /**
     * 根据订单号查询订单信息
     * @param $orderNo  订单号
     */
    public static function getOrderData($orderNo)
    {
        $orderArr = WorkOrder::find()->where(['order_no' => $orderNo])->asArray()->one();
        if($orderArr)
        {
            return [
                'plan_time'  => $orderArr['plan_time'],
                'address_id' => $orderArr['address_id'],
                'src_type'   => $orderArr['src_type'],
                'src_id'     => $orderArr['src_id']
            ];
        }
        return ModelBase::errorMsg('未找到要删除的订单数据',20004);
    }
}