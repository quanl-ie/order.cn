<?php
//入口
namespace webapp\modules\v3;
use Yii;
use yii\web\Response;
use yii\web\Request;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'webapp\modules\v3\controllers';
    public $requestedRoute = '';

    public function init()
    {
        parent::init();  
    }
}
