<?php
namespace webapp\modules\v3\controllers;

use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkAppraisal;
use webapp\modules\v3\models\WorkOrder;
use Yii;
use webapp\controllers\ApiBaseController;
use webapp\modules\v3\models\WorkOrderXcx;

class OrderXcxController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v3\models\Order';



    public function actionList()
    {

        $accountId    = Yii::$app->request->post('account_id','');
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $status       = Yii::$app->request->post('status','');

        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pagesize',10));

        if($accountId == 0){
            return ModelBase::errorMsg('参数 account_id 不能为空',20003);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20004);
        }

        $where =[
            ['a.account_id', 'in', $accountId],
            ['a.direct_company_id', '=', $departmentId],
            ['c.status', 'in', $status],
            ['c.department_id', '=', $departmentId],
        ];
        
        return WorkOrderXcx::getList($where,$page,$pageSize);
    }

    /**
     * 待评价列表
     * @return array
     */
    public function actionWaitAppraisalList()
    {

        $accountId    = Yii::$app->request->post('account_id','');
        $departmentId = intval(Yii::$app->request->post('department_id',0));

        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pagesize',10));

        if($accountId == 0){
            return ModelBase::errorMsg('参数 account_id 不能为空',20003);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg('参数 department_id 不能为空',20004);
        }

        $where =[
            ['a.account_id', 'in', $accountId],
            ['a.direct_company_id', '=', $departmentId],
            ['c.department_id', '=', $departmentId],
        ];

        return WorkOrderXcx::getWaitAppraisalList($where,$page,$pageSize);
    }

    /**
     * 取消订单
     * @author xi
     * @date 2018-03-21
     */
    public static function actionCancel()
    {
        $orderNo      = trim(Yii::$app->request->post('order_no',''));
        $accountId    = intval(Yii::$app->request->post('account_id',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $cancelReason = trim(Yii::$app->request->post('cancel_reason',''));
    
    
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($accountId == 0){
            return ModelBase::errorMsg('操作人不能为空',20004);
        }

        return WorkOrderXcx::cancelOrder($orderNo,$accountId,$departmentId,$cancelReason);
    }

    /**
     * 订单详情
     * @author xi
     * @date 2018-03-21
     */
    public function actionDetail()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $srcType   = intval(Yii::$app->request->post('src_type',0));
        $srcId     = intval(Yii::$app->request->post('src_id',0));
        $accountId = intval(Yii::$app->request->post('account_id',0));

        if($accountId == 0){
            return ModelBase::errorMsg('用户id不能为空',20005);
        }
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($srcType == 0){
            return ModelBase::errorMsg('查询来源不能为空',20003);
        }
        if($srcId == 0 ){
            return ModelBase::errorMsg('查询来源id不能为空',20004);
        }

        $result = WorkOrderXcx::getOrderDetail($orderNo,$srcType,$srcId,$accountId);

        return $result;
    }

    /**
     * 查出服务记录及收费项目
     * @author xi
     */
    public function actionViewServiceLogAndCost()
    {
        $workNo    = trim(Yii::$app->request->post('work_no',''));

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20003);
        }

        return WorkOrderXcx::serviceLogAndCost($workNo);
    }

    /**
     * 订单删除
     * @author xi
     */
    public function actionDel()
    {
        $orderNo   = trim(Yii::$app->request->post('order_no',''));
        $accountId = intval(Yii::$app->request->post('account_id',0));

        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }
        if($accountId == 0){
            return ModelBase::errorMsg('操作人不能为空',20003);
        }

        return WorkOrderXcx::del($orderNo,$accountId);
    }

    /**
     * 根据订单号查询订单信息
     */
    public function actionGetOrderData()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }

        return WorkOrderXcx::getOrderData($orderNo) ;
    }

    /**
     * 查询订单是否评价
     * @author xi
     */
    public function actionIsAppraisal()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }

        $status = WorkAppraisal::appraisalStatus($orderNo);

        return [
            'appraisal_status' => $status
        ];
    }

    /**
     * 未评价的工单信息
     * @author xi
     */
    public function actionWaitAppraisalWork()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        if($orderNo == ''){
            return ModelBase::errorMsg('订单号不能为空',20002);
        }

        return WorkAppraisal::getWaitAppraisalWork($orderNo);
    }

}