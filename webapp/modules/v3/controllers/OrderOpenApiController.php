<?php
namespace webapp\modules\v3\controllers;

use common\models\AccountAddress;
use common\models\Common;
use webapp\models\ModelBase;
use webapp\modules\v3\models\Work;
use webapp\modules\v3\models\WorkCost;
use webapp\modules\v3\models\WorkOrderAssign;
use webapp\modules\v3\models\WorkOrderDetail;
use webapp\modules\v3\models\WorkOrderDynamic;
use webapp\modules\v3\models\WorkOrderOpenApi;
use webapp\modules\v1\models\WorkOrderStatus as WorkOrderStatusV1;
use webapp\modules\v3\models\WorkOrderProcess;
use webapp\modules\v3\models\WorkRelTechnician;
use Yii;
use webapp\controllers\ApiBaseController;

use webapp\modules\v1\models\Work as WorkV1;

class OrderOpenApiController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v3\models\Order';


    /**
     *  订单列表（只有未完成的订单）
     * @return array
     * @author xi
     */
    public function actionList()
    {
        $departmentIds      = Yii::$app->request->post('department_ids',0);
        $createUserId       = intval(Yii::$app->request->post('create_user_id'));

        //查询源机构直属公司id
        $srcDirectCompanyId = Yii::$app->request->post('src_directCompany_id',0);
        //查询源机构公司id
        $srcDepartmentId    = Yii::$app->request->post('src_department_id',0);

        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pagesize',10));

        if(!$createUserId && !$departmentIds){
            return ModelBase::errorMsg('参数 department_ids 不能为空',20004);
        }
        if(!$srcDirectCompanyId){
            return ModelBase::errorMsg('参数 src_directCompany_id 不能为空',20005);
        }
        if(!$srcDepartmentId){
            return ModelBase::errorMsg('参数 src_department_id 不能为空',20006);
        }

        $where =[
            ['b..status', 'in', [1,2,3,4,7,8,9,10,11,12,13]],
            ['a.source','=',5]
        ];

        if($departmentIds){
            $where [] = ['b.department_id', 'in', $departmentIds];
        }

        if($createUserId > 0){
            $where[] = ['a.create_user_id', '=', $createUserId];
        }

        return WorkOrderOpenApi::getList($where,$srcDirectCompanyId,$srcDepartmentId,$page,$pageSize);
    }

    /**
     * 查出订单状态
     * @author xi
     */
    public function actionGetStatus()
    {
        $orderNo = Yii::$app->request->post('external_order_no');

        //查询源机构直属公司id
        $srcDirectCompanyId = Yii::$app->request->post('src_directCompany_id',0);
        //查询源机构公司id
        $srcDepartmentId    = Yii::$app->request->post('src_department_id',0);

        if(!$orderNo){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if(!is_array($orderNo)){
            return ModelBase::errorMsg("订单号必须是数组",20002);
        }
        if(!$srcDirectCompanyId){
            return ModelBase::errorMsg('参数 src_directCompany_id 不能为空',20003);
        }
        if(!$srcDepartmentId){
            return ModelBase::errorMsg('参数 src_department_id 不能为空',20004);
        }

        $where['a.external_order_no'] = $orderNo;

        return WorkOrderOpenApi::getStatus($where,$srcDirectCompanyId,$srcDepartmentId);
    }

    
    /**
     * 订单填加
     * @author xi
     */
    public function actionAdd()
    {
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no')); //第三方订单号
        $subject         = trim(Yii::$app->request->post('subject',''));
        $workType        = intval(Yii::$app->request->post('work_type','')); //服务类型
        $workStage       = intval(Yii::$app->request->post('work_stage',0));
        $scope           = intval(Yii::$app->request->post('scope',0)); //质保状态 1 保内 2保外 0 协商
        $plantime        = intval(Yii::$app->request->post('plantime',0));
        $plantimeType    = intval(Yii::$app->request->post('plantime_type',5));
        $remark          = trim(Yii::$app->request->post('remark','')); //备注
        $faultImgs       = Yii::$app->request->post('fault_imgs'); //故障图片
        $departmentId    = intval(Yii::$app->request->post('department_id',0));
        $directCompanyId = intval(Yii::$app->request->post('direct_company_id',0));
        $createUserId    = intval(Yii::$app->request->post('create_user_id',0)); //创建人
        $accountId       = intval(Yii::$app->request->post('account_id')); //客户信息
        $addressId       = Yii::$app->request->post('address_id'); //客户地址
        $saleOrderArr    = Yii::$app->request->post('sale_order_arr',[]); //服务产品（有可能是多个产品）
        $technicianArr   = Yii::$app->request->post('technician_arr'); //技师信息

        if($externalOrderNo == ''){
            return ModelBase::errorMsg("第三方订单号不能为空",20002);
        }
        if($plantime <= 0){
            return ModelBase::errorMsg("服务时间不能为空",20003);
        }
        if($workType <= 0){
            return ModelBase::errorMsg("服务类型不能为空",20006);
        }
        if($workStage <= 0){
            return ModelBase::errorMsg("服务流程不能为空",20007);
        }
        if($createUserId <= 0){
            return ModelBase::errorMsg("创建人不能为空",20008);
        }
        if($accountId < 0){
            return ModelBase::errorMsg("客户不能为空",20009);
        }
        if($addressId < 0){
            return ModelBase::errorMsg("客户地址不能为空",20010);
        }
        if(!$saleOrderArr){
            return ModelBase::errorMsg("服务产品不能为空",20011);
        }
        if(!is_array($saleOrderArr)){
            return ModelBase::errorMsg("服务产品必须是数组",2001101);
        }
        if($saleOrderArr)
        {
            foreach ($saleOrderArr as $val){
                if(!isset($val['id']) || !isset($val['num'])){
                    return ModelBase::errorMsg("服务产品格式不正确",2001101);
                }
            }
        }

        if($technicianArr && !is_array($technicianArr)){
            return ModelBase::errorMsg("技师信息必须是数组",20012);
        }
        if($technicianArr)
        {
            foreach ($technicianArr as $val)
            {
                if(!isset($val['id']) || !isset($val['is_self'])){
                    return ModelBase::errorMsg("技师信息格式不正确",2001121);
                }
            }
        }


        $transaction = WorkOrderOpenApi::getDb()->beginTransaction();
        try
        {
            $accountArr = Common::getAccountInfo($accountId);

            $model = new WorkOrderOpenApi();
            $model->order_no          = ModelBase::createOrderNo(13, $workType);
            $model->external_order_no = $externalOrderNo;
            $model->department_id     = $departmentId;
            $model->direct_company_id = $directCompanyId;
            $model->subject_name      = $subject;
            $model->is_scope          = $scope;
            $model->work_type         = $workType;
            $model->work_stage        = $workStage;
            $model->account_id        = $accountId;
            $model->address_id        = $addressId;
            $model->operator_user_id  = $createUserId;
            $model->create_user_id    = $createUserId;
            $model->description       = $remark;
            $model->plan_time         = $plantime;
            $model->plan_time_type    = $plantimeType;
            $model->create_time       = time();
            $model->update_time       = time();
            $model->source            = 5;
            $model->customer_name    = isset($accountArr['account_name'])?$accountArr['account_name']:'';
            $model->customer_mobile  = isset($accountArr['mobile'])?$accountArr['mobile']:'';
            $model->customer_address = Common::getAccountAddress($addressId);
            if($model->save())
            {
                //订单详情加上服务产品
                foreach ($saleOrderArr as $val)
                {
                    $detailModel = new WorkOrderDetail();
                    $detailModel->order_no       = $model->order_no;
                    $detailModel->sale_order_id  = $val['id'];
                    $detailModel->sale_order_num = $val['num'];
                    $detailModel->fault_img      = $faultImgs ? implode(',' , $faultImgs):'';
                    $detailModel->create_time    = time();
                    $detailModel->update_time    = time();
                    $detailModel->is_scope       = $scope;
                    if(!$detailModel->save()){
                        throw new \Exception("添加订单详情失败",30001);
                    }
                }

                //生成工单
                $workNo = WorkV1::autoCreate($departmentId,$model->order_no,$workStage, $model->plan_time, $model->plan_time_type );
                //指派技师
                WorkRelTechnician::addTechnician($technicianArr,$model->order_no,$workNo,$createUserId,$directCompanyId);
                //默认生成一条当前机构的数据
                WorkOrderAssign::add($model->order_no,3,$departmentId,$departmentId,$createUserId,$technicianArr?2:0);
                //动态（指派与不指派不一样）
                $params = [
                    'accountId' => $createUserId,
                    'workType'  => $workType,
                    'workStage' => $workStage,
                    'serviceJsArr' => $technicianArr
                ];
                $this->addOrderDynamic($technicianArr,$model->order_no,$departmentId,$params);

            }
            else {
                throw new \Exception("添加订单失败",30002);
            }

            $transaction->commit();
            return [
                'orderNo' => $model->order_no,
                'workNo'  => $workNo
            ];
        }
        catch(\Exception $e) {

            $transaction->rollBack();
            return ModelBase::errorMsg($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 订单修改
     * @return array
     * @author xi
     */
    public function actionModify()
    {
        $source          = intval(Yii::$app->request->post('source',0));
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no')); //第三方订单号
        $subject         = trim(Yii::$app->request->post('subject',''));
        $scope           = intval(Yii::$app->request->post('scope',0)); //质保状态 1 保内 2保外 0 协商
        $remark          = trim(Yii::$app->request->post('remark','')); //备注
        $faultImgs       = Yii::$app->request->post('fault_imgs'); //故障图片
        $directCompanyId = intval(Yii::$app->request->post('direct_company_id',0));
        $createUserId    = intval(Yii::$app->request->post('create_user_id',0)); //创建人
        $saleOrderArr    = Yii::$app->request->post('sale_order_arr',[]); //服务产品（有可能是多个产品）
        $technicianArr   = Yii::$app->request->post('technician_arr'); //技师信息

        if($source <= 0){
            return ModelBase::errorMsg("来源不能为空",200020);
        }
        if($externalOrderNo == ''){
            return ModelBase::errorMsg("第三方订单号不能为空",200021);
        }
        if($createUserId <= 0){
            return ModelBase::errorMsg("创建人不能为空",20008);
        }
        if(!$saleOrderArr){
            return ModelBase::errorMsg("服务产品不能为空",20011);
        }
        if(!is_array($saleOrderArr)){
            return ModelBase::errorMsg("服务产品必须是数组",2001101);
        }
        if($saleOrderArr)
        {
            foreach ($saleOrderArr as $val){
                if(!isset($val['id']) || !isset($val['num'])){
                    return ModelBase::errorMsg("服务产品格式不正确",2001101);
                }
            }
        }

        if($technicianArr && !is_array($technicianArr)){
            return ModelBase::errorMsg("技师信息必须是数组",20012);
        }
        if($technicianArr)
        {
            foreach ($technicianArr as $val)
            {
                if(!isset($val['id']) || !isset($val['is_self'])){
                    return ModelBase::errorMsg("技师信息格式不正确",200121);
                }
            }
        }

        $transaction = WorkOrderOpenApi::getDb()->beginTransaction();
        try
        {
            $where = [
                'source'            => $source,
                'external_order_no' => $externalOrderNo,
                'direct_company_id' => $directCompanyId
            ];
            $model = WorkOrderOpenApi::findOne($where);
            if($model)
            {
                $model->subject_name      = $subject;
                $model->is_scope          = $scope;
                $model->operator_user_id  = $createUserId;
                $model->create_user_id    = $createUserId;
                $model->description       = $remark;
                $model->create_time       = time();
                $model->update_time       = time();
                if($model->save())
                {
                    //更新服务产品
                    WorkOrderDetail::pullChange($model->order_no,$faultImgs,$scope,$saleOrderArr);
                    //更新生成工单
                    $workNo = Work::pullChange($model->order_no, $model->work_stage, $model->plan_time, $model->plan_time_type);
                    //指派技师
                    WorkRelTechnician::pullChange($technicianArr,$workNo,$createUserId);
                    //更新指派技师类型
                    if($technicianArr){
                        WorkOrderAssign::pullChangeAssignType($model->order_no);
                    }
                    //动态
                    $params = [
                        'accountId' => $createUserId,
                    ];
                    WorkOrderDynamic::add(28, $model->order_no, $params);
                }
                else {
                    throw new \Exception("更新订单失败",30002);
                }

                $transaction->commit();
                return [
                    'orderNo' => $model->order_no,
                    'workNo'  => $workNo
                ];
            }
            else
            {
                return ModelBase::errorMsg("未找到工单号",20013);
            }
        }
        catch(\Exception $e) {

            $transaction->rollBack();
            return ModelBase::errorMsg($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 动态填加
     * @param $technicianArr
     * @param $orderNo
     * @param $departmentId
     * @param $params
     * @throws \Exception
     */
    private function addOrderDynamic($technicianArr,$orderNo,$departmentId,$params)
    {
        try
        {
            //指派技师的
            if($technicianArr)
            {
                //新建订单动态，指派给机构/部门
                WorkOrderDynamic::add(26,$orderNo,$params);
                WorkOrderDynamic::add(27,$orderNo,$params);

                //订单状态为待服务
                WorkOrderStatusV1::add($orderNo,$departmentId,2,$departmentId);
                //订单的创建时间
                WorkOrderProcess::add($orderNo,1,'');
                //订单的指派时间
                WorkOrderProcess::add($orderNo,2,'');
            }
            else
            {
                //订单状态 待指派
                WorkOrderStatusV1::add($orderNo, $departmentId, 1, $departmentId);
                //新建订单动态，不指派给机构或技师
                WorkOrderDynamic::add(26, $orderNo, $params);
                //订单的创建时间
                WorkOrderProcess::add($orderNo, 1, '');
            }
        }
        catch (\Exception $e){
            throw new \Exception($e->getMessage(),$e->getCode());
        }
    }

    /**
     * 获取动态
     * @return array
     */
    public function actionDynamic()
    {
        $orderNo         = trim(Yii::$app->request->post('order_no'));
        $directCompanyId = intval(Yii::$app->request->post('direct_company_id'));

        if($orderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if($directCompanyId == 0){
            return ModelBase::errorMsg("机构id 不能为空",20003);
        }

        return WorkOrderDynamic::getDynamic($orderNo,$directCompanyId);
    }

    /**
     * 订单基础信息
     * @author xi
     */
    public function actionWorkBasics()
    {
        $orderNo         = trim(Yii::$app->request->post('order_no'));
        $directCompanyId = intval(Yii::$app->request->post('direct_company_id'));
        $departmentId    = intval(Yii::$app->request->post('department_id'));

        if($orderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if($directCompanyId == 0){
            return ModelBase::errorMsg("机构id 不能为空",20003);
        }
        if($departmentId == 0){
            return ModelBase::errorMsg("机构id 不能为空",20004);
        }

        return WorkOrderOpenApi::getWorkBasics($orderNo,$departmentId,$directCompanyId);
    }

    /**
     * 获取产品信息
     * @return array
     * @author xi
     */
    public function actionWorkProduct()
    {
        $orderNo = trim(Yii::$app->request->post('order_no'));
        if($orderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }

        return WorkOrderDetail::listAllByOrderNo($orderNo);
    }

    /**
     * 获取工单服务记录
     * @author xi
     */
    public function actionWorkServicesLog()
    {
        $orderNo = trim(Yii::$app->request->post('order_no'));
        if($orderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }

        return Work::getWorkServicesLog($orderNo);
    }

    /**
     * 配件使用记录
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public function actionGetPartsUseLog()
    {
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no',''));
        if($externalOrderNo == ''){
            ModelBase::errorMsg("订单号不能为空",20001);
        }

        $query = WorkOrderOpenApi::findOneByAttributes(['external_order_no' => $externalOrderNo],'order_no');
        if($query){
            return WorkCost::getPartsUseLog($query['order_no']);
        }

        return [];
    }

    /**
     * 检查外部订单号是否存在
     * @return array
     * @author xi
     */
    public function actionHasExist()
    {
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no'));
        $source          = intval(Yii::$app->request->post('source'));
        if($externalOrderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if($source <= 0 ){
            return ModelBase::errorMsg("来源不能为空",20003);
        }

        $orderNo = WorkOrderOpenApi::getOrderNoByExt($externalOrderNo,$source);

        return [
            'exist' => $orderNo ? true : false
        ];
    }

    /**
     * 根据外部订单号查本系统订单号
     * @return array
     * @author xi
     */
    public function actionGetOrderno()
    {
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no'));
        $source          = intval(Yii::$app->request->post('source'));
        if($externalOrderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if($source <= 0 ){
            return ModelBase::errorMsg("来源不能为空",20003);
        }

        $orderNo = WorkOrderOpenApi::getOrderNoByExt($externalOrderNo,$source);
        return [
            'order_no' => $orderNo
        ];
    }

    /**
     * 订单详情
     * @return array|NULL|string|\yii\db\ActiveRecord
     * @author xi
     */
    public function actionOrderInfo()
    {
        $externalOrderNo = trim(Yii::$app->request->post('external_order_no'));
        $select          = trim(Yii::$app->request->post('select','*'));
        $source          = intval(Yii::$app->request->post('source'));

        if($externalOrderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }
        if($source <= 0 ){
            return ModelBase::errorMsg("来源不能为空",20003);
        }
        if($select == '' ){
            $select = '*';
        }

        $query = WorkOrderOpenApi::findOneByAttributes(['external_order_no' => $externalOrderNo , 'source' =>$source] , $select);
        return $query;
    }

}