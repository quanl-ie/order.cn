<?php
namespace webapp\modules\v3\controllers;

use webapp\models\ModelBase;
use webapp\modules\v1\models\WorkAppraisal;
use webapp\modules\v1\models\WorkAppraisalDict;
use webapp\modules\v1\models\WorkAppraisalTag;
use webapp\modules\v3\models\WorkRelTechnician;
use Yii;
use webapp\controllers\ApiBaseController;

class AppraisalController extends ApiBaseController
{
    public $modelClass = 'webapp\modules\v1\models\Order';

    /**
     * 根据星级查相应的标签
     * @author xi
     */
    public function actionGetTag()
    {
        $workNo = trim(Yii::$app->request->post('work_no',0));
        $star   = intval(Yii::$app->request->post('star',0));

        if($workNo == 0){
            return ModelBase::errorMsg("工单号不能为空",20003);
        }
        if($star == 0){
            return ModelBase::errorMsg("参数 star 不能为空",20002);
        }

        $directCompanyId = WorkRelTechnician::getDirectCompanyIdByWorkNo($workNo);
        $list =  WorkAppraisalDict::getTagByStar($directCompanyId,$star);

        $data = [
            'starDesc' => WorkAppraisalDict::getStarDesc($star),
            'list'     => $list
        ];

        return $data;
    }

    /**
     * 填加评价
     * $params = [
                    [
                        'work_no' => '12121',
                        'star_num' => 3,
                        'tag_arr'  => [30,61,62],
                        'desc'     => '描述'
                    ],
                    [
                        'work_no' => '12121',
                        'star_num' => 3,
                        'tag_arr'  => [30,61,62],
                        'desc'     => '描述'
                    ]
     *         ];
     * @author xi
     */
    public function actionAdd()
    {
        $userId         = intval(Yii::$app->request->post('user_id',0));
        $userType       = intval(Yii::$app->request->post('user_type',0));
        $wechatOpenid   = trim(Yii::$app->request->post('wechat_openid',''));
        $wechatNickname = trim(Yii::$app->request->post('wechat_nickname',''));
        $params         = Yii::$app->request->post('params');

        if(in_array($userType,[1,2]) && $userId == 0){
            return ModelBase::errorMsg('用户id 不能为空',20006);
        }
        if(!in_array($userType,[1,2,3])){
            return ModelBase::errorMsg('用户类型取值范围不正确',20007);
        }
        if($userType == 3 && ($wechatOpenid == '' || $wechatNickname == '')){
            return ModelBase::errorMsg('微信openid，微信昵称不能为空',20008);
        }

        if(!$params || !is_array($params)){
            return ModelBase::errorMsg('请正确传入参数',20002);
        }

        //验证
        foreach ($params as $val)
        {
            if(!isset($val['work_no']) || trim($val['work_no']) == '' ||
                !isset($val['star_num']) || !in_array($val['star_num'],[1,2,3,4,5]) ||
                !isset($val['tag_arr']) || !isset($val['desc']))
            {
                return ModelBase::errorMsg('请正确传入参数',20003);
            }
            if($val['tag_arr'])
            {
                if(!is_array($val['tag_arr'])){
                    return ModelBase::errorMsg('请正确传入参数',20004);
                }
                else
                {
                    foreach ($val['tag_arr'] as $v){
                        if(intval($v) == 0){
                            return ModelBase::errorMsg('请正确传入参数',20005);
                        }
                    }
                }
            }
        }

        $transaction = Yii::$app->order_db->beginTransaction();
        try
        {
            //保存数据库
            foreach ($params as $val)
            {
                $model = WorkAppraisal::findOne(['work_no'=>$val['work_no']]);
                if(!$model){
                    throw new \Exception("未找到要评价的工单",20009);
                }
                else if($model && $model->appraisal_status == 2){
                    throw new \Exception("该工单已评价过了",20010);
                }
                else
                {
                    $model->user_id   = $userId;
                    $model->user_type = $userType;
                    $model->star_num  = $val['star_num'];
                    $model->star      = WorkAppraisalDict::getStar($val['star_num']);
                    $model->desc      = $val['desc'];
                    $model->wechat_openid    = $wechatOpenid;
                    $model->wechat_nickname  = $wechatNickname;
                    $model->appraisal_status = 2;
                    if($model->save())
                    {
                        if($val['tag_arr'])
                        {
                            foreach ($val['tag_arr'] as $tagId)
                            {
                                $tagModel = new WorkAppraisalTag();
                                $tagModel->work_no = $val['work_no'];
                                $tagModel->tag_id  = $tagId;
                                if(!$tagModel->save()){
                                    throw new \Exception("保存失败",20012);
                                }
                            }
                        }
                    }
                    else {
                        throw new \Exception("保存失败",20013);
                    }
                }
            }

            $transaction->commit();
            return [];
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            return ModelBase::errorMsg($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 我的评价
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public function actionMyAppraisal()
    {
        $userIds = Yii::$app->request->post('user_ids');
        $page    = intval(Yii::$app->request->post('page',1));
        $pageSize= intval(Yii::$app->request->post('pagesize',10));

        return WorkAppraisal::getMyAppraisal($userIds,$page,$pageSize);
    }

}