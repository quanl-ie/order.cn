<?php
namespace webapp\modules\v3\controllers;

use webapp\models\ModelBase;
use webapp\modules\v3\models\Work;
use webapp\modules\v3\models\WorkOrder;
use webapp\modules\v3\models\WorkRelTechnician;
use Yii;
use webapp\controllers\ApiBaseController;

class WorkJsController extends ApiBaseController
{

    public $modelClass = 'webapp\modules\v2\models\Work';

    /**
     * 查出工单标题（服务流程 + 第一个产品名称）
     * @author xi
     */
    public function actionWorkTitle()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }

        $title = Work::getTitle($workNo);
        if($title){
            return $title;
        }
        return ModelBase::errorMsg('未找到数据',20003);
    }

    /**
     * 获取经纬度
     * @author xi
     */
    public function actionGetAddressId()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($departmentId <= 0){
            return ModelBase::errorMsg('机构id不能为空',20003);
        }

        $addressId = Work::getAddressId($workNo,$departmentId);
        if($addressId > 0){
            return $addressId;
        }

        return ModelBase::errorMsg('获取地址失败',20004);
    }

    /**
     * 打卡 关联单
     * @author xi
     */
    public function actionSignList()
    {
        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pageSize',10));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $accountAddressIds = Yii::$app->request->post('account_address_ids');

        if($technicianId == 0){
            return ModelBase::errorMsg('参数 technician_id 不能为空',20002);
        }
        if($accountAddressIds && !is_array($accountAddressIds)){
            return ModelBase::errorMsg('参数 account_address_ids 不是一个数组',20003);
        }

        $data = Work::getList($technicianId,$accountAddressIds,$page,$pageSize);
        return $data;
    }

    /**
     * 查出订单号
     * @return array|mixed
     * @author xi
     */
    public function actionGetOrderno()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }

        $workArr = Work::findOneByAttributes(['work_no'=>$workNo],'order_no');
        if($workNo){
            return $workArr['order_no'];
        }

        return ModelBase::errorMsg('未找到工单',20003);

    }

    /**
     * 查出工单服务时间以及 服务技师
     * @author xi
     */
    public function actionWorkInfo()
    {
        $workNo = trim(Yii::$app->request->post('work_no',''));
        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }

        $result = Work::getPlantimeAndTechnician($workNo);
        if($result){
            return $result;
        }

        return ModelBase::errorMsg("未找到数据",20003);
    }

    /**
     * 改约
     * @author xi
     */
    public function actionChangeReschedule()
    {
        $workNo               = trim(Yii::$app->request->post('work_no',''));
        $plantime             = intval(Yii::$app->request->post('plantime',0)); //服务时间
        $plantimeType         = intval(Yii::$app->request->post('plan_time_type',5)); //服务时间类型
        $technicianId         = intval(Yii::$app->request->post('technician_id',0)); //操作组长技师名称
        $serviceTechnicianArr = Yii::$app->request->post('service_technician_arr',0); //上门技师
        $reason               = trim(Yii::$app->request->post('reason','')); //改派原因
        $assignType           = intval(Yii::$app->request->post('assign_type',1)); //指派类型 1 指派技师 2 暂不指定

        if($workNo == ''){
            return ModelBase::errorMsg('工单号不能为空',20002);
        }
        if($technicianId <=0 ){
            return ModelBase::errorMsg('组长id不能为空',20003);
        }
        if($plantime > 0 && !in_array($plantimeType,[1,2,3,4,5])){
            return ModelBase::errorMsg('服务时间类型不正确',20004);
        }
        if($plantime > 0 && $plantime < time()){
            return ModelBase::errorMsg('服务时间不能小于当前时间',20004);
        }

        $jsId = WorkRelTechnician::getTechnicianIdsByWorkNo($workNo,2);
        if(!in_array($technicianId,$jsId)){
            return ModelBase::errorMsg('组长与工单不符',20006);
        }

        return Work::changeReschedule($workNo,$assignType,$plantime,$plantimeType,$technicianId,$serviceTechnicianArr,$reason);
    }

    /**
     * 订单基础信息
     * @author xi
     */
    public function actionWorkBasics()
    {
        $orderNo         = trim(Yii::$app->request->post('order_no'));
        if($orderNo == ''){
            return ModelBase::errorMsg("订单号不能为空",20002);
        }

        return WorkOrder::getWorkBasics($orderNo);
    }
}
