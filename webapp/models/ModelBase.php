<?php
namespace webapp\models;

use common\helpers\Helper;
use webapp\modules\v1\models\Work;
use webapp\modules\v1\models\WorkProcess;
use Yii;
use yii\db\ActiveRecord;

class ModelBase extends ActiveRecord
{
    //商家
    const SRC_SJ  = 14;
    //服务商
    const SRC_FWS = 13;
    //门店
    const SRC_MD  = 12;
    //技师
    const SRC_JS  = 11;
    //用户
    const SRC_YH = 20;

    // 1 商家 2 服务商 3 门店 4 小程序
    const SOURCE_SJ  = 1;
    const SOURCE_FWS = 2;
    const SOURCE_MD  = 3;
    const SOURCE_XCX = 4;

    /**
     * 构造用于分页的数据
     */
    public static function createPageData($totalCount, $list)
    {
        return array(
            'list' => $list,
            'totalCount' => intval($totalCount)
        );
    }

    /**
     * 统一错误处理
     */
    public static function errorMsg($msg, $code = 20001)
    {
        return [
            'success' => false,
            'message' => $msg,
            'code' => $code
        ];
    }

    /**
     * 根据条件查询所有数据
     * @param string/array $where
     * @param string $select
     * @return array
     * @author xi
     * @date 2017-12-15
     */
    public static function findAllByAttributes($where, $select="*", $index = false,$limit = 1000,$group=0)
    {

        if($group){
            $query = static::find()
                ->where($where)
                ->select($select)
                ->groupBy($group)
                ->limit($limit)
                ->asArray()
                ->all();
        }else{
            $query = static::find()
                ->where($where)
                ->select($select)
                ->limit($limit)
                ->asArray()
                ->all();
        }
        if($query)
        {
            if( is_string($index) && in_array($index,array_keys((new static())->getAttributes())) )
            {
                $result = [];
                foreach ($query as $val){
                    $result[$val[$index]] = $val;
                }
                return $result;
            }

            return $query;
        }
        return [];
    }

    /**
     * 根据条件查出一条数据
     * @param array/string $where
     * @param string $returnAttr
     * @return \yii\db\ActiveRecord|NULL|string
     * @author xi
     * @date 2017-12-15
     */
    public static function findOneByAttributes($where, $select="*", $returnAttr='')
    {
        $query = static::find()
            ->where($where)
            ->select($select)
            ->asArray()
            ->one();
        if($query)
        {
            if($returnAttr!='' && is_string($returnAttr) && in_array($returnAttr,array_keys((new static())->getAttributes())) )
            {
                return $query[$returnAttr];
            }
            return $query;
        }
        return [];
    }

    /**
     * 日子记录
     * @param stirng $message
     * @author xi
     */
    public static function logs($message,$file='')
    {
        if($file == ''){
            $file = '/tmp/logs/'.date('Ym').'/app.log';
        }
        else {
            $file = '/tmp/logs/'.date('Ym').'/'.basename($file);
        }

        if( !file_exists(dirname($file))){
            @mkdir(dirname($file),0777,true);
        }
        @file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] ", FILE_APPEND);
        @file_put_contents($file, $message . " \n", FILE_APPEND);
    }

    /**
     * 生成订单号
     * @param $srcType
     * @param $workType
     * @return string
     */
    public static function createOrderNo($srcType,$workType)
    {
        $rand = self::getIncrement();
        return $srcType.$workType.date('Ymd').$rand;
    }

    /**
     * 生成4位数
     * @return string
     * @author xi
     * @date 2017-12-15
     */
    private static function getIncrement()
    {
        $date = date('d');
        $cacheKey = 'increment'.$date;

        if(!Yii::$app->cache->exists($cacheKey))
        {
            $str      = md5($date);
            $basicNum = '';
            for ($i = 0; $i < strlen($str); $i++) {
                if (($temp = ord($str{$i})) < 100) {
                    $basicNum .= $temp;
                    if (strlen($basicNum) == 4) {
                        break;
                    }
                }
            }
            Yii::$app->cache->set($cacheKey, $basicNum, strtotime('Tomorrow') - time());
        }
        else
        {
            $basicNum = Yii::$app->cache->get($cacheKey);
            $basicNum++;
            if($basicNum>9999){
                $basicNum = 1;
            }
            Yii::$app->cache->set($cacheKey, $basicNum, strtotime('Tomorrow') - time());
        }
        return sprintf('%04d',$basicNum);
    }

    /**
     * 获取账户姓名
     * @return array
     */
    public static function getAccountName($accountId)
    {
        if($accountId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-account-name";
            $postData = [
                'account_id' => $accountId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取登录账户姓名（商家，服务商，门店登录信息）
     * @return array
     */
    public static function getUserName($userId)
    {
        if($userId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-user-name";
            $postData = [
                'user_id' => $userId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取登录账户姓名（商家，服务商，门店登录信息）
     * @return array
     */
    public static function getUserIds($srcType,$srcId)
    {
        if($srcType && $srcId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-user-ids";
            $postData = [
                'src_type' => $srcType,
                'src_id'    => $srcId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);

            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['ids'];
            }
        }
        return [];
    }

    /**
     * 获取技师姓名
     * @return array
     */
    public static function getTechnicianName($technicianId)
    {
        if($technicianId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-technician-name";
            $postData = [
                'technician_id' => $technicianId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取服务商名称
     * @return array
     */
    public static function getStoreName($storeId)
    {
        if($storeId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-store-name";
            $postData = [
                'store_id' => $storeId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取类目名称
     * @return array
     */
    public static function getClassName($classId)
    {
        if($classId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-class-name";
            $postData = [
                'class_id' => $classId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取服务类型
     * @return array
     */
    public static function getWorktypeName($workTypeId)
    {
        if($workTypeId)
        {
            $url = Yii::$app->params['sj.uservices.cn']."/common/get-worktype-name";
            $postData = [
                'work_type_id' => $workTypeId
            ];
            $jsonStr = Helper::curlPost($url,$postData);
            $result = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取服务类型
     * @return array
     */
    public static function getClassNameByStoreId($saleId)
    {
        if($saleId)
        {
            $url = Yii::$app->params['sj.uservices.cn'] . "/common/get-class-name-by-sale-id";
            $postData = [
                'sale_id' => $saleId
            ];
            $jsonStr = Helper::curlPost($url, $postData);
            $result = json_decode($jsonStr, true);
            if (isset($result['success']) && $result['success'] == true) {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取门店商家名称
     * @return array
     */
    public static function getBName($id)
    {
        if($id)
        {
            $url = Yii::$app->params['sj.uservices.cn'] . "/common/get-bname";
            $postData = [
                'id' => $id
            ];
            $jsonStr = Helper::curlPost($url, $postData);
            $result = json_decode($jsonStr, true);
            if (isset($result['success']) && $result['success'] == true) {
                return $result['data']['name'];
            }
        }
        return '';
    }

    /**
     * 获取门店商家名称
     * @return array
     */
    public static function getServiceProviderUserIds($srcType,$srcId,$page=1 ,$pageSize=1000)
    {
        $url = Yii::$app->params['sj.uservices.cn'] . "/common/get-service-provider-user-ids";
        $postData = [
            'src_type' => $srcType,
            'src_id'   => $srcId,
            'page'     => $page,
            'pageSize' => $pageSize
        ];
        $jsonStr = Helper::curlPost($url, $postData);
        $result = json_decode($jsonStr, true);
        if (isset($result['success']) && $result['success'] == true) {
            return $result['data']['ids'];
        }
        return [];
    }
}
